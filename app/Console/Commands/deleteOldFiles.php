<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class deleteOldFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteOldFiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete old files in public-files folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = __DIR__;
        $path = substr($path, 0, strlen($path) - 20);
        $path = $path . 'public' . DIRECTORY_SEPARATOR . 'files';

        $dir = scandir($path);
        $now = time();

        $time = \App\Models\Setting::where('name', 'symlink_time')->pluck('value')->first();

        foreach ($dir as $name) {
            $fileTime = substr($name, 10, strlen($name));
            if ($fileTime) {
                if ($now - $fileTime > $time) {
                    rmdir($path . DIRECTORY_SEPARATOR . $name);
                }
            }
        }

    }
}
