<?php

namespace App\Repositories;

use App\Models\ProductPost;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductPostRepository
 * @package App\Repositories
 * @version January 30, 2018, 2:06 am UTC
 *
 * @method ProductPost findWithoutFail($id, $columns = ['*'])
 * @method ProductPost find($id, $columns = ['*'])
 * @method ProductPost first($columns = ['*'])
*/
class ProductPostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'title',
        'text'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductPost::class;
    }
}
