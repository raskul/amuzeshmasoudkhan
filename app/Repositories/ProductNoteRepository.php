<?php

namespace App\Repositories;

use App\Models\ProductNote;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductNoteRepository
 * @package App\Repositories
 * @version February 27, 2018, 3:53 pm +0330
 *
 * @method ProductNote findWithoutFail($id, $columns = ['*'])
 * @method ProductNote find($id, $columns = ['*'])
 * @method ProductNote first($columns = ['*'])
*/
class ProductNoteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'user_id',
        'title',
        'text'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductNote::class;
    }
}
