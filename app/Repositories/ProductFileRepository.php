<?php

namespace App\Repositories;

use App\Models\ProductFile;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProductFileRepository
 * @package App\Repositories
 * @version February 27, 2018, 10:25 am +0330
 *
 * @method ProductFile findWithoutFail($id, $columns = ['*'])
 * @method ProductFile find($id, $columns = ['*'])
 * @method ProductFile first($columns = ['*'])
*/
class ProductFileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'user_id',
        'title',
        'file',
        'size'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductFile::class;
    }
}
