<?php

namespace App\Repositories;

use App\Models\IndexNotification;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IndexNotificationRepository
 * @package App\Repositories
 * @version March 6, 2018, 8:10 pm +0330
 *
 * @method IndexNotification findWithoutFail($id, $columns = ['*'])
 * @method IndexNotification find($id, $columns = ['*'])
 * @method IndexNotification first($columns = ['*'])
*/
class IndexNotificationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'text',
        'who_edit',
        'can_sign_up'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IndexNotification::class;
    }
}
