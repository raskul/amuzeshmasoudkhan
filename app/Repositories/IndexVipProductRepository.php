<?php

namespace App\Repositories;

use App\Models\IndexVipProduct;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IndexVipProductRepository
 * @package App\Repositories
 * @version March 6, 2018, 8:14 pm +0330
 *
 * @method IndexVipProduct findWithoutFail($id, $columns = ['*'])
 * @method IndexVipProduct find($id, $columns = ['*'])
 * @method IndexVipProduct first($columns = ['*'])
*/
class IndexVipProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'title',
        'text',
        'who_edit',
        'can_sign_up'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IndexVipProduct::class;
    }
}
