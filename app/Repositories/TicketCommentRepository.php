<?php

namespace App\Repositories;

use App\Models\TicketComment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TicketCommentRepository
 * @package App\Repositories
 * @version February 16, 2018, 2:45 pm +0330
 *
 * @method TicketComment findWithoutFail($id, $columns = ['*'])
 * @method TicketComment find($id, $columns = ['*'])
 * @method TicketComment first($columns = ['*'])
*/
class TicketCommentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ticket_id',
        'user_id',
        'text',
        'importance'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketComment::class;
    }
}
