<?php

namespace App\Repositories;

use App\Models\Mobile;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MobileRepository
 * @package App\Repositories
 * @version March 6, 2018, 11:31 pm +0330
 *
 * @method Mobile findWithoutFail($id, $columns = ['*'])
 * @method Mobile find($id, $columns = ['*'])
 * @method Mobile first($columns = ['*'])
*/
class MobileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mobile'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mobile::class;
    }
}
