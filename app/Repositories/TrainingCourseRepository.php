<?php

namespace App\Repositories;

use App\Models\TrainingCourse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TrainingCourseRepository
 * @package App\Repositories
 * @version January 28, 2018, 4:40 pm UTC
 *
 * @method TrainingCourse findWithoutFail($id, $columns = ['*'])
 * @method TrainingCourse find($id, $columns = ['*'])
 * @method TrainingCourse first($columns = ['*'])
*/
class TrainingCourseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description_mini',
        'description',
        'description_full',
        'specification',
        'download_link',
        'count',
        'price',
        'discount',
        'sold_count',
        'teacher_name',
        'duration',
        'start_at',
        'end_at',
        'vote',
        'total_voters',
        'is_active',
        'image_main',
        'image_two',
        'image_three',
        'image_four',
        'image_five',
        'image_five'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TrainingCourse::class;
    }
}
