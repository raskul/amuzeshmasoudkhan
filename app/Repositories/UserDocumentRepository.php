<?php

namespace App\Repositories;

use App\Models\UserDocument;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserDocumentRepository
 * @package App\Repositories
 * @version February 13, 2018, 11:00 am +0330
 *
 * @method UserDocument findWithoutFail($id, $columns = ['*'])
 * @method UserDocument find($id, $columns = ['*'])
 * @method UserDocument first($columns = ['*'])
*/
class UserDocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'file',
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserDocument::class;
    }
}
