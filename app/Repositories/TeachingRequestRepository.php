<?php

namespace App\Repositories;

use App\Models\TeachingRequest;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TeachingRequestRepository
 * @package App\Repositories
 * @version February 13, 2018, 10:58 am +0330
 *
 * @method TeachingRequest findWithoutFail($id, $columns = ['*'])
 * @method TeachingRequest find($id, $columns = ['*'])
 * @method TeachingRequest first($columns = ['*'])
*/
class TeachingRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'title',
        'text',
        'files'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TeachingRequest::class;
    }
}
