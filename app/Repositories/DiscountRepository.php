<?php

namespace App\Repositories;

use App\Models\Discount;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DiscountRepository
 * @package App\Repositories
 * @version February 1, 2018, 12:14 am UTC
 *
 * @method Discount findWithoutFail($id, $columns = ['*'])
 * @method Discount find($id, $columns = ['*'])
 * @method Discount first($columns = ['*'])
*/
class DiscountRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_fa',
        'code',
        'active',
        'who_edit'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Discount::class;
    }
}
