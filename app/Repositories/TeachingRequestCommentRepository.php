<?php

namespace App\Repositories;

use App\Models\TeachingRequestComment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TeachingRequestCommentRepository
 * @package App\Repositories
 * @version February 13, 2018, 10:59 am +0330
 *
 * @method TeachingRequestComment findWithoutFail($id, $columns = ['*'])
 * @method TeachingRequestComment find($id, $columns = ['*'])
 * @method TeachingRequestComment first($columns = ['*'])
*/
class TeachingRequestCommentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'teaching_request_id',
        'user_id',
        'parent_id',
        'text'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TeachingRequestComment::class;
    }
}
