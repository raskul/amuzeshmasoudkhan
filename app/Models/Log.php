<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $table = 'logs';

    protected $fillable = [
        'ip', 'url', 'call_back_url', 'os', 'browser', 'username'
    ];
}
