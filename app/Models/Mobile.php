<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Mobile
 * @package App\Models
 * @version March 6, 2018, 11:31 pm +0330
 *
 * @property integer mobile
 * @property integer mobile1
 */
class Mobile extends Model
{

    public $table = 'mobiles';
    


    public $fillable = [
        'mobile',
        'ip'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'mobile' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
