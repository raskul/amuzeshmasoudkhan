<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class IndexNotification
 * @package App\Models
 * @version March 6, 2018, 8:10 pm +0330
 *
 * @property integer product_id
 * @property string text
 * @property string who_edit
 * @property integer can_sign_up
 */
class IndexNotification extends Model
{

    public $table = 'index_notifications';
    


    public $fillable = [
        'product_id',
        'text',
        'who_edit',
        'can_sign_up'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'text' => 'string',
        'who_edit' => 'string',
        'can_sign_up' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
