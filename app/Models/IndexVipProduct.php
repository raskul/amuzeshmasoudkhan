<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class IndexVipProduct
 * @package App\Models
 * @version March 6, 2018, 8:14 pm +0330
 *
 * @property integer product_id
 * @property string title
 * @property string text
 * @property string who_edit
 * @property integer can_sign_up
 */
class IndexVipProduct extends Model
{

    public $table = 'index_vip_products';
    


    public $fillable = [
        'product_id',
        'title',
        'text',
        'who_edit',
        'can_sign_up'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'title' => 'string',
        'text' => 'string',
        'who_edit' => 'string',
        'can_sign_up' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
