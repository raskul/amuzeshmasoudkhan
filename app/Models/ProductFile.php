<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class ProductFile
 * @package App\Models
 * @version February 27, 2018, 10:25 am +0330
 *
 * @property integer product_id
 * @property integer user_id
 * @property string title
 * @property string file
 * @property string size
 */
class ProductFile extends Model
{

    public $table = 'product_files';
    


    public $fillable = [
        'product_id',
        'user_id',
        'title',
        'file',
        'size'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'file' => 'string',
        'size' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
}
