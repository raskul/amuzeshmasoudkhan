<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class Comment
 * @package App\Models
 * @version January 18, 2018, 8:45 am UTC
 *
 * @property integer user_id
 * @property integer product_id
 * @property string text
 * @property integer likes
 */
class Comment extends Model
{

    public $table = 'comments';
    


    public $fillable = [
        'user_id',
        'product_id',
        'text',
        'likes',
        'parent_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'product_id' => 'integer',
        'text' => 'string',
        'likes' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'product_id' => 'required',
        'text' => 'required',
        'likes' => 'required'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

//    public function products()
//    {
//        return $this->belongsTo(Product::class);
//    }

    public function children()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }
}
