<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ProductPost
 * @package App\Models
 * @version January 30, 2018, 2:06 am UTC
 *
 * @property integer product_id
 * @property string title
 * @property string text
 */
class ProductPost extends Model
{

    public $table = 'product_posts';
    


    public $fillable = [
        'product_id',
        'title',
        'text',
        'user_id',
        'duration',
        'size',
        'show_type',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'title' => 'string',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
