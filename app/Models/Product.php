<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class Product
 * @package App\Models
 * @version January 15, 2018, 6:07 pm UTC
 *
 * @property string title
 * @property string description
 * @property string download_link
 * @property integer count
 * @property integer price
 * @property integer discount
 * @property string image_main
 * @property string image_two
 * @property string image_three
 * @property string image_four
 * @property string image_five
 */
class Product extends Model
{

    public $table = 'products';

    public $fillable = [
        'title',
        'description_mini',
        'description',
        'description_full',
        'specification',
        'download_link',
        'price',
        'real_price',
        'discount',
        'total_count',
        'sold_count',
        'brand',
        'duration',
        'vote',
        'total_voters',
        'is_active',
        'image_main',
        'user_earn_vote',
        'can_get_license',
        'can_sign_up',
        'rating_movie',
        'rating_voice',
        'rating_complete',
        'rating_teacher',
        'rating_support',
        'rating_rezayat',
        'start_at',
        'end_at',
        'status',
        'product_type_id',
        'user_id',
        'category_id',
        'special_duration',
        'special_city',
        'special_teacher_name',
        'special_address',
        'special_time',
        'special_tel',
        'total_visit',
        'thumbnail',


    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'download_link' => 'string',
        'count' => 'integer',
        'price' => 'integer',
        'discount' => 'integer',
        'image_main' => 'string',
        'image_two' => 'string',
        'image_three' => 'string',
        'image_four' => 'string',
        'image_five' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required'
    ];

    public function getStatusStrItemsAttribute()
    {
        return [
            0 => 'به زودی برگزار خواهد شد.',
            1 => 'در حال برگزاری',
            2 => 'تکمیل شده',
        ];
    }

    public function getStatusStrAttribute()
    {
        switch ($this->status) {
            case 0:
                return 'به زودی برگزار خواهد شد.';
            case 1:
                return 'در حال برگزاری';
            case 2:
                return 'تکمیل شده';
        }
    }

    public function factors()
    {
        return $this->belongsToMany(Factor::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function faqs()
    {
        return $this->hasMany(Faq::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function discounts()
    {
        return $this->belongsToMany(Discount::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function productPosts()
    {
        return $this->hasMany(ProductPost::class);
    }

    public function students()
    {
        return $this->belongsToMany('App\User', 'lesson_student','lesson_id',  'student_id');
    }

    public function studentsDetail()
    {
        return $this->belongsToMany('App\User', 'lesson_student_detail','lesson_id',  'student_id');
    }

    public function productFiles()
    {
        return $this->hasMany(ProductFile::class);
    }

    public function productNotes()
    {
        return $this->hasMany(ProductNote::class);
    }

    public function UserRatings()//used
    {
        return $this->belongsToMany('App\User', 'product_rating_user', 'product_id', 'user_id');
    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class);
    }

    public function eshantions()
    {
        return $this->belongsToMany(Product::class, 'eshantion_mahsulasli', 'mahsulasli_id', 'eshantion_id');
    }
}
