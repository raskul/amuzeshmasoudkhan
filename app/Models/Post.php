<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Post
 * @package App\Models
 * @version January 22, 2018, 12:18 pm UTC
 *
 * @property string title
 * @property string text
 */
class Post extends Model
{

    public $table = 'posts';
    


    public $fillable = [
        'title',
        'text'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
