<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class TeachingRequest
 * @package App\Models
 * @version February 13, 2018, 10:58 am +0330
 *
 * @property string user_id
 * @property string title
 * @property string text
 * @property string files
 */
class TeachingRequest extends Model
{

    public $table = 'teaching_requests';


    public $fillable = [
        'files',
        'user_id',
        'category_id',
        'title',
        'description',
        'price',
        'start_at',
        'end_at',
        'total_count',
        'image_main',
        'can_get_license',
        'can_sign_up',
        'product_status',
        'status',
        'product_type_id',
        'thumbnail',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'string',
        'title' => 'string',
        'text' => 'string',
        'files' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case 0:
                return 'در انتظار تایید';
            case 1:
                return 'در انتظار تایید...';
            case 2:
                return 'شما مدرس این درس هستید';
            case 3:
                return 'درخواست شما تایید نشد';
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
