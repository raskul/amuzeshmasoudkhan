<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class UserDocument
 * @package App\Models
 * @version February 13, 2018, 11:00 am +0330
 *
 * @property integer user_id
 * @property string file
 * @property string title
 */
class UserDocument extends Model
{

    public $table = 'user_documents';
    


    public $fillable = [
        'user_id',
        'file',
        'size'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'file' => 'string',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
