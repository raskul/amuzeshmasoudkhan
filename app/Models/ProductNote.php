<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class ProductNote
 * @package App\Models
 * @version February 27, 2018, 3:53 pm +0330
 *
 * @property integer product_id
 * @property integer user_id
 * @property string title
 * @property string text
 */
class ProductNote extends Model
{

    public $table = 'product_notes';
    


    public $fillable = [
        'product_id',
        'user_id',
        'title',
        'text'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
}
