<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class Discount
 * @package App\Models
 * @version February 1, 2018, 12:14 am UTC
 *
 * @property string name
 * @property string name_fa
 * @property string code
 * @property integer active
 * @property string who_edit
 */
class Discount extends Model
{

    public $table = 'discounts';
    


    public $fillable = [
        'name',
        'name_fa',
        'code',
        'price',
        'start_at',
        'end_at',
        'type',
        'active',
        'is_percent',
        'who_edit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_fa' => 'string',
        'code' => 'string',
        'price' => 'integer',
        'type' => 'integer',
        'is_percent' => 'integer',
        'active' => 'integer',
        'who_edit' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function usersBurned()
    {
        return $this->belongsToMany('App\User', 'discount_user_burned', 'discount_id', 'user_id');
    }
}
