<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Message
 * @package App\Models
 * @version January 29, 2018, 11:42 pm UTC
 *
 * @property integer from_user_id
 * @property integer to_user_id
 * @property integer parent_id
 * @property string who_answer
 * @property string text
 */
class Message extends Model
{

    public $table = 'messages';
    


    public $fillable = [
        'from_user_id',
        'to_user_id',
        'parent_id',
        'who_answer',
        'text'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'from_user_id' => 'integer',
        'to_user_id' => 'integer',
        'parent_id' => 'integer',
        'who_answer' => 'string',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
