<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Ticket
 * @package App\Models
 * @version January 22, 2018, 12:37 pm UTC
 *
 * @property string username
 * @property string email
 * @property string text
 */
class Ticket extends Model
{

    public $table = 'tickets';
    


    public $fillable = [
        'user_id',
        'email',
        'text',
        'closed',
        'is_read',
        'parent_id',
        'importance',
        'title',
        'who_answered',
        'files'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'username' => 'string',
        'email' => 'string',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getImportanceNameAttribute()
    {
        switch ($this->importance){
            case 1:
                return 'مهم';
            case 2:
                return 'عادی';
            case 3:
                return 'کم اهمیت';
        }
    }

    public function getClosedNameAttribute()
    {
        switch ($this->closed){
            case 0:
                return 'بسته نشده';
            case 1:
                return 'بسته شده';
        }
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
