<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class Faq
 * @package App\Models
 * @version March 2, 2018, 5:01 pm +0330
 *
 * @property integer user_id
 * @property integer product_id
 * @property integer parent_id
 * @property string text
 * @property integer likes
 */
class Faq extends Model
{

    public $table = 'faqs';


    public $fillable = [
        'user_id',
        'product_id',
        'parent_id',
        'text',
        'likes'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id'    => 'integer',
        'product_id' => 'integer',
        'parent_id'  => 'integer',
        'text'       => 'string',
        'likes'      => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id'    => 'required',
        'product_id' => 'required',
        'parent_id'  => 'required',
        'text'       => 'required',
        'likes'      => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function children()
    {
        return $this->hasMany(Faq::class, 'parent_id');
    }
}
