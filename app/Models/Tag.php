<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Tag
 * @package App\Models
 * @version January 15, 2018, 6:09 pm UTC
 *
 * @property string name
 */
class Tag extends Model
{

    public $table = 'tags';
    


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
    
}
