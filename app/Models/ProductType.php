<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ProductType
 * @package App\Models
 * @version March 2, 2018, 1:34 pm +0330
 *
 * @property string name
 * @property string name_fa
 */
class ProductType extends Model
{

    public $table = 'product_types';
    


    public $fillable = [
        'name',
        'name_fa'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_fa' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
