<?php

namespace App\Models;

use App\User;
use Eloquent as Model;

/**
 * Class TeachingRequestComment
 * @package App\Models
 * @version February 13, 2018, 10:59 am +0330
 *
 * @property integer teaching_request_id
 * @property integer user_id
 * @property integer parent_id
 * @property string text
 */
class TeachingRequestComment extends Model
{

    public $table = 'teaching_request_comments';
    


    public $fillable = [
        'teaching_request_id',
        'user_id',
        'parent_id',
        'text'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'teaching_request_id' => 'integer',
        'user_id' => 'integer',
        'parent_id' => 'integer',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
