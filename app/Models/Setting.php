<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Setting
 * @package App\Models
 * @version February 10, 2018, 2:14 pm +0330
 *
 * @property string name
 * @property string name_fa
 * @property string value
 */
class Setting extends Model
{

    public $table = 'settings';
    


    public $fillable = [
        'name',
        'name_fa',
        'value',
        'symlink_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_fa' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'name_fa' => 'required',
        'value' => 'required'
    ];

    
}
