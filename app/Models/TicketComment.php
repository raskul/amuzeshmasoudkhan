<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TicketComment
 * @package App\Models
 * @version February 16, 2018, 2:45 pm +0330
 *
 * @property integer ticket_id
 * @property integer user_id
 * @property string text
 * @property integer importance
 */
class TicketComment extends Model
{

    public $table = 'ticket_comments';
    


    public $fillable = [
        'ticket_id',
        'user_id',
        'text',
        'importance'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ticket_id' => 'integer',
        'user_id' => 'integer',
        'text' => 'string',
        'importance' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
