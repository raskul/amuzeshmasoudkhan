<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class CreateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        return User::$rules;
        return [
            'name' => 'required|max:190',
            'password' => 'max:20',
            'education' => 'max:190',
            'postal_code' => 'max:11',
            'telephone' => 'max:12',
            'telegram' => 'max:190',
            'instagram' => 'max:190',
            'linkedin' => 'max:190',
            'twitter' => 'max:190',
            'address' => 'max:300',
            'tel' => 'max:12',
            'about' => 'max:900',
        ];

    }
}
