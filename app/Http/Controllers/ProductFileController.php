<?php

namespace App\Http\Controllers;

use App\DataTables\ProductFileDataTable;
use App\Http\Requests\CreateProductFileRequest;
use App\Http\Requests\UpdateProductFileRequest;
use App\Models\Product;
use App\Repositories\ProductFileRepository;
use Flash;
use Response;

class ProductFileController extends AppBaseController
{
    /** @var  ProductFileRepository */
    private $productFileRepository;

    public function __construct(ProductFileRepository $productFileRepo)
    {
        $this->productFileRepository = $productFileRepo;
    }

    /**
     * Display a listing of the ProductFile.
     *
     * @param ProductFileDataTable $productFileDataTable
     * @return Response
     */
    public function index(ProductFileDataTable $productFileDataTable)
    {
        $this->authorize('is-employee');

        return $productFileDataTable->render('product_files.index');
    }

    /**
     * Show the form for creating a new ProductFile.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        $products = Product::pluck('title', 'id');

        return view('product_files.create', compact('products'));
    }

    /**
     * Store a newly created ProductFile in storage.
     *
     * @param CreateProductFileRequest $request
     *
     * @return Response
     */
    public function store(CreateProductFileRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        $productFile = $this->productFileRepository->create($input);

        Flash::success('فایل ضمیمه دوره با موفقیت ذخیره شد.');

        return redirect(route('productFiles.index'));
    }

    /**
     * Display the specified ProductFile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $productFile = $this->productFileRepository->findWithoutFail($id);

        if (empty($productFile)) {
            Flash::error('فایل ضمیمه پیدا نشد.');

            return redirect(route('productFiles.index'));
        }

        return view('product_files.show')->with('productFile', $productFile);
    }

    /**
     * Show the form for editing the specified ProductFile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $productFile = $this->productFileRepository->findWithoutFail($id);

        if (empty($productFile)) {
            Flash::error('فایل ضمیمه پیدا نشد.');

            return redirect(route('productFiles.index'));
        }
        $products = Product::pluck('title', 'id');

        return view('product_files.edit', compact('products', 'productFile'));
    }

    /**
     * Update the specified ProductFile in storage.
     *
     * @param  int              $id
     * @param UpdateProductFileRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductFileRequest $request)
    {
        $this->authorize('is-employee');

        $productFile = $this->productFileRepository->findWithoutFail($id);

        if (empty($productFile)) {
            Flash::error('فایل ضمیمه پیدا نشد.');

            return redirect(route('productFiles.index'));
        }

        $productFile = $this->productFileRepository->update($request->all(), $id);

        Flash::success('فایل ضمیمه دوره با موفقیت بروزرسانی شد.');

        return redirect(route('productFiles.index'));
    }

    /**
     * Remove the specified ProductFile from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (\Gate::allows('is-employee')) {
            $productFile = $this->productFileRepository->findWithoutFail($id);

            if (empty($productFile)) {
                Flash::error('فایل ضمیمه پیدا نشد.');

                return redirect(route('productFiles.index'));
            }
            //delete from storage
            $path = $productFile->file;
            $path = 'public' . substr($path, 7);
            \Storage::delete($path);

            $this->productFileRepository->delete($id);

            Flash::success('فایل این محصول با موفقیت پاک شد.');

            return redirect(route('productFiles.index'));
        }

        if (\Gate::allows('is-teacher')) {

            $productFile = $this->productFileRepository->findWithoutFail($id);

            if (empty($productFile)) {
                Flash::error('فایل ضمیمه پیدا نشد.');

                return redirect(route('productFiles.index'));
            }
            if (auth()->user()->id == $productFile->user_id) {//delete file it self

                //delete from storage
                $path = $productFile->file;
                $path = 'public' . substr($path, 7);
                \Storage::delete($path);

                $this->productFileRepository->delete($id);

                Flash::success('فایل این محصول با موفقیت پاک شد.');

            return redirect()->back();
            }
        }

    }
}
