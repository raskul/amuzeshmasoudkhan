<?php

namespace App\Http\Controllers;

use App\DataTables\TeachingRequestDataTable;
use App\Http\Requests\CreateTeachingRequestRequest;
use App\Http\Requests\UpdateTeachingRequestRequest;
use App\Repositories\TeachingRequestRepository;
use Flash;
use Response;

class TeachingRequestController extends AppBaseController
{
    /** @var  TeachingRequestRepository */
    private $teachingRequestRepository;

    public function __construct(TeachingRequestRepository $teachingRequestRepo)
    {
        $this->teachingRequestRepository = $teachingRequestRepo;
    }

    /**
     * Display a listing of the TeachingRequest.
     *
     * @param TeachingRequestDataTable $teachingRequestDataTable
     * @return Response
     */
    public function index(TeachingRequestDataTable $teachingRequestDataTable)
    {
        $this->authorize('is-employee');

        return $teachingRequestDataTable->render('teaching_requests.index');
    }

    /**
     * Show the form for creating a new TeachingRequest.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        return view('teaching_requests.create');
    }

    /**
     * Store a newly created TeachingRequest in storage.
     *
     * @param CreateTeachingRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateTeachingRequestRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        $teachingRequest = $this->teachingRequestRepository->create($input);

        Flash::success('Teaching Request saved successfully.');

        return redirect(route('teachingRequests.index'));
    }

    /**
     * Display the specified TeachingRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $teachingRequest = $this->teachingRequestRepository->findWithoutFail($id);

        if (empty($teachingRequest)) {
            Flash::error('Teaching Request not found');

            return redirect(route('teachingRequests.index'));
        }

        return view('teaching_requests.show')->with('teachingRequest', $teachingRequest);
    }

    /**
     * Show the form for editing the specified TeachingRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $teachingRequest = $this->teachingRequestRepository->findWithoutFail($id);

        if (empty($teachingRequest)) {
            Flash::error('Teaching Request not found');

            return redirect(route('teachingRequests.index'));
        }

        return view('teaching_requests.edit')->with('teachingRequest', $teachingRequest);
    }

    /**
     * Update the specified TeachingRequest in storage.
     *
     * @param  int              $id
     * @param UpdateTeachingRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTeachingRequestRequest $request)
    {
        $this->authorize('is-employee');

        $teachingRequest = $this->teachingRequestRepository->findWithoutFail($id);

        if (empty($teachingRequest)) {
            Flash::error('Teaching Request not found');

            return redirect(route('teachingRequests.index'));
        }

        $teachingRequest = $this->teachingRequestRepository->update($request->all(), $id);

        Flash::success('Teaching Request updated successfully.');

        return redirect(route('teachingRequests.index'));
    }

    /**
     * Remove the specified TeachingRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $teachingRequest = $this->teachingRequestRepository->findWithoutFail($id);

        if (empty($teachingRequest)) {
            Flash::error('Teaching Request not found');

            return redirect(route('teachingRequests.index'));
        }

        $this->teachingRequestRepository->delete($id);

        Flash::success('Teaching Request deleted successfully.');

        return redirect(route('teachingRequests.index'));
    }
}
