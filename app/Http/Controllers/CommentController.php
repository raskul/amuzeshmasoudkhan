<?php

namespace App\Http\Controllers;

use App\DataTables\CommentDataTable;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Comment;
use App\Repositories\CommentRepository;
use Flash;
use Illuminate\Http\Request;
use Response;

class CommentController extends AppBaseController
{
    /** @var  CommentRepository */
    private $commentRepository;

    public function __construct(CommentRepository $commentRepo)
    {
        $this->commentRepository = $commentRepo;
    }

    /**
     * Display a listing of the Comment.
     *
     * @param CommentDataTable $commentDataTable
     * @return Response
     */
    public function index(CommentDataTable $commentDataTable)
    {
         $this->authorize('is-employee');

        return $commentDataTable->render('comments.index');
    }

    /**
     * Show the form for creating a new Comment.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        return view('comments.create');
    }

    /**
     * Store a newly created Comment in storage.
     *
     * @param CreateCommentRequest $request
     *
     * @return Response
     */
    public function store(CreateCommentRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $comment = $this->commentRepository->create($input);

        Flash::success('Comment saved successfully.');

        return redirect(route('comments.index'));
    }


    /**
     * Display the specified Comment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment)) {
            Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        return view('comments.show')->with('comment', $comment);
    }

    /**
     * Show the form for editing the specified Comment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment)) {
            Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        return view('comments.edit')->with('comment', $comment);
    }

    /**
     * Update the specified Comment in storage.
     *
     * @param  int              $id
     * @param UpdateCommentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommentRequest $request)
    {
         $this->authorize('is-employee');

        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment)) {
            Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        $comment = $this->commentRepository->update($request->all(), $id);

        Flash::success('Comment updated successfully.');

        return redirect(route('comments.index'));
    }

    /**
     * Remove the specified Comment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment)) {
            Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        $this->commentRepository->delete($id);

        Flash::success('Comment deleted successfully.');

        return redirect(route('comments.index'));
    }

    public function storesec(Request $request)
    {
        if (!(auth()->check())) {
            Flash::error('ابتدا وارد شوید');

            return redirect()->back();
        }

        if(isset($request->id)){
            $input['user_id'] = auth()->user()->id;
            $input['text'] = $request->text;
            $comment = Comment::find($request->id);
            if (($comment->user_id == auth()->user()->id) || (\Gate::allows('is-employee'))) {
                $comment->update($input);
            }

            if (isset($comment->parent_id)){
                $comment = Comment::find($comment->parent_id);
            }

            return redirect(route('show.product.page', $comment->product_id));
        }
        $input['product_id'] = $request->product_id;

        $input['user_id'] = auth()->user()->id;

        $input['text'] = $request->text;

        $comment = $this->commentRepository->create($input);

        Flash::success('دیدگاه شما با موفقیت ارسال شد.');

        return redirect()->back();
    }

    public function commentAnswer(Request $request, Comment $comment)
    {
        if ($request->isMethod('POST')) {
            if (!(auth()->check())) {
                Flash::error('ابتدا وارد شوید');

                return redirect()->back();
            }
//            dd($request->all());
//            dd($comment);
            $inputs['user_id'] = auth()->user()->id;
            $inputs['product_id'] = $comment->product_id;
            $inputs['text'] = $request->text;
            $inputs['parent_id'] = $comment->id;
            Comment::create($inputs);

            return redirect(route('show.product.page', $comment->product_id));
        }

        return view('index.commentAnswer', compact('comment'));
    }

    public function commentEdit(Comment $comment)
    {
         return view('index.commentEdit', compact('comment'));
    }
}
