<?php

namespace App\Http\Controllers;

use App\BalanceHistrory;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\WithdrawRequest;
use App\LessonStudentDetail;
use App\Models\Category;
use App\Models\Factor;
use App\Models\Product;
use App\Models\ProductFile;
use App\Models\ProductNote;
use App\Models\ProductPost;
use App\Models\ProductType;
use App\Models\Setting;
use App\Models\TeachingRequest;
use App\Models\TeachingRequestComment;
use App\Models\Ticket;
use App\Models\TicketComment;
use App\Models\UserDocument;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Image;
use Laracasts\Flash\Flash;
use Storage;

class ProfileController extends Controller
{
    public function profileEdit()
    {
        $user = User::find(auth()->user()->id);
        $user->password = null;

        return view('profile.userProfileEdit', compact('user'));
    }

    public function profilePost(CreateUserRequest $request)
    {
//        dd($request->all());
        //تغییر تصویر پروفایل من
        if (isset($request->image)) {
            $this->validate($request, [
                'image' => 'mimes:jpg,jpeg,png,gif|max:2048',
            ]);
            $inputs['image'] = time() . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/profile/image/';
            $request->image->move($path, $inputs['image']);
            Image::make($path . $inputs['image'])
                ->resize('110', '110')
                ->save(public_path() . '/uploads/profile/image/thumbnail/' . $inputs['image']);
            $inputs['thumbnail'] = $inputs['image'];
        }

        //نام:
        $inputs['name'] = $request->name;

        //آخرین مدرک تحصیلی
        $inputs['education'] = $request->education;

        //بیوگرافی من
        $inputs['about'] = $request->about;

        if (isset($request->city)){
            $this->validate($request, [
                'age' => 'numeric|max:120',
            ]);
            //سن
            $inputs['age'] = $request->age;
        }

        if (isset($request->city)){
            $this->validate($request, [
                'job' => 'string',
            ]);
            //شغل
            $inputs['job'] = $request->job;
        }

        if (isset($request->city)){
            $this->validate($request, [
                'city' => 'max:190|string',
            ]);
            //شهر محل سکونت
            $inputs['city'] = $request->city;
        }

        if (isset($request->city)){
            $this->validate($request, [
                'state' => 'string|max:190',
            ]);
            //استان محل سکونت
            $inputs['state'] = $request->state;
        }
        //کد پستی ده رقمی
        $inputs['postal_code'] = $request->postal_code;

        //آدرس دقیق پستی
        $inputs['address'] = $request->address;

        //شماره موبایل
        $inputs['tel'] = $request->tel;

        //تلفن ثابت
        $inputs['telephone'] = $request->telephone;

        //تلگرام
        $inputs['telegram'] = $request->telegram;

        //اینستاگرام
        $inputs['instagram'] = $request->instagram;

        //لینکدین
        $inputs['linkedin'] = $request->linkedin;

        //توئیتر
        $inputs['twitter'] = $request->twitter;

        //اطلاعات بانکی
        $inputs['bank'] = $request->bank;
        $inputs['bank_number'] = $request->bank_number;
        $inputs['bank_sheba'] = $request->bank_sheba;


        //رمزعبور:
        if ($request->password == $request->password_retype) {
            if (isset($request->password)) {
                if (strlen($request->password) >= 6) {
                    $inputs['password'] = bcrypt($request->password);
                } else {
                    Flash::error('رمز عبور شما ضعیف است. رمز عبور باید باید بیشتر از 6 کاراکتر باشد.');

                    return redirect()->back();
                }
            }
        } else {
            Flash::error('رمز عبور و تکرار رمز عبور مطابقت ندارند دقت کنید که هر دو را مشابه هم وارد کنید.');

            return redirect()->back();
        }

        $user = User::find(auth()->user()->id);
        $user->update($inputs);
        Flash::success('پروفایل شما با موفقیت بروز شد');

        return redirect(route('profile.edit'));
    }

    public function increaseBalanceGet()
    {
        return view('profile.increaseBalance');
    }

    public function increaseBalancePost(Request $request)
    {
        dd($request);
        //must be complete...

        Flash::success('مبلغ حساب شما افزایش پیدا کرد.');

        return redirect(route('profile.get'));
    }

    public function financialWithdraw(WithdrawRequest $request)
    {
        if ($request->isMethod('POST')) {
            if ($request->amount > 0) {
                if (auth()->user()->balance > 0) {
                    if ($request->amount >= auth()->user()->balance) {
                        BalanceHistrory::create([
                            'user_id' => auth()->user()->id,
                            'old_balance' => auth()->user()->balance,
                            'new_balance' => (auth()->user()->balance) - ($request->amount),
                            'type' => 3,
                            'description' => 'درخواست برداشت وجه',
                        ]);
                        $user = User::find(auth()->user()->id);
                        $balanceTemp = $user->balance - $request->amount;
                        $user->update(['balance' => $balanceTemp]);
                        Flash::success('درخواست برداشت وجه برای شما ثبت شد. لطفا منتظر تایید درخواست بمانید.');
                    } else {
                        Flash::error('موجودی شما کمتر از میزان وجه درخواستی شما میباشد.');
                    }
                } else {
                    Flash::error('موجودی شما صفر است و قابل برداشت نیست.');
                }
            } else {
                Flash::error('مبلغ درخواستی شما نمیتواند صفر باشد.');
            }

            return redirect()->back();
        }
        $balanceHistories = BalanceHistrory::where('user_id', auth()->user()->id)->get();

        return view('profile.financialWithdraw', compact('balanceHistories'));
    }

    //امور مالی صفحه اصلی
    public function financialDepartment(Request $request)
    {
        $filter = $request->filter;

        $user = User::find(auth()->user()->id);

        if ($request->filter == null) {
            $kharid_man = LessonStudentDetail::where('student_id', $user->id)->get();
        }
        if ($filter == 'kharid_man') {
            $kharid_man = LessonStudentDetail::where('student_id', $user->id)->get();
        }
        if ($filter == 'forush_man') {
            $forush_man = LessonStudentDetail::where('teacher_id', $user->id)->get();
        }
        if ($filter == 'daryafti_man') {
            $daryafti_man = LessonStudentDetail::where('teacher_id', $user->id)->get();
        }
        if ($filter == 'kif_pul') {
            $kif_pul = BalanceHistrory::where('user_id', $user->id)->get();
        }

        return view('profile.financialDepartment', compact('user', 'kharid_man', 'forush_man', 'daryafti_man', 'kif_pul'));
    }

    public function teachingIndex()
    {
        $user = auth()->user();
        $teaching_requests = TeachingRequest::where('user_id', $user->id)->where('status', '!=', 2)->get();
        if (Gate::allows('is-employee')) {
            $teaching_requests = TeachingRequest::orderByDesc('created_at')->get();

            return view('indexAdmin.teachingRequests', compact('teaching_requests'));
        }
        $products = Product::where('user_id', $user->id)->with('productPosts')->get();

        return view('profile.teachingIndex', compact('teaching_requests', 'products'));
    }

    public function teachingRequestCreate(Request $request)
    {
        if ($request->isMethod('POST')) {
            //استاد
            $inputs['user_id'] = auth()->user()->id;

            //دسته بندی
            $inputs['category_id'] = $request->category;

            //عنوان
            $inputs['title'] = $request->title;

            //نوع محصول
            $inputs['product_type_id'] = $request->product_type_id;

            //وضعیت
            $inputs['product_status'] = $request->status;


            //در انتظار پاسخ
            $inputs['status'] = 0;

            //عکس دوره
            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'mimes:jpg,jpeg,png,gif|max:2048',
                ]);
                $inputs['image_main'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
                $path = public_path() . '/uploads/product/image/';
                $request->image->move($path, $inputs['image_main']);
                Image::make($path . $inputs['image_main'])
                    ->resize('300', '200')
                    ->save(public_path() . '/uploads/product/image/thumbnail/' . $inputs['image_main']);
                $inputs['thumbnail'] = $inputs['image_main'];
            }

            //توضیحات کامل دوره
            $inputs['description'] = $request->description;

            //قیمت دوره
            $inputs['price'] = $request->price;

            //تاریخ شروع دوره
            $inputs['start_at'] = savePDateToDatabase($request->start_at);

            //تاریخ پایان دوره
            $inputs['end_at'] = savePDateToDatabase($request->end_at);

            //موجودی انبار
            $inputs['total_count'] = $request->total_count;

            //دریافت گواهینامه
            $inputs['can_get_license'] = $request->can_get_license;

            //ثبت نام دانشجویان
            $inputs['can_sign_up'] = $request->can_sign_up;

            //فایل ضمیمه
            if (isset($request->file)) {
                $this->validate($request, [
                    'file' => 'mimes:jpg,jpeg,png,gif,zip,mp4,pdf|max:2048',
                ]);
                $fullName = storeTicketsFiles($request, 'teachingRequest');
                $inputs['files'] = serialize([$fullName]);
            }

            TeachingRequest::create($inputs);
            Flash::success('محصول با موفقیت ثبت گردید. به محض تایید مدیریت به لیست محصولات شما افزوده خواهد شد.');


            return redirect(route('teachingIndex'));
        }

        $category = Category::where('id', 1)->first();
        $productTypes = ProductType::get();
        $product = Product::first();

        return view('profile.teachingRequestCreate', compact('category', 'productTypes', 'product'));
    }

    public function teachingRequestComments(Request $request, $id)
    {
        if ($request->isMethod('POST')) {
            if ($request->rad) {
                $teachingRequest = TeachingRequest::find($id);
                $teachingRequest->update(['status' => 3]);
                Flash::error('این درخواست رد شد.');

                return redirect()->back();
            }
            if ($request->ghabul) {
                $teachingRequest = TeachingRequest::find($id);
                $user = User::find($teachingRequest->user_id);
                if ($user->role_id == 1) {
                    $commission = Setting::where('name', 'default_commission')->pluck('value')->first();
                    $user->update(['role_id' => 2, 'commission' => $commission]);//add default commission to user if user not teacher
                }

                //start create product
                $inputs['user_id'] = $teachingRequest->user_id;
                $inputs['category_id'] = $teachingRequest->category_id;
                $inputs['title'] = $teachingRequest->title;
                $inputs['description'] = $teachingRequest->description;
                $inputs['price'] = $teachingRequest->price;
                $inputs['real_price'] = $teachingRequest->price;
                $inputs['start_at'] = $teachingRequest->start_at;
                $inputs['end_at'] = $teachingRequest->end_at;
                $inputs['total_count'] = $teachingRequest->total_count;
                $inputs['image_main'] = $teachingRequest->image_main;
                $inputs['thumbnail'] = $teachingRequest->thumbnail;
                $inputs['can_get_license'] = $teachingRequest->can_get_license;
                $inputs['can_sign_up'] = $teachingRequest->can_sign_up;
                $inputs['product_type_id'] = $teachingRequest->product_type_id;
                $inputs['status'] = $teachingRequest->product_status;

                if ($teachingRequest->status != 2)//sabt nakardane chand mahsul baraye yek darkhast
                    Product::create($inputs);
                //end create product
                $teachingRequest->update(['status' => 2]);
                Flash::success('این درخواست قبول شد.');

                return redirect()->back();
            }
            if (isset($request->file)) {
                $fullName = storeTicketsFiles($request, 'teachingRequest');
                $teachingRequest = TeachingRequest::find($id);
                $files = unserialize($teachingRequest->files);
                $files[] = $fullName;
                $files = serialize($files);
                $teachingRequest->update(['files' => $files]);
            }

            $user = auth()->user();
            TeachingRequestComment::create([
                'user_id' => $user->id,
                'teaching_request_id' => $id,
                'text' => $request->text,
            ]);

            if (Gate::allows('is-employee', $user)) {
                $teachingRequest = TeachingRequest::find($id);
                if ($teachingRequest->status == 0) {
                    $teachingRequest->update(['status' => 1]);
                }
            }

            return redirect()->back();
        }

        $teachingRequest = TeachingRequest::find($id);
        $teachingRequestComments = TeachingRequestComment::where('teaching_request_id', $id)->get();
        $productType = ProductType::where('id', $teachingRequest->product_type_id)->pluck('name')->first();

        if (Gate::allows('is-employee')) {
            return view('indexAdmin.teachingRequestComments', compact('teachingRequestComments', 'teachingRequest', 'productType'));
        }

        return view('profile.teachingRequestComments', compact('teachingRequestComments', 'teachingRequest'));
    }


    public function ticketIndex()
    {
        if (Gate::allows('is-employee')) {
            $tickets = Ticket::orderByDesc('created_at')->get();

            return view('indexAdmin.tickets', compact('tickets'));
        }
        $tickets = Ticket::where('user_id', auth()->user()->id)->get();

        return view('profile.ticketIndex', compact('tickets'));
    }

    public function ticketCreate(Request $request)
    {
        if ($request->isMethod('POST')) {
            $user = auth()->user();
            $inputs['user_id'] = $user->id;
            $inputs['email'] = $user->email;
            $inputs['title'] = $request->title;
            $inputs['text'] = $request->text;
            $inputs['is_read'] = 0;
            $inputs['closed'] = 0;
            $inputs['importance'] = $request->importance;
            $inputs['who_answered'] = $user->name;
            if (isset($request->file)) {
                $fullName = storeTicketsFiles($request);
                $inputs['files'] = serialize([$fullName]);
            }

            if (strlen($inputs['title']) < 1) {
                Flash::error('قسمت عنوان را تکمیل کنید');

                return redirect()->back();
            }

            if (strlen($inputs['text']) < 1) {
                Flash::error('قسمت متن را تکمیل کنید.');

                return redirect()->back();
            }

            Ticket::create($inputs);
            Flash::success('تیکت شما با موفقیت ارسال شد. لطفا منتظر پاسخ باشید.');

            return redirect(route('ticketIndex'));
        }

        return view('profile.ticketCreate');
    }

    public function ticketComments(Request $request, $id)
    {
        $user = auth()->user();
        if ($request->isMethod('POST')) {
            if ($request->close) {
                $ticket = Ticket::find($id);
                $ticket->update(['closed' => 1]);
                Flash::success('این تیکت بسته شد.');

                return redirect()->back();
            }

            if (isset($request->file)) {
                $fullName = storeTicketsFiles($request);
                $ticket = Ticket::find($id);
                $files = unserialize($ticket->files);
                $files[] = $fullName;
                $files = serialize($files);
                $ticket->update(['files' => $files]);
            }

            TicketComment::create([
                'user_id' => $user->id,
                'ticket_id' => $id,
                'text' => $request->text,
            ]);
            Ticket::where('id', $id)->update(['who_answered' => auth()->user()->name]);

            return redirect()->back();
        }

        if (Gate::allows('is-employee', $user)) {
            $ticket = Ticket::find($id);
            if ($ticket->status == 0) {
                $ticket->update(['is_read' => 1]);
            }
            $ticketComments = TicketComment::where('ticket_id', $id)->get();

            return view('indexAdmin.ticketComments', compact('ticketComments', 'ticket'));
        }

        $ticket = Ticket::find($id);
        $ticketComments = TicketComment::where('ticket_id', $id)->get();

        return view('profile.ticketComments', compact('ticketComments', 'ticket'));
    }

    public function createProductPostByTeacher(Product $product, Request $request, $id = null)
    {
        $this->authorize('is-teacher');

        if ($request->isMethod('POST')) {
            if ($product->user_id == auth()->user()->id) {//each teacher can create or update its post of products

                $inputs['title'] = $request->title;
                $inputs['user_id'] = auth()->user()->id;
                $inputs['product_id'] = $product->id;
                $inputs['duration'] = $request->duration;
                $inputs['show_type'] = $request->show_type;

                if (isset($request->video)) {
                    $this->validate($request, [
                        'video' => 'mimes:mp4,mov,ogg|max:1100000',
                    ]);

                    $server_download_path = 'public_html/files/' ;//maybe change when download server changed
                    $emai = substr(auth()->user()->email, 6);
                    $video_name = Storage::disk('ftp')->put( $server_download_path . $emai, $request->video);
                    $video_name = str_replace('public_html', env('UPLOAD_SERVER_URL'), $video_name);

                    UserDocument::create(['user_id' => auth()->user()->id, 'file' => $video_name, 'size' => $request->file('video')->getSize()]);
                    $inputs['text'] = "<p><video controls='controls'><source src='$video_name' type='video/mp4' /></video></p>";
                    $inputs['size'] = $request->file('video')->getSize();
                } else {
                    $inputs['text'] = $request->text;
                    $inputs['size'] = 5;
                }

                if (!(isset($id))) {
                    ProductPost::create($inputs);
                    Flash::success("جلسه آموزشی متعلق به محصول
                    $product->title
                    با موفقیت ثبت شد.
                    ");
                } else {
                    ProductPost::where('id', $id)->update($inputs);
                    Flash::success("جلسه آموزشی متعلق به محصول
                    $product->title
                    با موفقیت ویرایش شد.
                    ");
                }

                return redirect(route('teachingIndex'));

            } else {
                Flash::error('مالکیت این محصول در اختیار شما نیست');

                return redirect()->back();
            }
        }
        if ($request->isMethod('DELETE')) {
            if ($product->user_id == auth()->user()->id) {//each teacher can create or update its post of products
                $post = ProductPost::find($id);
                $post->delete();
                Flash::success('درس مورد نظر شما با موفقیت پاک گردید.');

                return redirect()->back();
            }
        }

        $productPost = ProductPost::find($id);

        //create the teacher path if not exist:
        $email = auth()->user()->email;
        $path = __DIR__;
        $path = substr($path, 0, strlen($path) - 20)
            . 'storage' . DIRECTORY_SEPARATOR
            . 'app' . DIRECTORY_SEPARATOR
            . 'public' . DIRECTORY_SEPARATOR
            . 'files' . DIRECTORY_SEPARATOR
            . "$email";
        if (!(file_exists($path))) {
            mkdir($path);
        }

        return view('profile.createProductPostByTeacher', compact('product', 'productPost'));
    }

    public function ProductFilesByTeacher(Product $product, Request $request, $id = 0)
    {
        if ($request->isMethod('POST')) {
            if ($id == 0) {//ezaf kardane file
                $inputs['title'] = $request->title;
                if (isset($request->file)){
                    $this->validate($request, [
                        'file' => 'mimes:jpg,jpeg,png,gif,pdf,zip,mp4|max:8192',
                    ]);
                }
                $inputs['file'] = storeTicketsFiles($request, 'productFiles');
                $inputs['user_id'] = auth()->user()->id;
                $inputs['product_id'] = $product->id;
                $inputs['size'] = $request->file('file')->getSize();
                ProductFile::create($inputs);
                Flash::success(' فایل شما با موفقیت به محصول ' . $product->title . ' ضمیه شد. ');

                return redirect()->back();
            }
        }

        $productFiles = $product->productFiles()->get();

        return view('profile.ProductFilesByTeacher', compact('product', 'productFiles'));
    }

    public function ProductNotesByTeacher(Product $product, Request $request, $id = 0)
    {
        if ($request->isMethod('POST')) {
            $inputs['title'] = $request->title;
            $inputs['text'] = $request->text;
            $inputs['product_id'] = $product->id;
            $inputs['user_id'] = auth()->user()->id;
            ProductNote::create($inputs);
            Flash::success(' اطلاعیه شما با موفقیت به محصول ' . $product->title . ' افزوده شد. ');

            return redirect()->back();
        }

        $productNotes = $product->productNotes()->get();

        return view('profile.ProductNoteByTeacher', compact('product', 'productNotes'));
    }

    public function userCourses()
    {
        $user = User::find(auth()->user()->id);
        $products = $user->lessons()->get();

        return view('profile.userCourses', compact('products'));
    }

    public function courseStudent(Request $request)
    {
        if (count($request->all())) {
//            dd($request->all());
//            dd($request->product);
            $product = Product::find($request->product);
            if ($product->user_id == auth()->user()->id) {
                $students = $product->students()->get();
            }
        }
        $teacherProducts = Product::where('user_id', auth()->user()->id)->get();

        return view('profile.courseStudent', compact('teacherProducts', 'students'));
    }
}
