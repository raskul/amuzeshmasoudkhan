<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIndexVipProductRequest;
use App\Http\Requests\UpdateIndexVipProductRequest;
use App\Models\Product;
use App\Repositories\IndexVipProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IndexVipProductController extends AppBaseController
{
    /** @var  IndexVipProductRepository */
    private $indexVipProductRepository;

    public function __construct(IndexVipProductRepository $indexVipProductRepo)
    {
        $this->indexVipProductRepository = $indexVipProductRepo;
    }

    /**
     * Display a listing of the IndexVipProduct.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
         $this->authorize('is-employee');

        $this->indexVipProductRepository->pushCriteria(new RequestCriteria($request));
        $indexVipProducts = $this->indexVipProductRepository->all();

        return view('index_vip_products.index')
            ->with('indexVipProducts', $indexVipProducts);
    }

    /**
     * Show the form for creating a new IndexVipProduct.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        $products = Product::pluck('title', 'id');

        return view('index_vip_products.create', compact('products'));
    }

    /**
     * Store a newly created IndexVipProduct in storage.
     *
     * @param CreateIndexVipProductRequest $request
     *
     * @return Response
     */
    public function store(CreateIndexVipProductRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $indexVipProduct = $this->indexVipProductRepository->create($input);

        Flash::success('دوره حضوری ویژه با موفقیت ذخیره شد.');

        return redirect(route('indexVipProducts.index'));
    }

    /**
     * Display the specified IndexVipProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $indexVipProduct = $this->indexVipProductRepository->findWithoutFail($id);

        if (empty($indexVipProduct)) {
            Flash::error('دوره حضوری ویژه پیدا نشد.');

            return redirect(route('indexVipProducts.index'));
        }

        return view('index_vip_products.show')->with('indexVipProduct', $indexVipProduct);
    }

    /**
     * Show the form for editing the specified IndexVipProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $indexVipProduct = $this->indexVipProductRepository->findWithoutFail($id);

        if (empty($indexVipProduct)) {
            Flash::error('دوره حضوری ویژه پیدا نشد.');

            return redirect(route('indexVipProducts.index'));
        }

        $products = Product::pluck('title', 'id');

        return view('index_vip_products.edit', compact('products', 'indexVipProduct'));
    }

    /**
     * Update the specified IndexVipProduct in storage.
     *
     * @param  int              $id
     * @param UpdateIndexVipProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIndexVipProductRequest $request)
    {
         $this->authorize('is-employee');

        $indexVipProduct = $this->indexVipProductRepository->findWithoutFail($id);

        if (empty($indexVipProduct)) {
            Flash::error('دوره حضوری ویژه پیدا نشد.');

            return redirect(route('indexVipProducts.index'));
        }

        $indexVipProduct = $this->indexVipProductRepository->update($request->all(), $id);

        Flash::success('دوره حضوری ویژه با موفقیت بروز رسانی شد.');

        return redirect(route('indexVipProducts.index'));
    }

    /**
     * Remove the specified IndexVipProduct from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $indexVipProduct = $this->indexVipProductRepository->findWithoutFail($id);

        if (empty($indexVipProduct)) {
            Flash::error('دوره حضوری ویژه پیدا نشد.');

            return redirect(route('indexVipProducts.index'));
        }

        $this->indexVipProductRepository->delete($id);

        Flash::success('دوره حضوری ویژه با موفقیت پاک شد.');

        return redirect(route('indexVipProducts.index'));
    }
}
