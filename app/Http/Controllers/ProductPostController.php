<?php

namespace App\Http\Controllers;

use App\DataTables\ProductPostDataTable;
use App\Http\Requests\CreateProductPostRequest;
use App\Http\Requests\UpdateProductPostRequest;
use App\Models\Product;
use App\Repositories\ProductPostRepository;
use Flash;
use Response;

class ProductPostController extends AppBaseController
{
    /** @var  ProductPostRepository */
    private $productPostRepository;

    public function __construct(ProductPostRepository $productPostRepo)
    {
        $this->productPostRepository = $productPostRepo;
    }

    /**
     * Display a listing of the ProductPost.
     *
     * @param ProductPostDataTable $productPostDataTable
     * @return Response
     */
    public function index(ProductPostDataTable $productPostDataTable)
    {
        $this->authorize('is-employee');

        return $productPostDataTable->render('product_posts.index');
    }

    /**
     * Show the form for creating a new ProductPost.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        $products = Product::pluck('title', 'id');

        return view('product_posts.create', compact('products'));
    }

    /**
     * Store a newly created ProductPost in storage.
     *
     * @param CreateProductPostRequest $request
     *
     * @return Response
     */
    public function store(CreateProductPostRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        $input['user_id'] = auth()->user()->id;

        $productPost = $this->productPostRepository->create($input);

        Flash::success('پست دوره آموزشی با موفقیت ذخیره شد.');

        return redirect(route('productPosts.index'));
    }

    /**
     * Display the specified ProductPost.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $productPost = $this->productPostRepository->findWithoutFail($id);

        if (empty($productPost)) {
            Flash::error('پست دوره آموزشی پیدا نشد.');

            return redirect(route('productPosts.index'));
        }

        return view('product_posts.show')->with('productPost', $productPost);
    }

    /**
     * Show the form for editing the specified ProductPost.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $productPost = $this->productPostRepository->findWithoutFail($id);

        if (empty($productPost)) {
            Flash::error('پست دوره آموزشی پیدا نشد.');

            return redirect(route('productPosts.index'));
        }

        $products = Product::pluck('title', 'id');

        return view('product_posts.edit', compact('productPost', 'products'));
    }

    /**
     * Update the specified ProductPost in storage.
     *
     * @param  int              $id
     * @param UpdateProductPostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductPostRequest $request)
    {
        $this->authorize('is-employee');

        $productPost = $this->productPostRepository->findWithoutFail($id);

        if (empty($productPost)) {
            Flash::error('پست دوره آموزشی پیدا نشد.');

            return redirect(route('productPosts.index'));
        }

        $productPost = $this->productPostRepository->update($request->all(), $id);

        Flash::success('پست دوره آموزشی با موفقیت ویرایش شد.');

        return redirect(route('productPosts.index'));
    }

    /**
     * Remove the specified ProductPost from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $productPost = $this->productPostRepository->findWithoutFail($id);

        if (empty($productPost)) {
            Flash::error('پست دوره آموزشی پیدا نشد.');

            return redirect(route('productPosts.index'));
        }

        $this->productPostRepository->delete($id);

        Flash::success('پست دوره آموزشی با موفقیت پاک شد.');

        return redirect(route('productPosts.index'));
    }
}
