<?php

namespace App\Http\Controllers;

use App\DataTables\CategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     *
     * @param CategoryDataTable $categoryDataTable
     * @return Response
     */
    public function index()
    {
         $this->authorize('is-employee');

        $categories = Category::whereNull('parent_id')->get();

        return view('categories.index', compact('categories', 'arr'));
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        $categories = Category::pluck('name_fa', 'id');

        return view('categories.create', compact('categories'));
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        if (!(isset($request->order))){
            $input['order'] = 10;
        }

        $category = $this->categoryRepository->create($input);

        Flash::success('دسته با موفقیت ذخیره شد.');

        return redirect(route('categories.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('دسته پیدا نشد.');

            return redirect(route('categories.index'));
        }

        return view('categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('دسته پیدا نشد.');

            return redirect(route('categories.index'));
        }

        $categories = Category::pluck('name', 'id');


        return view('categories.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $this->authorize('is-employee');

        if ($id == 1){//don't update the root
            Flash::error('گروه ریشه قابل ویرایش نمیباشد.');

            return redirect(route('categories.index'));
        }

        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('دسته پیدا نشد.');

            return redirect(route('categories.index'));
        }

        $category = $this->categoryRepository->update($request->all(), $id);

        Flash::success('دسته با موفقیت بروز رسانی شد.');

        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        if ($id == 1){//don't update the root
            Flash::error('گروه ریشه قابل حذف نمیباشد.');

            return redirect(route('categories.index'));
        }

        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('دسته پیدا نشد.');

            return redirect(route('categories.index'));
        }

        $this->categoryRepository->delete($id);

        Flash::success('دسته با موفقیت پاک شد.');

        return redirect(route('categories.index'));
    }
}
