<?php

namespace App\Http\Controllers;

use App\DataTables\ProductTypeDataTable;
use App\Http\Requests\CreateProductTypeRequest;
use App\Http\Requests\UpdateProductTypeRequest;
use App\Repositories\ProductTypeRepository;
use Flash;
use Response;

class ProductTypeController extends AppBaseController
{
    /** @var  ProductTypeRepository */
    private $productTypeRepository;

    public function __construct(ProductTypeRepository $productTypeRepo)
    {
        $this->productTypeRepository = $productTypeRepo;
    }

    /**
     * Display a listing of the ProductType.
     *
     * @param ProductTypeDataTable $productTypeDataTable
     * @return Response
     */
    public function index(ProductTypeDataTable $productTypeDataTable)
    {
        $this->authorize('is-employee');

        return $productTypeDataTable->render('product_types.index');
    }

    /**
     * Show the form for creating a new ProductType.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        return view('product_types.create');
    }

    /**
     * Store a newly created ProductType in storage.
     *
     * @param CreateProductTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateProductTypeRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        $productType = $this->productTypeRepository->create($input);

        Flash::success('نوع دوره با موفقیت ذخیره شد.');

        return redirect(route('productTypes.index'));
    }

    /**
     * Display the specified ProductType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $productType = $this->productTypeRepository->findWithoutFail($id);

        if (empty($productType)) {
            Flash::error('نوع دوره پیدا نشد.');

            return redirect(route('productTypes.index'));
        }

        return view('product_types.show')->with('productType', $productType);
    }

    /**
     * Show the form for editing the specified ProductType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $productType = $this->productTypeRepository->findWithoutFail($id);

        if (empty($productType)) {
            Flash::error('نوع دوره پیدا نشد.');

            return redirect(route('productTypes.index'));
        }

        return view('product_types.edit')->with('productType', $productType);
    }

    /**
     * Update the specified ProductType in storage.
     *
     * @param  int              $id
     * @param UpdateProductTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductTypeRequest $request)
    {
        $this->authorize('is-employee');

        $productType = $this->productTypeRepository->findWithoutFail($id);

        if (empty($productType)) {
            Flash::error('نوع دوره پیدا نشد.');

            return redirect(route('productTypes.index'));
        }

        $productType = $this->productTypeRepository->update($request->all(), $id);

        Flash::success('نوع دوره با موفقیت بروزرسانی شد.');

        return redirect(route('productTypes.index'));
    }

    /**
     * Remove the specified ProductType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $productType = $this->productTypeRepository->findWithoutFail($id);

        if (empty($productType)) {
            Flash::error('نوع دوره پیدا نشد.');

            return redirect(route('productTypes.index'));
        }

        $this->productTypeRepository->delete($id);

        Flash::success('نوع دوره با موفقیت پاک شد.');

        return redirect(route('productTypes.index'));
    }
}
