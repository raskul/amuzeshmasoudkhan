<?php

namespace App\Http\Controllers;

use App\DataTables\TicketDataTable;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Repositories\TicketRepository;
use Auth;
use Flash;
use Response;

class TicketController extends AppBaseController
{
    /** @var  TicketRepository */
    private $ticketRepository;

    public function __construct(TicketRepository $ticketRepo)
    {
        $this->ticketRepository = $ticketRepo;
    }

    /**
     * Display a listing of the Ticket.
     *
     * @param TicketDataTable $ticketDataTable
     * @return Response
     */
    public function index(TicketDataTable $ticketDataTable)
    {
        $this->authorize('is-employee');

        return $ticketDataTable->render('tickets.index');
    }

    /**
     * Show the form for creating a new Ticket.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        return view('tickets.create');
    }

    /**
     * Store a newly created Ticket in storage.
     *
     * @param CreateTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketRequest $request)
    {
        $this->authorize('is-employee');

        if (Auth::check()) {
            $input['user_id'] = auth()->user()->id;
            $input['email'] = auth()->user()->email;
        } else {
            $input['user_id'] = $request->user_id;
            $input['email'] = $request->email;
        }
        $input['text'] = $request->text;
        $input['is_read'] = 0;
        $input['who_answer'] = null;


        $ticket = $this->ticketRepository->create($input);

        Flash::success('تیک با موفقیت ذخیره شد.');

        return redirect(route('tickets.index'));
    }

    /**
     * Display the specified Ticket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('تیکت پیدا نشد.');

            return redirect(route('tickets.index'));
        }

        return view('tickets.show')->with('ticket', $ticket);
    }

    /**
     * Show the form for editing the specified Ticket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('تیکت پیدا نشد.');

            return redirect(route('tickets.index'));
        }

        return view('tickets.edit')->with('ticket', $ticket);
    }

    /**
     * Update the specified Ticket in storage.
     *
     * @param  int              $id
     * @param UpdateTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketRequest $request)
    {
        $this->authorize('is-employee');

        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('تیکت پیدا نشد.');

            return redirect(route('tickets.index'));
        }

//        if (Auth::check()) {
//            $input['user_id'] = auth()->user()->id;
//            $input['email'] = auth()->user()->email;
//        } else {
        $input['user_id'] = $request->user_id;
        $input['email'] = $request->email;
//        }
        $input['text'] = $request->text;
        $input['is_read'] = 0;
        $input['who_answer'] = $request->who_answer;

        $ticket = $this->ticketRepository->update($input, $id);

        Flash::success('تیکت با موفقیت بروز رسانی شد.');

        return redirect(route('tickets.index'));
    }

    /**
     * Remove the specified Ticket from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('تیکت پیدا نشد.');

            return redirect(route('tickets.index'));
        }

        $this->ticketRepository->delete($id);

        Flash::success('تیکت با موفقیت پاک شد.');

        return redirect(route('tickets.index'));
    }
}
