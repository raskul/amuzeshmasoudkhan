<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIndexNotificationRequest;
use App\Http\Requests\UpdateIndexNotificationRequest;
use App\Models\Product;
use App\Repositories\IndexNotificationRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IndexNotificationController extends AppBaseController
{
    /** @var  IndexNotificationRepository */
    private $indexNotificationRepository;

    public function __construct(IndexNotificationRepository $indexNotificationRepo)
    {
        $this->indexNotificationRepository = $indexNotificationRepo;
    }

    /**
     * Display a listing of the IndexNotification.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->authorize('is-employee');

        $this->indexNotificationRepository->pushCriteria(new RequestCriteria($request));
        $indexNotifications = $this->indexNotificationRepository->all();

        return view('index_notifications.index')
            ->with('indexNotifications', $indexNotifications);
    }

    /**
     * Show the form for creating a new IndexNotification.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        $products = Product::pluck('title', 'id');

        return view('index_notifications.create', compact('products'));
    }

    /**
     * Store a newly created IndexNotification in storage.
     *
     * @param CreateIndexNotificationRequest $request
     *
     * @return Response
     */
    public function store(CreateIndexNotificationRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $indexNotification = $this->indexNotificationRepository->create($input);

        Flash::success('اطلاعیه آموزشی صفحه اصلی با موفقیت ذخیره شد.');

        return redirect(route('indexNotifications.index'));
    }

    /**
     * Display the specified IndexNotification.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $indexNotification = $this->indexNotificationRepository->findWithoutFail($id);

        if (empty($indexNotification)) {
            Flash::error('اطلاعیه آموزشی صفحه اصلی پیدا نشد.');

            return redirect(route('indexNotifications.index'));
        }

        return view('index_notifications.show')->with('indexNotification', $indexNotification);
    }

    /**
     * Show the form for editing the specified IndexNotification.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $indexNotification = $this->indexNotificationRepository->findWithoutFail($id);

        if (empty($indexNotification)) {
            Flash::error('اطلاعیه آموزشی صفحه اصلی پیدا نشد.');

            return redirect(route('indexNotifications.index'));
        }
        $products = Product::pluck('title', 'id');

        return view('index_notifications.edit', compact('indexNotification', 'products'));
    }

    /**
     * Update the specified IndexNotification in storage.
     *
     * @param  int              $id
     * @param UpdateIndexNotificationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIndexNotificationRequest $request)
    {
         $this->authorize('is-employee');

        $indexNotification = $this->indexNotificationRepository->findWithoutFail($id);

        if (empty($indexNotification)) {
            Flash::error('اطلاعیه آموزشی صفحه اصلی پیدا نشد.');

            return redirect(route('indexNotifications.index'));
        }

        $indexNotification = $this->indexNotificationRepository->update($request->all(), $id);

        Flash::success('اطلاعیه آموزشی صفحه اصلی با موفقیت بروز رسانی شد.');

        return redirect(route('indexNotifications.index'));
    }

    /**
     * Remove the specified IndexNotification from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $indexNotification = $this->indexNotificationRepository->findWithoutFail($id);

        if (empty($indexNotification)) {
            Flash::error('اطلاعیه آموزشی صفحه اصلی پیدا نشد.');

            return redirect(route('indexNotifications.index'));
        }

        $this->indexNotificationRepository->delete($id);

        Flash::success('اطلاعیه آموزشی صفحه اصلی با موفقیت پاک شد.');

        return redirect(route('indexNotifications.index'));
    }
}
