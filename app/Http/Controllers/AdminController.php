<?php

namespace App\Http\Controllers;

use App\BalanceHistrory;
use App\Models\Category;
use App\Models\Log;
use App\Models\Product;
use App\Models\UserDocument;
use DB;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class AdminController extends Controller
{
    public function __construct()
    {

    }

    public function sortCategory(Request $request)
    {
        $this->authorize('is-employee');

        $list = collect(explode('&', $request->list));
        $keyboards = [];
        $list->map(function ($item, $key) use (&$keyboards) {
            $item = str_replace('list[', '', $item);
            $keyboards[] = explode(']=', $item);
        });
        foreach ($keyboards as $keyboard) {
            $category = Category::find($keyboard[0]);
            if (is_numeric($keyboard[1])){
                $category->update(['parent_id' => $keyboard[1]]);
            }else{
                $category->update(['parent_id' => null]);
            }
        }
    }

    public function adminFinancialWithdraw(Request $request, BalanceHistrory $balanceHistory)
    {
        $this->authorize('is-employee');

        if ($request->isMethod('POST')){
            $balanceHistory->update(['type' => 2]);

            return redirect()->back();
        }
        $balanceHistories = BalanceHistrory::where('type', 3)->get();

        $oldBalanceHistories = BalanceHistrory::orderByDesc('updated_at')->paginate(50);

        return view('indexAdmin.adminFinancialWithdraw', compact('balanceHistories', 'oldBalanceHistories'));
    }

    public function eshantion($id = null, Request $request)
    {
        $this->authorize('is-employee');

        if ($id == null) {//index of eshantion
            if ($request->isMethod('GET')) {
                $all_table = DB::table('eshantion_mahsulasli')->get();

                return view('indexAdmin.eshantion', compact('all_table'));
            }
            if ($request->isMethod('POST')) {//create post
                $mahsulasli = Product::find($request->mahsulasli);
                $mahsulasli->eshantions()->attach([$request->eshantion]);
                Flash::success('اشانتیون با موفقیت به محصول افزوده شد.');

                return redirect()->route('eshantion');
            }

        } else {//create edit delete eshantion
            if ($request->isMethod('GET')) {
                if ($id == 'new') {//create get
                    $mahsulasli = Product::get();
                    $eshantion = $mahsulasli;

                    return view('indexAdmin.eshantionCreate', compact('mahsulasli', 'eshantion'));
                }
                if (is_numeric($id)) {//edit get
                    $item = DB::table('eshantion_mahsulasli')->find($id);
                    $products = Product::get();

                    return view('indexAdmin.eshantionEdit' , compact('item', 'products'));
                }
            }
            if ($request->isMethod('PUT')) {//edit eshantion put
                DB::table('eshantion_mahsulasli')->where('id', $id)->update([
                    'mahsulasli_id' => $request->mahsulasli,
                    'eshantion_id' => $request->eshantion
                ]);
                Flash::success('اشانتیون با موفقیت ویرایش شد.');

                return redirect()->route('eshantion');
            }
            if ($request->isMethod('DELETE')) {//delete eshantion
                DB::table('eshantion_mahsulasli')->where('id', $id)->delete();
                Flash::success('اشانتیون با موفقیت پاک شد.');

                return redirect()->route('eshantion');
            }
        }
    }

    public function visitorlog()
    {
        $logs = Log::orderByDesc('created_at')->paginate(50);

        return view('indexAdmin.visitorlog', compact('logs'));
    }

    public function managerReport()
    {
        $this->authorize('is-employee');

        return view('report');
    }

    public function teacherUploadFileReport()
    {
        $this->authorize('is-employee');

        $docs = UserDocument::orderByDesc('created_at')->paginate(50);

        return view('indexAdmin.teacherUploadFileReport', compact('docs'));
    }
}
