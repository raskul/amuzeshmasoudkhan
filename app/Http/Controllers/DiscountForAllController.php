<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Discount;
use App\Models\Product;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Morilog\Jalali\jDateTime;

class DiscountForAllController extends DiscountController
//class DiscountForAllController extends Controller
{
    public function discountForProducts(Request $request, $my_product_ids = null, $my_price = null, $my_is_percent = null)
    {
        $this->authorize('is-employee');

        if (isset($my_product_ids)) {
//            dd('hi');
            $price = $my_price;
            $products_id_array = $my_product_ids;
            $is_percent = $my_is_percent;
            goto anotherFunctions;
        }
        if (!(empty($request->all()))) {
            $price = $request->price;
            $products_id_array = $request->product_id;
            $is_percent = $request->is_percent;

            if (!($request->price)) {
                $price = 0;
            }
            anotherFunctions:
            if ($request->all_checked) {
                $products_id_array = Product::pluck('id');
            }
            if ($request->delete_all_discount) {
                $price = 0;
                $products_id_array = Product::pluck('id');
            }
            if ($is_percent == 0) {//price in toman
                Product::whereIn('id', $products_id_array)->update(['discount' => $price]);
                $products = Product::whereIn('id', $products_id_array)->get();
                foreach ($products as $product) {
                    $real_price = ($product->price) - ($product->discount);
                    Product::where('id', $product->id)->update(['real_price' => $real_price]);
                }
            } else {//price in percent
                $products = Product::whereIn('id', $products_id_array)->get();
                foreach ($products as $product) {
                    $real_price = number_format((integer)round((($product->price) - (($product->price) * ($price) / 100)) / 100), 2, '', '');
                    $discount = ($product->price) - ($real_price);
                    Product::where('id', $product->id)->update(['real_price' => $real_price, 'discount' => $discount]);
                }
            }

            if (count($products_id_array))
                Flash::success('تخفیف ها با موفقیت ثبت شدند.');
            else
                Flash::error('شما هیچ محصولی انتخاب نکرده اید.');

            return redirect()->back();
        }

        $products = Product::get();

        return view('discountForAll.discountForProducts', compact('products', 'discounted_products'));
    }

    public function discountForCategoryOfProducts(Request $request)
    {
        $this->authorize('is-employee');

        if (!(empty($request->all()))) {
            $category_ids = $request->category_id;
            $categories = Category::whereIn('id', $category_ids)->get();
            $product_ids = [];
            foreach ($categories as $category) {
                $id = Product::where('category_id', $category->id)->pluck('id')->toArray();
                foreach ($id as $i) {
                    $product_ids[] = $i;
                }
            }
            $this->discountForProducts(Request(), $product_ids, $request->price, $request->is_percent);

            return redirect()->back();
        }
        $categories = Category::get();

        return view('discountForAll.discountForCategoryOfProducts', compact('categories'));
    }

    public function discountFestival(Request $request)
    {
        $this->authorize('is-employee');

        if (!(empty($request->all()))) {
            $inputs["name"] = $request->name;
            $inputs["name_fa"] = $request->name_fa;
            $inputs["code"] = $request->code;
            $inputs["price"] = $request->price;
            $inputs["start_at"] = $request->start_at;
            $inputs["end_at"] = $request->end_at;
            $inputs["is_percent"] = $request->is_percent;
            $inputs["active"] = $request->active;
            $inputs["type"] = $request->type;
            $inputs["who_edit"] = auth()->user()->name;

            $inputs["start_at"] = normalCalendarFormat($inputs["start_at"]);
            $inputs['start_at'] = jDateTime::createDatetimeFromFormat('j m Y g:i', $inputs["start_at"])->getTimestamp();
            $inputs['start_at'] = date('Y-m-d H:i:s', $inputs['start_at']);

            $inputs["end_at"] = normalCalendarFormat($inputs["end_at"]);
            $inputs['end_at'] = jDateTime::createDatetimeFromFormat('j m Y g:i', $inputs["end_at"])->getTimestamp();
            $inputs['end_at'] = date('Y-m-d H:i:s', $inputs['end_at']);
            Discount::create($inputs);
            Flash::success('جشنواره تخفیف با موفقیت ایجاد شد.');

            return redirect()->back();
        }

        return view('discountForAll.discountFestival');
    }

    public function discountForUsers(Request $request)
    {
        $this->authorize('is-employee');

        if (!(empty($request->all()))) {
            $discount_id = $request->discount_id;
            $user_ids = $request->user_id;
            if (isset($request->all_checked)) {
                $user_ids = User::pluck('id');
            }
            foreach ($user_ids as $user_id) {
//                DB::table('discount_user')->insert(['user_id' => $user_id, 'discount_id' => $discount_id]);
                $user = User::find($user_id);
                $user->discounts()->sync($discount_id);
            }
            Flash::success('تخفیف کاربران انتخاب شده با موفقیت ثبت شد.');

            return redirect()->back();
        }
        $discounts = Discount::get();
        $users = User::get();

        return view('discountForAll.discountForUsers', compact('users', 'discounts'));
    }

    public function discountCodeForProducts(Request $request, $my_product_ids = null, $my_discount_id =null)
    {
        $this->authorize('is-employee');

        if (isset($my_product_ids)){
            $discount_id = $my_discount_id;
            $product_ids = $my_product_ids;
            goto anotherfunctio;
        }
        if (!(empty($request->all()))) {
            $discount_id = $request->discount_id;
            $product_ids = $request->product_id;
            if (isset($request->all_checked)) {
                $product_ids = Product::pluck('id');
            }
            anotherfunctio:
            foreach ($product_ids as $product_id) {
//                DB::table('discount_product')->insert(['product_id' => $product_id, 'discount_id' => $discount_id]);
                $product = Product::find($product_id);
                $product->discounts()->sync($discount_id);
            }
            Flash::success('تخفیف محصولات انتخاب شده با موفقیت ثبت شد.');

            return redirect()->back();
        }
        $discounts = Discount::get();
        $products = Product::get();

        return view('discountForAll.discountCodeForProducts', compact('products', 'discounts'));
    }

    public function discountCodeForCategoryOfProducts(Request $request)
    {
        $this->authorize('is-employee');

        if (!(empty($request->all()))) {
//            dd($request->all());
            $category_ids = $request->category_id;
            $all_checked = $request->all_checked;
            $discount_id = $request->discount_id;
            if (isset($all_checked)){
                $category_ids = Category::pluck('id')->toArray();
            }
            $product_ids = [];
            foreach ($category_ids as $category_id){
                $ids = Product::where('category_id', $category_id)->pluck('id')->toArray();
                foreach ($ids as $id){
                    $product_ids[] = $id;
                }
            }
            $this->discountCodeForProducts(Request(), $product_ids, $discount_id);

            return redirect()->back();
        }
        $categories = Category::get();
        $discounts = Discount::get();

        return view('discountForAll.discountCodeForCategoryOfProducts', compact('discounts', 'categories'));
    }

    public function discountCodeForUsers()
    {
        $this->authorize('is-employee');

        return view('discountForAll.discountCodeForUsers');
    }

    public function discountForPrice(Request $request)
    {
        $this->authorize('is-employee');

        if (!(empty($request->all()))) {
            Setting::where('name', 'takhfif_line')->update(['value' => $request->takhfif_line]);
            Setting::where('name', 'takhfif_value')->update(['value' => $request->takhfif_value]);
            Setting::where('name', 'takhfif_type')->update(['value' => $request->takhfif_type]);
            Flash::success('تنظیمات تخفیف ذخیره شد.');

            return redirect()->back();
        }
        $settings = Setting::get();
        $takhfif_line = $settings->where('name', 'takhfif_line')->pluck('value')->first();
        $takhfif_value = $settings->where('name', 'takhfif_value')->pluck('value')->first();
        $takhfif_type = $settings->where('name', 'takhfif_type')->pluck('value')->first();

        return view('discountForAll.discountForPrice', compact('takhfif_line', 'takhfif_value', 'takhfif_type'));
    }

    public function reportListOfAllProductsHasDiscount()
    {
        $this->authorize('is-employee');

        $products = Product::where('discount', '>', 0)->get();

        return view('discountForAll.reportListOfAllProductsHasDiscount', compact('products'));
    }

    public function reportListOfAllProductsHasDiscountCode()
    {
        $this->authorize('is-employee');

        $products = Product::with('discounts')->get();

        return view('discountForAll.reportListOfAllProductsHasDiscountCode', compact('products'));
    }

    public function reportListOfAllUsersHasDiscountCode()
    {
        $this->authorize('is-employee');

        $users = User::with('discounts')->get();

        return view('discountForAll.reportListOfAllUsersHasDiscountCode', compact('users'));
    }

    public function reportListOfAllUsersBuyWithDiscountCode()
    {
        $this->authorize('is-employee');

        $users = User::with('discountsBurned')->get();

        return view('discountForAll.reportListOfAllUsersBuyWithDiscountCode', compact('users'));
    }
}
