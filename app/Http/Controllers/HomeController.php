<?php

namespace App\Http\Controllers;

use App\BalanceHistrory;
use App\Models\Factor;
use App\Models\TeachingRequest;
use App\Models\Ticket;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('is-employee');
        $ticket_not_read = Ticket::where('is_read', 0)->count();
        $ticket_not_closed = Ticket::where('closed', 0)->count();
        $teaching_request = TeachingRequest::whereIn('status', [0,1])->count();
        $factor_by_post_not_send = Factor::where('is_by_post', 1)->where('is_sent', 0)->where('is_paid', 1)->count();
        $withdraw_request = BalanceHistrory::where('type', 3)->count();

        return view('home', compact(
            'ticket_not_read',
            'ticket_not_closed',
            'teaching_request',
            'factor_by_post_not_send',
            'withdraw_request'
        ));
    }
}
