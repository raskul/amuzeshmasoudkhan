<?php

namespace App\Http\Controllers;

use App\DataTables\PostDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Repositories\PostRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;

    public function __construct(PostRepository $postRepo)
    {
        $this->postRepository = $postRepo;
    }

    /**
     * Display a listing of the Post.
     *
     * @param PostDataTable $postDataTable
     * @return Response
     */
    public function index(PostDataTable $postDataTable)
    {
         $this->authorize('is-employee');

        return $postDataTable->render('posts.index');
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        return view('posts.create');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $post = $this->postRepository->create($input);

        Flash::success('پست با موفقیت ذخیره شد.');

        return redirect(route('posts.index'));
    }

    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('پست پیدا نشد.');

            return redirect(route('posts.index'));
        }

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('پست پیدا نشد.');

            return redirect(route('posts.index'));
        }

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
         $this->authorize('is-employee');

        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('پست پیدا نشد.');

            return redirect(route('posts.index'));
        }

        $post = $this->postRepository->update($request->all(), $id);

        Flash::success('پست با موفقیت ویرایش شد.');

        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $post = $this->postRepository->findWithoutFail($id);

        if (empty($post)) {
            Flash::error('پست پیدا نشد.');

            return redirect(route('posts.index'));
        }

        $this->postRepository->delete($id);

        Flash::success('پست با موفقیت پاک شد.');

        return redirect(route('posts.index'));
    }
}
