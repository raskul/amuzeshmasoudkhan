<?php

namespace App\Http\Controllers;

use App\DataTables\UserDocumentDataTable;
use App\Http\Requests\CreateUserDocumentRequest;
use App\Http\Requests\UpdateUserDocumentRequest;
use App\Repositories\UserDocumentRepository;
use Flash;
use Response;

class UserDocumentController extends AppBaseController
{
    /** @var  UserDocumentRepository */
    private $userDocumentRepository;

    public function __construct(UserDocumentRepository $userDocumentRepo)
    {
        $this->userDocumentRepository = $userDocumentRepo;
    }

    /**
     * Display a listing of the UserDocument.
     *
     * @param UserDocumentDataTable $userDocumentDataTable
     * @return Response
     */
    public function index(UserDocumentDataTable $userDocumentDataTable)
    {
        $this->authorize('is-employee');
         
        return $userDocumentDataTable->render('user_documents.index');
    }

    /**
     * Show the form for creating a new UserDocument.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');
         
        return view('user_documents.create');
    }

    /**
     * Store a newly created UserDocument in storage.
     *
     * @param CreateUserDocumentRequest $request
     *
     * @return Response
     */
    public function store(CreateUserDocumentRequest $request)
    {
        $this->authorize('is-employee');
         
        $input = $request->all();

        $userDocument = $this->userDocumentRepository->create($input);

        Flash::success('User Document saved successfully.');

        return redirect(route('userDocuments.index'));
    }

    /**
     * Display the specified UserDocument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');
         
        $userDocument = $this->userDocumentRepository->findWithoutFail($id);

        if (empty($userDocument)) {
            Flash::error('User Document not found');

            return redirect(route('userDocuments.index'));
        }

        return view('user_documents.show')->with('userDocument', $userDocument);
    }

    /**
     * Show the form for editing the specified UserDocument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');
         
        $userDocument = $this->userDocumentRepository->findWithoutFail($id);

        if (empty($userDocument)) {
            Flash::error('User Document not found');

            return redirect(route('userDocuments.index'));
        }

        return view('user_documents.edit')->with('userDocument', $userDocument);
    }

    /**
     * Update the specified UserDocument in storage.
     *
     * @param  int              $id
     * @param UpdateUserDocumentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserDocumentRequest $request)
    {
        $this->authorize('is-employee');
         
        $userDocument = $this->userDocumentRepository->findWithoutFail($id);

        if (empty($userDocument)) {
            Flash::error('User Document not found');

            return redirect(route('userDocuments.index'));
        }

        $userDocument = $this->userDocumentRepository->update($request->all(), $id);

        Flash::success('User Document updated successfully.');

        return redirect(route('userDocuments.index'));
    }

    /**
     * Remove the specified UserDocument from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');
         
        $userDocument = $this->userDocumentRepository->findWithoutFail($id);

        if (empty($userDocument)) {
            Flash::error('User Document not found');

            return redirect(route('userDocuments.index'));
        }

        $this->userDocumentRepository->delete($id);

        Flash::success('User Document deleted successfully.');

        return redirect(route('userDocuments.index'));
    }
}
