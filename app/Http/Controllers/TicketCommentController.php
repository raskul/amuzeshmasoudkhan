<?php

namespace App\Http\Controllers;

use App\DataTables\TicketCommentDataTable;
use App\Http\Requests\CreateTicketCommentRequest;
use App\Http\Requests\UpdateTicketCommentRequest;
use App\Repositories\TicketCommentRepository;
use Flash;
use Response;

class TicketCommentController extends AppBaseController
{
    /** @var  TicketCommentRepository */
    private $ticketCommentRepository;

    public function __construct(TicketCommentRepository $ticketCommentRepo)
    {
        $this->ticketCommentRepository = $ticketCommentRepo;
    }

    /**
     * Display a listing of the TicketComment.
     *
     * @param TicketCommentDataTable $ticketCommentDataTable
     * @return Response
     */
    public function index(TicketCommentDataTable $ticketCommentDataTable)
    {
        $this->authorize('is-employee');
         
        return $ticketCommentDataTable->render('ticket_comments.index');
    }

    /**
     * Show the form for creating a new TicketComment.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');
         
        return view('ticket_comments.create');
    }

    /**
     * Store a newly created TicketComment in storage.
     *
     * @param CreateTicketCommentRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketCommentRequest $request)
    {
        $this->authorize('is-employee');
         
        $input = $request->all();

        $ticketComment = $this->ticketCommentRepository->create($input);

        Flash::success('Ticket Comment saved successfully.');

        return redirect(route('ticketComments.index'));
    }

    /**
     * Display the specified TicketComment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');
         
        $ticketComment = $this->ticketCommentRepository->findWithoutFail($id);

        if (empty($ticketComment)) {
            Flash::error('Ticket Comment not found');

            return redirect(route('ticketComments.index'));
        }

        return view('ticket_comments.show')->with('ticketComment', $ticketComment);
    }

    /**
     * Show the form for editing the specified TicketComment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');
         
        $ticketComment = $this->ticketCommentRepository->findWithoutFail($id);

        if (empty($ticketComment)) {
            Flash::error('Ticket Comment not found');

            return redirect(route('ticketComments.index'));
        }

        return view('ticket_comments.edit')->with('ticketComment', $ticketComment);
    }

    /**
     * Update the specified TicketComment in storage.
     *
     * @param  int              $id
     * @param UpdateTicketCommentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketCommentRequest $request)
    {
        $this->authorize('is-employee');
         
        $ticketComment = $this->ticketCommentRepository->findWithoutFail($id);

        if (empty($ticketComment)) {
            Flash::error('Ticket Comment not found');

            return redirect(route('ticketComments.index'));
        }

        $ticketComment = $this->ticketCommentRepository->update($request->all(), $id);

        Flash::success('Ticket Comment updated successfully.');

        return redirect(route('ticketComments.index'));
    }

    /**
     * Remove the specified TicketComment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');
         
        $ticketComment = $this->ticketCommentRepository->findWithoutFail($id);

        if (empty($ticketComment)) {
            Flash::error('Ticket Comment not found');

            return redirect(route('ticketComments.index'));
        }

        $this->ticketCommentRepository->delete($id);

        Flash::success('Ticket Comment deleted successfully.');

        return redirect(route('ticketComments.index'));
    }
}
