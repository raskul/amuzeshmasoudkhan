<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSettingRequest;
use App\Http\Requests\UpdateSettingRequest;
use App\Repositories\SettingRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SettingController extends AppBaseController
{
    /** @var  SettingRepository */
    private $settingRepository;

    public function __construct(SettingRepository $settingRepo)
    {
        $this->settingRepository = $settingRepo;
    }

    /**
     * Display a listing of the Setting.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->authorize('is-employee');

        $this->settingRepository->pushCriteria(new RequestCriteria($request));
        $settings = $this->settingRepository->all();

        return view('settings.index')
            ->with('settings', $settings);
    }

    /**
     * Show the form for creating a new Setting.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        return view('settings.create');
    }

    /**
     * Store a newly created Setting in storage.
     *
     * @param CreateSettingRequest $request
     *
     * @return Response
     */
    public function store(CreateSettingRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        $setting = $this->settingRepository->create($input);

        Flash::success('تنظیمات با موفقیت ذخیره شد.');

        return redirect(route('settings.index'));
    }

    /**
     * Display the specified Setting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $setting = $this->settingRepository->findWithoutFail($id);

        if (empty($setting)) {
            Flash::error('تنظیمات پیدا نشد.');

            return redirect(route('settings.index'));
        }

        return view('settings.show')->with('setting', $setting);
    }

    /**
     * Show the form for editing the specified Setting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $setting = $this->settingRepository->findWithoutFail($id);

        if (empty($setting)) {
            Flash::error('تنظیمات پیدا نشد.');

            return redirect(route('settings.index'));
        }

        return view('settings.edit')->with('setting', $setting);
    }

    /**
     * Update the specified Setting in storage.
     *
     * @param  int              $id
     * @param UpdateSettingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSettingRequest $request)
    {
        $this->authorize('is-employee');

        $setting = $this->settingRepository->findWithoutFail($id);

        if (empty($setting)) {
            Flash::error('تنظیمات پیدا نشد.');

            return redirect(route('settings.index'));
        }

        $setting = $this->settingRepository->update($request->all(), $id);

        Flash::success('تنظیمات با موفقیت بروزرسانی شد.');

        return redirect(route('settings.index'));
    }

    /**
     * Remove the specified Setting from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $setting = $this->settingRepository->findWithoutFail($id);

        if (empty($setting)) {
            Flash::error('تنظیمات پیدا نشد.');

            return redirect(route('settings.index'));
        }

        $this->settingRepository->delete($id);

        Flash::success('تنظیمات با موفقیت پاک شدند.');

        return redirect(route('settings.index'));
    }
}
