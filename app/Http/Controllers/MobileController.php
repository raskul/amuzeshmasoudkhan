<?php

namespace App\Http\Controllers;

use App\DataTables\MobileDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMobileRequest;
use App\Http\Requests\UpdateMobileRequest;
use App\Repositories\MobileRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MobileController extends AppBaseController
{
    /** @var  MobileRepository */
    private $mobileRepository;

    public function __construct(MobileRepository $mobileRepo)
    {
        $this->mobileRepository = $mobileRepo;
    }

    /**
     * Display a listing of the Mobile.
     *
     * @param MobileDataTable $mobileDataTable
     * @return Response
     */
    public function index(MobileDataTable $mobileDataTable)
    {
        $this->authorize('is-employee');

        return $mobileDataTable->render('mobiles.index');
    }

    /**
     * Show the form for creating a new Mobile.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        return view('mobiles.create');
    }

    /**
     * Store a newly created Mobile in storage.
     *
     * @param CreateMobileRequest $request
     *
     * @return Response
     */
    public function store(CreateMobileRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $mobile = $this->mobileRepository->create($input);

        Flash::success('Mobile saved successfully.');

        return redirect(route('mobiles.index'));
    }

    /**
     * Display the specified Mobile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $mobile = $this->mobileRepository->findWithoutFail($id);

        if (empty($mobile)) {
            Flash::error('Mobile not found');

            return redirect(route('mobiles.index'));
        }

        return view('mobiles.show')->with('mobile', $mobile);
    }

    /**
     * Show the form for editing the specified Mobile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $mobile = $this->mobileRepository->findWithoutFail($id);

        if (empty($mobile)) {
            Flash::error('Mobile not found');

            return redirect(route('mobiles.index'));
        }

        return view('mobiles.edit')->with('mobile', $mobile);
    }

    /**
     * Update the specified Mobile in storage.
     *
     * @param  int              $id
     * @param UpdateMobileRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMobileRequest $request)
    {
         $this->authorize('is-employee');

        $mobile = $this->mobileRepository->findWithoutFail($id);

        if (empty($mobile)) {
            Flash::error('Mobile not found');

            return redirect(route('mobiles.index'));
        }

        $mobile = $this->mobileRepository->update($request->all(), $id);

        Flash::success('Mobile updated successfully.');

        return redirect(route('mobiles.index'));
    }

    /**
     * Remove the specified Mobile from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $mobile = $this->mobileRepository->findWithoutFail($id);

        if (empty($mobile)) {
            Flash::error('Mobile not found');

            return redirect(route('mobiles.index'));
        }

        $this->mobileRepository->delete($id);

        Flash::success('Mobile deleted successfully.');

        return redirect(route('mobiles.index'));
    }
}
