<?php

namespace App\Http\Controllers;

use App\DataTables\DiscountDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDiscountRequest;
use App\Http\Requests\UpdateDiscountRequest;
use App\Repositories\DiscountRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DiscountController extends AppBaseController
{
    /** @var  DiscountRepository */
    private $discountRepository;

    public function __construct(DiscountRepository $discountRepo)
    {
        $this->discountRepository = $discountRepo;
    }

    /**
     * Display a listing of the Discount.
     *
     * @param DiscountDataTable $discountDataTable
     * @return Response
     */
    public function index(DiscountDataTable $discountDataTable)
    {
         $this->authorize('is-employee');

        return $discountDataTable->render('discounts.index');
    }

    /**
     * Show the form for creating a new Discount.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        return view('discounts.create');
    }

    /**
     * Store a newly created Discount in storage.
     *
     * @param CreateDiscountRequest $request
     *
     * @return Response
     */
    public function store(CreateDiscountRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $discount = $this->discountRepository->create($input);

        Flash::success('تخفیف با موفقیت ذخیره شد.');

        return redirect(route('discounts.index'));
    }

    /**
     * Display the specified Discount.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $discount = $this->discountRepository->findWithoutFail($id);

        if (empty($discount)) {
            Flash::error('تخفیف پیدا نشد.');

            return redirect(route('discounts.index'));
        }

        return view('discounts.show')->with('discount', $discount);
    }

    /**
     * Show the form for editing the specified Discount.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $discount = $this->discountRepository->findWithoutFail($id);

        if (empty($discount)) {
            Flash::error('تخفیف پیدا نشد.');

            return redirect(route('discounts.index'));
        }

        return view('discounts.edit')->with('discount', $discount);
    }

    /**
     * Update the specified Discount in storage.
     *
     * @param  int              $id
     * @param UpdateDiscountRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDiscountRequest $request)
    {
         $this->authorize('is-employee');

        $discount = $this->discountRepository->findWithoutFail($id);

        if (empty($discount)) {
            Flash::error('تخفیف پیدا نشد.');

            return redirect(route('discounts.index'));
        }

        $discount = $this->discountRepository->update($request->all(), $id);

        Flash::success('تخفیف با موفقیت ویرایش شد.');

        return redirect(route('discounts.index'));
    }

    /**
     * Remove the specified Discount from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $discount = $this->discountRepository->findWithoutFail($id);

        if (empty($discount)) {
            Flash::error('تخفیف پیدا نشد.');

            return redirect(route('discounts.index'));
        }

        $this->discountRepository->delete($id);

        Flash::success('تخفیف با موفقیت پاک شد.');

        return redirect(route('discounts.index'));
    }
}
