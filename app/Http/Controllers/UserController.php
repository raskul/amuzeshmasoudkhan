<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Repositories\UserRepository;
use App\User;
use Auth;
use Flash;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        $this->authorize('is-employee');

        return $userDataTable->render('users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        $roles = Role::pluck('name_fa', 'id');

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        //تغییر تصویر پروفایل من
        if (isset($request->image)) {
            $this->validate($request, [
                'image' => 'mimes:jpg,jpeg,png,gif|max:2048',
            ]);
            $input['image'] = time() . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/profile/image/';
            $request->image->move($path, $input['image']);
        }

        if (isset($request->password)) {
            $input['password'] = bcrypt($input['password']);
        }

        $user = $this->userRepository->create($input);

        Flash::success('کاربر با موفقیت ذخیره شد.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('کاربر پیدا نشد.');

            return redirect(route('users.index'));
        }

        $factors = $user->factors()->where('is_paid', 1)->get();

        return view('users.show', compact('user','factors'));
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('کاربر پیدا نشد.');

            return redirect(route('users.index'));
        }

        $user->password = null;

        $roles = Role::pluck('name_fa', 'id');

        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $this->authorize('is-employee');

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('کاربر پیدا نشد.');

            return redirect(route('users.index'));
        }

        $input = $request->all();

        //تغییر تصویر پروفایل من
        if (isset($request->image)) {
            $input['image'] = time() . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/profile/image/';
            $request->image->move($path, $input['image']);
        }

        if (isset($request->password)) {
            $input['password'] = bcrypt($input['password']);
        } else {
            unset($input['password']);
        }

        $user = $this->userRepository->update($input, $id);

        Flash::success('کاربر با موفقیت بروزرسانی شد.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('کاربر پیدا نشد.');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('کاربر با موفقیت پاک شد.');

        return redirect(route('users.index'));
    }

    public function userRegister(CreateUserRequest $request)
    {
        if ($request->password == $request->password_confirmation) {
            if (is_numeric($request->tel)) {
                $inputs['name'] = $request->name;
                $inputs['email'] = $request->email;
                $inputs['tel'] = $request->tel;
                $inputs['password'] = bcrypt($request->password);
                $inputs['balance'] = 0;
                $inputs['role_id'] = 1;

                $user = User::create($inputs);

                Flash::success('ثبت نام شما با موفقیت انجام شد.');

                if (!(is_null($user))) {
                    Auth::login($user);

                    return redirect(route('profile.edit'));
                }

            } else {
                Flash::error('شماره موبایل باید عدد باشد.');
            }

        } else {
            Flash::error('تکرار کلمه عبور با کلمه عبور مطابقت ندارد.');
        }

        return redirect()->back();
    }
}
