<?php

namespace App\Http\Controllers;

use App\DataTables\FactorDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFactorRequest;
use App\Http\Requests\UpdateFactorRequest;
use App\Repositories\FactorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FactorController extends AppBaseController
{
    /** @var  FactorRepository */
    private $factorRepository;

    public function __construct(FactorRepository $factorRepo)
    {
        $this->factorRepository = $factorRepo;
    }

    /**
     * Display a listing of the Factor.
     *
     * @param FactorDataTable $factorDataTable
     * @return Response
     */
    public function index(FactorDataTable $factorDataTable)
    {
         $this->authorize('is-employee');

        return $factorDataTable->render('factors.index');
    }

    /**
     * Show the form for creating a new Factor.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        return view('factors.create');
    }

    /**
     * Store a newly created Factor in storage.
     *
     * @param CreateFactorRequest $request
     *
     * @return Response
     */
    public function store(CreateFactorRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $factor = $this->factorRepository->create($input);

        Flash::success('Factor saved successfully.');

        return redirect(route('factors.index'));
    }

    /**
     * Display the specified Factor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $factor = $this->factorRepository->findWithoutFail($id);

        if (empty($factor)) {
            Flash::error('Factor not found');

            return redirect(route('factors.index'));
        }

        return view('factors.show')->with('factor', $factor);
    }

    /**
     * Show the form for editing the specified Factor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $factor = $this->factorRepository->findWithoutFail($id);

        if (empty($factor)) {
            Flash::error('Factor not found');

            return redirect(route('factors.index'));
        }

        return view('factors.edit')->with('factor', $factor);
    }

    /**
     * Update the specified Factor in storage.
     *
     * @param  int              $id
     * @param UpdateFactorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFactorRequest $request)
    {
         $this->authorize('is-employee');

        $factor = $this->factorRepository->findWithoutFail($id);

        if (empty($factor)) {
            Flash::error('Factor not found');

            return redirect(route('factors.index'));
        }

        $factor = $this->factorRepository->update($request->all(), $id);

        Flash::success('Factor updated successfully.');

        return redirect(route('factors.index'));
    }

    /**
     * Remove the specified Factor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $factor = $this->factorRepository->findWithoutFail($id);

        if (empty($factor)) {
            Flash::error('Factor not found');

            return redirect(route('factors.index'));
        }

        $this->factorRepository->delete($id);

        Flash::success('Factor deleted successfully.');

        return redirect(route('factors.index'));
    }
}
