<?php

namespace App\Http\Controllers;

use App\BalanceHistrory;
use App\LessonStudentDetail;
use App\Models\Factor;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Session;
use SoapClient;

class GateController extends Controller
{
    //bad az zadane dokme pardakht in fanction fara khande mishavad
    public function functionGate(Factor $factor)
    {
        ////////////////start test just for test.
        ////////////////put factor id
//        $this->afterPeyment(6);//test finished
        ////////////////end testing

        $MerchantID = env('MerchantID');
        $Amount = $factor->amount; //Amount will be based on Toman - Required
        $Description = 'خرید محصولات آموزشی'; // Required
        $Email = auth()->user()->email; // Optional
        $Mobile = auth()->user()->tel; // Optional
        $CallbackURL = route('index.index') . "/callbackFunctionGate"; // Required

        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );

//Redirect to URL You can do it also by creating a form
        if ($result->Status == 100) {

            //////////////////////////////////
            $factor->update([
                'description' => $Description,
                'email' => $Email,
                'mobile' => $Mobile,
                'authority' => $result->Authority,
            ]);
            //////////////////////////////////

            Header('Location: https://www.zarinpal.com/pg/StartPay/' . $result->Authority);
//برای استفاده از زرین گیت باید ادرس به صورت زیر تغییر کند:
//Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority.'/ZarinGate');
        } else {
            echo 'ERR: ' . $result->Status;
        }
    }

    //zarinpal ya har dargahe digari be inja vasl mishe bad az pardakhte movafaghiyat amiz ba method get
    public function callbackFunctionGate(Request $request)
    {
        $MerchantID = env('MerchantID');
        $Authority = $_GET['Authority'];

        ///////////////////////////
        $factor = Factor::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->get();
        $factor = $factor->where('authority', 'LIKE', $Authority)->first();
        ///////////////////////////

        $Amount = $factor->amount; //Amount will be based on Toman

        if ($_GET['Status'] == 'OK') {

            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ]
            );

            if ($result->Status == 100) {

                ///////////////////////////
                $factor->update([
                    'status' => $result->Status,
                    'refid' => $result->RefID,
                    'is_paid' => 1,
                ]);
                $this->afterPeyment($factor->id);
                ////////////////////////////

                echo ' تراکنش با موفقیت انجام شد. شماره پیگیری: ' . '<br>' . $result->RefID . '<br>' .
                    '<a href="' .
                    route('index.index') .
                    '">بازگشت به صفحه اصلی</a>';
            } else {
                echo 'Transaction failed. Status:' . $result->Status;
            }
        } else {
            echo 'Transaction canceled by user';
        }

    }

    // used just in callbackFunctionGate
    public function afterPeyment($factor_id)
    {
        $factor = Factor::find($factor_id);
        $student = User::find($factor->user_id);

        $factorProducts = unserialize($factor->detail);

        foreach ($factorProducts as $factorProduct) {
            $codes_user_discount = unserialize($factorProduct['array_discount_cods_users']);//burn user_discount_code
//            $codes_product_discount = unserialize($factorProduct['array_discount_cods_products']);//

            //burn user_discount_code :
            if (count($codes_user_discount)) {
                foreach ($codes_user_discount as $key => $item) {
                    $student->discountsBurned()->attach($key);
                }
            }

            //user be student
            $student->lessons()->attach($factorProduct['product_id']);

            //details of student payment
            LessonStudentDetail::create([
                'student_id' => $student->id,
                'lesson_id' => $factorProduct['product_id'],
                'factor_id' => $factor->id,
                'teacher_id' => $factorProduct['teacher_id'],
                'lesson_payment' => $factorProduct['product_pay_price'],
                'teacher_interest' => $factorProduct['teacher_commission_money'],
            ]);

            //increase teacher balance
            $teacher = User::find($factorProduct['teacher_id']);
            $teacherBalance = $teacher->balance;
            $teacher->update(['balance' => $teacherBalance + $factorProduct['teacher_commission_money']]);

            //add to balance history of teacher
            $productTemp = Product::find($factorProduct['product_id']);
            BalanceHistrory::create([
                'user_id' => $teacher->id,
                'old_balance' => $teacher->balance,
                'new_balance' => $teacherBalance + $factorProduct['teacher_commission_money'],
                'type' => 1,
                'description' => $productTemp->title
            ]);

            //add one to sold_count
            $sold_count = $productTemp->sold_count + 1;
            $productTemp->update(['sold_count' => $sold_count]);

            //add user earn score to user
            $user_score = $student->score + $productTemp->user_earn_score;
            $student->update(['score' => $user_score ]);

            //start add eshantions of product
            $eshantions = $productTemp->eshantions()->get();
            foreach ($eshantions as $eshantion) {
                $student->lessons()->attach($eshantion->id);
            }
            if (count($eshantions)) {
                $priceTemp = $eshantion->price;//ask from masudkhah ???
                $commissionTemp = $eshantion->user->commission;
                $teacherInterest = $priceTemp * $commissionTemp / 100;
                $befor_sign_up = LessonStudentDetail::where('student_id', $student->id)->where('lesson_id', $eshantion->id)->get();//agar ghablan in dars ro nadash sabtenamesh kon
                if (!($befor_sign_up)) {//agar ghablan in dars ro nadash sabtenamesh kon
                    LessonStudentDetail::create([
                        'student_id'       => $student->id,
                        'lesson_id'        => $eshantion->id,
                        'factor_id'        => $factor->id,
                        'teacher_id'       => $eshantion->user_id,
                        'lesson_payment'   => 0,//eshantion
                        'teacher_interest' => $teacherInterest,
                    ]);
                }
            }
            //end add eshantions of product


        }
        //remove items from basket
        Session::forget('itemId');

    }
}
