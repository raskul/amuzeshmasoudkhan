<?php

namespace App\Http\Controllers;

use App\DataTables\TeachingRequestCommentDataTable;
use App\Http\Requests\CreateTeachingRequestCommentRequest;
use App\Http\Requests\UpdateTeachingRequestCommentRequest;
use App\Repositories\TeachingRequestCommentRepository;
use Flash;
use Response;

class TeachingRequestCommentController extends AppBaseController
{
    /** @var  TeachingRequestCommentRepository */
    private $teachingRequestCommentRepository;

    public function __construct(TeachingRequestCommentRepository $teachingRequestCommentRepo)
    {
        $this->teachingRequestCommentRepository = $teachingRequestCommentRepo;
    }

    /**
     * Display a listing of the TeachingRequestComment.
     *
     * @param TeachingRequestCommentDataTable $teachingRequestCommentDataTable
     * @return Response
     */
    public function index(TeachingRequestCommentDataTable $teachingRequestCommentDataTable)
    {
        $this->authorize('is-employee');

        return $teachingRequestCommentDataTable->render('teaching_request_comments.index');
    }

    /**
     * Show the form for creating a new TeachingRequestComment.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        return view('teaching_request_comments.create');
    }

    /**
     * Store a newly created TeachingRequestComment in storage.
     *
     * @param CreateTeachingRequestCommentRequest $request
     *
     * @return Response
     */
    public function store(CreateTeachingRequestCommentRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        $teachingRequestComment = $this->teachingRequestCommentRepository->create($input);

        Flash::success('Teaching Request Comment saved successfully.');

        return redirect(route('teachingRequestComments.index'));
    }

    /**
     * Display the specified TeachingRequestComment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $teachingRequestComment = $this->teachingRequestCommentRepository->findWithoutFail($id);

        if (empty($teachingRequestComment)) {
            Flash::error('Teaching Request Comment not found');

            return redirect(route('teachingRequestComments.index'));
        }

        return view('teaching_request_comments.show')->with('teachingRequestComment', $teachingRequestComment);
    }

    /**
     * Show the form for editing the specified TeachingRequestComment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $teachingRequestComment = $this->teachingRequestCommentRepository->findWithoutFail($id);

        if (empty($teachingRequestComment)) {
            Flash::error('Teaching Request Comment not found');

            return redirect(route('teachingRequestComments.index'));
        }

        return view('teaching_request_comments.edit')->with('teachingRequestComment', $teachingRequestComment);
    }

    /**
     * Update the specified TeachingRequestComment in storage.
     *
     * @param  int              $id
     * @param UpdateTeachingRequestCommentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTeachingRequestCommentRequest $request)
    {
        $this->authorize('is-employee');

        $teachingRequestComment = $this->teachingRequestCommentRepository->findWithoutFail($id);

        if (empty($teachingRequestComment)) {
            Flash::error('Teaching Request Comment not found');

            return redirect(route('teachingRequestComments.index'));
        }

        $teachingRequestComment = $this->teachingRequestCommentRepository->update($request->all(), $id);

        Flash::success('Teaching Request Comment updated successfully.');

        return redirect(route('teachingRequestComments.index'));
    }

    /**
     * Remove the specified TeachingRequestComment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $teachingRequestComment = $this->teachingRequestCommentRepository->findWithoutFail($id);

        if (empty($teachingRequestComment)) {
            Flash::error('Teaching Request Comment not found');

            return redirect(route('teachingRequestComments.index'));
        }

        $this->teachingRequestCommentRepository->delete($id);

        Flash::success('Teaching Request Comment deleted successfully.');

        return redirect(route('teachingRequestComments.index'));
    }
}
