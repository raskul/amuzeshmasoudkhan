<?php

namespace App\Http\Controllers;

use App\DataTables\ProductNoteDataTable;
use App\Http\Requests\CreateProductNoteRequest;
use App\Http\Requests\UpdateProductNoteRequest;
use App\Models\Product;
use App\Repositories\ProductNoteRepository;
use Flash;
use Response;

class ProductNoteController extends AppBaseController
{
    /** @var  ProductNoteRepository */
    private $productNoteRepository;

    public function __construct(ProductNoteRepository $productNoteRepo)
    {
        $this->productNoteRepository = $productNoteRepo;
    }

    /**
     * Display a listing of the ProductNote.
     *
     * @param ProductNoteDataTable $productNoteDataTable
     * @return Response
     */
    public function index(ProductNoteDataTable $productNoteDataTable)
    {
        $this->authorize('is-employee');

        return $productNoteDataTable->render('product_notes.index');
    }

    /**
     * Show the form for creating a new ProductNote.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

        $products = Product::pluck('title', 'id');

        return view('product_notes.create', compact('products'));
    }

    /**
     * Store a newly created ProductNote in storage.
     *
     * @param CreateProductNoteRequest $request
     *
     * @return Response
     */
    public function store(CreateProductNoteRequest $request)
    {
        $this->authorize('is-employee');

        $input = $request->all();

        $productNote = $this->productNoteRepository->create($input);

        Flash::success('اعلامیه دورهه با موفقیت ذخیره شد.');

        return redirect(route('productNotes.index'));
    }

    /**
     * Display the specified ProductNote.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $productNote = $this->productNoteRepository->findWithoutFail($id);

        if (empty($productNote)) {
            Flash::error('اعلامیه دوره پیدا نشد.');

            return redirect(route('productNotes.index'));
        }

        return view('product_notes.show')->with('productNote', $productNote);
    }

    /**
     * Show the form for editing the specified ProductNote.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $productNote = $this->productNoteRepository->findWithoutFail($id);

        if (empty($productNote)) {
            Flash::error('اعلامیه دوره پیدا نشد.');

            return redirect(route('productNotes.index'));
        }

        $products = Product::pluck('title', 'id');

        return view('product_notes.edit', compact('productNote', 'products'));
    }

    /**
     * Update the specified ProductNote in storage.
     *
     * @param  int              $id
     * @param UpdateProductNoteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductNoteRequest $request)
    {
        $this->authorize('is-employee');

        $productNote = $this->productNoteRepository->findWithoutFail($id);

        if (empty($productNote)) {
            Flash::error('اعلامیه دوره پیدا نشد.');

            return redirect(route('productNotes.index'));
        }

        $productNote = $this->productNoteRepository->update($request->all(), $id);

        Flash::success('اعلامیه دوره با موفقیت بروز رسانی شد.');

        return redirect(route('productNotes.index'));
    }

    /**
     * Remove the specified ProductNote from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $productNote = $this->productNoteRepository->findWithoutFail($id);

        if (empty($productNote)) {
            Flash::error('اعلامیه دوره پیدا نشد.');

            return redirect(route('productNotes.index'));
        }

        $this->productNoteRepository->delete($id);

        Flash::success('اطلاعیه با موفقیت پاک شد.');

        return redirect()->back();
    }
}
