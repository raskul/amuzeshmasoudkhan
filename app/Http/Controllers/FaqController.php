<?php

namespace App\Http\Controllers;

use App\DataTables\FaqDataTable;
use App\Http\Requests\CreateFaqRequest;
use App\Http\Requests\UpdateFaqRequest;
use App\Models\Faq;
use App\Repositories\FaqRepository;
use Flash;
use Illuminate\Http\Request;
use Response;

class FaqController extends AppBaseController
{
    /** @var  FaqRepository */
    private $faqRepository;

    public function __construct(FaqRepository $faqRepo)
    {
        $this->faqRepository = $faqRepo;
    }

    /**
     * Display a listing of the Faq.
     *
     * @param FaqDataTable $faqDataTable
     * @return Response
     */
    public function index(FaqDataTable $faqDataTable)
    {
         $this->authorize('is-employee');

        return $faqDataTable->render('faqs.index');
    }

    /**
     * Show the form for creating a new Faq.
     *
     * @return Response
     */
    public function create()
    {
         $this->authorize('is-employee');

        return view('faqs.create');
    }

    /**
     * Store a newly created Faq in storage.
     *
     * @param CreateFaqRequest $request
     *
     * @return Response
     */
    public function store(CreateFaqRequest $request)
    {
         $this->authorize('is-employee');

        $input = $request->all();

        $faq = $this->faqRepository->create($input);

        Flash::success('Faq saved successfully.');

        return redirect(route('faqs.index'));
    }

    /**
     * Display the specified Faq.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
         $this->authorize('is-employee');

        $faq = $this->faqRepository->findWithoutFail($id);

        if (empty($faq)) {
            Flash::error('Faq not found');

            return redirect(route('faqs.index'));
        }

        return view('faqs.show')->with('faq', $faq);
    }

    /**
     * Show the form for editing the specified Faq.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
         $this->authorize('is-employee');

        $faq = $this->faqRepository->findWithoutFail($id);

        if (empty($faq)) {
            Flash::error('Faq not found');

            return redirect(route('faqs.index'));
        }

        return view('faqs.edit')->with('faq', $faq);
    }

    /**
     * Update the specified Faq in storage.
     *
     * @param  int              $id
     * @param UpdateFaqRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFaqRequest $request)
    {
         $this->authorize('is-employee');

        $faq = $this->faqRepository->findWithoutFail($id);

        if (empty($faq)) {
            Flash::error('Faq not found');

            return redirect(route('faqs.index'));
        }

        $faq = $this->faqRepository->update($request->all(), $id);

        Flash::success('Faq updated successfully.');

        return redirect(route('faqs.index'));
    }

    /**
     * Remove the specified Faq from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
         $this->authorize('is-employee');

        $faq = $this->faqRepository->findWithoutFail($id);

        if (empty($faq)) {
            Flash::error('Faq not found');

            return redirect(route('faqs.index'));
        }

        $this->faqRepository->delete($id);

        Flash::success('Faq deleted successfully.');

        return redirect(route('faqs.index'));
    }

    public function storesec(Request $request)
    {

//        dd($request->all());
        if(isset($request->id)){
            $input['user_id'] = auth()->user()->id;
            $input['text'] = $request->text;
            $faq = Faq::find($request->id);
            if (($faq->user_id == auth()->user()->id) || (\Gate::allows('is-employee'))) {
                $faq->update($input);
            }

            if (isset($faq->parent_id)){
                $faq = Faq::find($faq->parent_id);
            }

            return redirect(route('show.product.page', $faq->product_id));
        }
        $input['product_id'] = $request->product_id;
        $input['user_id'] = auth()->user()->id;
        $input['text'] = $request->text;
        $faq = $this->faqRepository->create($input);

        Flash::success('دیدگاه شما با موفقیت ارسال شد.');

        return redirect()->back();
    }

    public function faqAnswer(Request $request, Faq $faq)
    {
        if ($request->isMethod('POST')) {
            if (!(auth()->check())) {
                Flash::error('ابتدا وارد اکانت کاربری خود شوید.');

                return redirect()->back();
            }
//            dd($request->all());
//            dd($faq);
            $inputs['user_id'] = auth()->user()->id;
            $inputs['product_id'] = $faq->product_id;
            $inputs['text'] = $request->text;
            $inputs['parent_id'] = $faq->id;
            Faq::create($inputs);

            return redirect(route('show.product.page', $faq->product_id));
        }

        return view('index.faqAnswer', compact('faq'));
    }

    public function faqEdit(Faq $faq)
    {
        return view('index.faqEdit', compact('faq'));
    }
}
