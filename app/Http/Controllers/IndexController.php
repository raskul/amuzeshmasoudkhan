<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Discount;
use App\Models\Factor;
use App\Models\IndexNotification;
use App\Models\IndexVipProduct;
use App\Models\Mobile;
use App\Models\Post;
use App\Models\Product;
use App\Models\ProductFile;
use App\Models\ProductNote;
use App\Models\ProductPost;
use App\Models\ProductType;
use App\Models\Setting;
use App\User;
use Auth;
use DB;
use Gate;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Session;


class IndexController extends Controller
{

    public function index()
    {
        $countStudents = User::where('role_id', 1)->count();
        $countProducts = Product::count();
        $countTeachers = User::where('role_id', 2)->count();
        $countPostsDuration = ProductPost::sum('duration');

        $discountProducts = Product::where('discount', '>', '0')->orderByDesc('discount')->take(8)->get();//age bishtar az 8 ta bashe ghaleb be ham mirize
        $newProducts = Product::orderByDesc('created_at')->take(8)->get();
        $mostSoldProducts = Product::orderByDesc('sold_count')->take(8)->get();

        $indexVipProducts = IndexVipProduct::orderByDesc('created_at')->take(10)->get();
        $indexNotifications = IndexNotification::orderByDesc('created_at')->take(10)->get();
        $futureProducts = Product::where('start_at', '>', date("Y-m-d H:i:s"))->take(20)->get();

        return view('index.index', compact(
            'countPostsDuration',
            'countStudents',
            'countProducts',
            'countTeachers',
            'discountProducts',
            'newProducts',
            'mostSoldProducts',
            'indexVipProducts',
            'indexNotifications',
            'futureProducts'

        ));
    }

    public function category($cat = null, Request $request)//show category page
    {
        if ($cat == null) {
            $category = Category::where('id', 1)->first();
        } else {
            $category = Category::where('name', $cat)->first();
        }
//        dd($category);
        $categoryId[] = $category->id;
        $categoriesTemp = $category->children()->get();
        foreach ($categoriesTemp as $categoryTemp1) {
            $categoryId[] = $categoryTemp1->id;
            $categoryTemp2 = $categoryTemp1->children()->get();
            foreach ($categoryTemp2 as $categoryTemp3) {
                $categoryId[] = $categoryTemp3->id;
                $categoryTemp4 = $categoryTemp3->children()->get();
                foreach ($categoryTemp4 as $categoryTemp5){
                    $categoryId[] = $categoryTemp5->id;
                }
            }
        }

        $products = Product::whereIn('category_id', $categoryId);
        $productTypes = ProductType::get();
        $productTypeArray = [];
        $productCategoryArray = [];
        $search = 'دنبال چه میگردی؟';

        $requestNoPage = $request->all();//if page change, it works correctly.
        unset($requestNoPage['page']);
        if (count($requestNoPage)) {
            foreach ($request->all() as $key => $value) {
                if (isset($request->search)){
                    $search = $request->search;
                    $products = $products->where('title', 'like', "%$search%");
                }
                if ($value == 'on') {

                    //type_
                    if (strpos($key, 'ype_')) {
                        if (strpos($key, 'ype_all')) {
                            $productTypeArray = ProductType::pluck('id')->toArray();
                            $productTypeArray = array_unique($productTypeArray);
                        } else {
                            $keyTemp = substr($key, strpos($key, '_') + 1);
                            $productTypeArray[] = ProductType::where('name', $keyTemp)->pluck('id')->first();
                        }
                    }

                    //category_
                    if (strpos($key, 'ategory_')) {
                        if (strpos($key, 'ategory_all')) {
                            $productCategoryArray = Category::whereIn('id', $categoryId)->pluck('id')->toArray();
                            $productCategoryArray = array_unique($productCategoryArray);
                        } else {
                            $keyTemp = substr($key, strpos($key, '_') + 1);
                            $productCategoryArray[] = Category::where('name', $keyTemp)->pluck('id')->first();
                        }
                    }
                }
            }
            $products = $products->whereIn('product_type_id', $productTypeArray);
            $products = $products->whereIn('category_id', $productCategoryArray);

            if (isset($request->price_all)) {
                //do nothing
            }
            if (isset($request->price_free)) {
                $products = $products->where('real_price', '=', 0);
            }
            if (isset($request->price_not_free)) {
                $products = $products->where('real_price', '>', 0);
            }

            //sort
            if (isset($request->sort_all)) {
                //do nothing
            } else {
                if (isset($request->sort_vote)) {
                    $products->orderByDesc('vote');
                }
                if (isset($request->sort_sold)) {
                    $products->orderByDesc('sold_count');
                }
                if (isset($request->sort_visit)) {
                    $products->orderByDesc('total_visit');
                }
                if (isset($request->sort_price_high)) {
                    $products->orderByDesc('price');
                }
                if (isset($request->sort_price_low)) {
                    $products->orderBy('price');
                }
            }

        }


        $products = $products->paginate(21);

        unset($categoryId[0]);//hazfe root az categories

        $categories = Category::whereIn('id', $categoryId)->get();

        //do not show too many categories
        if (count($categories) > 50) {
            $categories = [];
        }

        return view('index.category', compact('products', 'productTypes', 'cat', 'categories', 'productTypeArray', 'productCategoryArray', 'search'));
    }

    //unused
    public function pmBox()
    {
        return view('index.pmbox');
    }

    public function showAPost(Post $post)
    {
        return view('index.showAPost', compact('post'));
    }


    public function showProduct(Product $product, $id = 1)
    {
        if (($id == 1)) {//if post of product is not set go to first post of product
            if (substr_count(url()->current(), '/') < 4)
                return redirect(route('index.index') . '/' . $product->id . '/' . "1");
        }
        if (!(count($product->productPosts()->get()))) {
            $product_post['text'] = 'در حال حاضر هیچ محتوایی برای نمایش وجود ندارد';
        } else {
            $product_post = $product->productPosts()->get()->toArray()[$id - 1];
            $product_post['text'] = preg_replace('/width="\d+" height="\d+"/', '', $product_post['text']);
        }

        $productFiles = ProductFile::where('product_id', $product->id)->get();
        $user = User::find($product->user_id);
        $comments = $product->comments()->whereNull('parent_id')->orderByDesc('created_at')->paginate(10, ['*'], 'comment');
        $faqs = $product->faqs()->whereNull('parent_id')->orderByDesc('created_at')->paginate(10, ['*'], 'faq');
        $productPosts = ProductPost::where('product_id', $product->id)->get();

        //create symlink
        if (auth()->check()) {
            checkShortcutFolder();
            $post = $product_post['text'];
            $product_post['text'] = replaceSymlinkLinksInPost($post);
        }

        $total_visit = $product->total_visit;
        $product->update(['total_visit' => $total_visit + 1]);

        $productNotes = ProductNote::where('product_id', $product->id)->orderByDesc('created_at')->get();

        return view('index.showProduct', compact('product', 'comments', 'productPosts', 'user', 'product_post', 'productFiles', 'productNotes', 'faqs'));
    }

    //sabte emtiyaz mahsulat amuzeshi
    public function productRating(Request $request)
    {
        if (($request->rating_movie >= 1) && ($request->rating_movie <= 5)
            && ($request->rating_voice >= 1) && ($request->rating_voice <= 5)
            && ($request->rating_complete >= 1) && ($request->rating_complete)
            && ($request->rating_teacher >= 1) && ($request->rating_teacher <= 5)
            && ($request->rating_support >= 1) && ($request->rating_support <= 5)
            && ($request->rating_rezayat >= 1) && ($request->rating_rezayat <= 5)) {

            if (auth()->check()) {

                $user = User::find(auth()->user()->id);

                $products = $user->ProductRatings()->get();
                foreach ($products as $product) {
                    if ($product->id == $request->product_id) {
                        $result = $product;
                    }
                }

                $product = Product::find($request->product_id);

                if (isset($result)) {
                    Flash::error("رای شما ثبت نشد. شما به این محصول قبلا رای داده اید. با تشکر.");
                    $repeat = 0;
                } else {
                    if (Gate::denies('is-product-student', $product)) {
                        Flash::error('شما دانشجوی این درس نیستید.');
                    } else {
                        Flash::success("رای شما ثبت شد. با تشکر از این که در نظر سنجی ما شرکت کردید.");
                        $repeat = 1;

                        $total_voters = $product->total_voters;
                        $product->UserRatings()->attach(auth()->user()->id);

                        $avrage = (
                                $request->rating_movie +
                                $request->rating_voice +
                                $request->rating_complete +
                                $request->rating_teacher +
                                $request->rating_support +
                                $request->rating_rezayat
                            ) / 6;

                        $vote = (($product->vote * $product->total_voters) + $avrage) / ($product->total_voters + 1);
                        $rating_movie = (($product->rating_movie * $product->total_voters) + $request->rating_movie) / ($product->total_voters + 1);
                        $rating_voice = (($product->rating_voice * $product->total_voters) + $request->rating_voice) / ($product->total_voters + 1);
                        $rating_complete = (($product->rating_complete * $product->total_voters) + $request->rating_complete) / ($product->total_voters + 1);
                        $rating_teacher = (($product->rating_teacher * $product->total_voters) + $request->rating_teacher) / ($product->total_voters + 1);
                        $rating_support = (($product->rating_support * $product->total_voters) + $request->rating_support) / ($product->total_voters + 1);
                        $rating_rezayat = (($product->rating_rezayat * $product->total_voters) + $request->rating_rezayat) / ($product->total_voters + 1);

                        $product->update([
                            'rating_movie'    => $rating_movie,
                            'rating_voice'    => $rating_voice,
                            'rating_complete' => $rating_complete,
                            'rating_teacher'  => $rating_teacher,
                            'rating_support'  => $rating_support,
                            'rating_rezayat'  => $rating_rezayat,
                            'vote'            => $vote,
                            'total_voters'    => $total_voters + $repeat,
                        ]);
                    }
                }

            } else {
                Flash::error('برای شرکت در نظر سنجی باید وارد سایت شوید یا ثبت نام کنید.');
            }

            return redirect()->back();
        }

    }

    //un used
//    public function showProductPost(ProductPost $productPost)
//    {
//        dd('here');
//
//        checkShortcutFolder();
//        $post = $productPost->text;
//        $productPost->text = replaceSymlinkLinksInPost($post);
////        dd($images);
////        dd($html->find('img'));
//
//        return view('index.showProductPost', compact('productPost'));
//    }

    public function addToBasket($id)
    {
        $arr = Session::get('itemId');
        $arr[] = $id;
        Session::put('itemId', $arr);
        if (request()->ajax()) {
            if (Session::has('itemId')) {
                $id = session()->get('itemId');
                $whereData = ['myselect' => $id];
                $query = '';
                foreach ($whereData['myselect'] as $select) {
                    $query .= " id = $select or";
                }
                $query = 'select * from products where' . $query . ' id = 0';
                $products = DB::select(DB::raw($query));
            } else {
                $products = [];
            }

            return response(count($products), 200);
        }

        return redirect()->back();
    }

    public function removeFromBasket($id)
    {
        $arr = Session::get('itemId');
        $arr = array_unique($arr);
        if (count($arr) > 1) {
            if ($key = array_search($id, $arr) !== false) {
                $key = array_search($id, $arr);
                unset($arr[$key]);
                Session::forget('itemId');
                Session::put('itemId', $arr);
            }
        } else {
            Session::forget('itemId');
        }

        return redirect()->back();
    }

    //register
    public function Reg()
    {
        if (Auth::check()) {
            Flash::success('شما قبلا وارد سایت شدید.');

            return redirect(route('index.index'));
        }

        return view('index.register');
    }

    public function signUpNoPrice(Request $request, $id)
    {
        if ($request->isMethod('PUT')) {
            if (Auth::check()) {
                $product = Product::find($id);
                if (Gate::denies('is-product-student', $product)) {
                    if (($product->price - $product->discount) <= 0) {
                        $student = User::find(auth()->user()->id);
                        $student->lessons()->attach([$product->id]);
                        Flash::success('شما دانشجوی این درس هستید.');
                    }
                }else{
                    Flash::error('شما از قبل دانشجوی این درس بوده اید و نیازی به ثبت نام مجدد نیست.');
                }
            }else{
                Flash::error('برای ثبت نام در این درس، ابتدا باید در سایت ثبت نام کنید و یا وارد شوید.');
            }

            return redirect()->back();
        }
    }

    //afzudane shomare mobile bazdid konande dar footer
    public function giveMobileNumber(Request $request)
    {
        if ((strlen($request->mobile) == 10) || (strlen($request->mobile) == 11)) {
            if (is_numeric($request->mobile)) {
                $ip = Mobile::where('ip', $request->ip())->first();

                if ($ip) {
                    Flash::error('شما قبلا یک شماره ثبت کرده اید. هر کس فقط یک شماره میتواند ثبت کند.');
                } else {
                    $mobile = Mobile::where('mobile', $request->mobile)->first();
                    if ($mobile) {
                        Flash::sucess('ما شماره شما را ذخیره کرده بودیم، نیازی به وارد کردن مجدد شماره نیست.');
                    } else {
                        if (auth()->check()) {
                            User::where('id', auth()->user()->id)->update(['tel' => $request->mobile]);
                        }
                        Mobile::create(['mobile' => $request->mobile, 'ip' => $request->ip()]);
                        Flash::success('شماره موبایل شما در خبرنامه شد.');
                    }
                }
            } else {
                Flash::error('شماره موبایل را فقط به صورت عدد وارد نمایید');
            }
        } else {
            Flash::error('شماره موبایل باید 10 یا 11 رقم باشد.');
        }

        return redirect(route('index.index'));
    }

    public function editUserAddress(Request $request)
    {
        $user = User::find(auth()->user()->id);

        $inputs['name'] = $request->name;
        $inputs['tel'] = $request->tel;
        $inputs['telephone'] = $request->telephone;
        $inputs['state'] = $request->state;
        $inputs['city'] = $request->city;
        $inputs['postal_code'] = $request->postal_code;
        $inputs['address'] = $request->address;

        $user->update($inputs);

        return redirect()->back();
    }

    public function shoppingCart()
    {
        $id = session()->get('itemId');
        $products = selectAllProducts($id);

        ///////////////////////////// if has no products redirect to last page


        if ($products == "") {
            Flash::error('هنوز هیچ محصولی انتخاب نکرده اید. ابتدا یک یا چند محصول انتخاب کنید.');

            return redirect(route('index.index'));
        }

        /////////////////////////////

        $products = calculateDiscountOfProducts($products);

        ////////////////////// mohasebeye sum va discount

        $sum = 0;
        $discount = 0;
        $dis = 0;
        foreach ($products as $product) {
            $sum += $product->price;
            $discount += $product->discount;
        }

        if (auth()->check()) {
            //////////////////////////////// mohasebeye code takhfif marbut be user agar sukht nashode bashe

            $discount = calculateDiscountIfNotBurned($discount, $sum);

            ////////////////////////////////takhfif baraye kharide bala tar az takhfifline

            $dis = discountLine($discount, $sum);

            /////////////////////////////////////////

            $user = User::find(auth()->user()->id);

//        $factor = Factor::orderBy('id', 'desc')->first();

//        $products = $factor->products()->get();

//        $user = User::find(auth()->user()->id);

            $discounts = $user->discounts()->where('active', 1)->where('type', 1)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->get();//discount of products
            $discounts2 = $user->discounts()->where('active', 1)->where('type', 2)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->get();//discount of user
            $discounts2 = deleteUsersDiscountFromDiscounts($discounts2);
        } else {
            $discounts = [];
            $discounts2 = [];
        }

        return view('index.shoppingCart', compact('products', 'discounts', 'discounts2', 'sum', 'dis', 'user'));
    }

    public function checkOut(Request $request)
    {
        $is_by_post = 0;
        $post_amount = 0;
        if($request->is_by_post == 'yes'){
            $is_by_post = 1;
            $post_amount = Setting::where('name', 'send_by_post_amount')->pluck('value')->first();
        }

        if (!Auth::check()) {
            return view('index.index');
        }

        ///////////////////////select all products in session

        $id = session()->get('itemId');
        $products = selectAllProducts($id);

        //////////////////// mohasebeye takhfif haye mahsulat

        $products = calculateDiscountOfProducts($products);

        ////////////////////// mohasebeye sum va discount

        $sum = 0;
        $discount = 0;
        foreach ($products as $product) {
            $sum += $product->price;
            $discount += $product->discount;
        }

        //////////////////////////////// mohasebeye code takhfif marbut be user agar sukht nashode bashe

        $discount = calculateDiscountIfNotBurned($discount, $sum);

        ////////////////////////////////takhfif baraye kharide bala tar az takhfifline

        $discount = discountLine($discount, $sum);

        //////////////////////////////////////////mhoasebe code haye takhfif

        $user = User::find(auth()->user()->id);

        $discounts = $user->discounts()->where('active', 1)->where('type', 1)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->get();//discount of products
        $discounts2 = $user->discounts()->where('active', 1)->where('type', 2)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->get();//discount of user
        $discounts2 = deleteUsersDiscountFromDiscounts($discounts2);

        ////////////////////////////////serialize allDetails of factor

        $detail = [];
        $allDetails = [];
        foreach ($products as $item) {
            $detail['product_id'] = $item->id;
            $detail['product_price'] = $item->price;
            $productTemp = Product::find($item->id);
            $detail['product_self_discount'] = $productTemp->discount;
            $detail['product_code_and_line_discount'] = $item->discount - $detail['product_self_discount'];
            $detail['product_pay_price'] = $item->price - $item->discount;
            $detail['product_all_discount'] = $item->discount;
            $commissionPercent = $productTemp->user->commission;
            $detail['teacher_id'] = $productTemp->user->id;
            $commissionMoney = ($detail['product_pay_price']) * ($commissionPercent) / (100);
            $detail['teacher_commission_money'] = $commissionMoney;
            $detail['array_discount_cods_products'] = serialize($discounts->pluck('code', 'id'));//in every products repeat
            $detail['array_discount_cods_users'] = serialize($discounts2->pluck('code', 'id'));//in every products repeat

            $allDetails[] = $detail;
        }

        $allDetails = serialize($allDetails);

        ////////////////////////////////

        $sum = $sum + $post_amount;

        ////////////////////////////////

        Factor::create([
            'user_id' => auth()->user()->id,
            'sum' => $sum ,
            'discount_sum' => $discount,
            'amount' => $sum - $discount,
            'is_paid' => 0,
            'detail' => $allDetails,
            'is_by_post' => $is_by_post,
            'post_amount' => $post_amount,
        ]);

        ////////////////////////////////////

        $factor = DB::table('factors')->orderBy('id', 'desc')->first();

        foreach ($products as $product) {
            DB::table('factor_product')->insert(['factor_id' => $factor->id, 'product_id' => $product->id]);
        }

        ////////////////////////////////

        //Session::forget('itemId');//used in after peyment

        ////////////////////////////////

        $factor = Factor::orderBy('id', 'desc')->first();

        $products = $factor->products()->get();


        return view('index.checkOut', compact('factor', 'products', 'discounts', 'discounts2', 'is_by_post', 'post_amount'));
    }

    public function indexPost(Post $post)
    {
//        dd($post);
        return view('index.showAPost', compact('post'));
    }

    public function discountPost(Request $request)
    {
        $code = $request->code;
        $discount = Discount::where('code', $code)->where('active', 1)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->first();

        if (is_null($discount)) {
            Flash::error('این کد غیر فعال شده یا وجود ندارد.');

            return redirect()->back();
        }

        $user = $discount->users()->get();

        if ($discount->type == 1) {
            if (count($user)) {
                Flash::success('این کد برای شما ثبت شده و روی حساب شما فعال است.');

                return redirect()->back();
            } else {
                $user = User::find(auth()->user()->id);
                $user->discounts()->attach($discount->id);
                Flash::success('کد با موفقیت وارد شد. تخفیف ها برای شما محاسبه میگرددند.');

                return redirect()->back();
            }
        } else {//aya code type 2 ghalan estefade shode?
            $usertemp = User::find(auth()->user()->id);
            $burneds = $usertemp->discountsBurned()->get();

            foreach ($burneds as $burned) {
                if ($burned->id == $discount->id) {
                    Flash::error('شما از این کد قبلا استفاده کرده اید.');

                    return redirect()->back();
                }
            }
            if (count($user)) {
                Flash::success('این کد برای شما ثبت شده و روی حساب شما فعال است.');

                return redirect()->back();
            } else {
                $user = User::find(auth()->user()->id);
                $user->discounts()->attach($discount->id);
                Flash::success('کد با موفقیت وارد شد. تخفیف ها برای شما محاسبه میگرددند.');

                return redirect()->back();
            }
        }
    }

}
