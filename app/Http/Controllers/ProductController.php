<?php

namespace App\Http\Controllers;

use App\DataTables\ProductDataTable;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\Tag;
use App\Repositories\ProductRepository;
use App\User;
use Flash;
use Response;
use Image;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param ProductDataTable $productDataTable
     * @return Response
     */
    public function index(ProductDataTable $productDataTable)
    {
        $this->authorize('is-employee');

        return $productDataTable->render('products.index');
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-employee');

//        $tags = Tag::pluck('name', 'id');

        $categories = Category::pluck('name_fa', 'id');
        $productTypes = ProductType::get();
        $users = User::pluck('email', 'id');

        return view('products.create', compact('categories', 'productTypes', 'users'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $this->authorize('is-employee');

        $inputs = $request->all();

        //عکس دوره
        if (isset($request->image)) {
            $inputs['image_main'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/product/image/';
            $request->image->move($path, $inputs['image_main']);
            Image::make($path . $inputs['image_main'])
                ->resize('300', '200')
                ->save(public_path() . '/uploads/product/image/thumbnail/' . $inputs['image_main']);
            $inputs['thumbnail'] = $inputs['image_main'];
        }

        //تاریخ شروع دوره
        $inputs['start_at'] = savePDateToDatabase($request->start_at);

        //تاریخ پایان دوره
        $inputs['end_at'] = savePDateToDatabase($request->end_at);


        if (!(isset($request->discount))){
            $inputs['discount'] = 0;
        }
        $inputs['real_price'] = $inputs['price'] - $inputs['discount'];
        unset($inputs['image']);

        Product::create($inputs);

        Flash::success('محصول آموزشی با موفقیت ایجاد شد.');

        return redirect(route('products.index'));

//        $input['title'] = $request->title;
//        $input['description'] = $request->description;
//        $input['description_full'] = $request->description_full;
//        $input['specification'] = $request->specification;
//        $input['download_link'] = $request->download_link;
//        $input['count'] = $request->count;
//        $input['price'] = $request->price;
//        $input['discount'] = $request->discount;
//        $input['sold_count'] = $request->sold_count;
//        $input['teacher_name'] = $request->teacher_name;
//        $input['duration'] = $request->duration;
//        $input['vote'] = $request->vote;
//        $input['total_voters'] = $request->total_voters;
//        $input['image_main'] = $request->image_main;
//        $input['image_two'] = $request->image_two;
//        $input['image_three'] = $request->image_three;
//        $input['image_four'] = $request->image_four;
//        $input['image_five'] = $request->image_five;
//        $input['user_id'] = $request->user_id;
//        $input['category_id'] = $request->category_id;

//
//
//
//        if (isset($request->is_active))
//            $input['is_active'] = $request->is_active;
//
//        $product = $this->productRepository->create($input);

//        $product->tags()->sync($request->tag_id);


//        Flash::success('دوره آموزشی با موفقیت ذخیره گردید.');

//        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-employee');

        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('دوره آموزشی یافت نشد.');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-employee');

        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('دوره آموزشی یافت نشد.');

            return redirect(route('products.index'));
        }

//        $tags = Tag::pluck('name', 'id');

        $categories = Category::pluck('name_fa', 'id');
        $productTypes = ProductType::get();
        $users = User::pluck('email', 'id');

        return view('products.edit', compact('product', 'productTypes', 'categories', 'users'));
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $this->authorize('is-employee');

        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('دوره آموزشی یافت نشد.');

            return redirect(route('products.index'));
        }

        ////////////////////////////
        ///
        $inputs = $request->all();

        //عکس دوره
        if (isset($request->image)) {
            $inputs['image_main'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/product/image/';
            $request->image->move($path, $inputs['image_main']);
            Image::make($path . $inputs['image_main'])
                ->resize('300', '200')
                ->save(public_path() . '/uploads/product/image/thumbnail/' . $inputs['image_main']);
            $inputs['thumbnail'] = $inputs['image_main'];
        }

        //تاریخ شروع دوره
        $inputs['start_at'] = savePDateToDatabase($request->start_at);

        //تاریخ پایان دوره
        $inputs['end_at'] = savePDateToDatabase($request->end_at);


        if (!(isset($request->discount))){
            $inputs['discount'] = 0;
        }
        $inputs['real_price'] = $inputs['price'] - $inputs['discount'];
        unset($inputs['image']);

        $product->update($inputs);
        Flash::success('تنظیمات با موفقیت ذخیره گردید');

        return redirect(route('products.index'));


        //////
//        $input['title'] = $request->title;
//        $input['description_mini'] = $request->description_mini;
//        $input['description'] = $request->description;
//        $input['description_full'] = $request->description_full;
//        $input['specification'] = $request->specification;
//        $input['download_link'] = $request->download_link;
//        $input['count'] = $request->count;
//        $input['price'] = $request->price;
//        $input['discount'] = $request->discount;
//        $input['sold_count'] = $request->sold_count;
//        $input['teacher_name'] = $request->teacher_name;
//        $input['duration'] = $request->duration;
//        $input['vote'] = $request->vote;
//        $input['total_voters'] = $request->total_voters;
//        $input['image_main'] = $request->image_main;
//        $input['image_two'] = $request->image_two;
//        $input['image_three'] = $request->image_three;
//        $input['image_four'] = $request->image_four;
//        $input['image_five'] = $request->image_five;

        $input['real_price'] = $input['price'] - $input['discount'];

        if (isset($request->is_active))
            $input['is_active'] = $request->is_active;

//        $product = $this->productRepository->update($request->all(), $id);
        Product::where('id', $id)->update($input);


        $product = Product::find($id);

        $product->tags()->sync($request->tag_id);

        $product->categories()->sync($request->category_id);

        Flash::success('دوره آموزشی با موفقیت بروز رسانی شد.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-employee');

        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('دوره آموزشی یافت نشد.');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('دوره آموزشی با موفقیت پاک شد.');

        return redirect(route('products.index'));
    }

    public function teacherEditProducts(Product $product)
    {
        if ($product->user_id == auth()->user()->id) {//user is product owner
            $productTypes = ProductType::get();

            return view('profile.teacherEditProducts', compact('product', 'productTypes'));
        }
    }

    public function teacherUpdateProducts(Product $product, UpdateProductRequest $request)
    {
        if ($product->user_id == auth()->user()->id) {//user is product owner

            $inputs['product_type_id'] = $request->product_type_id;
            $inputs['status'] = $request->status;

            //عکس دوره
            if (isset($request->image)) {
                $this->validate($request, [
                    'image' => 'mimes:jpg,jpeg,png,gif|max:2048',
                ]);
                $inputs['image_main'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
                $path = public_path() . '/uploads/product/image/';
                $request->image->move($path, $inputs['image_main']);
                Image::make($path . $inputs['image_main'])
                    ->resize('300', '200')
                    ->save(public_path() . '/uploads/product/image/thumbnail/' . $inputs['image_main']);
                $inputs['thumbnail'] = $inputs['image_main'];
            }

            //توضیح مختصر
            $inputs['description_mini'] = $request->description_mini;

            //توضیحات کامل دوره
            $inputs['description'] = $request->description;

            //قیمت دوره
            $inputs['price'] = $request->price;

            $inputs['real_price'] = $inputs['price'];

            //تاریخ شروع دوره
            $inputs['start_at'] = savePDateToDatabase($request->start_at);

            //تاریخ پایان دوره
            $inputs['end_at'] = savePDateToDatabase($request->end_at);

            $product->update($inputs);
            Flash::success('تنظیمات با موفقیت ذخیره گردید');

            return redirect()->back();
        }
    }
}
