<?php

namespace App\Http\Middleware;

use Closure;

class SetConfigElfinder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = auth()->user()->email;
        $path = __DIR__;
        $path = substr($path, 0, strlen($path) - 19)
            . 'storage'
            . DIRECTORY_SEPARATOR . 'app'
            . DIRECTORY_SEPARATOR . 'public'
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . "$email";

        \Config::set(['elfinder.roots.0.path' => "$path"]);
//        \Config::set(['elfinder.roots.0.path'=> 'C:\laragon\www\amuzeshmasoudkhan\storage/app/public/files/sonya@sonya.com']);//it works

//        checkShortcutFolder();
//        $path2 = route('index.index') . '/files/' . auth()->user()->symlink . '/' . $email;

        //teachers can see preview posts and edit its
        $path2 = route('index.index') . '/storage/files/' . $email;
        \Config::set(['elfinder.root_options.URL' => "$path2"]);

        return $next($request);
    }
}
