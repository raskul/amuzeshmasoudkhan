<?php

namespace App;

use App\Models\Factor;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class LessonStudentDetail extends Model
{
    public $table = 'lesson_student_detail';

    protected $fillable = [
        'student_id',
        'lesson_id',
        'factor_id',
        'teacher_id',
        'lesson_payment',
        'teacher_interest',
    ];

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id');
    }

    public function lesson()
    {
        return $this->belongsTo(Product::class, 'lesson_id');
    }

    public function factor()
    {
        return $this->belongsTo(Factor::class, 'factor_id');
    }

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }
}
