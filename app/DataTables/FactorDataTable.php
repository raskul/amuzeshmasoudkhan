<?php

namespace App\DataTables;

use App\Models\Factor;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class FactorDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'factors.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Factor $model)
    {
        $factor = Factor::where('is_paid', 1)->with('user');

        return $this->applyScopes($factor);
//        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'title' => 'مشاهده'])
            ->parameters([
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Persian.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name' => 'factors.id', 'data' => 'id', 'title' => 'شماره فاکتور'],
            ['name' => 'user.email', 'data' => 'email', 'title' => 'ایمیل کاربر'],
            ['name' => 'sum', 'data' => 'sum', 'title' => 'مجموع'],
            ['name' => 'discount_sum', 'data' => 'discount_sum', 'title' => 'مجوع تخفیفات'],
            ['name' => 'created_at', 'data' => 'created_at', 'title' => 'مجوع تخفیفات'],
//            ['name' => 'is_paid', 'data' => 'is_paid', 'title' => 'وضعیت پرداخت'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'factorsdatatable_' . time();
    }
}