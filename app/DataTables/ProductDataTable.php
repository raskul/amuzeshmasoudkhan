<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ProductDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'products.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        $products = Product::with('user')->with('productType');

        return $this->applyScopes($products);
//        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'title' => 'عملیات'])
            ->parameters([
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Persian.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name' => 'title', 'title' => 'عنوان', 'data' => 'title'],
            ['name' => 'user.email', 'title' => 'ایمیل استاد', 'data' => 'user.email'],
            ['name' => 'price', 'title' => 'قیمت', 'data' => 'price'],
            ['name' => 'discount', 'title' => 'تخفیف تکی', 'data' => 'discount'],
            ['name' => 'sold_count', 'title' => 'تعداد فروش', 'data' => 'sold_count'],
            ['name' => 'vote', 'title' => 'امتیاز', 'data' => 'vote'],
            ['name' => 'user_earn_score', 'title' => 'امتیاز دریافتی کاربران', 'data' => 'user_earn_score', 'width' => '20px'],
            ['name' => 'can_get_license', 'title' => 'دریافت گواهینامه', 'data' => 'can_get_license', 'width' => '20px'],
            ['name' => 'product_type_fa.name', 'title' => 'نوع', 'data' => 'product_type.name_fa', 'width' => '20px'],
            ['name' => 'start_at', 'title' => 'تاریخ شروع دوره', 'data' => 'start_at', 'width' => '60px'],
            ['name' => 'end_at', 'title' => 'تاریخ پایان دوره', 'data' => 'end_at', 'width' => '60px'],
            ['name' => 'created_at', 'title' => 'تاریخ ساخته شدن', 'data' => 'created_at', 'width' => '60px'],
            ['name' => 'updated_at', 'title' => 'تاریخ بروزرسانی', 'data' => 'updated_at', 'width' => '60px'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'productsdatatable_' . time();
    }
}