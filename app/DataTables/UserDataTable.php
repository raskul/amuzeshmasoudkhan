<?php

namespace App\DataTables;

use App\Models\Role;
use App\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'users.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
//        $boats = User::selectRaw('DISTINCT users.id, users.name, users.email, users.role_id, users.balance, users.created_at, users.updated_at');
//        return $this->applyScopes($boats);

        $user= User::with('role');
        return $this->applyScopes($user);
//        return $model->newQuery($user);

//        $posts = User::join('roles', 'user.role_id', '=', 'roles.id')
//            ->select(['users.id', 'users.email', 'roles.name', 'users.created_at', 'users.updated_at']);
//
//        return DataTable::of($posts)
//            ->editColumn('name', function ($model) {
//                return \HTML::mailto($model->email, $model->name);
//            })
//            ->make(true);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
//            ->column
//            ->addColumn(['name', '{{ hello .\' \'. world }}'])
//            ->addColumn([['name'=>'roles', 'title' => 'hi', 'data'=>'hl'] , function(){
//                return 'hello';
//            }])
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'title' => 'عملیات'])
            ->parameters([
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Persian.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name' => 'name', 'title' => 'نام', 'data' => 'name'],
            ['name' => 'users.email', 'title' => 'ایمیل', 'data' => 'email'],
            ['name' => 'role.name', 'title' => 'سمت', 'data' => 'role.name'],
            ['name' => 'balance', 'title' => 'موجودی', 'data' => 'balance'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }

//    public function getJoins()
//    {
//        return view('datatables.eloquent.joins');
//    }
//
//    public function getJoinsData()
//    {
//        $posts = User::join('users', '.user_id', '=', 'users.id')
//            ->select(['posts.id', 'posts.title', 'users.name', 'users.email', 'posts.created_at', 'posts.updated_at']);
//
//        return DataTable::of($posts)
//            ->editColumn('title', '{!! str_limit($title, 60) !!}')
//            ->editColumn('name', function ($model) {
//                return \HTML::mailto($model->email, $model->name);
//            })
//            ->make(true);
//    }

//    public function ajax()
//    {
//
//    }
}