<?php

namespace App\DataTables;

use App\Models\ProductNote;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ProductNoteDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'product_notes.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductNote $model)
    {
        $notes = ProductNote::with('user')->with('product');

        return $this->applyScopes($notes);
//        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'title' => 'عملیات'])
            ->parameters([
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Persian.json'],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['name' => 'title', 'title' => 'عنوان اعلامیه' , 'data' => 'title'],
            ['name' => 'text', 'title' => 'متن اعلامیه' , 'data' => 'text'],
            ['name' => 'product.title', 'title' => 'عنوان دوره' , 'data' => 'product.title'],
            ['name' => 'user.email', 'title' => 'ایمیل مدرس' , 'data' => 'user.email'],
            ['name' => 'created_at', 'title' => 'تاریخ ساخت اعلامیه' , 'data' => 'created_at'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'product_notesdatatable_' . time();
    }
}