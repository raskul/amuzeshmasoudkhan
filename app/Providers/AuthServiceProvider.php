<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is-user', function ($user) {
            if (($user->role_id == 1) || ($user->role_id == 2) || ($user->role_id == 6)) {
                return true;
            }
        });

        Gate::define('is-teacher', function ($user) {
            if ($user->role_id == 2 || ($user->role_id == 6)) {
                return true;
            }
        });

        Gate::define('is-employee', function ($user) {
            if (($user->role_id == 3) || ($user->role_id == 4) || ($user->role_id == 5) || ($user->role_id == 6)) {
                return true;
            }
        });

        Gate::define('is-salesmanager', function ($user) {
            if (($user->role_id == 4) || ($user->role_id == 5) || ($user->role_id == 6)) {
                return true;
            }
        });

        Gate::define('is-admin', function ($user) {
            if (($user->role_id == 5) || ($user->role_id == 6)) {
                return true;
            }
        });

        Gate::define('is-programmer', function ($user) {
//            if ($user->role_id == 6) {
                return true;
//            }
        });

        Gate::define('is-product-student', function ($user, $product) {
            $productTemps = $user->lessons()->get();
            foreach ($productTemps as $productTemp){
                if ($productTemp->id == $product->id){
                    return true;
                }
            }
        });

        Gate::define('is-post-owner', function ($user) {
//            ???
        });

        Gate::define('is-comment-owner', function ($user, $faq){//for comments and faqs
            if ($faq->user_id == $user->id ){
                return true;
            }
        });



    }
}
