<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceHistrory extends Model
{
    public $table = 'balance_history';

    protected $fillable = [
        'user_id',
        'old_balance',
        'new_balance',
        'type',
        'description',
    ];

    public function getTypeStrAttribute()
    {
        switch ($this->type){
            case 1:
                return 'واریز';
            case 2:
                return 'برداشت';
            case 3:
                return 'در انتظار تایید';
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
