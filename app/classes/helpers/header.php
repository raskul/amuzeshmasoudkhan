<?PHP
namespace App\classes\helpers;

use App\Models\Category;
use App\Models\Setting;

class Header{
    function getUserBalance(){
        $balance = 0;
        if (\Auth::check()){
            $balance = auth()->user()->balance;
        }
        return $balance;
    }

    function getCategories(){
        return Category::where('id', 1)->first();
    }

    function getSettingsForHeader(){
        return Setting::where('name', 'header_menu')
            ->orWhere( 'name', 'header_site_logo')
            ->orWhere( 'name', 'tel')
            ->orWhere( 'name', 'mail')
            ->orWhere( 'name', 'telegram')
            ->orWhere( 'name', 'facebook')
            ->orWhere( 'name', 'instagram')
            ->orWhere( 'name', 'twitter')
            ->orWhere( 'name', 'telegram_tel')
            ->orWhere( 'name', 'header_site_logo')
            ->pluck('value', 'name');
    }
}