<?PHP

use App\Models\Setting;
use App\User;
use Creativeorange\Gravatar\Facades\Gravatar;
use Falc\Flysystem\Plugin\Symlink\Local as LocalSymlinkPlugin;
use League\Flysystem\Adapter\Local as LocalAdapter;
use League\Flysystem\Filesystem;
use Morilog\Jalali\Facades\jDateTime;
use Sunra\PhpSimple\HtmlDomParser;

//namayeshe tarikh
function pDate($gDate, $withClock = false, $ago = false, $toPersianNumber = true, $withPersianMonth = false)
{
    if ($withClock)
        $jdate = \Morilog\Jalali\jDateTime::strftime('Y-m-d ساعت H:i:s', $gDate);
    else
        $jdate = \Morilog\Jalali\jDateTime::strftime('Y-m-d', $gDate);

    if ($ago) {
        $jdate = \Morilog\Jalali\jDate::forge($gDate)->ago();

        return $jdate;
    }

    if ($withPersianMonth)
        $jdate = \Morilog\Jalali\jDateTime::strftime('d %B Y', $gDate);

    if ($toPersianNumber) {
        $arr1 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        $arr2 = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

        $jdate = str_replace($arr1, $arr2, $jdate);
        $jdate = str_replace('-', '/', $jdate);
    }

    return $jdate;
}

//gereftane tarikh az pdatepicker va tabdil be miladi va amade sazi baraye zakhire dar data base
function savePDateToDatabase($date){
    $date = normalCalendarFormat($date);
    $date = jDateTime::createDatetimeFromFormat('j m Y g:i', $date)->getTimestamp();
    $date = date('Y-m-d H:i:s', $date);
    return $date;
}

//gereftane tarikh az pdatepicker va tabdil be formate ghabele fahm ke ba tabe zir ghable tabdil be timestamp hast
function normalCalendarFormat($date){
//    $inputs["start_at"] = normalCalendarFormat($inputs["start_at"]);
//    $inputs['start_at'] = jDateTime::createDatetimeFromFormat('j m Y g:i', $inputs["start_at"])->getTimestamp();
//    $inputs['start_at'] = date('Y-m-d H:i:s', $inputs['start_at']);

    $arr1 = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arr2 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    $date=str_replace($arr1, $arr2, $date);

    $arr1 = ['یکشنبه', 'جمعه', 'پنج شنبه', 'چهار شنبه', 'سه شنبه', 'دوشنبه', 'شنبه', 'پنج'];
    $arr2 = ['', '', '', '', '', '', '', ''];
    $date = str_replace($arr1, $arr2, $date);

    if (strpos($date, 'ب ظ')){
        $arr1 = [' 0:' , ' 1:' , ' 2:' , ' 3:' , ' 4:' , ' 5:' , ' 6:' , ' 7:' , ' 8:' , ' 9:' , ' 10:' , ' 11:'];
        $arr2 = [' 12:', ' 13:', ' 14:', ' 15:', ' 16:', ' 17:', ' 18:', ' 19:', ' 20:', ' 21:', ' 22:', ' 23:'];
        $date = str_replace($arr1, $arr2, $date);
    }

    $arr1 = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
    $arr2 = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    $date = str_replace($arr1, $arr2, $date);

    $date = trim($date);

    $arr1 = ['ق ظ', 'ب ظ'];
    $arr2 = ['', ''];
    $date = str_replace($arr1, $arr2, $date);

    $arr1 = ['  '];
    $arr2 = [' '];
    $date = str_replace($arr1, $arr2, $date);

    $date = ' ' . trim($date);
    if ((strpos($date, '‌ ')))
        $date = substr($date, 4);

    $date = trim($date);

    return $date;
}

/**
 * @param $id of_Sessions
 * @return array|string Products
 */
function selectAllProducts($id){
    $products = '';
    if (isset($id)) {
        $whereData = ['myselect' => $id];
        $query = '';
        foreach ($whereData['myselect'] as $select) {
            $query .= " id = $select or";
        }
        $query = 'select * from products where' . $query . ' id = 0';
        $products = DB::select(DB::raw($query));
    }
    return $products;
}

function storeTicketsFiles($request , $pathPart = 'tickets'){
    //$fullName = storeTicketsFiles($request);
    $filename = time() . $request->file('file')->getClientOriginalName();
    $path = "public/$pathPart/" . auth()->user()->email . '/';
    $request->file('file')->storeAs($path, $filename);
    $path = str_replace('public', 'storage', $path);
    $fullName = $path . $filename;

    return $fullName;
}

//check if user wants to visit productsPosts (lessons) ; put in when user wants see product posts
function checkShortcutFolder (){
    $user = auth()->user();
    $time = Setting::where('name', 'symlink_time')->pluck('value')->first();
    $updated_at = strtotime($user->updated_at);
    $now = now()->timestamp;
    $rand = rand(1000000000, 9000000000) . time();

    if (isset($user->symlink)) {
        if (($now - $updated_at) > $time) {
            $result = shortcutFolder($user->symlink, false, true);
            if ($result) {
                $result = shortcutFolder($user->symlink, false, false, true);
            }

            shortcutFolder("$rand");
            User::where('id', $user->id)->update(['symlink' => $rand]);
        } else {
            //continue
        }

    } else {
        shortcutFolder("$rand");
        User::where('id', $user->id)->update(['symlink' => $rand]);
    }
}

//create check delete shortcut of folders that used for users to download
function shortcutFolder($pathForSymlink, $symlink = true, $issymlink = false, $deletesymlink = false)
{
    $path = __DIR__;
    $path = substr($path, 0, strlen($path) - 19);

    $filesystem = new Filesystem(new LocalAdapter($path));
    $filesystem->addPlugin(new LocalSymlinkPlugin\Symlink());
    $filesystem->addPlugin(new LocalSymlinkPlugin\IsSymlink());
    $filesystem->addPlugin(new LocalSymlinkPlugin\DeleteSymlink());

    if ($symlink) {
        $success = $filesystem->symlink(
            'storage' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files',
            'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . "$pathForSymlink"
        );
        return $success;
    }
    if ($issymlink){
        $success = $filesystem->isSymlink(
            'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . "$pathForSymlink"
        );
        return $success;
    }
    if ($deletesymlink){
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            //windows work:
            $success = rmdir($path . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . "$pathForSymlink");
        }
        else {
            //linux work:
            $success = $filesystem->deleteSymlink('public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . "$pathForSymlink");
        }
        return $success;
    }

}

function replaceSymlinkLinksInPost($post)
{
    $user = User::find(auth()->user()->id);
    $userSymlink = $user->symlink;

    if (count(HtmlDomParser::str_get_html($post)->find('img'))) {
        $temp = HtmlDomParser::str_get_html($post)->find('img');
        foreach ($temp as $image) {
            $src = $image->src;
            $okImage = str_replace('/storage/files/', "/files/$userSymlink/", $src);
            $post = str_replace($src, $okImage, $post);
        }
    }

    if (count(HtmlDomParser::str_get_html($post)->find('video'))) {
        if (count(HtmlDomParser::str_get_html($post)->find('video', 0)->find('source'))) {
            $temp = HtmlDomParser::str_get_html($post)->find('video', 0)->find('source');
            foreach ($temp as $video) {
                $src = $video->src;
                $okVideo = str_replace('/storage/files/', "/files/$userSymlink/", $src);
                $post = str_replace($src, $okVideo, $post);
            }
        }
    }

    return $post;
}

function calculateProductDuration($product){//return duration integer
    $duration = 0 ;
    foreach($product->productPosts as $post){
        $duration += $post->duration;
    }

    return $duration;
}

function createProductLinkCreate($product){
    return $product->id . '-' . str_replace( ' ', '-',$product->title) . '/1' ;
}

function toPersianNumber($string)
{
    $arr1 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    $arr2 = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

    return str_replace($arr1, $arr2, $string);
}

function showUserImage($user_id)
{
    $user = User::find($user_id);
    if (isset($user->image)) {
        return route('index.index') . '/uploads/profile/image/thumbnail/' . $user->image;
    } else {
        return Gravatar::get($user->email);
    }
}

function showProductImage($product)
{
    return route('index.index') . '/uploads/product/image/thumbnail/' . $product->image_main;
}