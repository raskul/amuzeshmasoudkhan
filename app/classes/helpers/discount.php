<?php

use App\Models\Product;
use App\Models\Setting;
use App\User;

/**
 * Created by PhpStorm.
 * User: sadegh
 * Date: 2018-02-11
 * Time: 2:10 AM
 */
//takhfif mogheye residane gheymat be yek gheymate khas
/**
 * @param $discount of_all_products
 * @param $sum of_all_products_without_discount
 * @return float|int|mixed discount
 */
function discountLine($discount, $sum)
{
    $settings = Setting::get();
    $takhfif_line = $settings->where('name', 'takhfif_line')->pluck('value')->first();
    $takhfif_value = $settings->where('name', 'takhfif_value')->pluck('value')->first();
    $takhfif_type = $settings->where('name', 'takhfif_type')->pluck('value')->first();//0-toman , 1-percent

    if (($sum - $discount) > $takhfif_line) {
        if ($takhfif_type == 0) {
            $discount = $discount + $takhfif_value;
        }
        if ($takhfif_type == 1) {
            $discount = $discount + (($sum - $discount) * $takhfif_value / 100);
        }
    }

    return $discount;
}


/**
 * @param $discount of_all_products
 * @param $sum of_all_products_without_discount
 * @return float|int|mixed discount
 */
function calculateDiscountIfNotBurned($discount, $sum)
{
    //mohasebeye code takhfif marbut be user agar sukht nashode bashe
    $user = User::find(auth()->user()->id);
    $uDiscountsTypeTwo = $user->discounts()->where('active', 1)->where('type', 2)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->get();

    $burneds = $user->discountsBurned()->get();
    if (isset($uDiscountsTypeTwo)) {
        foreach ($uDiscountsTypeTwo as $uDTT) {
            foreach ($burneds as $burned) {
                if ($burned->id == $uDTT->id) {
                    goto abort;//khat haye paeen ro enjra nakon
                }
            }
            if ((($sum) - ($uDTT->price)) >= 0) {
                $discount = $discount + $uDTT->price;
            } else {
                $discount = $sum;
            }
            abort:
        }
    }

    return $discount;
}
///////////////////////ezaf kardane meghdare takhfifhaye code takhfif be mahsulat
function calculateDiscountOfProducts($products){
    if (Auth::check()) {
        $user = User::find(auth()->user()->id);
        $uDiscounts = $user->discounts()->where('active', 1)->where('type', 1)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->get();//takhfif haye in user
        //takhfifat in mahsutal
        foreach ($products as $product) {
            $p = Product::find($product->id);
            $pDiscounts = $p->discounts()->where('active', 1)->where('type', 1)->where('start_at', '<=', date('Y-m-d H:i:s', time()))->where('end_at', '>=', date('Y-m-d H:i:s', time()))->get();
            foreach ($pDiscounts as $pDiscount) {//takhfif in mahsul
                foreach ($uDiscounts as $uDiscount) {// takhfif in user
                    if ($pDiscount->id == $uDiscount->id) {
                        if ($pDiscount->is_percent == 0) {
                            $product->discount = $product->discount + $uDiscount->price;
                        } else {
                            $product->discount = ($product->discount) + (($product->price) * ($uDiscount->price) / 100);
                        }
                    }
                }
            }
        }
    }

    return $products;
}

//////////////////////hazfe takhfif haye sukhte shode az takhfif haye user
function deleteUsersDiscountFromDiscounts($discounts2){
    $user = User::find(auth()->user()->id);
    $discounts2burned = $user->discountsBurned()->get();//hazfe takhfif haye sukhte shodeye user
    foreach ($discounts2 as $d2) {
        foreach ($discounts2burned as $d2b) {
            if ($d2->id == $d2b->id) {
                $d2 = null;
            }
        }
    }

    return $discounts2;
}