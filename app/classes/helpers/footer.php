<?php
/**
 * Created by PhpStorm.
 * User: sadeghsadegh21@gmail.com
 * Date: 2018-03-06
 * Time: 10:28 PM
 */

namespace App\classes\helpers;

use App\Models\Setting;

class Footer
{
    function getSettingsForFooter()
    {
        return Setting::where('name', 'header_menu')
            ->orWhere('name', 'header_site_logo')
            ->orWhere('name', 'tel')
            ->orWhere('name', 'mail')
            ->orWhere('name', 'telegram_tel')
            ->orWhere('name', 'telegram')
            ->orWhere('name', 'facebook')
            ->orWhere('name', 'aparat')
            ->orWhere('name', 'instagram')
            ->orWhere('name', 'twitter')
            ->orWhere('name', 'googleplus')
            ->orWhere('name', 'linkedin')
            ->orWhere('name', 'footer_top')
            ->orWhere('name', 'footer_title')
            ->orWhere('name', 'footer_text')
            ->orWhere('name', 'footer_fast')
            ->orWhere('name', 'footer_services')
            ->orWhere('name', 'footer_image1')
            ->orWhere('name', 'footer_image2')
            ->orWhere('name', 'footer_image3')
            ->orWhere('name', 'footer_image4')
            ->pluck('value', 'name');
    }
}