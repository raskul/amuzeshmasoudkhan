<?php

namespace App;

use App\Models\Discount;
use App\Models\Factor;
use App\Models\Product;
use App\Models\Role;
use App\Models\TeachingRequest;
use App\Models\Ticket;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public $table = 'users';


    public $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'balance',
        'image',
        'state',
        'city',
        'address',
        'tel',
        'telephone',
        'commission',
        'about',
        'symlink',
        'is_spam',
        'is_ban',
        'point',
        'age',
        'job',
        'education',
        'postal_code',
        'telegram',
        'instagram',
        'linkedin',
        'twitter',
        'bank',
        'bank_number',
        'bank_sheba',
        'thumbnail',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function factors()
    {
        return $this->hasMany(Factor::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function discounts()
    {
        return $this->belongsToMany(Discount::class);
    }

    public function discountsBurned()
    {
        return $this->belongsToMany('App\Models\Discount', 'discount_user_burned', 'user_id', 'discount_id');
    }

    public function teachingRequests()
    {
        return $this->hasMany(TeachingRequest::class);
    }

    public function lessons()
    {
        return $this->belongsToMany('App\Models\Product', 'lesson_student', 'student_id', 'lesson_id');
    }

    public function lessonsDetail()
    {
        return $this->belongsToMany('App\Models\Product', 'lesson_student_detail', 'student_id', 'lesson_id');
    }

    public function ProductRatings()//used
    {
        return $this->belongsToMany('App\Models\Product', 'product_rating_user', 'user_id', 'product_id');
    }
}
