<?php

use App\Models\ProductType;
use Illuminate\Database\Seeder;

class ProductTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductType::create(['name' => 'special', 'name_fa' => 'دوره های حضوری (ویژه)']);
        ProductType::create(['name' => 'workshop', 'name_fa' => 'کارگاه های آموزشی']);
        ProductType::create(['name' => 'virtual', 'name_fa' => 'دوره های مجازی']);
        ProductType::create(['name' => 'seminar', 'name_fa' => 'کنفرانس ها و سمینارها']);
        ProductType::create(['name' => 'webinar', 'name_fa' => 'وبینارها']);
    }
}
