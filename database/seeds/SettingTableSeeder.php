<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::truncate();
        Setting::create(['name' => 'takhfif_line', 'name_fa' => 'قیمتی که شامل تخفیف میگردد', 'value' => '100000']);
        Setting::create(['name' => 'takhfif_value', 'name_fa' => 'مقدار تخفیف قیمتی که شامل تخفیف میگردد', 'value' => '5000']);
        Setting::create(['name' => 'takhfif_type', 'name_fa' => 'نوع تخفیف (0 - تومان) ( 1 - درصد)', 'value' => '0']);
        Setting::create(['name' => 'symlink_time', 'name_fa' => 'لینک دانلود کاربر هر چند دقیقه عوض شود؟', 'value' => '120']);
        Setting::create(['name' => 'tel', 'name_fa' => 'تلفن', 'value' => '02111559966']);
        Setting::create(['name' => 'mail', 'name_fa' => 'ایمیل', 'value' => 'mail.sitemail.com']);
        Setting::create(['name' => 'telegram', 'name_fa' => 'تلگرام', 'value' => 'https://t.me/mihanmelody']);
        Setting::create(['name' => 'telegram_tel', 'name_fa' => 'تلگرام', 'value' => '09030011223']);
        Setting::create(['name' => 'facebook', 'name_fa' => 'فیسبوک', 'value' => 'https://www.facebook.com/Trump/']);
        Setting::create(['name' => 'instagram', 'name_fa' => 'اینستاگرام', 'value' => 'https://www.instagram.com/realdonaldtrump/?hl=en']);
        Setting::create(['name' => 'twitter', 'name_fa' => 'توییتر', 'value' => 'https://twitter.com/Twitter']);
        Setting::create(['name' => 'linkedin', 'name_fa' => 'لینکداین', 'value' => 'https://www.linkedin.com/company/wine.com']);
        Setting::create(['name' => 'googleplus', 'name_fa' => 'گوگل پلاس', 'value' => 'https://plus.google.com/+foodandwine']);
        Setting::create(['name' => 'aparat', 'name_fa' => 'آپارات', 'value' => 'https://www.aparat.com/manzoom.ir']);
        Setting::create(['name' => 'header_menu', 'name_fa' => 'منو هدر', 'value' => '<ul><li><a href="/">صفحه اصلی</a></li><li><a href="/post/1">ثبت نام مدرس</a></li><li><a href="/post/2">قوانین سایت</a></li><li><a href="/post/3">درباره ما</a></li><li><a href="/post/4">تماس با ما</a></li></ul>']);
        Setting::create(['name' => 'header_site_logo', 'name_fa' => 'لوگوی سایت', 'value' => '//www.amuzesh.5001.ir/assets/images/logo.png']);
        Setting::create(['name' => 'footer_top', 'name_fa' => 'فوتر (بالاترین نوشته)', 'value' => '7 روز هفته 24 ساعته پاسخگوی سوالات شما هستیم ...']);
        Setting::create(['name' => 'footer_title', 'name_fa' => 'فوتر (اصلی عنوان)', 'value' => 'درباره آکادمی ایده پردازان']);
        Setting::create(['name' => 'footer_text', 'name_fa' => 'فوتر (متن)', 'value' => 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته،']);
        Setting::create(['name' => 'footer_fast', 'name_fa' => 'فوتر (دسترسی سریع)', 'value' => '<ul><li><a href="/post/5"><i></i>ثبت نام در سایت</a></li><li><a href="/post/6"><i></i>سوالات متداول</a></li><li><a href="/post/7"><i></i>تدریس در سایت</a></li><li><a href="/post/8"><i></i>مقالات آموزشی رایگان</a></li><li><a href="/post/9"><i></i>ثبت نام در سایت</a></li><li><a href="/post/10"><i></i>سوالات متداول</a></li></ul>']);
        Setting::create(['name' => 'footer_services', 'name_fa' => 'فوتر (خدمات و سرویس ها)', 'value' => '<ul><li><a href="/post/11"><i></i>ثبت نام در سایت</a></li><li><a href="/post/12"><i></i>سوالات متداول</a></li><li><a href="/post/13"><i></i>تدریس در سایت</a></li><li><a href="/post/14"><i></i>مقالات آموزشی رایگان</a></li><li><a href="/post/15"><i></i>ثبت نام در سایت</a></li><li><a href="/post/16"><i></i>سوالات متداول</a></li></ul>']);
        Setting::create(['name' => 'footer_image1', 'name_fa' => 'فوتر (عکس اول)', 'value' => '<a href="http://google.com"><img src="http://amuzesh.5001.ir/assets/images/cat-img/enamad.png" alt=""></a>']);
        Setting::create(['name' => 'footer_image2', 'name_fa' => 'فوتر (عکس دوم)', 'value' => '<a href="http://google.com"><img src="http://amuzesh.5001.ir/assets/images/cat-img/sep.png" alt=""></a>']);
        Setting::create(['name' => 'footer_image3', 'name_fa' => 'فوتر (عکس سوم)', 'value' => '<a href="http://google.com"><img src="http://amuzesh.5001.ir/assets/images/cat-img/nashr-d3.png" alt=""></a>']);
        Setting::create(['name' => 'footer_image4', 'name_fa' => 'فوتر (عکس چهارم)', 'value' => '<a href="http://google.com"><img src="http://amuzesh.5001.ir/assets/images/cat-img/samand.png" alt=""></a>']);
        Setting::create(['name' => 'default_commission', 'name_fa' => 'کمسیون پیشفرض معلمان', 'value' => '10']);
        Setting::create(['name' => 'send_by_post_amount', 'name_fa' => 'هزینه ارسال پستی', 'value' => '10000']);
    }
}
