<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoleTableSeeder::class);
         $this->call(SettingTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(ProductTypeTableSeeder::class);
         $this->call(PostTableSeeder::class);
        $this->call(UserTableSeeder::class);//must upper than ProductTableSeeder
        $this->call(ProductTableSeeder::class);
    }
}
