<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'user', 'name_fa' => 'کاربر معمولی']);
        Role::create(['name' => 'teacher', 'name_fa' => 'استاد']);
        Role::create(['name' => 'employee', 'name_fa' => 'کارمند']);
        Role::create(['name' => 'salesmanager', 'name_fa' => 'مدیر فروش']);
        Role::create(['name' => 'admin', 'name_fa' => 'مدیر']);
        Role::create(['name' => 'programmer', 'name_fa' => 'برنامه نویس']);
    }
}
