<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create(['user_id' => 1, 'category_id' => 1, 'title' => 'محصول تست', 'price' => 1000]);
    }
}
