<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->nullable()->default(10);
            $table->string('name',50);
            $table->string('name_fa', 50);
            $table->string('image')->nullable();
            $table->string('custom_link')->nullable();
            $table->enum('show_menu', ['show', 'hide'])->nullable()->default('show');
            $table->enum('show_index_page', ['show', 'hide'])->nullable()->default('hide');
            $table->unsignedInteger('parent_id')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
