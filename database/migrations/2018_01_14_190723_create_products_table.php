<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->string('title');
            $table->text('description_mini')->nullable();//un used
            $table->text('description')->nullable();
            $table->text('specification')->nullable();//un used
            $table->integer('price')->nullable()->default(0);
            $table->integer('real_price')->nullable()->default(0);
            $table->integer('discount')->nullable()->default(0);
            $table->integer('total_count')->nullable()->default(0);
            $table->integer('sold_count')->nullable()->default(0);
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->integer('total_voters')->nullable()->default(0);
            $table->float('vote')->nullable()->default(4);
            $table->float('rating_movie')->nullable()->default(4);
            $table->float('rating_voice')->nullable()->default(4);
            $table->float('rating_complete')->nullable()->default(4);
            $table->float('rating_teacher')->nullable()->default(4);
            $table->float('rating_support')->nullable()->default(4);
            $table->float('rating_rezayat')->nullable()->default(4);
            $table->integer('is_active')->nullable()->default(1);
            $table->integer('total_visit')->nullable()->default(0);
            $table->string('image_main')->nullable();
            $table->string('thumbnail')->nullable();
            $table->integer('user_earn_score')->nullable()->default(10);
            $table->integer('can_get_license')->nullable()->default(0);
            $table->integer('can_sign_up')->nullable()->default(1);
            $table->unsignedInteger('product_type_id')->nullable();
            $table->string('special_duration', 50)->nullable();
            $table->string('special_city', 50)->nullable();
            $table->string('special_teacher_name', 50)->nullable();
            $table->string('special_address', 900)->nullable();
            $table->string('special_time', 900)->nullable();
            $table->string('special_tel', 900)->nullable();
            $table->timestamps();

            $table->foreign('product_type_id')->references('id')->on('product_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
