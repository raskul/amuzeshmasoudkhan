<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->unsignedInteger('role_id')->unsigned();
            $table->integer('balance')->nullable()->default(0);
            $table->string('image')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('state',20)->nullable();
            $table->string('city', 20)->nullable();
            $table->string('address', 300)->nullable();
            $table->string('tel', 13)->nullable();
            $table->string('telephone', 13)->nullable();
            $table->integer('commission')->nullable()->default(0);
            $table->string('about', 900)->nullable();
            $table->integer('is_spam')->nullable()->default(0);
            $table->integer('is_ban')->nullable()->default(0);
            $table->integer('score')->nullable()->default(0);
            $table->string('symlink')->nullable();
            $table->string('age', 3)->nullable();
            $table->string('job', 30)->nullable();
            $table->string('education', 30)->nullable();
            $table->string('postal_code', 11)->nullable();
            $table->string('telegram')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();
            $table->string('bank')->nullable();
            $table->string('bank_number')->nullable();
            $table->string('bank_sheba')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
