<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableEshantionProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eshantion_mahsulasli', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('eshantion_id');
            $table->unsignedInteger('mahsulasli_id');

            $table->foreign('eshantion_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('mahsulasli_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eshantion_product');
    }
}
