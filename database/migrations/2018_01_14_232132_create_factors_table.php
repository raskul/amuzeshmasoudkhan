<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFactorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('sum')->nullable();
            $table->integer('discount_sum')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('is_paid')->nullable();
            $table->text('detail')->nullable();
            $table->string('description')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('status')->nullable();
            $table->string('refid')->nullable();
            $table->string('authority')->nullable();
            $table->integer('is_by_post')->nullable();
            $table->integer('post_amount')->nullable();
            $table->integer('is_sent')->nullable()->default(0);
            $table->string('who_sent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('factors');
    }
}
