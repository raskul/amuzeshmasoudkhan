<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndexNotificationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('index_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->text('text');
            $table->string('who_edit')->nullable();
            $table->integer('can_sign_up')->nullable()->default(1);
            $table->timestamp('start_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('index_notifications');
    }
}
