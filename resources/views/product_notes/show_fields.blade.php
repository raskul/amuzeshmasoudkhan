<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شناسه:') !!}
    <p>{!! $productNote->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'عنوان محصول:') !!}
    <p>{!! $productNote->product->title !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'ایمیل استاد:') !!}
    <p>{!! $productNote->user->email !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'عنوان اعلامیه:') !!}
    <p>{!! $productNote->title !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'متن اعلامیه:') !!}
    <p>{!! $productNote->text !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $productNote->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ ویرایش:') !!}
    <p>{!! $productNote->updated_at !!}</p>
</div>

