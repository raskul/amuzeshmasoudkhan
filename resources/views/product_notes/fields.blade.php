<!-- فیلد محصول را انتخاب کنید: -->
<div class="form-group col-sm-6 {{ $errors->has('product_id') ? ' has-error' : '' }}">
    {!! Form::label('product_id', 'محصول را انتخاب کنید:') !!}
    {!! Form::select('product_id', $products, (isset($products)?$products:null), ['class' => 'form-control select2']) !!}
    @if ($errors->has('product_id'))
        <span class="help-block"><strong>{{ $errors->first('product_id') }}</strong></span>
    @endif
</div>
<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'شناسه استاد:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'عنوان اعلامیه:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'متن اعلامیه:') !!}
    {!! Form::text('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('productNotes.index') !!}" class="btn btn-default">لغو</a>
</div>
@push( 'scripts')
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
            $('html, body').animate({
                scrollTop: $("#myDiv").offset().top
            }, 2000);
        });
    </script>
@endpush