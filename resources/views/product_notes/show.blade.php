@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            اعلامیه دوره
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding:0 20px">
                    @include('product_notes.show_fields')
                    <a href="{!! route('productNotes.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
