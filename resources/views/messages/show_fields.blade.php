<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره:') !!}
    <p>{!! $message->id !!}</p>
</div>

<!-- From User Id Field -->
<div class="form-group">
    {!! Form::label('from_user_id', 'از کاربر:') !!}
    <p>{!! $message->from_user_id !!}</p>
</div>

<!-- To User Id Field -->
<div class="form-group">
    {!! Form::label('to_user_id', 'به کاربر:') !!}
    <p>{!! $message->to_user_id !!}</p>
</div>

<!-- Parent Id Field -->
<div class="form-group">
    {!! Form::label('parent_id', 'شماره والد:') !!}
    <p>{!! $message->parent_id !!}</p>
</div>

<!-- Who Answer Field -->
<div class="form-group">
    {!! Form::label('who_answer', 'آخرین پاسخ دهنده:') !!}
    <p>{!! $message->who_answer !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'متن:') !!}
    <p>{!! $message->text !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ارسال:') !!}
    <p>{!! $message->created_at !!}</p>
</div>

