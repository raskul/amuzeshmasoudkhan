<!-- From User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_user_id', 'از کاربر:') !!}
    {!! Form::text('from_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- To User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_user_id', 'به کابر:') !!}
    {!! Form::text('to_user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Parent Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('parent_id', 'ای دی پدر:') !!}
    {!! Form::text('parent_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Who Answer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('who_answer', 'Who Answer:') !!}
    {!! Form::text('who_answer', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'متن:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('messages.index') !!}" class="btn btn-default">لغو</a>
</div>
