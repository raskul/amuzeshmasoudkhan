@extends('layouts.index')
@push('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

    <link rel="stylesheet" href="{{asset('assets/js/jquery-bar-rating-master/dist/themes/bars-movie.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/jquery-bar-rating-master/dist/themes/css-stars.css')}}">

    <script src="{{asset('assets/js/scroll/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('assets/js/scroll/jquery.mCustomScrollbar.css')}}">

    {{--full wide vido and image start--}}
    <style>
        #contentforpost * video, #contentforpost * img {
            width: 100%;
        }
    </style>
    {{--full wide vido and image end--}}
    {{--disable player download button start--}}
    <style>
        video::-internal-media-controls-download-button {
            display: none;
        }

        video::-webkit-media-controls-enclosure {
            overflow: hidden;
        }

        video::-webkit-media-controls-panel {
            width: calc(100% + 30px);
        }

        video::-webkit-media-controls {
            overflow: hidden !important
        }
    </style>
    {{--disable player download button end--}}

@endpush
@section('content')
    @include('partials.header')

    @include('flash::message')
    @include('adminlte-templates::common.errors')

    <div id="main">

        <div class="single_title">
            <div class="title_area">
                <div class="single_img">
                    <img src="{{showProductImage($product)}}" alt="">
                </div>
                <h1>{{$product->title}}</h1>
                <div class="nav-bar">
                    <ul style="width: 100%">

                        @php
                            $catTop = \App\Models\Category::find($product->category_id);
                            $catText[] = ['name' => $product->id, 'name_fa' => $product->title];
                            $catText[] = ['name' => $catTop->name, 'name_fa' => $catTop->name_fa];
                            if (!(is_null($catTop->father))){
                                if($catTop->father->id != 1){
                                    $catText[] = ['name' => $catTop->father->name, 'name_fa' => $catTop->father->name_fa];
                                    $catTop = $catTop->father;
                                }
                                if($catTop->father->id != 1){
                                    $catText[] = ['name' => $catTop->father->name, 'name_fa' => $catTop->father->name_fa];
                                    $catTop = $catTop->father;
                                }
                                if($catTop->father->id != 1){
                                    $catText[] = ['name' => $catTop->father->name, 'name_fa' => $catTop->father->name_fa];
                                    $catTop = $catTop->father;
                                }
                                $catText = array_reverse($catText);
                            }else{
                                $catText = [];
                            }
                            $i = 0;
                        @endphp

                        <li><a href="{{route('index.index')}}">
                                صفحه نخست
                            </a>
                            <i></i>
                        </li>
                        <li><a href="{{route('category', null)}}">
                                همه محصولات
                            </a>
                            <i></i>
                        </li>

                        @foreach($catText as $catTxt)
                            <li><a href="
                                @if(count($catText) > $i+1)
                                {{route('category', $catTxt['name'])}}
                                @endif
                                        ">
                                    {{$catTxt['name_fa']}}
                                </a>
                                @if(count($catText) > $i+1)
                                    <i></i>
                                @endif
                                @php
                                    $i++
                                @endphp

                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
        <div class="course_page">

            @if($product->productType->name == 'special')
                <div class="course_count_down">
                    <ul>

                        <li><i class="fas fa-user"></i>مدرس دوره :
                            @if(strlen($product->special_teacher_name) > 3)
                                {{$product->special_teacher_name}}
                            @else
                                {{$product->user->name}}
                            @endif
                        </li>
                        <li><i class="fas fa-calendar"></i>زمان شروع :
                            {{pDate($product->start_at, false, false, true, true)}}
                        </li>
                        <li><i class="fas fa-users"></i>ظرفیت باقی مانده :
                            @if($product->total_count > 0)
                                {{$product->total_count - $product->sold_count}}
                                نفر
                            @else
                                نامحدود
                            @endif
                        </li>
                        <li><i class="fas fa-map-marker"></i>محل برگزاری :
                            @if(strlen($product->city)>2)
                                {{$product->city}}
                            @else
                                مشخص نشده
                            @endif
                        </li>
                    </ul>
                </div>
            @endif

            <div class="course_details">
                <div class="course_price">

                    @cannot('is-product-student', $product)

                        <div class="icon_text"><i></i>

                            @if(((($product->total_count) == 0) || (($product->total_count - $product->sold_count) >= 1)) && (($product->can_sign_up) == 1))

                                @if(($product->price - $product->discount) > 0)

                                    {{--افزودن به سبد خرید--}}
                                    {{--                            {!! Form::open(['route'=>['add.to.basket', $product->id ], 'method' => 'post' ]) !!}--}}
                                    <button type="button" class="icon_text" @click="addToCart({{$product->id}})"
                                            style="background-color: rgba(12,12,12,0);border: 0;font-family: yekan;">
                                        افزودن به سبد خرید
                                    </button>
                                    {{--                            {!! Form::close() !!}--}}

                                @else

                                    {!! Form::open(['route'=>['signUpNoPrice', $product->id ], 'method' => 'put' ]) !!}
                                    <button type="submit" class="icon_text"
                                            style="background-color: rgba(12,12,12,0);border: 0;font-family: yekan;">
                                        ثبت نام در درس
                                    </button>
                                    {!! Form::close() !!}

                                @endif

                            @else

                                <button type="submit" class="icon_text"
                                        style="background-color: rgb(255,2,0);border: 0;font-family: yekan;">
                                    ظرفیت کلاس تکمیل است
                                </button>

                            @endif

                        </div>
                        <div class="price">
                            <div class="price price-real" style="font-family: iran-sans; line-height: 90%">

                                @if(($product->price-$product->discount) > 0)
                                    {{ toPersianNumber($product->price-$product->discount) }}
                                    تومان
                                @else
                                    رایگان
                                @endif

                            </div>
                            @if($product->discount > 0)
                                <div class="price"
                                     style="text-decoration: line-through; font-size: x-small; color: #ffb35b;">
                                    {{toPersianNumber($product->price)}}
                                </div>
                            @endif
                        </div>

                    @else

                        <div class="icon_text" style="background-color: rgba(0,174,36,0.98);border: 0;font-family: yekan;width:350px ">
                            شما دانشجوی این درس هستید
                        </div>

                    @endcannot

                </div>

                @php
                    $duration = 0;
                    $size = 0;
                @endphp
                @foreach($productPosts as $productPost)
                    @php
                        $duration += $productPost->duration;
                        $size += $productPost->size;
                    @endphp
                @endforeach

                <div class="course_features">
                    <ul>
                        <li>
                            @if($product->sold_count > 0)
                                <i style="background-position: -175px -14px;width: 75px;"></i>
                                تعداد دانشجویان : <span>
    {{--                                {{ $product->sold_count }}--}}
                                    {{ ($product->id) * ($product->id) + (26) + ($product->sold_count) }}
                                    دانشجو</span>
                            @endif
                        </li>
                        <li>
                            <i style="background-position: -117px -18px;"></i>
                            تعداد جلسات : <span>
                                {{ count($product->productPosts) }}
                                جلسه</span>
                        </li>
                        <li>
                            <i style="background-position: -57px -14px;"></i>
                            زمان دوره : <span>
                                {{$duration}}
                                دقیقه</span>
                        </li>
                    </ul>
                </div>
                <div class="rate_alert"><i></i>
                    <p>با خرید این دوره شما <span>
                            {{ $product->user_earn_score }}
                        </span> امتیاز به دست خواهید آورد</p></div>

                @if( $product->can_get_license == 1)
                    <div class="certificate_alert"><i></i>
                        <p>امکان دریافت گواهینامه برای این دوره وجود دارد</p>
                    </div>
                @endif

                <div class="rate_area">

                    <div id="rateYo"></div>

                    <script>
                        $(function () {
                            $("#rateYo").rateYo({
                                starWidth: "40px",
                                rating    : {{$product->vote}},
                                spacing   : "5px",
                                readOnly: true,
                                rtl: true
                            });
                        });
                    </script>

                    <div class="text_rate"><p><span>{{$product->vote}}</span> امتیاز</p></div>
                </div>
            </div>

            <div class="course_content" style="position: relative; overflow: auto;font-family: iran-sans">
                <div id="contentforpost">
                    @if((Auth::check()) && (auth()->user()->id == $product->user_id))
                        {!! $product_post['text'] !!}
                    @else
                        @can('is-product-student', $product)
                            {!! $product_post['text'] !!}
                        @else
                            @if(($product->price - $product->discount) <= 0)
                                {!! $product_post['text'] !!}
                            @else
                                @if(isset($product_post['show_type']))
                                    @if( $product_post['show_type'] == 0)
                                        {!! $product_post['text'] !!}
                                    @else
                                        برای مشاهده آموزش های این دوره باید ابتدا این دوره را خریداری نمایید.
                                    @endif
                                @else
                                    برای این دوره هنوز درسی ثبت نشده است.
                                @endif
                            @endif
                        @endcan
                    @endif
                </div>
            </div>

            <div class="clear"></div>
            <div class="course_list_teacher">
                <div class="course_teacher">
                    <div class="teacher_img">

                        <img src="{{showUserImage($product->user->id)}}"
                             style="width: 100%;height: 100%; border-radius: 100%" alt="">
                    </div>
                    <div class="teacher_name">
                        <h4><i></i><span>{{ $product->user->name }}</span></h4>
                    </div>
                    <div class="about_teacher">
                        <h4>درباره مدرس</h4>
                        <p>
                            {{$product->user->about}}
                        </p>
                    </div>
                    <div class="teacher_name">
                        @if((count($user->products))>=2)
                            <h4><i></i><span>سایر دوره های مدرس :</span></h4>
                            <ul>
                                @foreach($user->products as $userProduct)
                                    @if($userProduct->id == $product->id)
                                        @continue
                                    @endif
                                    <li>
                                        <a href="{{route('index.index')}}/{{createProductLinkCreate($userProduct)}}"><i></i>
                                            {{ $userProduct->title }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>

                </div>

                <div class="course_lecture">
                    <div class="course_tab">
                        <div class="course_lecture_details"><span></span><i
                                    style="background-position: -261px -97px"></i>توضیحات
                            دوره
                        </div>
                        <div class="course_lecture_list course_tab_active"><span></span><i
                                    style="background-position: -208px -97px"></i>فهرست
                            مطالب
                        </div>
                        <div class="course_lecture_file"><span></span><i style="background-position: -306px -97px"></i>فایل
                            های دوره
                        </div>
                        <div class="course_lecture_students"><span></span><i
                                    style="background-position: -143px -97px;width: 54px"></i>اطلاعیه های دوره
                        </div>
                    </div>
                    <div class="course_details_tab">
                        @if(strlen($product->description))
                            <h2 style="font-size: 12.5pt;text-align: center;">توضیحات دوره
                                <span>{{$product->title}}</span></h2>
                            {!! $product->description !!}
                        @else
                            هنوز توضیحات این محصول آموزشی ثبت نشده است.
                        @endif
                    </div>
                    <div class="course_list_tab">
                        <div class="mCustomScrollbar content fluid light" data-mcs-theme="inset-2-dark">
                            @if( count($productPosts))
                                <ul>
                                    @php
                                        $k = 1;
                                    @endphp
                                    @foreach($productPosts as $productPost)
                                        <li>
                                            <a href="{{route('show.product.page', $product->id)}}/{{$k}}">
                                <span class="num_lecture"><i class="num_icon"></i>جلسه
                                    {{ $k++ }}
                                    :</span>
                                                {{ $productPost->title }}
                                                <span class="time_lecture"><i class="time_icon"></i>
                                                    {{ $productPost->duration }}
                                                    min</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <div style="font-family: iran-sans">هنوز هیچ جلسه آموزشی برای این محصول ثبت نشده است.</div>
                            @endif
                        </div>
                    </div>
                    <div class="course_file_tab">
                        <div class="mCustomScrollbar content fluid light" data-mcs-theme="inset-2-dark">
                            @if(count($productFiles))
                                <ul>
                                    @foreach($productFiles as $productFile)
                                        <li>
                                            @php
                                                $productFilePath = $productFile->file;
                                                $productFilePath = substr($productFilePath, 21);
                                                $productFilePath = str_replace('/', ':', $productFilePath);
                                            @endphp
                                            <a href="{{ route('downloadFiles', $productFilePath)}}">
                                                <i class="download_icon"></i>
                                                {{$productFile->title}}
                                                @php
                                                    $productFileSize = floor(($productFile->size)/(1)) . 'Byte';
                                                    if($productFileSize >= 1000){
                                                        $productFileSize = floor(($productFile->size)/(1024)) . 'KB';
                                                    }
                                                    if($productFileSize >= 1000){
                                                        $productFileSize = floor(($productFile->size)/(1024 * 1024)) . 'MB';
                                                    }
                                                    if($productFileSize >= 1000){
                                                        $productFileSize = floor(($productFile->size)/(1024 * 1024 * 1024)) . 'GB';
                                                    }
                                                @endphp
                                                <i class="size_icon">{{$productFileSize}} </i>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <div style="font-family: iran-sans">هنوز هیچ فایلی به این محصول ضمیمه نشده است.</div>
                            @endif
                        </div>
                    </div>
                    <div class="course_student_tab">
                        @if(count($productNotes))
                            @foreach($productNotes as $productNote)
                                <ul>
                                    <li>
                                        <div class="date">
                                <span class="date">
                                    {{pDate($productNote->created_at)}}
                                </span>

                                        </div>
                                        <div class="title">
                                            <h5><i></i>{{$productNote->title}}</h5>
                                        </div>
                                        <div class="description">
                                            {{$productNote->text}}
                                        </div>
                                    </li>
                                </ul>
                            @endforeach
                        @else
                            <div>در حال حاضر هیچ اطلاعیه ای برای نمایش وجود ندارد.</div>
                        @endif
                    </div>
                </div>
                <div class="clear"></div>

                @if($product->productType->name == 'special')
                    <div class="course_locaton">
                        <div class="course_hozuri">
                            <h6><i class="far fa-calendar-alt"></i>تاریخ شروع و مدت زمان دوره :</h6>
                            <p style="margin-top: 10px;margin-right: 30px;">{{$product->special_time}}</p>
                            <h6 style="margin-top: 40px;"><i class="fas fa-map-marker"></i>مکان برگزاری کلاس ها :</h6>
                            <p style="margin-right: 30px;margin-top: 10px;">{{$product->special_address}}</p>
                            <h6 style="margin-top: 40px;"><i class="fas fa-phone-square"></i>شماره های تماس :</h6>
                            <p style="margin-right: 30px;margin-top: 10px;">{{$product->special_tel}}</p>
                        </div>
                        <div class="course_location_map">
                            <img style="width: 100%;height: 100%;" src="/assets/images/3.jpg" alt="">
                        </div>
                    </div>
                    <div class="clear"></div>
                @endif

                <div class="comment_qs">
                    <div class="comm_faq_tab">

                        @php
                            $active_faq = null;
                            $active_comments = null;
                            $display_faq = 'style=display:none';
                            $display_comments = 'style=display:none';
                            $script = false ;
                            if(strpos(Request::fullUrl(), 'faq')){
                                $active_faq = 'active';
                                $display_faq = 'style=display:block';
                                $script = true;
                            }else{
                                $active_comments = 'active';
                                $display_comments = 'style=display:block';
                            }
                            if (strpos(Request::fullUrl(), 'comment')){
                                $script = true;
                            }
                        @endphp

                        <div id="divscroll"></div>
                        <div class="faq"><span class="{{$active_faq}}"></span>پرسش و پاسخ دانشجویان</div>
                        <div class="comments"><span class="{{$active_comments}}"></span>نظرات و دیدگاه های کاربران</div>
                        <div class="rate_course"><span></span>امتیازدهی به
                            این دوره آموزشی
                        </div>
                    </div>
                    <div class="clear"></div>


                    <div class="faq_area" {{$display_faq}}>


                        @can('is-product-student', $product)
                            {!! Form::open(['route'=>['faq.store.sec', 'product_id' => $product->id], 'method' => 'post' ]) !!}
                            <div class="clear"></div>
                            <div class=" {{ $errors->has('text') ? ' has-error' : '' }}">
                                {!! Form::textarea('text', null, ['class' => 'description', 'id' => 'write-review-text', 'placeholder'=>"دیدگاه خود را اینجا بنویسید...", 'col' => '30', 'row' => '10']) !!}
                                @if ($errors->has('text'))
                                    <span class=""><strong>{{ $errors->first('text') }}</strong></span>
                                @endif
                            </div>
                            <div class="submit_comment" style="height: 50px; width: 100%; margin-top: 15px;">
                                <input type="submit" value="ارسال دیدگاه">
                            </div>
                            {!! Form::close() !!}
                        @else
                            <div style="font-family: iran-sans">
                                برای ثبت سوال باید این محصول را خریداری نمایید.
                            </div>
                        @endcan

                        <ul>
                            @foreach($faqs  as $faq)
                                <li>
                                    @can('is-product-student', $product)
                                        <div class="reply" style="position: absolute;left: 15px;top: 35px;">
                                            <a href="{{route('faqAnswer', $faq->id)}}">پاسخ دهید</a>
                                        </div>
                                    @endcan

                                    @can('is-comment-owner', $faq)
                                        <div class="comment-edit" style="position: absolute;left: 80px;top: 35px;">
                                            <a href="{{route('faqEdit', $faq->id)}}">ویرایش</a>
                                        </div>
                                    @endcan


                                    <div class="user_pic" style="float: right;width: 125px; height: 100%;">
                                        <img src="{{showUserImage($faq->user->id)}}" alt="">
                                    </div>
                                    <div class="user_comment">
                                        <p>{{$faq->user->name}}
                                            میگه :</p>
                                        <p>{{pDate($faq->create_at, true)}}</p>
                                        <span>{{$faq->text}}</span>
                                    </div>
                                    @if(count($faq->children))
                                        <ul>
                                            @foreach($faq->children as $child)
                                                <li>
                                                    @can('is-comment-owner', $child)
                                                        <div class="reply-edit" style="">
                                                            <a href="{{route('faqEdit', $child->id)}}">ویرایش</a>
                                                        </div>
                                                    @endcan
                                                    <div class="user_pic"
                                                         style="float: right;width: 125px; height: 100%;">
                                                        <img src="{{showUserImage($child->user->id)}}" alt="">
                                                    </div>
                                                    <div class="user_comment">
                                                        <p>{{$child->user->name}}
                                                            میگه :</p>
                                                        <p>{{pDate($child->create_at, true)}}</p>
                                                        <span>{{$child->text}}</span>
                                                    </div>

                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>

                        <div class="clear"></div>
                        <div class="pagnation">
                            <div class="page_align">
                                {{$faqs->links('vendor.pagination.simple-default')}}
                            </div>
                        </div>

                    </div>
                    <div class="comments_area" {{$display_comments}}>
                        @if(Auth::check())
                            {!! Form::open(['route'=>['comment.store.sec', 'product_id' => $product->id], 'method' => 'post' ]) !!}
                            <div class="clear"></div>
                            <div class=" {{ $errors->has('text') ? ' has-error' : '' }}">
                                {!! Form::textarea('text', null, ['class' => 'description', 'id' => 'write-review-text', 'placeholder'=>"دیدگاه خود را اینجا بنویسید...", 'col' => '30', 'row' => '10']) !!}
                                @if ($errors->has('text'))
                                    <span class=""><strong>{{ $errors->first('text') }}</strong></span>
                                @endif
                            </div>
                            <div class="submit_comment" style="height: 50px; width: 100%; margin-top: 15px;">
                                <input type="submit" value="ارسال دیدگاه">
                            </div>
                            {!! Form::close() !!}
                        @else
                            <div style="font-family: iran-sans">برای ثبت دیدگاه ابتدا
                                {{--                                        <a href="{{ route('login.or.register') }}">--}}
                                <strong>
                                    وارد
                                </strong>
                                </a>
                                اکانت خود شوید یا
                                <a href="{{ route('reg') }}">
                                    <strong>
                                        ثبت نام
                                    </strong>
                                </a>
                                کنید.
                            </div>
                        @endif
                        <div class="num_comment">
                            <span>تعداد نظرات :
                                {{count($product->comments)}}
                            </span>
                        </div>
                        <div class="clear"></div>
                        <ul>
                            @foreach($comments  as $comment)
                                <li>
                                    <div class="reply" style="position: absolute;left: 15px;top: 35px;">
                                        <a href="{{route('commentAnswer', $comment->id)}}">پاسخ دهید</a>
                                    </div>

                                    @can('is-comment-owner', $comment)
                                        <div class="" style="position: absolute;left: 80px;top: 35px;">
                                            <a href="{{route('commentEdit', $comment->id)}}">ویرایش</a>
                                        </div>
                                    @endcan

                                    <div class="user_pic" style="float: right;width: 125px; height: 100%;">
                                        <img src="{{showUserImage($comment->user->id)}}" alt="">
                                    </div>
                                    <div class="user_comment">
                                        <p>{{$comment->user->name}}
                                            میگه :</p>
                                        <p>{{pDate($comment->create_at, true)}}</p>
                                        <span>{{$comment->text}}</span>
                                    </div>
                                    @if(count($comment->children))
                                        <ul>
                                            @foreach($comment->children as $child)
                                                <li>

                                                    @can('is-comment-owner', $child)
                                                        <div class="" style="">
                                                            <a href="{{route('commentEdit', $child->id)}}">ویرایش</a>
                                                        </div>
                                                    @endcan

                                                    <div class="user_pic"
                                                         style="float: right;width: 125px; height: 100%;">
                                                        <img src="{{showUserImage($child->user->id)}}" alt="">
                                                    </div>
                                                    <div class="user_comment">
                                                        <p>{{$child->user->name}}
                                                            میگه :</p>
                                                        <p>{{pDate($child->create_at, true)}}</p>
                                                        <span>{{$child->text}}</span>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                        <div class="clear"></div>
                        <div class="pagnation">
                            <div class="page_align">
                                {{$comments->links('vendor.pagination.simple-default')}}
                            </div>
                        </div>
                    </div>
                    <div class="rate_area_cm">

                        <div class="rate_description">
                            <h5>چرا باید به دوره های آموزشی مجموعه مدیران ایده پرداز امتیاز بدهیم؟</h5>
                            <p>
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                                است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط
                                فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای
                                زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با
                                نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو
                                در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه
                                راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و
                                جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                            </p>
                        </div>


                        <div class="rate_cm_left deactivated rating-enable">

                            {!! Form::open(['route'=>['productRating', 'product_id' => $product->id], 'method' => 'post' ]) !!}

                            <div style="width: 50%; height: 100%;float: right">
                                <ul>
                                    <li style="height: 150px;">
                                        <h6 style="font-size: 10pt;">امتیاز شما به کیفیت تصویر آموزش</h6>

                                        <select id="movie" name="rating_movie" autocomplete="off">
                                            <option value="1"
                                                    @if(($product->rating_movie >= 0) && ($product->rating_movie <= 1)) selected @endif >
                                                بسیار ضعیف
                                            </option>
                                            <option value="2"
                                                    @if(($product->rating_movie > 1) && ($product->rating_movie <= 2)) selected @endif>
                                                ضعیف
                                            </option>
                                            <option value="3"
                                                    @if(($product->rating_movie > 2) && ($product->rating_movie <= 3)) selected @endif>
                                                متوسط
                                            </option>
                                            <option value="4"
                                                    @if(($product->rating_movie > 3) && ($product->rating_movie <= 4)) selected @endif>
                                                خوب
                                            </option>
                                            <option value="5"
                                                    @if(($product->rating_movie > 4) && ($product->rating_movie <= 5)) selected @endif>
                                                عالی
                                            </option>
                                        </select>


                                        <script type="text/javascript">
                                            $(function () {
                                                $('#movie').barrating({
                                                    theme: 'bars-movie'
                                                });
                                            });
                                        </script>


                                    </li>
                                    <li style="height: 150px;">
                                        <h6 style="font-size: 10pt;">امتیاز شما به کیفیت صدای آموزش</h6>


                                        <select id="voice" name="rating_voice" autocomplete="off">
                                            <option value="1"
                                                    @if(($product->rating_voice >= 0) && ($product->rating_voice <= 1)) selected @endif>
                                                بسیار ضعیف
                                            </option>
                                            <option value="2"
                                                    @if(($product->rating_voice > 1) && ($product->rating_voice <= 2)) selected @endif>
                                                ضعیف
                                            </option>
                                            <option value="3"
                                                    @if(($product->rating_voice > 2) && ($product->rating_voice <= 3)) selected @endif >
                                                متوسط
                                            </option>
                                            <option value="4"
                                                    @if(($product->rating_voice > 3) && ($product->rating_voice <= 4)) selected @endif >
                                                خوب
                                            </option>
                                            <option value="5"
                                                    @if(($product->rating_voice > 4) && ($product->rating_voice <= 5)) selected @endif>
                                                عالی
                                            </option>
                                        </select>


                                        <script type="text/javascript">
                                            $(function () {
                                                $('#voice').barrating({
                                                    theme: 'bars-movie'
                                                });
                                            });
                                        </script>


                                    </li>
                                    <li style="height: 150px;">
                                        <h6 style="font-size: 10pt;">امتیاز شما به جامع بودن سرفصل ها</h6>


                                        <select id="sarfasl" name="rating_complete" autocomplete="off">
                                            <option value="1"
                                                    @if(($product->rating_complete >= 0) && ($product->rating_complete <= 1)) selected @endif>
                                                بسیار ضعیف
                                            </option>
                                            <option value="2"
                                                    @if(($product->rating_complete > 1) && ($product->rating_complete <= 2)) selected @endif>
                                                ضعیف
                                            </option>
                                            <option value="3"
                                                    @if(($product->rating_complete > 2) && ($product->rating_complete <= 3)) selected @endif >
                                                متوسط
                                            </option>
                                            <option value="4"
                                                    @if(($product->rating_complete > 3) && ($product->rating_complete <= 4)) selected @endif>
                                                خوب
                                            </option>
                                            <option value="5"
                                                    @if(($product->rating_complete > 4) && ($product->rating_complete <= 5)) selected @endif>
                                                عالی
                                            </option>
                                        </select>


                                        <script type="text/javascript">
                                            $(function () {
                                                $('#sarfasl').barrating({
                                                    theme: 'bars-movie'
                                                });
                                            });
                                        </script>


                                    </li>
                                </ul>
                            </div>
                            <div style="width: 50%; height: 100%;float: left;box-sizing: border-box;">
                                <ul>
                                    <li style="height: 150px;">
                                        <h6 style="font-size: 10pt;">امتیاز شما به تسلط مدرس</h6>


                                        <select id="teacher" name="rating_teacher" autocomplete="off">
                                            <option value="1"
                                                    @if(($product->rating_teacher >= 0) && ($product->rating_teacher <= 1)) selected @endif>
                                                بسیار ضعیف
                                            </option>
                                            <option value="2"
                                                    @if(($product->rating_teacher > 1) && ($product->rating_teacher <= 2)) selected @endif >
                                                ضعیف
                                            </option>
                                            <option value="3"
                                                    @if(($product->rating_teacher > 2) && ($product->rating_teacher <= 3)) selected @endif>
                                                متوسط
                                            </option>
                                            <option value="4"
                                                    @if(($product->rating_teacher > 3) && ($product->rating_teacher <= 4)) selected @endif >
                                                خوب
                                            </option>
                                            <option value="5"
                                                    @if(($product->rating_teacher > 4) && ($product->rating_teacher <= 5)) selected @endif>
                                                عالی
                                            </option>
                                        </select>


                                        <script type="text/javascript">
                                            $(function () {
                                                $('#teacher').barrating({
                                                    theme: 'bars-movie'
                                                });
                                            });
                                        </script>


                                    </li>
                                    <li style="height: 150px;">
                                        <h6 style="font-size: 10pt;">امتیاز شما به پشتیبانی</h6>


                                        <select id="support" name="rating_support" autocomplete="off">
                                            <option value="1"
                                                    @if(($product->rating_support >= 0) && ($product->rating_support <= 1)) selected @endif>
                                                بسیار ضعیف
                                            </option>
                                            <option value="2"
                                                    @if(($product->rating_support > 1) && ($product->rating_support <= 2)) selected @endif>
                                                ضعیف
                                            </option>
                                            <option value="3"
                                                    @if(($product->rating_support > 2) && ($product->rating_support <= 3)) selected @endif >
                                                متوسط
                                            </option>
                                            <option value="4"
                                                    @if(($product->rating_support > 3) && ($product->rating_support <= 4)) selected @endif>
                                                خوب
                                            </option>
                                            <option value="5"
                                                    @if(($product->rating_support > 4) && ($product->rating_support <= 5)) selected @endif>
                                                عالی
                                            </option>
                                        </select>


                                        <script type="text/javascript">
                                            $(function () {
                                                $('#support').barrating({
                                                    theme: 'bars-movie'
                                                });
                                            });
                                        </script>


                                    </li>
                                    <li style="height: 150px;">
                                        <h6 style="font-size: 10pt;">میزان رضایت شما از دوره های آموزشی ما</h6>


                                        <select id="rezayat" name="rating_rezayat" autocomplete="off">
                                            <option value="1"
                                                    @if(($product->rating_rezayat >= 0) && ($product->rating_rezayat <= 1)) selected @endif>
                                                بسیار ضعیف
                                            </option>
                                            <option value="2"
                                                    @if(($product->rating_rezayat > 1) && ($product->rating_rezayat <= 2)) selected @endif>
                                                ضعیف
                                            </option>
                                            <option value="3"
                                                    @if(($product->rating_rezayat > 2) && ($product->rating_rezayat <= 3)) selected @endif>
                                                متوسط
                                            </option>
                                            <option value="4"
                                                    @if(($product->rating_rezayat > 3) && ($product->rating_rezayat <= 4)) selected @endif>
                                                خوب
                                            </option>
                                            <option value="5"
                                                    @if(($product->rating_rezayat > 4) && ($product->rating_rezayat <= 5)) selected @endif >
                                                عالی
                                            </option>
                                        </select>


                                        <script type="text/javascript">
                                            $(function () {
                                                $('#rezayat').barrating({
                                                    theme: 'bars-movie'
                                                });
                                            });
                                        </script>


                                    </li>
                                </ul>
                                <input type="submit" value="ثبت امتیاز">
                            </div>

                            {!! Form::close() !!}

                        </div>
                        <div class="rate_cm_right">
                            <img style="width: 200px;height: 200px;border-radius: 100%; padding: 3px; border: 1px solid #ccc;"
                                 src="{{asset('assets/images/learn.jpg')}}" alt="">
                            <h6><a href="#">دوره آموزش طراحی صفر تا صد سایت با html5 و css3</a></h6>
                        </div>


                    </div>


                </div>


            </div>
        </div>

        @include('partials.footer')

    </div>
@endsection
@push('scripts')

    <script src="{{asset('assets/js/jquery-bar-rating-master/jquery.barrating.js')}}"></script>
    <script>


        /*start jquery code for single course tab box*/

        $(".course_lecture_list").click(function () {
            $(this).addClass("course_tab_active");
            $(".course_lecture_details,.course_lecture_file,.course_lecture_students").removeClass("course_tab_active");
            $(".course_list_tab").fadeIn(500);
            $(".course_details_tab,.course_file_tab,.course_student_tab").fadeOut(0);
        });
        $(".course_lecture_details").click(function () {
            $(this).addClass("course_tab_active");
            $(".course_lecture_list,.course_lecture_file,.course_lecture_students").removeClass("course_tab_active");
            $(".course_details_tab").fadeIn(500);
            $(".course_list_tab,.course_file_tab,.course_student_tab").fadeOut(0);
        });
        $(".course_lecture_file").click(function () {
            $(this).addClass("course_tab_active");
            $(".course_lecture_details,.course_lecture_list,.course_lecture_students").removeClass("course_tab_active");
            $(".course_file_tab").fadeIn(500);
            $(".course_list_tab,.course_details_tab,.course_student_tab").fadeOut(0);
        });
        $(".course_lecture_students").click(function () {
            $(this).addClass("course_tab_active");
            $(".course_lecture_details,.course_lecture_list,.course_lecture_file").removeClass("course_tab_active");
            $(".course_student_tab").fadeIn(500);
            $(".course_list_tab,.course_details_tab,.course_file_tab").fadeOut(0);
        });



        /*start jquey code for comments tab*/

        var mainTag = $(".comment_qs");
        var faqTag = mainTag.find(".faq");
        var commentsTag = mainTag.find(".comments");
        var rateCourseTag = mainTag.find(".rate_course");

        faqTag.click(function () {
            $(".comments_area , .rate_area_cm").fadeOut(0);
            $(".faq_area").fadeIn(500);
            $(this).find("span").addClass("active");
            commentsTag.find("span").removeClass("active");
            rateCourseTag.find("span").removeClass("active");
        });
        commentsTag.click(function () {
            $(".faq_area , .rate_area_cm").fadeOut(0);
            $(".comments_area").fadeIn(500);
            $(this).find("span").addClass("active");
            faqTag.find("span").removeClass("active");
            rateCourseTag.find("span").removeClass("active");
        });
        rateCourseTag.click(function () {
            $(".faq_area , .comments_area").fadeOut(0);
            $(".rate_area_cm").fadeIn(500);
            $(this).find("span").addClass("active");
            commentsTag.find("span").removeClass("active");
            faqTag.find("span").removeClass("active");
        });


    </script>

    @if($script)
        @include('partials.productPageScrollScript')
    @endif
















    {{--<script>--}}
        {{--var expect = window.chai.expect;--}}

        {{--function createSelect() {--}}
            {{--$('<select />', { 'id':'rating', 'name':'rating' }).appendTo('body');--}}

            {{--for (var i = 1; i <= 10; i++) {--}}
                {{--var attributes = (i == 5) ?--}}
                    {{--attributes = { 'value':i, 'selected':'selected' } :--}}
                    {{--attributes = { 'value':i };--}}

                {{--$('<option />', attributes).appendTo('#rating').html('rating-text-'+i);--}}
            {{--}--}}
        {{--}--}}

        {{--function destroySelect() {--}}
            {{--$('#rating').remove();--}}
        {{--}--}}



        {{--describe('bar rating plugin on init with custom options', function () {--}}

            {{--it('should update defaults', function () {--}}
                {{--var BarRating;--}}
                {{--BarRating = new $.fn.barrating.BarRating();--}}
                {{--BarRating.init({--}}
                    {{--showValues: false--}}
                {{--});--}}
                {{--expect(BarRating.options).to.be.a('object');--}}
                {{--expect(BarRating.options.theme).to.equal('');--}}
                {{--expect(BarRating.options.initialRating).to.equal(null);--}}
                {{--expect(BarRating.options.allowEmpty).to.equal(null);--}}
                {{--expect(BarRating.options.emptyValue).to.equal('');--}}
                {{--expect(BarRating.options.showValues).to.equal(false);--}}
                {{--expect(BarRating.options.showSelectedRating).to.equal(true);--}}
                {{--expect(BarRating.options.deselectable).to.equal(true);--}}
                {{--expect(BarRating.options.reverse).to.equal(false);--}}
                {{--expect(BarRating.options.readonly).to.equal(false);--}}
                {{--expect(BarRating.options.fastClicks).to.equal(true);--}}
                {{--expect(BarRating.options.hoverState).to.equal(true);--}}
                {{--expect(BarRating.options.silent).to.equal(false);--}}
                {{--expect(BarRating.options.triggerChange).to.equal(true);--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on show', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}
                {{--$('#rating').barrating('show');--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should have data', function () {--}}
                {{--expect($('#rating').data('barrating')).to.be.a('object');--}}
            {{--});--}}

            {{--it('should wrap the select field into a wrapper div', function () {--}}
                {{--expect($('.br-widget').parent().hasClass('br-wrapper')).to.equal(true);--}}
            {{--});--}}

            {{--it('should transform the select field into a rating widget', function () {--}}
                {{--expect($('.br-widget a')).to.have.length(10);--}}
            {{--});--}}

            {{--it('should store rating values in data attributes', function () {--}}
                {{--expect($('.br-widget a:first').attr('data-rating-value')).to.equal('1');--}}
                {{--expect($('.br-widget a:nth-child(8)').attr('data-rating-value')).to.equal('8');--}}
                {{--expect($('.br-widget a:first').attr('data-rating-text')).to.equal('rating-text-1');--}}
                {{--expect($('.br-widget a:nth-child(8)').attr('data-rating-text')).to.equal('rating-text-8');--}}
            {{--});--}}

            {{--it('should read the selected rating from the select field', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal('5');--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('rating-text-5');--}}
            {{--});--}}

            {{--it('should set correct class', function () {--}}
                {{--expect($('.br-widget a:nth-child(4)').hasClass('br-selected')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(5)').hasClass('br-selected br-current')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(6)').hasClass('br-selected')).to.equal(false);--}}
            {{--});--}}

            {{--it('should append a rating div', function () {--}}
                {{--expect($('div.br-current-rating')).to.have.length(1);--}}
            {{--});--}}

            {{--it('should display a correct rating', function () {--}}
                {{--expect($('div.br-current-rating').html()).to.equal(--}}
                    {{--$('#rating').data('barrating').ratingText--}}
                {{--);--}}
            {{--});--}}

            {{--it('should hide the select field', function () {--}}
                {{--expect($('#rating').css('display')).to.equal('none');--}}
            {{--});--}}

        {{--});--}}

        {{--describe('bar rating plugin on set fractional value', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}
                {{--$('#rating')--}}
                    {{--.barrating('show', { initialRating: 3.3 });--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should set .br-half class', function () {--}}
                {{--expect($('.br-widget a:nth-child(4)').hasClass('br-fractional')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(4)').hasClass('br-fractional-30')).to.equal(true);--}}
            {{--});--}}

        {{--});--}}

        {{--describe('bar rating plugin on set fractional value < 1', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}
                {{--$('#rating')--}}
                    {{--.barrating('show', { initialRating: 0.99 });--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should set .br-half class', function () {--}}
                {{--expect($('.br-widget a:first').hasClass('br-fractional')).to.equal(true);--}}
                {{--expect($('.br-widget a:first').hasClass('br-fractional-90')).to.equal(true);--}}
            {{--});--}}

        {{--});--}}

        {{--describe('bar rating themes', function() {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should set the theme class', function() {--}}
                {{--$('#rating').barrating({--}}
                    {{--theme: 'bootstrap-stars'--}}
                {{--});--}}

                {{--expect($('.br-wrapper').hasClass('br-theme-bootstrap-stars')).to.be.true;--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on show and rating selected', function () {--}}

            {{--var valuesFromCallback = [];--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--$('#rating').barrating('show', {--}}
                    {{--onSelect:function (value, text, event) {--}}
                        {{--valuesFromCallback.push(value, text, event);--}}
                    {{--}--}}
                {{--});--}}

                {{--$('.br-widget a:nth-child(2)').trigger('click');--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should update data', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal('2');--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('rating-text-2');--}}
            {{--});--}}

            {{--it('should set correct class', function () {--}}
                {{--expect($('.br-widget a:nth-child(1)').hasClass('br-selected')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(2)').hasClass('br-selected br-current')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(3)').hasClass('br-selected')).to.equal(false);--}}
            {{--});--}}

            {{--it('should display a correct rating', function () {--}}
                {{--expect($('div.br-current-rating').html()).to.equal(--}}
                    {{--$('#rating').data('barrating').ratingText--}}
                {{--);--}}
            {{--});--}}

            {{--it('should pass correct values to a callback', function () {--}}
                {{--expect(valuesFromCallback[0]).to.equal('2');--}}
                {{--expect(valuesFromCallback[1]).to.equal('rating-text-2');--}}
                {{--expect(valuesFromCallback[2]).to.be.a('object');--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on show and empty ratings are allowed', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--$('#rating').barrating('show', {--}}
                    {{--allowEmpty: true,--}}
                    {{--emptyValue: '-- not defined --'--}}
                {{--});--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should update data', function () {--}}
                {{--expect($('#rating').data('barrating').allowEmpty).to.equal(true);--}}
                {{--expect($('#rating').data('barrating').emptyRatingValue).to.equal('-- not defined --');--}}
                {{--expect($('#rating').data('barrating').emptyRatingText).to.equal('');--}}
            {{--});--}}

            {{--it('should set correct class', function () {--}}
                {{--expect($('#rating option').first().val()).to.equal('-- not defined --');--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin reversed', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--$('#rating').barrating('show', {--}}
                    {{--reverse:true--}}
                {{--});--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should set correct widget class', function () {--}}
                {{--expect($('.br-widget').hasClass('br-reverse')).to.equal(true);--}}
            {{--});--}}

            {{--it('should set correct class', function () {--}}
                {{--expect($('.br-widget a:nth-child(4)').hasClass('br-selected')).to.equal(false);--}}
                {{--expect($('.br-widget a:nth-child(5)').hasClass('br-selected br-current')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(6)').hasClass('br-selected')).to.equal(true);--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin read-only', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--$('#rating').barrating('show', {--}}
                    {{--readonly:true--}}
                {{--});--}}

                {{--$('.br-widget a:nth-child(6)').trigger('click');--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should set correct class', function () {--}}
                {{--expect($('.br-widget a:nth-child(4)').hasClass('br-selected')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(5)').hasClass('br-selected br-current')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(6)').hasClass('br-selected')).to.equal(false);--}}
            {{--});--}}

            {{--it('should ignore user input', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal('5');--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('rating-text-5');--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on deselect', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--// prepend empty OPTION to test deselectable ratings--}}
                {{--$('#rating').prepend($('<option />', { 'value':'' }));--}}
                {{--$('#rating').barrating('show');--}}

                {{--// deselect rating--}}
                {{--$('.br-widget a:nth-child(5)').trigger('click');--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should successfully deselect rating', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal('');--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('');--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on clear', function () {--}}

            {{--var valuesFromCallback = [];--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--$('#rating').barrating('show', {--}}
                    {{--onClear:function (value, text) {--}}
                        {{--valuesFromCallback.push(value, text);--}}
                    {{--}--}}
                {{--});--}}
                {{--$('.br-widget a:nth-child(6)').trigger('click');--}}
                {{--$('#rating').barrating('clear');--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--$('#rating').barrating('destroy');--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should restore original rating', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal('5');--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('rating-text-5');--}}
            {{--});--}}

            {{--it('should reset select field', function () {--}}
                {{--expect($('#rating').val()).to.equal('5');--}}
            {{--});--}}

            {{--it('should set correct class', function () {--}}
                {{--expect($('.br-widget a:nth-child(4)').hasClass('br-selected')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(5)').hasClass('br-selected br-current')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(6)').hasClass('br-selected')).to.equal(false);--}}
            {{--});--}}

            {{--it('should pass correct values to a callback', function () {--}}
                {{--expect(valuesFromCallback[0]).to.equal('5');--}}
                {{--expect(valuesFromCallback[1]).to.equal('rating-text-5');--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on destroy', function () {--}}

            {{--var valuesFromCallback = [];--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--$('#rating').barrating('show', {--}}
                    {{--onDestroy:function (value, text) {--}}
                        {{--valuesFromCallback.push(value, text);--}}
                    {{--}--}}
                {{--});--}}

                {{--$('#rating').barrating('destroy');--}}

            {{--});--}}

            {{--after(function () {--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should remove data', function () {--}}
                {{--expect($('#rating').data('barrating')).to.equal(undefined);--}}
            {{--});--}}

            {{--it('should unwrap the select field', function () {--}}
                {{--expect($('.br-wrapper').length).to.equal(0);--}}
            {{--});--}}

            {{--it('should remove the widget', function () {--}}
                {{--expect($('.br-widget').length).to.equal(0);--}}
            {{--});--}}

            {{--it('should show the select field back again', function () {--}}
                {{--expect($('#rating').is(":visible")).to.equal(true);--}}
            {{--});--}}

            {{--it('should pass correct values to a callback', function () {--}}
                {{--expect(valuesFromCallback[0]).to.equal('5');--}}
                {{--expect(valuesFromCallback[1]).to.equal('rating-text-5');--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on set value', function () {--}}

            {{--var valuesFromCallback = [];--}}
            {{--var changeCount = 0;--}}

            {{--before(function () {--}}
                {{--createSelect();--}}

                {{--$('#rating').change(function () {--}}
                    {{--changeCount++;--}}
                {{--});--}}

                {{--$('#rating').barrating('show', {--}}
                    {{--onSelect:function (value, text) {--}}
                        {{--valuesFromCallback.push(value, text);--}}
                    {{--}--}}
                {{--}).barrating('set', 3);--}}

            {{--});--}}

            {{--after(function () {--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should set correct value', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal(3);--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('rating-text-3');--}}
            {{--});--}}

            {{--it('should set correct class', function () {--}}
                {{--expect($('.br-widget a:nth-child(3)').hasClass('br-selected')).to.equal(true);--}}
                {{--expect($('.br-widget a:nth-child(3)').hasClass('br-current')).to.equal(true);--}}
            {{--});--}}

            {{--it('should pass correct values to a callback', function () {--}}
                {{--expect(valuesFromCallback[0]).to.equal(3);--}}
                {{--expect(valuesFromCallback[1]).to.equal('rating-text-3');--}}
            {{--});--}}

            {{--it('should trigger change event on the select field', function () {--}}
                {{--expect(changeCount).to.equal(1);--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on set non-existing value', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}
                {{--$('#rating')--}}
                    {{--.barrating('show', { initialRating: 5 })--}}
                    {{--.barrating('set', 9999);--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should do nothing', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal('5');--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('rating-text-5');--}}
            {{--});--}}

        {{--});--}}


        {{--describe('bar rating plugin on change read-only state', function () {--}}

            {{--before(function () {--}}
                {{--createSelect();--}}
                {{--$('#rating')--}}
                    {{--.barrating('show', { initialRating: 1 })--}}
                    {{--.barrating('readonly', true);--}}

                {{--$('.br-widget a:last-child').trigger('click');--}}
            {{--});--}}

            {{--after(function () {--}}
                {{--destroySelect();--}}
            {{--});--}}

            {{--it('should ignore user input', function () {--}}
                {{--expect($('#rating').data('barrating').ratingValue).to.equal('1');--}}
                {{--expect($('#rating').data('barrating').ratingText).to.equal('rating-text-1');--}}
            {{--});--}}

            {{--it('should update data', function () {--}}
                {{--expect($('#rating').data('barrating').readOnly).to.equal(true);--}}
            {{--});--}}

            {{--it('should set correct widget class', function () {--}}
                {{--expect($('.br-widget').hasClass('br-readonly')).to.equal(true);--}}
            {{--});--}}

        {{--});--}}
    {{--</script>--}}


@endpush



