@extends('layouts.index')
@section('content')

    <div class="main-register">
        <div class="register-area">
            <div class="register-area-right">
                <h4>به جمع کاربران آکادمی مدیران ایده پرداز بپیوندید : </h4>
                <div style="border-bottom: 1px dotted #ddd;margin: 13px 0;"></div>
                <p>کد فعالسازی حساب کاربری به ایمیل شما ارسال خواهد شد. لطفا ایمیل صحیح وارد کنید</p>

                @include('flash::message')
                @include('adminlte-templates::common.errors')

                {!! Form::open(['route'=>['user.register' ], 'method' => 'post' ]) !!}
                <ul>
                    <li>
                        <label for="">
                            <i class="fas fa-user" style="margin-top: 13px;"></i>
                            نام
                        </label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Your Name']) !!}
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </li>
                    <li>
                        <label for="">
                            <i style="background-position: -266px -790px"></i>
                            شماره موبایل
                        </label>
                        {!! Form::text('tel', null, ['class' => 'form-control', 'placeholder' => 'Enter Your Mobile Number']) !!}
                        @if ($errors->has('tel'))
                            <span class="help-block"><strong>{{ $errors->first('tel') }}</strong></span>
                        @endif
                    </li>
                    <li>
                        <label for=""><i style="background-position: -317px -790px"></i>
                            پست الکترونیک
                        </label>
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Your Email']) !!}
                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </li>
                    <li>
                        <label for=""><i style="background-position: -364px -790px"></i>
                            رمز عبور
                        </label>
                        {!! Form::password('password', null, ['class' => 'form-control', 'placeholder' => 'Enter Password']) !!}
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </li>
                    <li>
                        <label for=""><i style="background-position: -364px -790px"></i>
                            تکرار رمز عبور
                        </label>
                        {!! Form::password('password_confirmation', null, ['class' => 'form-control', 'placeholder' => 'Enter Password']) !!}
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                        @endif
                    </li>
                    <div class="reg-check-area">
                        <input class="register-check-box" type="checkbox" required>
                        <p class="low"><a href="">حریم خصوصی </a>و <a href=""> شرایط و قوانین </a>خدمات و سرویس های
                            آکادمی مدیران ایده پردازان را خوانده ام و آنها را قبول دارم</p>
                        <div class="new-check-box-uncheck"></div>
                    </div>
                </ul>

                <div class="reg-submit" style="margin-top: 20px;">
                    <div class="reg-submit-icon"></div>
                    <input type="submit" name="register" value="ثبت نام در آکادمی ایده پردازان"
                           class="reg-submit-button">
                </div>

                {!! Form::close() !!}

                <div style="border-bottom: 1px dotted #ddd;margin: 40px 0;"></div>
                <p class="reg-text">قبلا در آکادمی ایده پردازان ثبت نام نموده اید؟ <a style="cursor: pointer;">برای ورود
                        کلیک کنید</a></p>
            </div>
            <div class="register-area-left">
                <div class="reg-left-icon">
                    <i></i>
                </div>
                <ul>
                    <li><i style="background-position: -1040px -575px"></i>به تمام محتوای رایگان سایت دسترسی داشته باشید
                    </li>
                    <li><i style="background-position: -911px -241px"></i>به دوره های خریداری شده خود در هر زمان و مکان
                        دسترسی داشته باشید
                    </li>
                    <li><i style="background-position: -978px -203px"></i>امکان درج نظرات و دادن امتیاز به دوره های
                        خریداری
                        شده
                    </li>
                    <li><i style="background-position: -105px -265px"></i>امکان پرسش و پاسخ با مدرس دوره ها و برخورداری
                        از
                        پشتیبانی آنلاین
                    </li>
                    <li><i style="background-position: -979px -164px"></i>اطاع از تخفیف های دوره ای و مناسبتی</li>
                    <li><i style="background-position: -980px -288px"></i>امکان شرکت در دوره های حضوری و کارگاه های
                        آموزشی و
                        دریافت گواهینامه
                    </li>
                </ul>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        /*کد های جی کوئری چک باکس*/
        $('.register-check-box').click(function () {
            if ($(this).is(':checked')) {
                $('.new-check-box-uncheck').addClass('new-check-box');
            } else {
                $('.new-check-box-uncheck').removeClass('new-check-box');
            }
        });
        $('.login-check-box').click(function () {
            if ($(this).is(':checked')) {
                $('.uncheck-login').addClass('new-log-check-box');
            } else {
                $('.uncheck-login').removeClass('new-log-check-box');
            }
        });
        $('.register-area .reg-text a').click(function () {
            $('#modal-box').fadeIn(300);
            $('.modal-box-area').fadeIn(300);
        });
        $('#top-login').click(function () {
            $('#modal-box').fadeIn(300);
            $('.modal-box-area').fadeIn(300);
        });
        $('.modal-box-area .close').click(function () {
            $('#modal-box').fadeOut(200);
            $('.modal-box-area').fadeOut(200);
        });
    </script>
@endpush