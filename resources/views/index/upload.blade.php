@extends('layouts.app')
@section('content')

    <section class="content-header">
        <h1 class="pull-left">صفحه آپلود</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('products.create') !!}">لیست فایل ها</a>
        </h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                @if(!Auth::check())
                    {{ die() }}
                @else
                    <br>
                    {{--        @can('is-writer')--}}

                    {{--@include('partials.header')--}}

                    {!! Form::open(['route'=>['upload.post' ], 'method' => 'post', 'files' => true ]) !!}

                    <input type="file" name="file1">
                    @if(isset($url1))
                        <div>{{ $url1 }}</div>@endif
                    <br>
                    <input type="file" name="file2">
                    @if(isset($url2))
                        <div>{{ $url2 }}</div>@endif
                    <br>
                    <input type="file" name="file3">
                    @if(isset($url3))
                        <div>{{ $url3 }}</div>@endif
                    <br>
                    <input type="file" name="file4">
                    @if(isset($url4))
                        <div>{{ $url4 }}</div>@endif
                    <br>
                    <input type="file" name="file5">
                    @if(isset($url5))
                        <div>{{ $url5 }}</div>@endif
                    <br>
                    <button type="submit" class="btn btn-primary">آپلود</button>

                    {!! Form::close() !!}

                    <br><br><br><br><br>
                    {{--@include('partials.footer')--}}

                    {{--@endcan--}}
                @endif
            </div>
        </div>
    </div>
@endsection