@extends('layouts.index')
@section('header')
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    {{--    <link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')
    <div style="padding: 20px">

        {{--'factor', 'products', 'discounts', 'discounts2'--}}
        <div>
            <h2> فاکتور فروش:</h2>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>شماره فاکتور</th>
                    <th>نام مشتری</th>
                    <th>مجموع قیمت ها</th>
                    <th>مجموع تخفیف ها</th>
                    @if($factor->is_by_post == 1)
                    <th>ارسال پستی</th>
                    <th>هزینه ارسال پستی</th>
                    @endif
                    <th>مبلغ قابل پرداخت</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$factor->id}}</td>
                    <td>{{$factor->user->name}}</td>
                    <td>{{$factor->sum}}</td>
                    <td>{{$factor->discount_sum}}</td>
                    @if($factor->is_by_post == 1)
                    <td>بله</td>
                    <td>{{$factor->post_amount}}</td>
                    @endif
                    <td>{{$factor->amount}}</td>
                </tr>
                </tbody>
            </table>

            {!! Form::open(['route'=>['functionGate', $factor->id ], 'method' => 'get' ]) !!}
            <button class="cusmo-btn narrow pull-right" type="submit">
                مجموع
                :
                {{ $factor->amount }}
                تومان
                <br>
                برای پرداخت کلیک کنید
            </button>
            {!! Form::close() !!}

        </div>
        <hr>
        <br>
        <br>
        <br>
        <br>
        <div>
            <h4>لیست محصولات:</h4>
            <hr>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>کد محصول</th>
                    <th>عنوان</th>
                    <th>قیمت</th>
                    <th>تخفیف</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{$product->title}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->discount}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <br>
        @if(isset($discounts) || isset($discounts2))
        <div>
            <h4>لیست تخفیف های استفاده شده در این خرید:</h4>
            <hr>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>عنوان تخفیف</th>
                    <th>کد تخفیف</th>
                    <th>مقدار تخفیف</th>
                </tr>
                </thead>
                <tbody>
                @foreach($discounts as $discount)
                    <tr>
                        <td>{{$discount->name_fa}}</td>
                        <td>{{$discount->code}}</td>
                        <td>{{$discount->price}}@if($discount->is_percent == 0) <span> تومان </span> @else <span> درصد </span> @endif</td>
                    </tr>
                @endforeach
                @foreach($discounts2 as $discount)
                    <tr>
                        <td>{{$discount->name_fa}}</td>
                        <td>{{$discount->code}}</td>
                        <td>{{$discount->price}}@if($discount->is_percent == 0) <span> تومان </span> @else <span> درصد </span> @endif</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif

    </div>
@endsection