@extends('layouts.index')
@section('content')
    <section class="content-header">
        <h1>
            ارسال تیکت
        </h1>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">
                <div class="row">
                    {!! Form::open(['route'=>['commentAnswer', $comment->id ], 'method' => 'POST' ]) !!}

                        <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                            {!! Form::label('text', 'متن') !!}
                            {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('text'))
                                <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                            @endif
                        </div>

                        <button type="submit" >ارسال</button>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
