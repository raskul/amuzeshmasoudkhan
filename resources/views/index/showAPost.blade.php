@extends('layouts.index')
@section('content')

    @include('partials.header')

    @include('flash::message')
    @include('adminlte-templates::common.errors')

    <div id="main">

        <div class="blogarea" style="font-family: iran-sans">

        <h1>{{$post->title}}</h1>
        <br>
        {!! $post->text !!}

        </div>

        @include('partials.footer')
    </div>

@endsection