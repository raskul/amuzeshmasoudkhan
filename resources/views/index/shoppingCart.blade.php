@extends('layouts.index')
@section('content')

    <div id="main">

        @include('partials.header')

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        @php
            if($products == ''){
                return redirect()->back();
            }
        @endphp

        <div class="show_cart_main">
            <div class="show_cart_table">
                <div class="show_cart_title">
                    <div class="title">سبد خرید شما در آکادمی مدیران ایده پرداز</div>
                </div>

                <table cellspacing="0">
                    <tr>
                        <td width="50%">شرح دوره</td>
                        <td width="16%">تخفیف</td>
                        <td width="16%">قیمت</td>
                        <td width="16%">قیمت کل</td>
                    </tr>
                    @foreach($products as $MyProduct)
                        @php
                            $product = \App\Models\Product::find($MyProduct->id);
                            $duration = 0;
                            foreach ($product->productPosts as $productPost){
                                $duration += $productPost->duration;
                            }
                        @endphp
                        <tr>
                            <td style="width: 50%;">
                                <div class="right_img">
                                    <img src="{{showProductImage($product)}}" style="width: 100px;height: 100px" alt="">
                                </div>
                                <div class="left_details">
                                    <h5><a href="{{ route('index.index') }}/{{createProductLinkCreate($product)}}">{{$product->title}}</a></h5>
                                    <ul>
                                        <li><i class="fas fa-user"></i>مدرس :
                                            {{$product->user->name}}
                                        </li>
                                        <li><i class="fas fa-clock"></i>زمان دوره :
                                            {{$duration}}
                                            دقیقه
                                        </li>
                                        <li><i class="fas fa-list-alt"></i>تعداد جلسات :
                                            {{count($product->productPosts)}}
                                            جلسه
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td style="text-align: center;font-family: iran-sans;color: #555;width: 16%;">
                                {{$product->discount}}
                                تومان
                            </td>
                            <td style="text-align: center;font-family: iran-sans;color: #555;width: 16%;">
                                {{$product->price}}
                                تومان
                            </td>
                            <td style="text-align: center;font-family: iran-sans;color: #555;width: 16%;">
                                {!! Form::open(['route'=>['remove.from.basket', $product->id ], 'method' => 'post' ]) !!}
                                <div class="remove_icon" style="display: inline;width: 28px;background-color: white">
                                    <button type="submit" style=";height: 20%;margin: 55px 0 0 0;padding: 0"><i
                                                class="fas fa-times-circle"
                                                style="margin: 7px 0 0 2px;padding: auto"></i></button>
                                </div>
                                <div class="total_price" style="display: inline;width: 120px">
                                    {{$product->price - $product->discount}}
                                    تومان
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
                <div class="discount_code">

                    <div class="discount">
                        @if(auth()->check())
                            {!! Form::open(['route'=>['discount.post'], 'method' => 'post']) !!}
                            <h5>آیا کد تخفیف دارید؟</h5>
                            <p style="font-family: yekan;font-size: 10pt;">در صورتی که کد تخفیفی در اختیار دارید برای
                                استفاده از آن کد را اینجا وارد کنید</p>

                            <input type="text"
                                   style="height: 35px;width: 300px;background-color: #ebdaff;border-top-right-radius: 20px;border-bottom-right-radius: 20px;border: 1px solid black;padding-right: 15px;"
                                   placeholder="کد تخفیف را وارد کنید" name="code">
                            <input type="submit" value="اعمال کد تخفیف"
                                   style="background-color: #9595ff;width: 100px;height: 35px;border-bottom-left-radius: 20px;border-top-left-radius: 20px;border: 1px solid black;margin-right: -5px">
                            {!! Form::close() !!}


                            @if(count($discounts) || count($discounts2))
                                <h3>لیست تخفیف ها</h3>
                                <table class="">
                                    <thead>
                                    <tr>
                                        <th>نام</th>
                                        <th>کد</th>
                                        <th>قیمت</th>
                                        <th>تاریخ شروع تخفیف</th>
                                        <th>تاریخ پایان تخفیف</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($discounts as $discount)
                                        <tr>
                                            <td>{{$discount->name_fa}}</td>
                                            <td>{{$discount->code}}</td>
                                            <td>{{$discount->price}}
                                                @if($discount->is_percent == 1)
                                                    <span> درصد </span>
                                                @else
                                                    <span> تومان </span>
                                                @endif
                                                <span style="font-size: x-small;color: grey"> (در بعضی از محصولات محاسبه میشود)</span>
                                            </td>
                                            <td>{{pDate($discount->start_at)}}</td>
                                            <td>{{pDate($discount->end_at, true)}}</td>
                                        </tr>
                                    @endforeach
                                    @foreach($discounts2 as $discount)
                                        <tr>
                                            <td>{{$discount->name_fa}}</td>
                                            <td>{{$discount->code}}</td>
                                            <td>{{$discount->price}}
                                                @if($discount->is_percent == 1)
                                                    <span> درصد </span>
                                                @else
                                                    <span> تومان </span>
                                                @endif
                                                <span style="font-size: x-small;color: grey"> (بن خرید در فاکتور محاسبه محاسبه میشود)</span>
                                            </td>
                                            <td>{{pDate($discount->start_at)}}</td>
                                            <td>{{pDate($discount->end_at, true)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <hr>
                            @endif

                        @else
                            <p style="font-family: yekan;font-size: 10pt;">در صورتی که کد تخفیفی دارید برای استفاده از
                                آن باید وارد سایت شوید.</p>
                        @endif

                    </div>

                    <div class="total_buy">
                        <div class="total_pay_cart">
                            مجموع خرید شما : <span>
                                {{$sum}}
                                تومان</span>

                            <i>تخفیف شما :
                                {{$dis}}
                                تومان</i>
                        </div>
                        <div class="total_pay_you">
                            مجموع پرداختی شما : <span>
                                {{$sum - $dis}}
                                تومان</span>
                        </div>
                        <div class="clear"></div>
                        <div class="final_pay" style="background-color: blue">

                            @if(Auth::check())

                                {!! Form::open(['route'=>['check.out' ], 'method' => 'GET' ]) !!}

                                <button type="submit" style="width: 500px;height: 40px;margin: auto;padding: auto;" class="final_pay_price">
                                    <div class="final_pay_text"  style="font-size: x-large">
                                        تایید نهایی سفارش و پرداخت :
                                    </div>
                                    <div class="final_pay_price" style="font-size: x-large">
                                        {{$sum - $dis}}
                                        تومان
                                        <i class="fas fa-shopping-cart"></i>
                                    </div>
                                </button>

                                <input type="hidden" name="is_by_post" id="is_by_post" value="no">

                                {!! Form::close() !!}

                            @else
                                <div class="">
                                    برای پرداخت ابتدا باید ثبت نام کنید یا وارد سایت شوید.
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="login_register_area">
                    <div class="login">
                        <h6>آیا تمایل به دریافت پستی محصول خریداری شده را دارید؟</h6>
                        <p>در صورتی که این گزینه را تیک بزنید دوره آموزشی خریداری شده توسط پست پیشتاز برای شما ارسال
                            خواهد شد. و در صورتی که تیک این گزینه را نزنید می توانید مجموعه آموزشی را به صورت لینک
                            دانلود در اختیار داشته باشید.</p>

                        @if(Auth::check())

                            <div class="post_checkbox">
                                <input class="chk_post" type="checkbox" name="send_by_post" id="send_by_post"
                                       onclick="my_check()">
                                <i></i>
                                <span>دریافت محصول از طریق پست پیشتاز (شامل هزینه پستی)</span>
                            </div>

                        @else
                            <span class="">
                                برای خرید پستی ابتدا باید ثبت نام کنید یا وارد سایت شوید.
                            </span>
                        @endif

                    </div>

                    @if(!(Auth::check()))

                        {!! Form::open(['route'=>['fastRegister' ], 'method' => 'POST' ]) !!}
                        <div class="register">
                            <h6>قبلا ثبت نام نکرده اید؟ تنها با وارد کردن ایمیل ثبت نام کنید!</h6>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::text('email', null, ['placeholder' => 'آدرس ایمیل']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                            <div class="clear"></div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                {!! Form::text('password', null, ['placeholder' => 'رمز عبور']) !!}
                                @if ($errors->has('password'))
                                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                            </div>
                            <div class="register_button">
                                <button type="submit"><i class="fas fa-user-plus"></i>ثبت نام سریع</button>
                            </div>
                            <div class="clear"></div>
                            <div class="login_modal">
                                قبلا ثبت نام کرده اید؟ <span class="lo_modal">وارد شوید</span>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    @endif

                </div>
                <div class="post_form_area">

                    @if(auth()->check())

                        <h6>آدرس پستی دقیق خود را در این بخش وارد کنید!</h6>
                        <p>در صورتی که از سرعت اینترنت پایینی برخوردار هستید و یا تمایل دارید مجموعه آموزشی خریداری شده
                            را
                            بر روی سی دی دریافت کنید لطفا فرم زیر را با دقت پر کنید</p>


                        {!! Form::model($user, ['route'=>['editUserAddress' ], 'method' => 'patch' ]) !!}

                        <div class="post_form_right">

                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for=""><i class="fas fa-user"></i>نام</label>
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('tel') ? ' has-error' : '' }}">
                                <label for=""><i class="fas fa-mobile-alt"></i>شماره موبایل</label>
                                {!! Form::text('tel', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('tel'))
                                    <span class="help-block"><strong>{{ $errors->first('tel') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                <label for=""><i class="fas fa-phone-square"></i>شماره ثابت</label>
                                {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('telephone'))
                                    <span class="help-block"><strong>{{ $errors->first('telephone') }}</strong></span>
                                @endif
                            </div>


                        </div>
                        <div class="post_form_left">

                            <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                                <label for=""><i class="fab fa-leanpub"></i>نام استان</label>
                                {!! Form::text('state', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('state'))
                                    <span class="help-block"><strong>{{ $errors->first('state') }}</strong></span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                <label for=""><i class="fas fa-map-signs"></i>نام شهر</label>
                                {!! Form::text('city', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('city'))
                                    <span class="help-block"><strong>{{ $errors->first('city') }}</strong></span>
                                @endif
                            </div>


                            <div class="form-group {{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                <label for=""><i class="fas fa-address-book"></i>کد پستی</label>
                                {!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
                                @if ($errors->has('postal_code'))
                                    <span class="help-block"><strong>{{ $errors->first('postal_code') }}</strong></span>
                                @endif
                            </div>

                            <div class="text {{ $errors->has('address') ? ' has-error' : '' }}">
                                {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'آدرس دقیق پستی', 'col' => 30, 'row' => '10']) !!}
                                @if ($errors->has('address'))
                                    <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                                @endif
                            </div>

                            <div class="submit_post">
                                <input type="submit" value="ثبت اطلاعات پستی">
                            </div>


                        </div>
                        {!! Form::close() !!}

                    @endif

                </div>
            </div>
        </div>


        @include('partials.footer')

    </div>

@endsection

@push('scripts')

    {{--send by post--}}
    <script>
        function my_check() {
            if (document.getElementById("is_by_post").value == 'no') {
                document.getElementById("is_by_post").value = 'yes';
            }else {
                document.getElementById("is_by_post").value = 'no';
            }
        }
    </script>

    <script>


        /*start select jquery code*/

        var order_num = $(".order_num_item");

        $(".order_ul").fadeOut(0);

        $(".num_order").mouseenter(function () {
            $(this).find("ul").fadeIn(200);
            order_num.click(function () {
                var text = $(this).text();
                $(this).parents(".num_order").find("span").attr("value", (text)).text(text);
            });

        });
        $(".num_order").mouseleave(function () {
            $(this).find("ul").fadeOut(0);
        });
        $(".order_ul").mouseleave(function () {
            $(this).find("ul").fadeOut(0);
        });


        /*start remove jquery code*/

        $(".remove_icon").click(function () {
            $(this).parents("tr").hide(0)
        });

        /*start modal for login*/
        $(".lo_modal").click(function () {
            $('#modal-box').fadeIn(300);
            $('.modal-box-area').fadeIn(300);
        });


        /*start check box jquery*/

        $(".chk_post").click(function () {
            if ($(this).is(':checked')) {
                $(this).parent().find("i").addClass('fas fa-check');
                $(".post_form_area").slideDown(500);
            } else {
                $(this).parent().find("i").removeClass('fas fa-check');
                $(".post_form_area").slideUp(500);
            }
        })


    </script>
@endpush

















{{--@extends('layouts.index')--}}
{{--@section('header')--}}
    {{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
    {{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">--}}
{{--@endsection--}}
{{--@section('content')--}}

    {{--@include('partials.miniHeader')--}}

{{--<section class="section-shopping-cart">--}}
{{--<div class="buttons-holder">--}}
{{--<a class="cusmo-btn gray narrow" href="{{ route('index.index') }}">اضافه کردن محصولات--}}
{{--دیگر</a>--}}
{{--<a class="cusmo-btn  narrow" href="{{ route('check.out') }}">تکمیل فرایند خرید</a>--}}
{{--</div>--}}
{{--<div class="container">--}}
{{--<div class="row-fluid">--}}


{{--<div class="span12">--}}
{{--<div class="page-content shopping-cart-page ">--}}

{{--@include('flash::message')--}}

{{--<div class="email-holder">--}}
{{--{!! Form::open(['route'=>['discount.post'], 'method' => 'post']) !!}--}}
{{--<div class="input-group email-field">--}}
{{--<input type="text" name="code"--}}
{{--style="border-bottom-left-radius: 0;border-top-left-radius: 0"--}}
{{--class="required email" placeholder="اگر کد تخفیف دارید وارد کنید">--}}
{{--<button class="cusmo-btn narrow input-group" type="submit"--}}
{{--style="height: 44px;border-bottom-right-radius: 0;border-top-right-radius: 0">--}}
{{--محاسبه کد تخفیف--}}
{{--</button>--}}
{{--</div>--}}
{{--{!! Form::close() !!}--}}
{{--</div>--}}

{{--@if(count($discounts) || count($discounts2))--}}
{{--<h3>لیست تخفیف ها</h3>--}}
{{--<table class="table table-hover">--}}
{{--<thead>--}}
{{--<tr>--}}
{{--<th>نام</th>--}}
{{--<th>کد</th>--}}
{{--<th>قیمت</th>--}}
{{--<th>تاریخ شروع تخفیف</th>--}}
{{--<th>تاریخ پایان تخفیف</th>--}}
{{--</tr>--}}
{{--</thead>--}}
{{--<tbody>--}}
{{--@foreach($discounts as $discount)--}}
{{--<tr>--}}
{{--<td>{{$discount->name_fa}}</td>--}}
{{--<td>{{$discount->code}}</td>--}}
{{--<td>{{$discount->price}}--}}
{{--@if($discount->is_percent == 1)--}}
{{--<span> درصد </span>--}}
{{--@else--}}
{{--<span> تومان </span>--}}
{{--@endif--}}
{{--<span style="font-size: x-small;color: grey"> (در بعضی از محصولات محاسبه میشود)</span>--}}
{{--</td>--}}
{{--<td>{{pDate($discount->start_at)}}</td>--}}
{{--<td>{{pDate($discount->end_at, true)}}</td>--}}
{{--</tr>--}}
{{--@endforeach--}}
{{--@foreach($discounts2 as $discount)--}}
{{--<tr>--}}
{{--<td>{{$discount->name_fa}}</td>--}}
{{--<td>{{$discount->code}}</td>--}}
{{--<td>{{$discount->price}}--}}
{{--@if($discount->is_percent == 1)--}}
{{--<span> درصد </span>--}}
{{--@else--}}
{{--<span> تومان </span>--}}
{{--@endif--}}
{{--<span style="font-size: x-small;color: grey"> (بن خرید در فاکتور محاسبه محاسبه میشود)</span>--}}
{{--</td>--}}
{{--<td>{{pDate($discount->start_at)}}</td>--}}
{{--<td>{{pDate($discount->end_at, true)}}</td>--}}
{{--</tr>--}}
{{--@endforeach--}}
{{--</tbody>--}}
{{--</table>--}}
{{--<hr>--}}
{{--@endif--}}

{{--@if($products != '')--}}

{{--<table class="table ">--}}
{{--<thead>--}}
{{--<tr>--}}
{{--<th class="span2">تصویر</th>--}}
{{--<th class="span5">نام دوره</th>--}}
{{--<th class="span2 price-column" style="min-width: 100px">قیمت</th>--}}
{{--<th class="span2 price-column" style="min-width: 100px">تخفیف</th>--}}
{{--<th class="span1 price-column" style="min-width: 100px">جمع</th>--}}
{{--<th class="span2">&nbsp;</th>--}}
{{--</tr>--}}
{{--</thead>--}}
{{--<tbody>--}}

{{--@foreach($products as $product)--}}
{{--<tr>--}}
{{--<td>--}}
{{--<div class="thumb">--}}
{{--<img alt="" src="{{ $product->image_main }}" width="100px" height="100px"/>--}}
{{--</div>--}}
{{--</td>--}}
{{--<td>--}}
{{--<div class="desc">--}}
{{--<h3>{{ $product->title }}</h3>--}}
{{--<div class="tag-line">--}}
{{--{{ $product->description }}--}}
{{--</div>--}}
{{--<div class="pid">کد محصول: {{ $product->id }}</div>--}}
{{--</div>--}}
{{--</td>--}}

{{--<td>--}}
{{--<div class="price">--}}
{{--{{ $product->price }}--}}
{{--</div>--}}
{{--</td>--}}
{{--<td>--}}
{{--<div class="price">--}}
{{--{{ $product->discount }}--}}
{{--</div>--}}
{{--</td>--}}
{{--<td>--}}
{{--<div class="price">--}}
{{--{{ ($product->price)-($product->discount) }}--}}
{{--</div>--}}
{{--</td>--}}
{{--{!! Form::open(['route'=>['remove.from.basket', $product->id ], 'method' => 'post' ]) !!}--}}
{{--<td>--}}
{{--<div>--}}
{{--<button type="submit" class="btn-danger btn">حذف</button>--}}
{{--</div>--}}
{{--</td>--}}
{{--{!! Form::close() !!}--}}
{{--</tr>--}}
{{--@endforeach--}}

{{--</tbody>--}}
{{--</table>--}}

{{--<div class="buttons-holder">--}}
{{--<a class="cusmo-btn gray narrow" href="{{ route('index.index') }}">اضافه کردن محصولات--}}
{{--دیگر</a>--}}
{{--<a class="cusmo-btn narrow" href="{{ route('check.out') }}">تکمیل فرایند خرید</a>--}}
{{--</div>--}}


{{--@else--}}
{{--<div> هیچ محصولی در سبد شما وجود ندارد.</div>--}}
{{--<div class="buttons-holder pull-right">--}}
{{--<a class="cusmo-btn gray narrow" href="{{ route('index.index') }}">بازگشت</a>--}}
{{--</div>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}



    {{--@include('partials.footer')--}}
    {{--@include('partials.miniFooter')--}}

{{--@endsection--}}