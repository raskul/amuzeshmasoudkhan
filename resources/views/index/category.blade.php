@extends('layouts.index')
@push('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

    <style>
        .filterbtn{
            width: 60px;
            float: left;
            height: 40px;
            background-color: #00A5FF;
            border-radius: 7px;
            margin-left: 20px;
            font-size: large;
        }
    </style>

@endpush
@section('content')

    <div id="main">

        @include('partials.header')

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        @php
            $catTop = \App\Models\Category::where('name', $cat)->first();
            if (!(is_null($catTop))){
                $catHeader = $catTop->name_fa;

                $catText[] = ['name' => $catTop->name, 'name_fa' => $catTop->name_fa];
                if($catTop->father->id != 1){
                    $catText[] = ['name'=>$catTop->father->name, 'name_fa'=>$catTop->father->name_fa];
                    $catTop = $catTop->father;
                }
                if($catTop->father->id != 1){
                    $catText[] = ['name'=>$catTop->father->name, 'name_fa'=>$catTop->father->name_fa];
                    $catTop = $catTop->father;
                }
                if($catTop->father->id != 1){
                    $catText[] = ['name'=>$catTop->father->name, 'name_fa'=>$catTop->father->name_fa];
                    $catTop = $catTop->father;
                }

                $catText = array_reverse($catText);
            }else{
                $catHeader = 'همه محصولات';
                $catText =[];
            }
            $i = 0;
        @endphp

        {!! Form::open(['route'=>['category', $cat], 'method' => 'GET', 'id' => 'filterForm' ]) !!}

        <div class="cat-search">
            <div class="cat-search-right">
                {{--<div class="cat-img">--}}
                {{--<img style="width: 80px;height: 80px;border-radius: 100%;" src="assets/images/learn.jpg" alt="">--}}
                {{--</div>--}}
                <h2>{{$catHeader}}</h2>
                <div class="nav-bar">
                    <ul>

                        <li><a href="{{route('index.index')}}">
                                صفحه تخست
                            </a>
                            <i></i>
                        </li>

                        <li><a href="{{route('category', null)}}">
                                همه محصولات
                            </a>
                            <i></i>
                        </li>

                        @foreach($catText as  $catTxt)
                            <li><a href="{{route('category', $catTxt['name'])}}">
                                    {{$catTxt['name_fa']}}
                                </a>
                                @if(count($catText) > $i+1)
                                    <i></i>
                                @endif
                            </li>
                            @php
                                $i++;
                            @endphp
                        @endforeach

                    </ul>
                </div>
            </div>
            <div class="cat-search-center">

                <input id="seach" type="text" placeholder="{{$search}}" name="search">
                <button type="submit" class="search-bottom"><i></i></button>

            </div>
            <div class="cat-search-left" id="search-pills">
                <ul>
                    <li
                            @if((strpos(url()->full(), 'price_all')) || ((!(strpos(url()->full(), 'price_free')))&&(!(strpos(url()->full(), 'price_not_free')))))
                            class="active"
                            @endif
                            style="width: 33.333%"
                            id="search-pills-all">
                        <div>

                            <input type="checkbox" name="price_all" id="all"
                                   @if((strpos(url()->full(), 'price_all')) || ((!(strpos(url()->full(), 'price_free')))&&(!(strpos(url()->full(), 'price_not_free')))))
                                   checked="checked"
                                    @endif
                            >
                        </div>
                        <lable style="font-family: iran-sans">همه</lable>
                    </li>
                    <li style="width: 33.333%"
                        @if(strpos(url()->full(), 'price_free'))
                        class="active"
                        @endif
                        id="search-pills-free"
                    >
                        <div>
                            <input type="checkbox" name="price_free"
                                   @if(strpos(url()->full(), 'price_free'))
                                   checked="checked"
                                    @endif

                            >
                        </div>
                        <lable style="font-family: iran-sans">رایگان</lable>
                    </li>
                    <li style="width: 33.333%"
                        @if(strpos(url()->full(), 'price_not_free'))
                        class="active"
                        @endif
                        id="search-pills-purchasable"
                    >
                        <div>
                            <input type="checkbox" name="price_not_free"
                                   @if(strpos(url()->full(), 'price_not_free'))
                                   checked="checked"
                                    @endif
                            >
                        </div>
                        <lable style="font-family: iran-sans">خریدنی</lable>
                    </li>

                </ul>
            </div>
        </div>
        <div class="cat-contant-list">
            <div class="cat-contant-list-filters">

                <section>
                    <h4><i></i>فیلتر بر اساس نوع دوره</h4>
                    <ul>

                        <li class="cat_filter">
                            <div
{{--                                                                        {{dd(count($productTypeArray))}}--}}
                                    @if(((count($productTypeArray)) == (\App\Models\ProductType::get()->count()) )||(count($productTypeArray) == 0))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="type_all"
                                       @if(((count($productTypeArray)) == (\App\Models\ProductType::get()->count()) )||(count($productTypeArray) == 0))
                                       checked
                                        @endif
                                >
                            </div>
                            <label>همه مجموعه ها</label>
                        </li>

                        @foreach($productTypes as $productType )

                            <li class="cat_filter">
                                <div
                                        @if(strpos(url()->full(), 'type_'.$productType->name))
                                        class="cat_filter_active"
                                        @else
                                        class="cat_filter"
                                        @endif
                                >
                                    <input type="checkbox" name="type_{{$productType->name}}"
                                           @if(strpos(url()->full(), 'type_'.$productType->name))
                                           checked
                                            @endif
                                    >
                                </div>
                                <label>{{$productType->name_fa}}</label>
                            </li>
                        @endforeach

                    </ul>

                    <button class="filterbtn" type="submit" >فیلتر</button>
                    <div class="clear"></div>

                </section>
                <div style="width: 90%;height: 2px;background-color: #ececec;margin: 15px auto;"></div>
                <section>
                    <h4><i></i>فیلتر بر اساس امتیاز</h4>
                    <ul>
                        <li class="cat_filter">

                            <div
                                    @if((strpos(url()->full(), 'sort_all')) || (
                                    (!(strpos(url()->full(), 'sort_vote')))&&
                                    (!(strpos(url()->full(), 'sort_sold')))&&
                                    (!(strpos(url()->full(), 'sort_visit')))&&
                                    (!(strpos(url()->full(), 'sort_price_high')))&&
                                    (!(strpos(url()->full(), 'sort_price_low')))
                                    ))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="sort_all"
                                       @if((strpos(url()->full(), 'sort_all')) || (
                                           (!(strpos(url()->full(), 'sort_vote')))&&
                                           (!(strpos(url()->full(), 'sort_sold')))&&
                                           (!(strpos(url()->full(), 'sort_visit')))&&
                                           (!(strpos(url()->full(), 'sort_price_high')))&&
                                           (!(strpos(url()->full(), 'sort_price_low')))
                                           ))
                                       checked
                                       @endif
                                       id="cat_filter_rank_all"
                                >
                            </div>
                            <label>همه مجموعه ها</label>
                        </li>
                        <li class="cat_filter">
                            <div
                                    @if(strpos(url()->full(), 'sort_vote'))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="sort_vote"
                                       @if(strpos(url()->full(), 'sort_vote'))
                                       checked
                                        @endif
                                >
                            </div>
                            <label>محبوبترین دوره ها</label>
                        </li>
                        <li class="cat_filter">
                            <div
                                    @if(strpos(url()->full(), 'sort_sold'))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="sort_sold"
                                       @if(strpos(url()->full(), 'sort_sold'))
                                       checked
                                        @endif
                                >
                            </div>
                            <label>پرفروشترین دوره ها</label>
                        </li>
                        <li class="cat_filter">
                            <div
                                    @if(strpos(url()->full(), 'sort_visit'))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="sort_visit"
                                       @if(strpos(url()->full(), 'sort_visit'))
                                       checked
                                        @endif
                                >
                            </div>
                            <label>پربازدیدترین دوره ها</label>
                        </li>
                        <li class="cat_filter">
                            <div
                                    @if(strpos(url()->full(), 'sort_price_high'))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="sort_price_high"
                                       @if(strpos(url()->full(), 'sort_price_high'))
                                       checked
                                        @endif
                                >
                            </div>
                            <label>گرانترین دوره ها</label>
                        </li>
                        <li class="cat_filter">
                            <div
                                    @if(strpos(url()->full(), 'sort_price_low'))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="sort_price_low"
                                       @if(strpos(url()->full(), 'sort_price_low'))
                                       checked
                                        @endif
                                >
                            </div>
                            <label>ارزانترین دوره ها</label>
                        </li>
                    </ul>

                    <button class="filterbtn" type="submit">فیلتر</button>
                    <div class="clear"></div>
                </section>
                <div style="width: 90%;height: 2px;background-color: #ececec;margin: 15px auto;"></div>
                <section>
                    <h4><i></i>فیلتر بر اساس موضوعات</h4>
                    <ul>
                        <li class="cat_filter">

                            <div
                                    {{--{{dd($productCategoryArray)}}--}}
                                    @if(((count($productCategoryArray)) == (count($categories)) )||(count($productCategoryArray) == 0))
                                    class="cat_filter_active"
                                    @else
                                    class="cat_filter"
                                    @endif
                            >
                                <input type="checkbox" name="category_all"
                                       @if(((count($productCategoryArray)) == (count($categories)) )||(count($productCategoryArray) == 0))
                                       checked
                                        @endif
                                >
                            </div>
                            <label>همه موضوعات</label>
                        </li>

                        @foreach($categories as $category)
                            <li class="cat_filter">
                                <div>
                                    <input type="checkbox" name="category_{{$category->name}}">
                                </div>
                                <label>{{$category->name_fa}}</label>
                            </li>
                        @endforeach

                    </ul>

                    <button class="filterbtn" type="submit">فیلتر</button>
                    <div class="clear"></div>
                </section>

                <input type="submit" value="فیلتر" id="filter">

                {!! Form::close() !!}

            </div>
            <div class="cat-contant-list-left">

                <section>
                    <ul>
                        @php
                        $i = 1;
                        @endphp

                        @foreach($products as $product)
                            <li>
                                <a href="{{route('index.index')}}/{{createProductLinkCreate($product)}}"
                                   class="discount-scroll-item">
                                    <div class="course-img"><img src="{{showProductImage($product)}}" alt=""></div>
                                    <h3>{{$product->title}}</h3>
                                    <div class="rate-teacher">
                                        <div class="teacher">
                                            <img src="{{showUserImage($product->user->id)}}" alt="">
                                            <span style="font-family: yekan;font-size: 9pt;color: #0b9240">{{$product->user->name}}</span>
                                        </div>


                                        <div id="rateYo{{$i}}" class="rate"></div>
                                        <script>
                                            $(function () {
                                                $("#rateYo{{$i++}}").rateYo({
                                                    starWidth: "15px",
                                                    rating    :{{$product->vote}},
                                                    spacing   : "5px",
                                                    readOnly: true,
                                                    rtl: true
                                                });
                                            });
                                        </script>



                                    </div>
                                    <div class="time-old-price">
                                        <div class="time">
                                            <i class="time"></i>

                                            @php
                                                $duration = 0;
                                            @endphp
                                            @foreach($product->productPosts as $productPost)
                                                @php
                                                    $duration += $productPost->duration;
                                                @endphp
                                            @endforeach

                                            <span style="font-family: yekan;">{{$duration}}
                                                دقیقه
                                            </span>
                                            <div class="clear"></div>
                                            @if(($product['sold_count']) > 50)
                                                <i class="student"></i>
                                                <span style="font-family: yekan">
                                                {{ $product['sold_count'] }}
                                                    دانشجو
                                                </span>
                                            @endif
                                        </div>
                                        <div class="old-price">
                                            @if(($product->discount) > 0)
                                                <span style="font-size: 12pt;font-family: Arial;margin-right: 10px;display: block;margin-top: 5px;">{{toPersianNumber($product->price)}}</span>
                                                <span style="border: 1px solid red;display: block;width: 50px;transform: rotate(-14deg);margin: -12px 45px 0 0;"></span>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="course-price">
                                        @if(($product->price - $product->discount) > 0)
                                            {{ toPersianNumber($product->price - $product->discount) }}
                                        @else
                                            رایگان
                                        @endif
                                    </div>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                    <div class="clear"></div>
                </section>
                <div class="pagnation">
                    <div class="page_align">
                        {{$products->appends(\Illuminate\Support\Facades\Input::except('page'))->links('vendor.pagination.simple-default')}}
                    </div>
                </div>
            </div>
        </div>

        @include('partials.footer')

    </div>

@endsection
@push('scripts')
    <script>
        $(".cat_filter input").click(function () {
            if ($(this).is(":checked")) {
                $(this).parent().addClass("cat_filter_active");
            } else {
                $(this).parent().removeClass("cat_filter_active");
            }
            // document.getElementById("filterForm").submit();
        })
        // $(".cat_filter input").click(function () {
        //     if ($(this).is(":checked")) {
        //         $(this).parent().addClass("cat_filter_active");
        //         $("#cat_filter_rank_all").prop("checked", false);
        //         $("#cat_filter_rank_all").parent().removeClass("cat_filter_active");
        //         $("#filter").submit();
        //     } else {
        //         $(this).parent().removeClass("cat_filter_active");
        //         $("#filter").trigger("click");
        //     }
        // })
    </script>
    <script>
    </script>
    <script>
        $("#search-pills-all").click(function () {
            $("#search-pills-all input").prop("checked", true);
            $("#search-pills-all").addClass("active");
            $("#search-pills-free input").prop("checked", false);
            $("#search-pills-free").removeClass("active");
            $("#search-pills-purchasable input").prop("checked", false);
            $("#search-pills-purchasable").removeClass("active");
            $("#filter").trigger("click");
        });
        $("#search-pills-free").click(function () {
            $("#search-pills-all input").prop("checked", false);
            $("#search-pills-all").removeClass("active");
            $("#search-pills-free input").prop("checked", true);
            $("#search-pills-free").addClass("active");
            $("#search-pills-purchasable input").prop("checked", false);
            $("#search-pills-purchasable").removeClass("active");
            $("#filter").trigger("click");
        });
        $("#search-pills-purchasable").click(function () {
            $("#search-pills-all input").prop("checked", false);
            $("#search-pills-all").removeClass("active");
            $("#search-pills-free input").prop("checked", false);
            $("#search-pills-free").removeClass("active");
            $("#search-pills-purchasable input").prop("checked", true);
            $("#search-pills-purchasable").addClass("active");
            $("#filter").trigger("click");
        });
    </script>

@endpush