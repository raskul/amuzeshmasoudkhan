@extends('layouts.index')
@push('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
@endpush
@section('content')

    @include('partials.header')

    @include('flash::message')
    @include('adminlte-templates::common.errors')

    <div id="main">
        <div id="sliders">
            <div class="main-slider">
                <div class="main-slider-image"><img src="{{asset('assets/images/slider/5.jpg')}}" alt=""></div>
            </div>
            <div class="main-slider">
                <div class="main-slider-image"><img src="{{asset('assets/images/slider/2.jpg')}}" alt=""></div>
            </div>
            <div class="main-slider">
                <div class="main-slider-image"><img src="{{asset('assets/images/slider/3.jpg')}}" alt=""></div>
            </div>
            <div class="main-slider">
                <div class="main-slider-image"><img src="{{asset('assets/images/slider/4.jpg')}}" alt=""></div>
            </div>
            <div id="slider-right-arrow"><img src="{{asset('assets/images/slider-arrow-right.png')}}" alt=""></div>
            <div id="slider-left-arrow"><img src="{{asset('assets/images/slider-arrow-left.png')}}" alt=""></div>
            <div class="pishnahad-lahzei">

                <a href="/"><img src="{{asset('assets/images/cat-img/telegram.jpg')}}" alt=""></a>
                <a href="/post/1"><img src="{{asset('assets/images/cat-img/tadris.jpg')}}" alt=""></a>
                <a href="/"><img style="width: 280px;height: 240px"
                                 src="{{asset('assets/images/cat-img/elearning.jpg')}}"
                                 alt=""></a>
            </div>
        </div>

        @if(count($discountProducts))
            <div class="discount">

                <h4><i style="background-position: -799px -539px;"></i>تخفیف های ویژه این هفته</h4>

                <div class="discount-scroll" onmouseenter="slider_arrow(this)">
                    <div class="discount-scroll-next" onclick="slide_scroll('next',this)">
                        <i style="background-position: -23px -25px"></i>
                    </div>
                    <div class="clear"></div>
                    <div class="discount-scroll-content">
                        <ul>
                            تخفیف های ویژه این هفته
                            @php
                            $s1 = 0;
                            @endphp
                            @foreach($discountProducts as $product)
                                <li>
                                    <a href="{{route('index.index')}}/{{createProductLinkCreate($product)}}"
                                       class="discount-scroll-item">
                                        <div class="course-img"><img src="{{showProductImage($product)}}" alt=""></div>
                                        <h3>{{$product->title}}</h3>
                                        <div class="rate-teacher">
                                            <div class="teacher">
                                                <img src="{{showUserImage($product->user->id)}}" alt="">
                                                <span style="font-family: yekan;font-size: 9pt;color: #0b9240">{{$product->user->name}}</span>
                                            </div>

                                            <div id="rateYo1{{$s1}}" class="rate"></div>
                                            <script>
                                                $(function () {
                                                    $("#rateYo1{{$s1++}}").rateYo({
                                                        starWidth: "15px",
                                                        rating    : {{$product->vote}},
                                                        spacing   : "5px",
                                                        readOnly: true,
                                                        rtl: true
                                                    });
                                                });
                                            </script>

                                        </div>
                                        <div class="time-old-price">
                                            <div class="time">
                                                <i class="time"></i>
                                                <span style="font-family: yekan;">
                                                    {{calculateProductDuration($product)}}
                                                    دقیقه</span>
                                                <div class="clear"></div>
                                                @if(($product['sold_count']) > 50)
                                                    <i class="student"></i>
                                                    <span style="font-family: yekan">
                                                        {{$product['sold_count']}}
                                                        دانشجو</span>
                                                @endif
                                            </div>
                                            @if($product->discount > 0)
                                                <div class="old-price">
                                                    <span style="font-size: 12pt;font-family: Arial;margin-right: 10px;display: block;margin-top: 5px;">{{toPersianNumber($product->price)}}</span>
                                                    <span style="border: 1px solid red;display: block;width: 50px;transform: rotate(-14deg);margin: -12px 45px 0 0;"></span>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="course-price">
                                            @if(($product->price - $product->discount) > 0)
                                                {{ toPersianNumber($product->price - $product->discount) }}
                                            @else
                                                رایگان
                                            @endif
                                        </div>
                                        @if($product->discount > 0)
                                            <div class="discount-label">
                                                {{(integer)((($product->discount) * 100) / ($product->price))}}
                                                %
                                            </div>
                                        @endif
                                    </a>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="discount-scroll-prev" onclick="slide_scroll('prev',this)">
                        <i style="background-position: -23px -73px"></i>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        @endif

        @if(count($futureProducts) || count($indexVipProducts) || count($indexNotifications))
            <div id="course-info">
                @if(count($indexNotifications))
                    <h4><i style="background-position: -581px -783px;"></i>اطلاعیه های آموزشی</h4>
                @endif
                <div class="clear-left"></div>
                @if(count($indexVipProducts))
                    <h4><i style="background-position: -677px -783px;"></i>دوره های حضوری ویژه</h4>;
                @endif
                <div class="clear-right"></div>
                <div class="right-list">
                    <ul>
                        {{--اطلاعیه های آموزشی--}}
                        @foreach($indexNotifications as $indexNotification)
                            <li>
                                <a href="{{createProductLinkCreate($indexNotification->product)}}">
                                    <div class="img">
                                        <img src="{{showProductImage($indexNotification->product)}}">
                                    </div>
                                    <div class="title">
                                        <h4>{{$indexNotification->text}}</h4>
                                    </div>
                                    <div class="info-icons">
                                        <ul>
                                            <li><i style="background-position: -433px -239px;"></i>شروع :
                                                {{pDate($indexNotification->start_at, false, false, true)}}
                                            </li>
                                            <li><i style="background-position: -105px -306px;"></i>ثبت نام :
                                                @if($indexNotification->can_sign_up == 1)
                                                    فعال
                                                @else
                                                    غیر فعال
                                                @endif
                                            </li>
                                        </ul>
                                    </div>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>
                <div class="clear-left"></div>
                <div class="left-list">
                    <ul>
                        {{--دوره های حضوری ویژه--}}
                        @foreach($indexVipProducts as $vipProduct)
                            <li style="background-color: rgba(71,200,220,0.16);border:1px solid #80d9ec;">
                                <a href="{{createProductLinkCreate($vipProduct->product)}}">
                                    <div class="img">
                                        <img src="{{showProductImage($vipProduct->product)}}"
                                             style="width: 40px;height: 40px;border-radius: 50%"
                                             alt="">
                                    </div>
                                    <div class="title">
                                        <h3>{{$vipProduct->title}}</h3>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="content">
                                        <p>{{$vipProduct->text}}</p>
                                    </div>
                                    <div class="clear"></div>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                    @if(count($futureProducts))
                        <div class="future-courses">
                            <h4><i style="background-position: -1045px -603px;"></i>لیست دوره های در صف انتشار</h4>
                            <ul>
                                {{--لیست دوره های در صف انتشار--}}
                                @foreach($futureProducts as $product)
                                    <li>
                                        <a href="{{createProductLinkCreate($product)}}">
                                            <div class="future-list">
                                                <p><i></i>
                                                    {{$product->title}}
                                                </p>
                                            </div>
                                            <div class="clear-left"></div>
                                            <div class="future-date">
                                                <i></i><span>{{pDate($product->start_at, false, false, true)}}</span>
                                            </div>
                                            <div class="clear-right"></div>
                                        </a>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    @endif
                </div>
                <div class="clear"></div>
            </div>
        @endif

        <div class="discount">

            <h4><i style="background-position: -799px -539px;"></i>جدیدترین دوره های آموزشی</h4>

            <div class="discount-scroll" onmouseenter="slider_arrow(this)">
                <div class="discount-scroll-next" onclick="slide_scroll('next' , this)">
                    <i style="background-position: -23px -25px"></i>
                </div>
                <div class="clear"></div>
                <div class="discount-scroll-content">
                    <ul>
                        {{--جدیدترین دوره های آموزشی--}}
                        @php
                        $s2 = 1;
                        @endphp
                        @foreach($newProducts as $product)
                            <li>
                                <a href="{{route('index.index')}}/{{createProductLinkCreate($product)}}"
                                   class="discount-scroll-item">
                                    <div class="course-img"><img src="{{showProductImage($product)}}" alt=""></div>
                                    <h3>{{$product->title}}</h3>
                                    <div class="rate-teacher">
                                        <div class="teacher">
                                            <img src="{{showUserImage($product->user->id)}}" alt="">
                                            <span style="font-family: yekan;font-size: 9pt;color: #0b9240">{{$product->user->name}}</span>
                                        </div>
                                        <div id="rateYo2{{$s2}}" class="rate"></div>
                                        <script>
                                            $(function () {
                                                $("#rateYo2{{$s2++}}").rateYo({
                                                    starWidth: "15px",
                                                    rating    : {{$product->vote}},
                                                    spacing   : "5px",
                                                    readOnly: true,
                                                    rtl: true
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="time-old-price">
                                        <div class="time">
                                            <i class="time"></i>
                                            <span style="font-family: yekan;">
                                                {{calculateProductDuration($product)}}
                                                دقیقه</span>
                                            <div class="clear"></div>
                                            @if(($product['sold_count']) > 50)
                                                <i class="student"></i>
                                                <span style="font-family: yekan">
                                                    {{$product['sold_count']}}
                                                    دانشجو</span>
                                            @endif
                                        </div>
                                        @if(($product->discount) > 0)
                                            <div class="old-price">
                                                <span style="font-size: 12pt;font-family: Arial;margin-right: 10px;display: block;margin-top: 5px;">{{toPersianNumber($product->price)}}</span>
                                                <span style="border: 1px solid red;display: block;width: 50px;transform: rotate(-14deg);margin: -12px 45px 0 0;"></span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="course-price">
                                        @if(($product->price - $product->discount) > 0)
                                            {{ toPersianNumber($product->price - $product->discount) }}
                                        @else
                                            رایگان
                                        @endif
                                    </div>
                                    @if(($product->discount) > 0)
                                        <div class="discount-label">
                                            {{(integer)((($product->discount) * 100) / ($product->price))}}
                                            %
                                        </div>
                                    @endif
                                </a>
                            </li>
                        @endforeach


                    </ul>
                </div>
                <div class="discount-scroll-prev" onclick="slide_scroll('prev' , this)">
                    <i style="background-position: -23px -73px"></i>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="discount">

            <h4><i style="background-position: -799px -539px;"></i>پرفروشترین دوره های آموزشی</h4>

            <div class="discount-scroll" onmouseenter="slider_arrow(this)">
                <div class="discount-scroll-next" onclick="slide_scroll('next' , this)">
                    <i style="background-position: -23px -25px"></i>
                </div>
                <div class="clear"></div>
                <div class="discount-scroll-content">
                    <ul>
                        {{--پرفروشترین دوره های آموزشی--}}
                        @php
                        $s3 = 1;
                        @endphp
                        @foreach($mostSoldProducts as $product)
                            <li>
                                <a href="{{route('index.index')}}/{{createProductLinkCreate($product)}}"
                                   class="discount-scroll-item">
                                    <div class="course-img"><img src="{{showProductImage($product)}}" alt="">
                                    </div>
                                    <h3>{{$product->title}}</h3>
                                    <div class="rate-teacher">
                                        <div class="teacher">
                                            <img src="{{showUserImage($product->user->id)}}" alt="">
                                            <span style="font-family: yekan;font-size: 9pt;color: #0b9240">{{$product->user->name}}</span>
                                        </div>
                                        <div id="rateYo3{{$s3}}" class="rate"></div>
                                        <script>
                                            $(function () {
                                                $("#rateYo3{{$s3++}}").rateYo({
                                                    starWidth: "15px",
                                                    rating    : {{$product->vote}},
                                                    spacing   : "5px",
                                                    readOnly: true,
                                                    rtl: true
                                                });
                                            });
                                        </script>
                                    </div>
                                    <div class="time-old-price">
                                        <div class="time">
                                            <i class="time"></i>
                                            <span style="font-family: yekan;">
                                                    {{calculateProductDuration($product)}}
                                                دقیقه</span>
                                            <div class="clear"></div>
                                            @if(($product['sold_count']) > 50)
                                                <i class="student"></i>
                                                <span style="font-family: yekan">
                                                        {{$product['sold_count']}}
                                                    دانشجو</span>
                                            @endif
                                        </div>
                                        @if($product->discount > 0)
                                            <div class="old-price">
                                                <span style="font-size: 12pt;font-family: Arial;margin-right: 10px;display: block;margin-top: 5px;">{{toPersianNumber($product->price)}}</span>
                                                <span style="border: 1px solid red;display: block;width: 50px;transform: rotate(-14deg);margin: -12px 45px 0 0;"></span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="course-price">
                                        @if(($product->price - $product->discount) > 0)
                                            {{ toPersianNumber($product->price - $product->discount) }}
                                        @else
                                            رایگان
                                        @endif
                                    </div>
                                    @if($product->discount > 0)
                                        <div class="discount-label">
                                            {{(integer)((($product->discount) * 100) / ($product->price))}}
                                            %
                                        </div>
                                    @endif
                                </a>
                            </li>
                        @endforeach


                    </ul>
                </div>
                <div class="discount-scroll-prev" onclick="slide_scroll('prev' , this)">
                    <i style="background-position: -23px -73px"></i>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>

        @include('partials.footer')

    </div>

@endsection




