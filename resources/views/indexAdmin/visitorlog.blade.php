@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            لاگ بازدید کنندگان
        </h1>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="box box-primary">

            <div class="box-body " style="padding: 0 20px">
                <div class="row">

                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>شماره</th>
                            <th>نام کاربری</th>
                            <th>ای پی</th>
                            <th>یو ار ال</th>
                            <th>کال بک یو ار ال</th>
                            <th>سیستم عامل</th>
                            <th>مرورگر</th>
                            <th>تاریخ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($logs as $log)
                        <tr>
                            <td>{{$log->id}}</td>
                            <td>{{$log->username}}</td>
                            <td>{{$log->ip}}</td>
                            <td>{{$log->url}}</td>
                            <td>{{$log->call_back_url}}</td>
                            <td>{{$log->os}}</td>
                            <td>{{$log->browser}}</td>
                            <td>{{pDate($log->crated_at, true)}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div>
                        {{$logs->render()}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
