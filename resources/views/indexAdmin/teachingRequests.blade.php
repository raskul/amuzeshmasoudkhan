@extends('layouts.app')
@section('content')

    <div class="content">

        @include('flash::message')

        <br>
        <section class="content-header">
            <h1>
                لیست درخواست های شما
            </h1>
        </section>
        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">
                <div class="row">
                    <div class="col-sm-12 ">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>شماره درخواست</th>
                                <th>عنوان درخواست</th>
                                <th>وضعیت</th>
                                <th>تاریخ</th>
                                <th>مشاهده</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($teaching_requests as $teaching_request)
                                <tr>
                                    <td scope="row">{{ $teaching_request->id }}</td>
                                    <td>{{ $teaching_request->title }}</td>
                                    <td>{{ $teaching_request->statusName }}</td>
                                    <td>{{ $teaching_request->created_at }}</td>
                                    <td>
                                        <a href="{{route('teachingRequestComments', $teaching_request->id)}}"
                                           type="button" class="btn btn-default" aria-label="Left Align">
                                            <span class="fa fa-eye fa-lg" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
