@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1 style="display: inline">
            ویرایش اشانتیون
        </h1>
        <a href="{{route('eshantion')}}" class="btn btn-default pull-left">بازگشت</a>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">
                <div class="row">

                    {!! Form::open(['route'=>['eshantion', $item->id], 'method' => 'PUT' ]) !!}

                    <div class="col-sm-4">
                        <select name="mahsulasli" id="" class="form-control select2">
                            @foreach( $products as $product)
                                <option value="{{$product->id}}"
                                        {{$product->id == $item->mahsulasli_id?'selected':null}}
                                >{{$product->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <select name="eshantion" id="" class="form-control select2">
                            @foreach( $products as $product)
                                <option value="{{$product->id}}"
                                        {{$product->id == $item->eshantion_id?'selected':null}}
                                >{{$product->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <button type="submit" style="height: 28px" class="btn btn-success">ذخیره</button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
