@extends('layouts.app')
@section('content')

    <section class="content-header">
        <span style="font-size: xx-large">
            فایل های آپلودی در پست ها توسط اساتید
        </span>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-body " style="padding-right: 20px">
                        <div class="row">

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ایمیل</th>
                                    <th>فایل</th>
                                    <th>حجم MB</th>
                                    <th>تاریخ ساخت میلادی</th>
                                    <th>تاریخ ساخت شمسی</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($docs as $doc)
                                    <tr>
                                        <td>{{$doc->user->email}}</td>
                                        <td>{{$doc->file}}</td>
                                        <td>{{(integer)(($doc->size)/(1024*1024))}}</td>
                                        <td>{{$doc->created_at}}</td>
                                        <td>{{pDate($doc->created_at)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{$docs->render()}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection