@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            ارسال تیکت
        </h1>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">
                <div class="row">

                    <table class="table">
                        <thead>
                        <tr>
                            <th style="max-width: 10px">شماره تیکت</th>
                            <th>عنوان تیکت</th>
                            <th>تاریخ</th>
                            <th>اولیت</th>
                            <th>کاربر</th>
                            <th>ایمیل</th>
                            <th style="max-width: 10px">عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets as $ticket)
                            <tr>
                                <td scope="row">{{ $ticket->id }}</td>
                                <td><a href="{{route('ticketComments', $ticket->id)}}">{{ $ticket->title }}</a></td>
                                <td>{{ pDate($ticket->created_at, true) }}</td>
                                <td>{{ $ticket->importanceName }}</td>
                                <td>{{ $ticket->user->name }}</td>
                                <td>{{ $ticket->user->email }}</td>
                                <td><a href="{{route('ticketComments', $ticket->id)}}" class="btn btn-default">مشاهده</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
