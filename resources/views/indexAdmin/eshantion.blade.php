@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            اشانتیون
        </h1>
    </section>
    <a href="{{route('eshantion')}}/new" class="btn btn-primary">افزودن اشانتیون جدید به محصولات</a>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="box box-primary">

            <div class="box-body " style="padding:0 20px">
                <div class="row">

                    @if(count($all_table))

                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>کد محصول اصلی</th>
                            <th>عنوان محصول اصلی</th>
                            <th>کد اشانتیون</th>
                            <th>عنوان اشانتیون</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($all_table as $item)
                            @php
                            $mahsulasli = \App\Models\Product::find($item->mahsulasli_id);
                            $eshantion = \App\Models\Product::find($item->eshantion_id);
                            @endphp
                        <tr>
                            <td>{{$mahsulasli->id}}</td>
                            <td>{{$mahsulasli->title}}</td>
                            <td>{{$eshantion->id}}</td>
                            <td>{{$eshantion->title}}</td>
                            <td>
                                {!! Form::open(['route' => ['eshantion', $item->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('eshantion', [$item->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('آیا اطمینان دارید؟')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @else
                    هنوز هیچ اشانتیونی ثبت نشده است.
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
