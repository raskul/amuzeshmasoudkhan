@extends('layouts.app')
@section('content')



    <section class="content-header">
        <span style="font-size: xx-large">
            ارسال تیکت
        </span>
        <span class="pull-left">
            <a href="{{route('ticketIndex')}}" class="btn btn-default">بازگشت</a>
        </span>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="row" style="padding: 0 20px 0 20px">
            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-body " style="padding-right: 20px">
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>شماره درخواست</td>
                                    <th>عنوان</th>
                                    <th>نام</th>
                                    <th>ایمیل</th>
                                    <th>تاریخ ایجاد</th>
                                    <th>وضعیت</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$ticket->id}}</td>
                                    <td scope="row">{{$ticket->title}}</td>
                                    <td>{{$ticket->user->name}}</td>
                                    <td>{{$ticket->user->email}}</td>
                                    <td>{{pDate($ticket->created_at, true )}}</td>
{{--                                    <td>{{$ticket->statusName}}</td>--}}
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        {!!  $ticket->text!!}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="background-color: whitesmoke">
                    <div class="box-body " style="padding-left: 20px;padding-right: 20px">

                        @can('is-employee')
                            <hr>
                            <div class="row">
                                <div class="col-sm-12">
                                    {!! Form::open(['route'=>['ticketComments', $ticket->id], 'method' => 'post', 'style' => 'display: inline' ]) !!}
                                    <input type="hidden" name="close" value="1">
                                    <button type="submit" class="btn btn-danger" style="display: inline">بستن تیکت
                                    </button>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                            <hr>
                        @endcan

                        @foreach($ticketComments as $comment)
                            <div class="row"
                                 style="background-color:white; margin: 0 50px 0 50px;padding-top: 10px; border-bottom: 1px solid #F5F5F5">
                                <div class="col-sm-1">
                                    <img class="img-circle" src="{{Gravatar::get($comment->user->email)}}" alt=""
                                         height="100%" width="100%">
                                </div>
                                <div class="col-sm-11">
                                    <div>
                                    <span class="btn-default btn" style="padding: 0 5px 0 5px ;color: black;">
                                        <strong>{{$comment->user->name}}</strong>
                                    </span>
                                        <span class="btn btn-warning pull-left" style="padding: 0 5px 0 5px">
                                        {{pDate($comment->created_at, false, true)}}
                                    </span>
                                    </div>
                                    <div style="padding: 10px 40px 10px 40px">{!! $comment->text !!}</div>
                                </div>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <hr>
                                {!! Form::open(['route'=>['ticketComments', $ticket->id], 'method' => 'post' ]) !!}
                                <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                    {!! Form::label('text', 'متن پیام:') !!}
                                    {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
                                    @if ($errors->has('text'))
                                        <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                                    @endif
                                </div>
                                <button class="btn btn-success">ارسال پیام</button>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
            $('html, body').animate({
                scrollTop: $("#myDiv").offset().top
            }, 2000);
        });
    </script>
@endpush