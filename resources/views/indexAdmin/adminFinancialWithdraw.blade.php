@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            درخواست های برداشت وجه
        </h1>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px;padding-left: 20px;">
                <div class="row">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>شماره درخواست</th>
                            <th>نقش</th>
                            <th>ایمیل</th>
                            <th>موجودی قبلی</th>
                            <th>موجودی پس از تایید</th>
                            <th>مبلغ درخواستی</th>
                            <th>بانک</th>
                            <th>شماره حساب</th>
                            <th>شماره شبا</th>
                            <th>واریز انجام شد</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($balanceHistories as $balanceHistory)
                            <tr>
                                <td>{{$balanceHistory->id}}</td>
                                <td>{{$balanceHistory->user->role->name_fa}}</td>
                                <td>{{$balanceHistory->user->email}}</td>
                                <td>{{$balanceHistory->old_balance}}</td>
                                <td>{{$balanceHistory->new_balance}}</td>
                                <td>{{$balanceHistory->old_balance - $balanceHistory->new_balance}}</td>
                                <td>{{$balanceHistory->user->bank}}</td>
                                <td>{{$balanceHistory->user->bank_number}}</td>
                                <td>{{$balanceHistory->user->bank_sheba}}</td>
                                <td>
                                    {!! Form::open(['route'=>['adminFinancialWithdraw', $balanceHistory->id ], 'method' => 'POST']) !!}
                                     <button type="submit" class="btn btn-success">تایید</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <section class="content-header">
            <h1>
                تغییرات کیف پول ها
            </h1>
        </section>

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px;padding-left: 20px;">
                <div class="row">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>شماره درخواست</th>
                            <th>نقش</th>
                            <th>ایمیل</th>
                            <th>موجودی قبلی</th>
                            <th>موجودی پس از تایید</th>
                            <th>مبلغ درخواستی</th>
                            <th>بانک</th>
                            <th>شماره حساب</th>
                            <th>شماره شبا</th>
                            <th>وضعیت</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($oldBalanceHistories as $balanceHistory)
                            <tr>
                                <td>{{$balanceHistory->id}}</td>
                                <td>{{$balanceHistory->user->role->name_fa}}</td>
                                <td>{{$balanceHistory->user->email}}</td>
                                <td>{{$balanceHistory->old_balance}}</td>
                                <td>{{$balanceHistory->new_balance}}</td>
                                <td>{{$balanceHistory->old_balance - $balanceHistory->new_balance}}</td>
                                <td>{{$balanceHistory->user->bank}}</td>
                                <td>{{$balanceHistory->user->bank_number}}</td>
                                <td>{{$balanceHistory->user->bank_sheba}}</td>
                                <td>{{$balanceHistory->typeStr}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$oldBalanceHistories->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
