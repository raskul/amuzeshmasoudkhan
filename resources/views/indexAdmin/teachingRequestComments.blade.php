@extends('layouts.app')
@section('content')

    <section class="content-header">
        <span style="font-size: xx-large">
            ارسال تیکت
        </span>
        <span class="pull-left">
            <a href="{{route('teachingIndex')}}" class="btn btn-default">بازگشت</a>
        </span>
    </section>
    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-body " style="padding-right: 20px">
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>شماره درخواست</td>
                                    <th>عنوان</th>
                                    <th>نام</th>
                                    <th>ایمیل</th>
                                    <th>تاریخ ایجاد</th>
                                    <th>وضعیت</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$teachingRequest->id}}</td>
                                    <td>{{$teachingRequest->title}}</td>
                                    <td>{{$teachingRequest->user->name}}</td>
                                    <td>{{$teachingRequest->user->email}}</td>
                                    <td>{{pDate($teachingRequest->created_at, true )}}</td>
                                    <td>{{$teachingRequest->statusName}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        قیمت:
                                        {!!  $teachingRequest->price!!}
                                    </td>
                                    <td colspan="2">
                                        تاریخ شروع:
                                        {!!  $teachingRequest->start_at!!}
                                    </td>
                                    <td colspan="2">
                                        تاریخ پایان:
                                        {!!  $teachingRequest->end_at!!}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        گواهی نامه داده میشود؟
                                        @if($teachingRequest->can_get_license == 0 )
                                            خیر
                                        @else
                                            بله
                                        @endif
                                    </td>
                                    <td colspan="2">
                                        ثبت نام دانشجو در حال حاضر:
                                        @if($teachingRequest->can_sign_up == 0)
                                            غیر فعال
                                        @else
                                            فعال
                                        @endif
                                    </td>
                                    <td colspan="2">
                                        {!!  $productType!!}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        توضیحات کامل:
                                        <br>
                                        {!!  $teachingRequest->description!!}
                                    </td>
                                </tr>
                                @if (isset($teachingRequest->files))
                                    <tr>
                                        <td colspan="6">
                                            فایل های ضمیمه:
                                            <br>
                                            @foreach(unserialize($teachingRequest->files) as $file)
                                                <a href="{{ route('index.index') }}/{{ $file }}" target="_blank">
                                                    <span class="btn btn-primary" style="margin: 0 0 5px 5px; padding: 0">
                                                    {{ substr(strrev(substr(strrev($file), 0,strpos(strrev($file), '/'))), 10) }}
                                                    </span>
                                                </a>
                                            @endforeach
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="background-color: whitesmoke">
                    <div class="box-body " style="padding-left: 20px;padding-right: 20px">

                        @can('is-employee')
                            <hr>
                            <div class="row">
                                <div class="col-sm-12">
                                    {!! Form::open(['route'=>['teachingRequestComments', $teachingRequest->id], 'method' => 'post', 'style' => 'display: inline' ]) !!}
                                    <input type="hidden" name="rad" value="1">
                                    <button type="submit" class="btn btn-danger" style="display: inline">رد درخواست
                                    </button>
                                    {!! Form::close() !!}

                                    {!! Form::open(['route'=>['teachingRequestComments', $teachingRequest->id], 'method' => 'post', 'style' => 'display: inline' ]) !!}
                                    <input type="hidden"  name="ghabul" value="1">
                                    <button type="submit" class="btn btn-success" style="display: inline">قبول درخواست
                                    </button>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                            <hr>
                        @endcan

                        @foreach($teachingRequestComments as $comment)
                            <div class="row"
                                 style="background-color:white; margin: 0 50px 0 50px;padding-top: 10px; border-bottom: 1px solid #F5F5F5">
                                <div class="col-sm-1">
                                    <img class="img-circle" src="{{Gravatar::get($comment->user->email)}}" alt=""
                                         height="100%" width="100%">
                                </div>
                                <div class="col-sm-11">
                                    <div>
                                    <span class="btn-default btn" style="padding: 0 5px 0 5px ;color: black;">
                                        <strong>{{$comment->user->name}}</strong>
                                    </span>
                                        <span class="btn btn-warning pull-left" style="padding: 0 5px 0 5px">
                                        {{pDate($comment->created_at, false, true)}}
                                    </span>
                                    </div>
                                    <div style="padding: 10px 40px 10px 40px">{!! $comment->text !!}</div>
                                </div>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <hr>
                                {!! Form::open(['route'=>['teachingRequestComments', $teachingRequest->id], 'method' => 'post', 'files' => true ]) !!}
                                <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                    {!! Form::label('text', 'متن پیام:') !!}
                                    {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
                                    @if ($errors->has('text'))
                                        <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        @include('partials.uploadFileInput')
                                    </div>
                                </div>
                                <button class="btn btn-success">ارسال پیام</button>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
            $('html, body').animate({
                scrollTop: $("#myDiv").offset().top
            }, 2000);
        });
    </script>
@endpush