@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1 style="display: inline">
            افزودن اشانتیون
        </h1>
        <a href="{{route('eshantion')}}" class="btn btn-default pull-left">بازگشت</a>
    </section>

    <div class="content">

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">
                <div class="row">

                    {!! Form::open(['route'=>['eshantion'], 'method' => 'post' ]) !!}

                    <div class="col-sm-4">
                        <select name="mahsulasli" id="" class="form-control select2 ">
                            @foreach( $mahsulasli as $item)
                                <option value="{{$item->id}}">{{$item->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <select name="eshantion" id="" class="form-control select2">
                            @foreach($eshantion as $item)
                                <option value="{{$item->id}}">{{$item->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <button type="submit" style="height: 28px" class="btn btn-success">ذخیره</button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
