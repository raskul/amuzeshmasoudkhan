{!! Form::open(['route' => ['tickets.destroy', $id], 'method' => 'delete']) !!}
<div style="min-width: 150px" class='btn-group'>
    <a style=" background-color: green" href="{{ route('answer.ticket', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-pencil"> پاسخ </i>
    </a>
    <a style="display: inline" href="{{ route('tickets.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('آیا اطمینان دارید؟')"
    ]) !!}
</div>
{!! Form::close() !!}
