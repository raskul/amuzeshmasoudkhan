<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره:') !!}
    <p>{!! $ticket->id !!}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('user_id', 'نام کاربری:') !!}
    <p>{!! $ticket->username !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'ایمیل:') !!}
    <p>{!! $ticket->email !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('title', 'عنوان:') !!}
    <p>{!! $ticket->title !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'متن:') !!}
    <p>{!! $ticket->text !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $ticket->created_at !!}</p>
</div>

