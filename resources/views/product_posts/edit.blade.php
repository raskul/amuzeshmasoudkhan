@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ویرایش پست دوره آموزشیوی
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($productPost, ['route' => ['productPosts.update', $productPost->id], 'method' => 'patch']) !!}

                        @include('product_posts.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection