@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ساختن پست دوره آموزشی
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row col-sm-12">
                    {!! Form::open(['route' => 'productPosts.store']) !!}

                        @include('product_posts.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
