@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            پست دوره آموزشی
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding:0 20px">
                    @include('product_posts.show_fields')
                    <a href="{!! route('productPosts.index') !!}" class="btn btn-default">بازگشت</a>
                </div>
            </div>
        </div>
    </div>
@endsection
