<!-- فیلد محصول را انتخاب کنید: -->
<div class="form-group col-sm-6 {{ $errors->has('product_id') ? ' has-error' : '' }}">
    {!! Form::label('product_id', 'محصول را انتخاب کنید:') !!}
    {!! Form::select('product_id', $products, (isset($products)?$products:null), ['class' => 'form-control select2']) !!}
    @if ($errors->has('product_id'))
        <span class="help-block"><strong>{{ $errors->first('product_id') }}</strong></span>
    @endif
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'عنوان:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-12">
    {!! Form::label('text', 'متن:') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'مدت(دقیقه):') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('size', 'حجم:') !!}
    {!! Form::text('size', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('productPosts.index') !!}" class="btn btn-default">لغو</a>
</div>

@push('scripts')

    <script src="{{asset('bower_components/tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({
            selector: 'textarea',
            language: 'fa_IR',
            directionality: 'rtl',

            width: 800,
            height: 400,

            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",

            menubar: false,

            default_link_target: "_blank",
            media_live_embeds: true,

            file_picker_types: 'file image media',
            file_browser_callback_types: 'file image media',

            // theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern "
            ],
            toolbar1: "newdocument insertfile undo redo | styleselect | bold italic | hr | charmap | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
            toolbar2: "link image media | forecolor backcolor emoticons | fontsizeselect | visualblocks | table | searchreplace | fullscreen",

            image_advtab: true,

            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Image Responsive', value: 'img-responsive'}
            ],

            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],

            file_browser_callback: elFinderBrowser,

            relative_urls: false,
            convert_urls: false,
            browser_spellchek: false,
            fix_list_elements: true,
            entity_encoding: "row",
            keep_styles: false,
            preview_styles: "font-family font-size font-weight font-style text-decoration text-transform"
        });

        function elFinderBrowser(field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
                title: 'elFinder 2.0',
                width: 900,
                height: 450,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
            return false;
        }

    </script>

@endpush