<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره:') !!}
    <p>{!! $productPost->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'پست آموزشی:') !!}
    <p>{!! $productPost->product_id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'عنوان:') !!}
    <p>{!! $productPost->title !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'متن:') !!}
    <p>{!! $productPost->text !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('duration', 'مدت:') !!}
    <p>{!! $productPost->duration !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('size', 'زمان:') !!}
    <p>{!! $productPost->size !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $productPost->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ ویرایش:') !!}
    <p>{!! $productPost->updated_at !!}</p>
</div>

