@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Teaching Request
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($teachingRequest, ['route' => ['teachingRequests.update', $teachingRequest->id], 'method' => 'patch']) !!}

                        @include('teaching_requests.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection