@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">
                <h6 style="line-height: 100px;">درخواست برداشت از درآمدهای حاصل از فروش</h6>

                <div class="add_course">

                    {!! Form::open(['route'=>['financialWithdraw' ], 'method' => 'POST', ]) !!}

                    <label for="amount">میزان مبلغ درخواستی (بر حسب تومان)</label>
                    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
                    @if ($errors->has('amount'))
                        <span class="help-block"><strong>{{ $errors->first('amount') }}</strong></span>
                    @endif
                    <input type="submit" value="ثبت درخواست">

                    {!! Form::close() !!}

                </div>
                <div class="clear"></div>
                <div class="add_lecture">
                    <h6>لیست دریافتی های شما</h6>
                    <table cellspacing="0" style="margin-top: 30px;">
                        <tr style="height: 30px; background: #4fbaff;color: #fff;">
                            <td style="width: 10%">ردیف</td>
                            <td style="width: 30%">عنوان</td>
                            <td style="width: 10%">تاریخ</td>
                            <td style="width: 20%">مبلغ درخواستی</td>
                            <td style="width: 20%">رسید</td>
                            <td style="width: 10%">وضعیت</td>
                        </tr>
                        @php
                            $i = 1;
                        @endphp
                        @foreach($balanceHistories as $balanceHistory)
                            <tr style="height: 30px;">
                                <td style="width: 10%">{{$i++}}</td>
                                <td style="width: 30%">{{$balanceHistory->discription}}</td>
                                <td style="width: 10%">{{pDate($balanceHistory->created_at, true)}}</td>
                                <td style="width: 20%">
                                    {{$balanceHistory->old_balance - $balanceHistory->new_balance}}
                                    تومان
                                </td>
                                <td style="width: 20%">{{$balanceHistory->id}}</td>
                                <td style="width: 10%">{{$balanceHistory->typeStr}}</td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>

        @include('partials.footer')

    </div>


@endsection