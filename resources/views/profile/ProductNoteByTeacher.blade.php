@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">

            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>

            <div class="teacher_courses">
                <h6 style="line-height: 100px;">افزودن اطلاعیه جدید برای دوره : آموزش طراحی سایت</h6>

                <div class="add_course">

                    {!! Form::open(['route'=>['ProductNotesByTeacher', $product->id ], 'method' => 'post' ]) !!}

                    <!— فیلد عنوان فایل —>

                        <div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">
                            {!! Form::label('title', 'عنوان اطلاعیه') !!}
                            {!! Form::text('title', null, ['class' => 'form-control' , 'id' => 'title']) !!}
                            @if ($errors->has('title'))
                                <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group ">
                            {!! Form::label('text', 'متن اطلاعیه') !!}
                            {!! Form::textarea('text') !!}
                            @if ($errors->has('text'))
                                <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                            @endif
                        </div>

                        <input type="submit" value="ثبت اطلاعیه">


                    {!! Form::close() !!}

                </div>
                <div class="clear"></div>
                <div class="add_lecture">
                    <h6>لیست اطلاعیه های منتشر شده تا کنون : </h6>
                    <table cellspacing="0" style="margin-top: 30px;">
                        <tr style="height: 30px; background: #4fbaff;color: #fff;">
                            <td style="width: 5%">ردیف</td>
                            <td style="width: 50%;text-align: right;">عنوان اطلاعیه</td>
                            <td style="width: 10%">تاریخ انتشار</td>
                            <td style="width: 15%">وضعیت</td>
                            <td style="width: 10%">ویرایش</td>
                            <td style="width: 10%">حذف</td>
                        </tr>
                        @php
                        $i = 1;
                        @endphp
                        @foreach($productNotes as $productNote)
                            <tr style="height: 30px;">
                                <td style="width: 5%">{{$i++}}</td>
                                <td style="width: 50%;text-align: right">{{$productNote->tile}}</td>
                                <td style="width: 10%">{{pDate($productNote->created_at, true)}}</td>
                                <td style="width: 15%">--منتشر شده--</td>
                                <td style="width: 10%"><a href="#">--ویرایش--</a></td>
                                <td style="width: 10%">
                                    {!! Form::open(['route' => ['productNotes.destroy', $productNote->id], 'method' => 'delete']) !!}
                                    {{--                                    {{route('productFiles.destroy')}}--}}
                                    <div class='btn-group'>
                                        {!! Form::button('حذف', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'onclick' => "return confirm('آیا اطمینان دارید؟')"
                                        ]) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
        @include('partials.footer')

    </div>
@endsection
@push('scripts')
<script src="{{asset('bower_components/tinymce/tinymce.min.js')}}"></script>
<script>tinymce.init({
        selector: 'textarea',
        language: 'fa_IR',
        directionality: 'rtl',

        width: 800,
        height: 250,

        fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",

        menubar: false,

        default_link_target: "_blank",
        media_live_embeds: true,
        theme: "modern",

        toolbar1: "alignleft aligncenter alignright alignjustify |  bullist numlist outdent indent  | bold italic | fontsizeselect ",

        image_advtab: true,

        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Image Responsive', value: 'img-responsive'}
        ],

        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],

        relative_urls: false,
        convert_urls: false,
        browser_spellchek: false,
        fix_list_elements: true,
        entity_encoding: "row",
        keep_styles: false,
        preview_styles: "font-family font-size font-weight font-style text-decoration text-transform"
    });
</script>
@endpush

{{--@extends('layouts.index')--}}
{{--@push('header')--}}
    {{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
    {{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">--}}
{{--@endpush--}}
{{--@section('content')--}}
    {{--<section class="content-header">--}}
        {{--<h1>--}}
            {{--مدیریت اطلاعیه های محصول--}}
            {{--{{$product->title}}--}}
        {{--</h1>--}}
    {{--</section>--}}
    {{--<div class="content">--}}

        {{--@include('flash::message')--}}
        {{--@include('adminlte-templates::common.errors')--}}

        {{--<div class="box box-primary">--}}

            {{--<div class="box-body " style="padding-right: 20px">--}}
                {{--<div class="row">--}}

                    {{--{!! Form::open(['route'=>['ProductNotesByTeacher', $product->id ], 'method' => 'post' ]) !!}--}}

                    {{--<!— فیلد عنوان فایل —>--}}
                    {{--<div class="col-sm-3">--}}
                        {{--<div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">--}}
                            {{--{!! Form::label('title', 'عنوان اطلاعیه') !!}--}}
                            {{--{!! Form::text('title', null, ['class' => 'form-control' , 'id' => 'title']) !!}--}}
                            {{--@if ($errors->has('title'))--}}
                                {{--<span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>--}}
                            {{--@endif--}}
                        {{--</div>--}}

                        {{--<div class="form-group ">--}}
                            {{--{!! Form::label('text', 'متن اطلاعیه') !!}--}}
                            {{--{!! Form::textarea('text') !!}--}}
                            {{--@if ($errors->has('text'))--}}
                                {{--<span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>--}}
                            {{--@endif--}}
                        {{--</div>--}}

                        {{--<div class="form-group ">--}}
                            {{--<button type="submit" class="btn btn-success">انتشار</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--{!! Form::close() !!}--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<hr>--}}

        {{--<div class="box box-primary">--}}
            {{--<div class="box-body " style="padding-right: 20px">--}}
                {{--<div class="row">--}}

                    {{--<table class="table table-hover">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>عنوان</th>--}}
                            {{--<th>حذف</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--@foreach($productNotes as $productNote)--}}
                            {{--<tr>--}}
                                {{--<td>{{$productNote->title}}</td>--}}
                                {{--<td>--}}
                                    {{--{!! Form::open(['route' => ['productNotes.destroy', $productNote->id], 'method' => 'delete']) !!}--}}
                                    {{--                                    {{route('productFiles.destroy')}}--}}
                                    {{--<div class='btn-group'>--}}
                                        {{--{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [--}}
                                            {{--'type' => 'submit',--}}
                                            {{--'class' => 'btn btn-danger btn-xs',--}}
                                            {{--'onclick' => "return confirm('Are you sure?')"--}}
                                        {{--]) !!}--}}
                                    {{--</div>--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}


                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
{{--@endsection--}}
{{--@push('scripts')--}}
    {{--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
{{--@endpush--}}