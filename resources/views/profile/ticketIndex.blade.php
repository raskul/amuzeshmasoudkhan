@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">

                <div class="add_course_area">
                    <div class="add_course">
                        <a href="{{route('ticketCreate')}}">
                            ارسال تیکت جدید
                        </a>
                    </div>
                </div>
                <br><br>
                <br><br>

                <section class="content-header">
                    <h1>
                        لیست تیکت ها
                    </h1>
                </section>
                <div class="content">

                    <div class="box box-primary">

                        <div class="box-body " style="padding-right: 40px">
                            <div class="row">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th style="max-width: 10px">شماره تیکت</th>
                                        <th>عنوان تیکت</th>
                                        <th>آخرین پاسخ توسط</th>
                                        <th>تاریخ ارسال</th>
                                        <th style="max-width: 10px">عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tickets as $ticket)
                                        <tr>
                                            <td scope="row">{{ $ticket->id }}</td>
                                            <td><a href="{{route('ticketComments', $ticket->id)}}">{{ $ticket->title }}</a></td>
                                            <td>{{$ticket->who_answered}}</td>
                                            <td>{{ pDate($ticket->created_at, true) }}</td>
                                            <td><a href="{{route('ticketComments', $ticket->id)}}" class="btn btn-default">مشاهده</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include('partials.footer')
    </div>

@endsection
