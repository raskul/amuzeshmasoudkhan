@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">
                <h6 style="line-height: 100px;">شارژ کیف پول</h6>

                <div class="add_course">
                    {!! Form::open(['route'=>['increase.balance.post' ], 'method' => 'POST' ]) !!}

                        <label for="title">میزان مبلغ شارژ کیف پول (بر حسب تومان)</label>
                    {!! Form::text('amount', null, ['class' => 'form-control']) !!}

                        <input type="submit" value="پرداخت هزینه">

                    {!! Form::close() !!}

                </div>
                <div class="clear"></div>
                <div class="add_lecture">
                    <h6>تغییرات کیف پول شما</h6>
                    <table cellspacing="0" style="margin-top: 30px;">
                        <tr style="height: 30px; background: #4fbaff;color: #fff;">
                            <td style="width: 10%">ردیف</td>
                            <td  style="width: 20%">تاریخ</td>
                            <td  style="width: 20%">مبلغ شارژ</td>
                            <td  style="width: 20%">رسید</td>
                            <td  style="width: 20%">وضعیت</td>
                        </tr>
                        <tr style="height: 30px;">
                            <td style="width: 10%">1</td>
                            <td  style="width: 20%">12 بهمن 1392</td>
                            <td  style="width: 20%">35000 تومان</td>
                            <td  style="width: 20%">6556203</td>
                            <td  style="width: 20%">پرداخت موفق</td>
                        </tr>
                        <tr style="height: 30px;">
                            <td style="width: 10%">1</td>
                            <td  style="width: 20%">12 بهمن 1392</td>
                            <td  style="width: 20%">35000 تومان</td>
                            <td  style="width: 20%">6556203</td>
                            <td  style="width: 20%">پرداخت موفق</td>
                        </tr>
                        <tr style="height: 30px;">
                            <td style="width: 10%">1</td>
                            <td  style="width: 20%">12 بهمن 1392</td>
                            <td  style="width: 20%">35000 تومان</td>
                            <td  style="width: 20%">6556203</td>
                            <td  style="width: 20%">پرداخت موفق</td>
                        </tr>
                        <tr style="height: 30px;">
                            <td style="width: 10%">1</td>
                            <td  style="width: 20%">12 بهمن 1392</td>
                            <td  style="width: 20%">35000 تومان</td>
                            <td  style="width: 20%">6556203</td>
                            <td  style="width: 20%">پرداخت موفق</td>
                        </tr>



                    </table>
                </div>
            </div>
        </div>

        @include('partials.footer')

    </div>

@endsection
















            @extends('layouts.index')
@section('header')
    {{--<link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')

    @include('partials.miniHeader')

    <section class="content-header">
        <h1>
            افزایش موجودی
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">
            <div class="box-body " style="padding-right: 20px">
                <div class="row">


                    <div class="form-group col-sm-3 {{ $errors->has('amount') ? ' has-error' : '' }}">
                        {!! Form::label('amount', 'مبلغی که میخواهید حساب شما افزایش پیدا کند را بنویسید:') !!}
                        {!! Form::text('amount', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('amount'))
                            <span class="help-block"><strong>{{ $errors->first('amount') }}</strong></span>
                        @endif
                        <br>
                        <button class="btn btn-success">افزاش موجودی</button>
                    </div>

                    <div class="col-sm-3" style="padding-top: 45px">
                        <span style=" font-size: x-large">تومان</span>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>

    </div>

    @include('partials.miniFooter')

@endsection



