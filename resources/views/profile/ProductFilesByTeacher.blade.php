@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">

            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>

                <div class="teacher_courses">
                    <h6 style="line-height: 100px;">افزودن فایل ضمیمه جدید برای دوره :
                        {{$product->title}}
                    </h6>

                    <div class="add_course">

                    {!! Form::open(['route'=>['ProductFilesByTeacher', $product->id ], 'method' => 'post', 'files' => true ]) !!}

                    <!— فیلد عنوان فایل —>

                        <div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">
                            {!! Form::label('title', 'عنوان فایل') !!}
                            {!! Form::text('title', null, ['class' => 'form-control' , 'id' => 'title']) !!}
                            @if ($errors->has('title'))
                                <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group ">
                            {!! Form::label('file', 'انتخاب فایل') !!}
                            {!! Form::file('file') !!}
                            @if ($errors->has('file'))
                                <span class="help-block"><strong>{{ $errors->first('file') }}</strong></span>
                            @endif
                        </div>

                        <input type="submit" value="آپلود فایل این درس">


                        {!! Form::close() !!}


                    </div>
                    <div class="clear"></div>
                    <div class="add_lecture">
                        <h6>لیست فایل های آپلود شده تا کنون : </h6>
                        <table cellspacing="0" style="margin-top: 30px;">
                            <tr style="height: 30px; background: #4fbaff;color: #fff;">
                                <td style="width: 5%">ردیف</td>
                                <td style="width: 50%;text-align: right;">عنوان فایل</td>
                                <td style="width: 10%">تاریخ انتشار</td>
                                <td style="width: 5%">وضعیت</td>
                                <td style="width: 10%">حجم فایل</td>
                                <td style="width: 10%">ویرایش</td>
                                <td style="width: 10%">حذف</td>
                            </tr>


                            @foreach($productFiles as $productFile)
                                <tr style="height: 30px;">
                                    <td style="width: 5%">1</td>
                                    <td style="width: 50%;text-align: right">{{$productFile->title}}</td>
                                    <td style="width: 10%">{{pDate($productFile->create_at, true)}}</td>
                                    <td style="width: 15%">--منتشر شده--</td>
                                    <td style="width: 10%">
                                        {{number_format ($productFile->size/1000, 0, ".", "")}}
                                        KB
                                    </td>
                                    <td style="width: 5%"><a href="#">---ویرایش---</a></td>
                                    <td style="width: 5%">
                                    {!! Form::open(['route' => ['productFiles.destroy', $productFile->id], 'method' => 'delete']) !!}
                                    <div class='btn-group'>
                                        {!! Form::button('حذف', [
                                            'type' => 'submit',
                                            'class' => '',
                                            'onclick' => "return confirm('آیا اطمینان دارید؟')"
                                        ]) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>

        </div>
        </div>

        @include('partials.footer')

    </div>

@endsection


{{--@extends('layouts.index')--}}
{{--@push('header')--}}
{{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
{{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">--}}
{{--@endpush--}}
{{--@section('content')--}}
{{--<section class="content-header">--}}
{{--<h1>--}}
{{--مدیریت فایل های ضمیمه محصول--}}
{{--{{$product->title}}--}}
{{--</h1>--}}
{{--</section>--}}
{{--<div class="content">--}}

{{--@include('flash::message')--}}
{{--@include('adminlte-templates::common.errors')--}}

{{--<div class="box box-primary">--}}

{{--<div class="box-body " style="padding-right: 20px">--}}
{{--<div class="row">--}}


{{--{!! Form::open(['route'=>['ProductFilesByTeacher', $product->id ], 'method' => 'post', 'files' => true ]) !!}--}}

{{--<!— فیلد عنوان فایل —>--}}
{{--<div class="col-sm-3">--}}
{{--<div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">--}}
{{--{!! Form::label('title', 'عنوان فایل') !!}--}}
{{--{!! Form::text('title', null, ['class' => 'form-control' , 'id' => 'title']) !!}--}}
{{--@if ($errors->has('title'))--}}
{{--<span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>--}}
{{--@endif--}}
{{--</div>--}}

{{--<div class="form-group ">--}}
{{--{!! Form::label('file', 'انتخاب فایل') !!}--}}
{{--{!! Form::file('file') !!}--}}
{{--@if ($errors->has('file'))--}}
{{--<span class="help-block"><strong>{{ $errors->first('file') }}</strong></span>--}}
{{--@endif--}}
{{--</div>--}}

{{--<div class="form-group ">--}}
{{--<button type="submit" class="btn btn-success">ذخیره</button>--}}
{{--</div>--}}
{{--</div>--}}

{{--{!! Form::close() !!}--}}

{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<hr>--}}

{{--<div class="box box-primary">--}}
{{--<div class="box-body " style="padding-right: 20px">--}}
{{--<div class="row">--}}

{{--<table class="table table-hover">--}}
{{--<thead>--}}
{{--<tr>--}}
{{--<th>عنوان</th>--}}
{{--<th>حذف</th>--}}
{{--</tr>--}}
{{--</thead>--}}
{{--<tbody>--}}
{{--@foreach($productFiles as $productFile)--}}
{{--<tr>--}}
{{--<td>{{$productFile->title}}</td>--}}
{{--<td>--}}
{{--{!! Form::open(['route' => ['productFiles.destroy', $productFile->id], 'method' => 'delete']) !!}--}}
{{--<div class='btn-group'>--}}
{{--{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [--}}
{{--'type' => 'submit',--}}
{{--'class' => 'btn btn-danger btn-xs',--}}
{{--'onclick' => "return confirm('Are you sure?')"--}}
{{--]) !!}--}}
{{--</div>--}}
{{--{!! Form::close() !!}--}}
{{--</td>--}}
{{--</tr>--}}
{{--@endforeach--}}
{{--</tbody>--}}
{{--</table>--}}


{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}

{{--@endsection--}}
{{--@push('scripts')--}}
{{--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
{{--<script>--}}
{{--$('#title').val('');--}}
{{--</script>--}}
{{--@endpush--}}