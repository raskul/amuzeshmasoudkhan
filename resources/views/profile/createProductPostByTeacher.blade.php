@php
        $email = auth()->user()->email;
        \Config::set('elfinder.roots.0.path', storage_path()
            . DIRECTORY_SEPARATOR . 'app'
            . DIRECTORY_SEPARATOR . 'public'
            . DIRECTORY_SEPARATOR . 'files'
            . DIRECTORY_SEPARATOR . "$email");

@endphp
@extends('layouts.index')
@push('header')
    <style>
        .tablink {
            background-color: #555;
            color: white;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 50%;
        }

        .tablink:hover {
            background-color: #777;
        }

        /* Style the tab content */
        .tabcontent {
            color: white;
            display: none;
            padding: 50px;
            text-align: center;
        }


        #Tokyo {background-color: #cec3ff;}
        #Oslo {background-color:orange;}
    </style>

@endpush
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>

            <div class="teacher_courses">
                <h6 style="line-height: 100px;">افزودن درس جدید برای دوره :
                    {{$product->title}}
                </h6>

                <div class="add_course">


                    {!! Form::open(['route'=>['createProductPostByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title).(isset($productPost)?'/'.$productPost->id:null) ], 'method' => 'post', 'files' => true ]) !!}

                    <div class="form-group col-sm-8 col-sm-offset-2 {{ $errors->has('title') ? ' has-error' : '' }}">
                        {!! Form::label('title', 'عنوان جلسه') !!}
                        {!! Form::text('title', (isset($productPost->title)?$productPost->title:null), ['class' => 'form-control', 'required' => true ]) !!}
                        @if ($errors->has('title'))
                            <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                        @endif
                    </div>

                    <br>

                    <p>نوع آموزش فیلم آموزشی است یا عکس و متن؟</p>

                    <button type="button" class="tablink" onclick="openCity('Tokyo', this, '#cec3ff')">قرار دادن عکس و متن</button>
                    <button type="button" class="tablink" onclick="openCity('Oslo', this, 'orange')" id="defaultOpen">آپلود ویدئو</button>

                    <div id="Oslo" class="tabcontent">
                        <div class="form-group {{ $errors->has('video') ? ' has-error' : '' }}">
                            {!! Form::label('video', 'آپلود ویدئو') !!}
                            {!! Form::file('video') !!}
                            @if ($errors->has('video'))
                                <span class="help-block"><strong>{{ $errors->first('video') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div id="Tokyo" class="tabcontent">
                        <div class="form-group col-sm-8 col-sm-offset-2 {{ $errors->has('text') ? ' has-error' : '' }}">
                            {!! Form::label('text', 'توضیحات کامل درس و قرار دادن عکس و متن') !!}
                            {!! Form::textarea('text', (isset($productPost->text)?$productPost->text:null), ['class' => 'form-control', 'id' => 'editorcm', 'dir' => 'rtl']) !!}
                            @if ($errors->has('text'))
                                <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <br>

                    <label for="" style="display: inline">مدت زمان جلسه (دقیقه):</label>
                    <select name="duration" id="" style="width: 170px; display: inline">
                        <option value="1">1 دقیقه</option>
                        <option value="5">5 دقیقه</option>
                        <option value="10" selected >10 دقیقه</option>
                        <option value="20">20 دقیقه</option>
                        <option value="40">40 دقیقه</option>
                        <option value="60">1 ساعت</option>
                        <option value="90">1 ساعت و نیم</option>
                        <option value="120">2 ساعت</option>
                        <option value="180">3 ساعت</option>
                        <option value="240">4 ساعت</option>
                        <option value="300">5 ساعت</option>
                    </select>

                    <label for="" style="display:inline;">نوع نمایش جلسه</label>
                    <select name="show_type" id="" style="width: 170px; display: inline">
                        <option value="0">برای تست</option>
                        <option value="1" selected>فقط دانشجویان ببنند</option>
                    </select>

                    <input type="submit" value="ثبت درس" style="width: 200px; margin-top: 0px">
                    <div class="clear"></div>
                    <hr>
                    <div class="col-sm-8 col-sm-offset-2" style="margin-top: 20px">
                        <span style="color: red">*</span>
                        <span style="color: grey; font-size: x-small">متن جلسه میتواند شامل یک ویدیو یا چندین عکس و متن باشد</span>
                    </div>

                    <div class="col-sm-8 col-sm-offset-2" >
                        <span style="color: red">*</span>
                        <span style="color: grey; font-size: x-small">نوع نمایش جلسه اگر (برای تست) باشد همه بازدیدکنندگان جلسه را بدون ثبت نام میبینند. معمولا مدرسان برای تبلیغ نوع نمایش یک یا دو جلسه اول را برای تست انتخاب میکنند.</span>
                    </div>

                    {!! Form::close() !!}
                </div>


                <div class="clear"></div>
                <div class="add_lecture">
                    <h6>لیست درس های منتشر شده تا کنون : </h6>
                    <table cellspacing="0" style="margin-top: 30px;">
                        <tr style="height: 30px; background: #4fbaff;color: #fff;">
                            <td style="width: 5%">ردیف</td>
                            <td style="width: 50%;text-align: right;">عنوان درس</td>
                            <td style="width: 10%">تاریخ انتشار</td>
                            <td style="width: 5%">وضعیت</td>
                            <td style="width: 10%">مدت زمان</td>
                            <td style="width: 10%">ویرایش</td>
                            <td style="width: 10%">حذف</td>
                        </tr>

                        @php
                            $i = 1;
                        @endphp
                        @foreach($product->productPosts as $post)
                            <tr style="height: 30px;">
                                <td style="width: 5%">{{$i++}}</td>
                                <td style="width: 50%;text-align: right">{{$post->title}}</td>
                                <td style="width: 10%">{{pDate($post->created_at, false, false, true, true)}}</td>
                                <td style="width: 15%">
                                    {{--                                    {{$post->statusStr}}--}}
                                </td>
                                <td style="width: 10%">
                                    {{$post->duration}}
                                    دقیقه
                                </td>
                                <td style="width: 5%"><a
                                            href="{{ route('createProductPostByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title) )}}/{{$post->id}}">ویرایش</a>
                                </td>
                                <td style="width: 5%">
                                    {!! Form::open(['route'=>['createProductPostByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title) . '/' . $post->id ], 'method' => 'DELETE' ]) !!}
                                    {!! Form::button('حذف', [
                                            'type' => 'submit',
                                            'class' => '',
                                            'onclick' => "return confirm('آیا اطمینان دارید؟')"
                                        ]) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>


            </div>
        </div>

        @include('partials.footer')

    </div>

@endsection
@push('scripts')

    <script src="{{asset('bower_components/tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({
            selector: 'textarea',
            language: 'fa_IR',
            directionality: 'rtl',

            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",

            // menubar: true,

            default_link_target: "_blank",
            media_live_embeds: true,

            file_picker_types: 'file image media',
            file_browser_callback_types: 'file image media',

            // theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern "
            ],
            // toolbar1: "newdocument insertfile undo redo | styleselect | bold italic | hr | charmap | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
            // toolbar2: "link | forecolor backcolor emoticons | fontsizeselect | visualblocks | table | searchreplace | fullscreen",
            toolbar1: "image | media",

            image_advtab: true,

            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Image Responsive', value: 'img-responsive'}
            ],

            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],

            file_browser_callback : elFinderBrowser,

            relative_urls : false,
            convert_urls: false,
            browser_spellchek: false,
            fix_list_elements: true,
            entity_encoding: "row",
            keep_styles: false,
            preview_styles: "font-family font-size font-weight font-style text-decoration text-transform"
        });

        function elFinderBrowser(field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
                title: 'elFinder 2.0',
                width: 900,
                height: 800,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
            return false;
        }

    </script>

    <script>
        function openCity(cityName,elmnt,color) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].style.backgroundColor = "";
            }
            document.getElementById(cityName).style.display = "block";
            elmnt.style.backgroundColor = color;

        }
        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>


@endpush
