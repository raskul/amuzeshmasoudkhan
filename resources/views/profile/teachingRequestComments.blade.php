@extends('layouts.index')
@section('content')
    <div id="main">

        @include('partials.header')

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">

                @include('flash::message')
                @include('adminlte-templates::common.errors')

                <div style="font-size: xx-large;margin-top: 40px;margin-bottom: 40px">
                    درخواست تدریس
                </div>
                <div class="add_course_area">
                    <div class="add_course">
                        <a href="{{route('teachingIndex')}}">بازگشت</a>
                    </div>
                </div>

                <div class="">
                    <table class="">
                        <thead>
                        <tr>
                            <th>شماره درخواست</th>
                            <th>عنوان</th>
                            <th>نام</th>
                            <th>ایمیل</th>
                            <th>تاریخ ایجاد</th>
                            <th>وضعیت</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$teachingRequest->id}}</td>
                            <td>{{$teachingRequest->title}}</td>
                            <td>{{$teachingRequest->user->name}}</td>
                            <td>{{$teachingRequest->user->email}}</td>
                            <td>{{pDate($teachingRequest->created_at, true )}}</td>
                            <td>{{$teachingRequest->statusName}}</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <span><strong>متن:</strong></span>
                                <br>
                                {!!  $teachingRequest->description!!}
                            </td>
                        </tr>
                        @if (isset($teachingRequest->files))
                            <tr>
                                <td colspan="6">
                                    فایل های ضمیمه:
                                    <br>
                                    @foreach(unserialize($teachingRequest->files) as $file)
                                        @php
                                            $file = substr($file, 24);
                                            $sendFile = str_replace('/', ':', $file);
                                        @endphp
                                        <a href="{{ route('downloadTeachingRequestFiles', $sendFile) }}">
                                            {{--<a href="{{ route('downloadFile', $file) }}" target="_blank">--}}
                                            <span class="btn btn-primary"
                                                  style="margin: 0 0 5px 5px; padding: 0">
                                                    {{ substr(strrev(substr(strrev($file), 0,strpos(strrev($file), '/'))), 10) }}
                                                    </span>
                                        </a>
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

                @foreach($teachingRequestComments as $comment)
                    <div class=""
                         style="background-color:white; margin: 0 50px 0 50px;padding-top: 10px; border-bottom: 1px solid #F5F5F5">
                        <div class="col-sm-1">
                            <img class="img-circle" src="{{showUserImage($comment->user->id)}}"
                                 alt=""
                                 height="40px" width="40px">
                        </div>
                        <div class="">
                            <div>
                                <div class="btn-default btn" style="padding: 0 5px 0 5px ;color: black;">
                                    <strong>{{$comment->user->name}}</strong>
                                </div>
                                <div class="btn btn-warning pull-left" style="padding: 0 5px 0 5px">
                                    {{pDate($comment->created_at, false, true)}}
                                </div>
                            </div>
                            <div style="padding: 10px 40px 10px 40px">{!! $comment->text !!}</div>
                        </div>
                    </div>
                @endforeach

                <hr>
                {!! Form::open(['route'=>['teachingRequestComments', $teachingRequest->id], 'method' => 'post', 'files' => true ]) !!}
                <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                    {!! Form::label('text', 'متن پیام:') !!}
                    {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
                    @if ($errors->has('text'))
                        <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                    @endif
                </div>
                <div>
                    <label for="file">فایل ضمیمه</label>
                    <input type="file" name="file" id="file">
                </div>
                <input type="submit" value="ارسال پیام">

                {!! Form::close() !!}

            </div>
        </div>

        @include('partials.miniFooter')

    </div>

@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
            $('html, body').animate({
                scrollTop: $("#myDiv").offset().top
            }, 2000);
        });
    </script>
@endpush