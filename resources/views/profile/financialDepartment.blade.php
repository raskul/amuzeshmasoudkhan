@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>

            <div class="user_courses">
                <div class="mojudi">
                    <div class="text">موجودی کیف پول شما :</div>
                    <div class="money">
                        {{$user->balance}}
                        تومان
                    </div>
                </div>

                <h6 style="line-height: 100px;">لیست خریدها ، دریافت ها و تغییرات کیف پول شما</h6>
                <label for="">انتخاب نوع تراکنش</label>
                <div class="user_filter">

                    {!! Form::open(['route'=>['financialDepartment'], 'method' => 'GET' ]) !!}
                    <select name="filter" id=""
                            style="width: 30%;height: 35px;background: #eee;border: 1px solid #ccc; margin: 15px 0;">
                        <option value="" selected>انتخاب کنید</option>
                        <option class="kharid_man" value="kharid_man"
                                @if(isset($kharid_man)) selected @endif >خریدهای من
                        </option>
                        <option class="forush_man" value="forush_man"
                                @if(isset($forush_man)) selected @endif >فروش های من
                        </option>
                        <option class="daryafti_man" value="daryafti_man"
                                @if(isset($daryafti_man)) selected @endif >دریافتی های من از فروش
                        </option>
                        <option class="kif_pul" value="kif_pul"
                                @if(isset($kif_pul)) selected @endif>تغییرات کیف پول
                        </option>
                    </select>

                    <input type="submit" value="جست و جو" style="height: 35px;width: 100px;">

                    {!! Form::close() !!}

                </div>

                <div class="credit">
{{--                    <div class="sharj"><a href="{{route('increase.balance.get')}}">شارژ کیف پول</a></div>--}}
                    <div class="tasviye"><a href="{{route('financialWithdraw')}}">درخواست تسویه</a></div>
                </div>
                @php
                    $i = 1;
                @endphp
                <table class="my_purchases" cellspacing="0">
                    <tr style="background: #00c2e5;">
                        <td style="width: 10%">ردیف</td>
                        <td style="width: 40%;">عنوان</td>
                        <td style="width: 10%">تاریخ</td>
                        <td style="width: 15%">مبلغ</td>
                        {{--<td style="width: 10%">رسید</td>--}}
                        <td style="width: 15%">عنوان</td>
                    </tr>
                    @if(isset($kharid_man))
                        @foreach($kharid_man as $item)
                            <tr>
                                <td style="width: 10%">{{$i++}}</td>
                                @php
                                    $factorTemp = $item->factor()->first();
                                    $detailTemp = unserialize($factorTemp->detail);
                                    $productTemp = \App\Models\Product::find($detailTemp[0]['product_id']);
                                @endphp
                                <td style="width: 40%;">{{$productTemp->title}}</td>
                                <td style="width: 10%;">{{pDate($factorTemp->created_at, true)}}</td>
                                <td style="width: 15%">
                                    {{$detailTemp[0]['product_pay_price']}}
                                    تومان
                                </td>
                                {{--<td style="width: 10%">2569526652</td>--}}
                                <td style="width: 15%">پرداخت شده</td>
                            </tr>
                        @endforeach
                    @endif

                    @if(isset($forush_man))
                        @foreach($forush_man as $item)
                            <tr>
                                <td style="width: 10%">{{$i++}}</td>
                                @php
                                    $factorTemp = $item->factor()->first();
                                    $detailTemp = unserialize($factorTemp->detail);
                                    $productTemp = \App\Models\Product::find($detailTemp[0]['product_id']);
                                @endphp
                                <td style="width: 40%;">{{$productTemp->title}}</td>
                                <td style="width: 10%;">{{pDate($factorTemp->created_at, true)}}</td>
                                <td style="width: 15%">
                                    {{$detailTemp[0]['product_pay_price']}}
                                    تومان
                                </td>
                                {{--<td style="width: 10%">2569526652</td>--}}
                                <td style="width: 15%">پرداخت شده</td>
                            </tr>
                        @endforeach
                    @endif

                    @if(isset($daryafti_man))
                        @foreach($daryafti_man as $item)
                            <tr>
                                <td style="width: 10%">{{$i++}}</td>
                                @php
                                    $factorTemp = $item->factor()->first();
                                    $detailTemp = unserialize($factorTemp->detail);
                                    $productTemp = \App\Models\Product::find($detailTemp[0]['product_id']);
                                @endphp
                                <td style="width: 40%;">{{$productTemp->title}}</td>
                                <td style="width: 10%;">{{pDate($factorTemp->created_at, true)}}</td>
                                <td style="width: 15%">
                                    {{$detailTemp[0]['teacher_commission_money']}}
                                    تومان
                                </td>
                                {{--<td style="width: 10%">2569526652</td>--}}
                                <td style="width: 15%">پرداخت شده</td>
                            </tr>
                        @endforeach
                    @endif

                    @if(isset($kif_pul))
                        @foreach($kif_pul as $item)

                            <tr>
                                <td style="width: 10%">{{$i++}}</td>
                                <td style="width: 40%;">{{$item->description}}</td>
                                <td style="width: 10%;">{{$item->created_at}}</td>
                                <td style="width: 15%">
                                    {{$item->new_balance - $item->old_balance}}
                                    تومان
                                </td>
                                {{--<td style="width: 10%">2569526652</td>--}}
                                <td style="width: 15%">{{$item->typeStr}}</td>
                            </tr>
                        @endforeach
                    @endif

                </table>

            </div>
        </div>

        @include('partials.footer')

    </div>

@endsection
@push('scripts')
    <script>
        $(".kharid_man").click(function () {
            $("table").hide();
            $(".my_purchases").fadeIn(200);
        });
        $(".forush_man").click(function () {
            $("table").hide();
            $(".my_sales").fadeIn(200);
        });
        $(".daryafti_man").click(function () {
            $("table").hide();
            $(".my_receipts").fadeIn(200);
        });
        $(".kif_pul").click(function () {
            $("table").hide();
            $(".kif_pol").fadeIn(200);
        });
    </script>
@endpush