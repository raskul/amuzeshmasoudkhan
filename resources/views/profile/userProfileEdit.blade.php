@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_profile">
                <div class="user_img" style="position: absolute">
                    <img style="width: 150px;height: 150px;border-radius: 100%;" src="{{showUserImage($user->id)}}"
                         alt="">
                </div>
                <div class="user_profile" style="width: 800px; height: auto; margin: 0 auto">

                    {!! Form::model($user, ['route'=>['profile.post' ], 'method' => 'post', 'files' => true, 'autocomplete' => 'off' ]) !!}

                    <h6>اطلاعات حساب کاربری</h6>

                    <label for="">تغییر تصویر پروفایل من</label>
                    {{Form::file('image')}}


                    <div class=" {{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'نام:') !!}
                        {!! Form::text('name', (isset($user->name)?$user->name:null)) !!}
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>

                    <div class=" {{ $errors->has('password') ? ' has-error' : '' }}">
                        {!! Form::label('password', 'رمزعبور:') !!}
                        {!! Form::password('password', null, ['value' => 'qeqwr']) !!}
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>

                    <div class=" {{ $errors->has('password_retype') ? ' has-error' : '' }}">
                        {!! Form::label('password_retype', 'تکرار رمز عبور:') !!}
                        {!! Form::password('password_retype', null) !!}
                        @if ($errors->has('password_retype'))
                            <span class="help-block"><strong>{{ $errors->first('password_retype') }}</strong></span>
                        @endif
                    </div>


                    <h6>اطلاعات شخصی</h6>

                    <!-- فیلد سن -->
                    <div class=" {{ $errors->has('age') ? ' has-error' : '' }}">
                        {!! Form::label('age', 'سن') !!}
                        {!! Form::text('age', null) !!}
                        @if ($errors->has('age'))
                            <span class="help-block"><strong>{{ $errors->first('age') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد شغل -->
                    <div class=" {{ $errors->has('job') ? ' has-error' : '' }}">
                        {!! Form::label('job', 'شغل') !!}
                        {!! Form::text('job', null) !!}
                        @if ($errors->has('job'))
                            <span class="help-block"><strong>{{ $errors->first('job') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد آخرین مدرک تحصیلی -->
                    <div class=" {{ $errors->has('education') ? ' has-error' : '' }}">
                        {!! Form::label('education', 'آخرین مدرک تحصیلی') !!}
                        {!! Form::text('education', null) !!}
                        @if ($errors->has('education'))
                            <span class="help-block"><strong>{{ $errors->first('education') }}</strong></span>
                        @endif
                    </div>

                    <div class=" {{ $errors->has('about') ? ' has-error' : '' }}">
                        {!! Form::label('about', 'بیوگرافی من') !!}
                        {!! Form::textarea('about', (isset($user->about)?$user->about:null), ['class' => 'form-control', 'id' => 'editorcm']) !!}
                        @if ($errors->has('about'))
                            <span class="help-block"><strong>{{ $errors->first('about') }}</strong></span>
                        @endif
                    </div>

                    <h6>راه های ارتباطی</h6>

                    <div class=" {{ $errors->has('state') ? ' has-error' : '' }}">
                        {!! Form::label('state', 'استان محل سکونت') !!}
                        {!! Form::text('state', (isset($user->state)?$user->state:null)) !!}
                        @if ($errors->has('state'))
                            <span class="help-block"><strong>{{ $errors->first('state') }}</strong></span>
                        @endif
                    </div>

                    <div class=" {{ $errors->has('city') ? ' has-error' : '' }}">
                        {!! Form::label('city', 'شهر محل سکونت') !!}
                        {!! Form::text('city', (isset($user->city)?$user->city:null)) !!}
                        @if ($errors->has('city'))
                            <span class="help-block"><strong>{{ $errors->first('city') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد کد پستی ده رقمی -->
                    <div class=" {{ $errors->has('postal_code') ? ' has-error' : '' }}">
                        {!! Form::label('postal_code', 'کد پستی ده رقمی') !!}
                        {!! Form::text('postal_code', null) !!}
                        @if ($errors->has('postal_code'))
                            <span class="help-block"><strong>{{ $errors->first('postal_code') }}</strong></span>
                        @endif
                    </div>

                    <div class=" {{ $errors->has('address') ? ' has-error' : '' }}">
                        {!! Form::label('address', 'آدرس دقیق پستی') !!}
                        {!! Form::textarea('address', (isset($user->address)?$user->address:null)) !!}
                        @if ($errors->has('address'))
                            <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد شماره موبایل -->
                    <div class=" {{ $errors->has('tel') ? ' has-error' : '' }}">
                        {!! Form::label('tel', 'شماره موبایل') !!}
                        {!! Form::text('tel', null) !!}
                        @if ($errors->has('tel'))
                            <span class="help-block"><strong>{{ $errors->first('tel') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد تلفن ثابت -->
                    <div class=" {{ $errors->has('telephone') ? ' has-error' : '' }}">
                        {!! Form::label('telephone', 'تلفن ثابت') !!}
                        {!! Form::text('telephone', null) !!}
                        @if ($errors->has('telephone'))
                            <span class="help-block"><strong>{{ $errors->first('telephone') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد تلگرام -->
                    <div class=" {{ $errors->has('telegram') ? ' has-error' : '' }}">
                        {!! Form::label('telegram', 'تلگرام') !!}
                        {!! Form::text('telegram', null) !!}
                        @if ($errors->has('telegram'))
                            <span class="help-block"><strong>{{ $errors->first('telegram') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد اینستاگرام -->
                    <div class=" {{ $errors->has('instagram') ? ' has-error' : '' }}">
                        {!! Form::label('instagram', 'اینستاگرام') !!}
                        {!! Form::text('instagram', null) !!}
                        @if ($errors->has('instagram'))
                            <span class="help-block"><strong>{{ $errors->first('instagram') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد لینکدین -->
                    <div class=" {{ $errors->has('linkedin') ? ' has-error' : '' }}">
                        {!! Form::label('linkedin', 'لینکدین') !!}
                        {!! Form::text('linkedin', null) !!}
                        @if ($errors->has('linkedin'))
                            <span class="help-block"><strong>{{ $errors->first('linkedin') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد توئیتر -->
                    <div class=" {{ $errors->has('twitter') ? ' has-error' : '' }}">
                        {!! Form::label('twitter', 'توئیتر') !!}
                        {!! Form::text('twitter', null) !!}
                        @if ($errors->has('twitter'))
                            <span class="help-block"><strong>{{ $errors->first('twitter') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد نام بانک -->
                    <div class="form-group {{ $errors->has('bank') ? ' has-error' : '' }}">
                        {!! Form::label('bank', 'نام بانک') !!}
                        {!! Form::text('bank', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('bank'))
                            <span class="help-block"><strong>{{ $errors->first('bank') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد شماره حساب -->
                    <div class="form-group {{ $errors->has('bank_number') ? ' has-error' : '' }}">
                        {!! Form::label('bank_number', 'شماره حساب') !!}
                        {!! Form::text('bank_number', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('bank_number'))
                            <span class="help-block"><strong>{{ $errors->first('bank_number') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد شماره شبا -->
                    <div class="form-group {{ $errors->has('bank_sheba') ? ' has-error' : '' }}">
                        {!! Form::label('bank_sheba', 'شماره شبا') !!}
                        {!! Form::text('bank_sheba', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('bank_sheba'))
                            <span class="help-block"><strong>{{ $errors->first('bank_sheba') }}</strong></span>
                        @endif
                    </div>

                    <input type="submit" value="ثبت اطلاعات پروفایل">

                    {!! Form::close() !!}

                </div>
            </div>
        </div>

        @include('partials.footer')

    </div>

@endsection
@push('scripts')

@endpush
