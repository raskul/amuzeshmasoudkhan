@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="user_courses">
                <h6 style="line-height: 100px;">لیست دانشجویان ثبت نام کرده در دوره های شما</h6>
                <label for="">فیلتر بر اساس دوره</label>

                {!! Form::open(['route'=>['courseStudent'], 'method' => 'GET' ]) !!}
                <div class="user_filter" style="display: inline">
                    <select id=""
                            style="width: 30%;height: 35px;background: #eee;border: 1px solid #ccc; margin: 15px 0;"
                            name="product">
                        <option value="">انتخاب کنید</option>
                        @foreach($teacherProducts as $product)
                            <option value="{{$product->id}}">{{$product->title}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" style="display: inline-block;height: 35px;width: 100px">مشاهده</button>
                {!! Form::close() !!}

                <table cellspacing="0">
                    <tr style="background: #00c2e5;">
                        <td style="width: 10%">ردیف</td>
                        <td style="width: 20%;">نام و نام خانوادگی دانشجو</td>
                        <td style="width: 40%">دوره آموزشی</td>
                        <td style="width: 15%">تاریخ ثبت نام</td>
                        <td style="width: 15%">مبلغ پرداختی</td>
                    </tr>
                    @if(isset($students))
                        @foreach($students as $student)
                            <tr>
                                <td style="width: 10%">1</td>
                                <td style="width: 20%;">{{$student->name}}</td>
                                <td style="width: 40%;">
                                    <a style="text-decoration: none;color: #555;" href="#">---</a></td>
                                <td style="width: 15%">---</td>
                                <td style="width: 15%">---</td>
                            </tr>
                        @endforeach
                    @endif

                </table>
            </div>
        </div>

        @include('partials.footer')

    </div>
@endsection