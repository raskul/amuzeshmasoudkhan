@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="user_courses">
                <h6 style="line-height: 100px;">لیست دوره هایی که در حال یادگیری هستید</h6>

                <table cellspacing="0">
                    <tr style="background: #00c2e5;">
                        <td style="width: 10%">ردیف</td>
                        <td style="width: 40%;">نام دوره</td>
                        <td style="width: 20%">تعداد دانشجویان</td>
                        <td style="width: 15%">تاریخ شروع دوره</td>
                        <td style="width: 15%">قیمت</td>
                    </tr>

                    @php
                        $i = 1
                    @endphp
                    @foreach($products as $product)
                        <tr>
                            <td style="width: 10%">{{$i++}}</td>
                            <td style="width: 40%;text-align: right">
                                <a style="text-decoration: none;color: #555;" href="#">{{$product->title}}</a></td>
                            <td style="width: 20%">
                                {{$product->sold_count}}
                                نفر
                            </td>
                            <td style="width: 15%">{{pDate($product->start_at)}}</td>
                            <td style="width: 15%">
                                {{$product->price}}
                                تومان
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>

        @include('partials.footer')

    </div>
@endsection