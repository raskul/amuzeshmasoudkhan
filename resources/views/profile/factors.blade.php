{{--unused--}}
@extends('layouts.index')
@section('header')
    {{--<link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">
@endsection
@section('content')

    @include('partials.miniHeader')

    <section class="content-header">
        <h1>
            فاکتور های ساخته شده
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 40px">
                <div class="row">
                    <div style="font-size: large">لیست فاکتور ها:</div>
                    @if(count($factors))
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>شماره فاکتور</th>
                                <th>نام شما</th>
                                <th style="max-width:350px">محصولات</th>
                                <th>مجموع</th>
                                <th>مجموع تخفیف ها</th>
                                <th>مبلغ قابل پرداخت</th>
                                <th>وضعیت پرداخت</th>
                                <th>پرداخت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($factors as $factor)
                                <tr>
                                    <td>{{$factor->id}}</td>
                                    <td>{{$factor->user->name}}</td>
                                    <td>
                                        @foreach($factor->products as $product)
                                            {{$product->title}}
                                            <br>
                                            {{$product->price}} -
                                            تومان
                                            <br>
                                        @endforeach
                                    </td>
                                    <td>{{$factor->sum}}</td>
                                    <td>{{$factor->discount_sum}}</td>
                                    <td>{{$factor->sum - $factor->discount_sum}} <span> تومان </span></td>
                                    @if($factor->is_paid == 0)
                                        <td>پرداخت نشده</td>
                                        <td>
                                            {!! Form::open(['route'=>['pay.factor', 'factor_id' => $factor->id ], 'method' => 'post']) !!}
                                            <button type="submit" class="btn btn-success"> پرداخت </button>
                                            {!! Form::close() !!}
                                        </td>
                                    @else
                                        <td>پرداخت شده</td>
                                        <td>-</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        هنوز فاکتری برای شما صادر نشده است.
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('partials.miniFooter')

@endsection
