@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">

                <section class="content-header">
                    <span style="font-size: xx-large">
                        ارسال تیکت
                    </span>
                </section>
                <div class="content">


                    <div class="box box-primary">

                        <div class="box-body " style="padding-right: 40px">

                            {!! Form::open(['route' => 'ticketCreate', 'files' => true]) !!}
                            <div class="row">
                                <div class="form-group col-sm-3">
                                    {!! Form::label('title', 'عنوان:') !!}
                                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="importance">اولویت:</label>
                                    <select class="form-control" id="importance" name="importance">
                                        <option value="1">مهم</option>
                                        <option value="2" selected>عادی</option>
                                        <option value="3">خیلی ضروری نیست</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::label('text', 'متن:') !!}
                                    {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="">فایل ضمیمه:</label>
                                    <input type="file" name="file">
                                </div>
                            </div>
                            <br>
                            <input type="submit" value="ارسال تیکت" style="width: 200px; margin-top: 0px">
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @include('partials.footer')

    </div>

@endsection
@push('scripts')
    <script src="{{asset('bower_components/tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({
            selector: 'textarea',
            language: 'fa_IR',
            directionality: 'rtl',

            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",

            menubar: false,

            default_link_target: "_blank",
            media_live_embeds: true,

            file_picker_types: 'file image media',
            file_browser_callback_types: 'file image media',

            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern "
            ],
            toolbar1: "newdocument undo redo | bold italic | hr | alignleft aligncenter alignright alignjustify ",

            image_advtab: true,

            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Image Responsive', value: 'img-responsive'}
            ],

            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],

            relative_urls : false,
            convert_urls: false,
            browser_spellchek: false,
            fix_list_elements: true,
            entity_encoding: "row",
            keep_styles: false,
            preview_styles: "font-family font-size font-weight font-style text-decoration text-transform"
        });


    </script>
@endpush
