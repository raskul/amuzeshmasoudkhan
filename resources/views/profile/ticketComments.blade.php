@extends('layouts.index')
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">

                <section class="content-header">
                    <span style="font-size: xx-large">
                        مشاهده تیکت
                    </span>
                    <span class="pull-left">
                        {!! Form::open(['route'=>['teachingRequestComments', $ticket->id], 'method' => 'post', 'style' => 'display: inline' ]) !!}
                        <input type="hidden" name="close" value="1">
                        <button type="submit" class="btn btn-danger" style="display: inline; float: left; width: 150px; height: 50px;margin-left: 22px;background-color: #ff6773">
                            بستن تیکت
                        </button>
                        {!! Form::close() !!}
                    </span>
                </section>
                <div class="clear"></div>
                <div class="content">

                    <div class="row" style="padding: 0 20px 0 20px">
                        <div class="col-sm-12">
                            <div class="box box-primary">
                                <div class="box-body " style="padding-right: 20px">
                                    <div class="row">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td>شماره درخواست</td>
                                                <th>عنوان</th>
                                                <th>نام</th>
                                                <th>ایمیل</th>
                                                <th>تاریخ ایجاد</th>
                                                <th>آخرین پاسخ</th>
                                                <th>وضعیت</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{$ticket->id}}</td>
                                                <td>{{$ticket->title}}</td>
                                                <td>{{$ticket->user->name}}</td>
                                                <td>{{$ticket->user->email}}</td>
                                                <td>{{pDate($ticket->created_at, true )}}</td>
                                                <td>{{$ticket->who_answered}}</td>
                                                <td>{{$ticket->closedName}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="7">
                                                    {!!  $ticket->text!!}
                                                </td>
                                            </tr>
                                            @if (isset($ticket->files))
                                                <tr>
                                                    <td>
                                                        فایل های ضمیمه:
                                                        <br>
                                                        @foreach(unserialize($ticket->files) as $file)
                                                            @php
                                                                $file = substr($file, 16);
                                                                $sendFile = str_replace('/', ':', $file);
                                                            @endphp
                                                            <a href="{{ route('downloadTicketFiles', $sendFile) }}" >
                                                                <span class="btn btn-primary" style="margin: 0 0 5px 5px; padding: 0">
                                                                {{ substr(strrev(substr(strrev($file), 0,strpos(strrev($file), '/'))), 10) }}
                                                                </span>
                                                            </a>
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-primary" >
                                <div class="box-body " style="padding-left: 20px;padding-right: 20px">

                                    @foreach($ticketComments as $comment)
                                        <div class="row"
                                             style="background-color:white; margin: 0 50px 0 50px;padding-top: 10px; border-bottom: 1px solid #F5F5F5">
                                            <div class="col-sm-1">
                                                <img class="img-circle" src="{{showUserImage($comment->user->id)}}" alt=""
                                                     height="30px" width="30px">
                                            </div>
                                            <div class="col-sm-11">
                                                <div>
                                                <span class="btn-default btn" style="padding: 0 5px 0 5px ;color: black;">
                                                    <strong>{{$comment->user->name}}</strong>
                                                </span>
                                                    <span class="btn btn-warning pull-left" style="padding: 0 5px 0 5px">
                                                    {{pDate($comment->created_at, false, true)}}
                                                </span>
                                                </div>
                                                <div >{!! $comment->text !!}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <hr>
                                            {!! Form::open(['route'=>['ticketComments', $ticket->id], 'method' => 'post', 'files' => true ]) !!}
                                            <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                                {!! Form::label('text', 'ارسال پاسخ:') !!}
                                                {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
                                                @if ($errors->has('text'))
                                                    <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                                                @endif
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="">فایل ضمیمه:</label>
                                                    <input type="file" name="file">
                                                </div>
                                            </div>
                                            <br>

                                            <input type="submit" value="ارسال پیام" style="width: 200px; margin-top: 0px">
                                            {!! Form::close() !!}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    @include('partials.footer')

    </div>

@endsection
@push('scripts')
    <script src="{{asset('bower_components/tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({
            selector: 'textarea',
            language: 'fa_IR',
            directionality: 'rtl',

            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",

            menubar: false,

            default_link_target: "_blank",
            media_live_embeds: true,

            file_picker_types: 'file image media',
            file_browser_callback_types: 'file image media',

            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern "
            ],
            toolbar1: "newdocument undo redo | bold italic | hr | alignleft aligncenter alignright alignjustify ",

            image_advtab: true,

            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Image Responsive', value: 'img-responsive'}
            ],

            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],

            relative_urls : false,
            convert_urls: false,
            browser_spellchek: false,
            fix_list_elements: true,
            entity_encoding: "row",
            keep_styles: false,
            preview_styles: "font-family font-size font-weight font-style text-decoration text-transform"
        });


    </script>
@endpush