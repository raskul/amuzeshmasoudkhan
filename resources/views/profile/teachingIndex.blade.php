@extends('layouts.index')
@section('content')
    <div id="main">

        @include('partials.header')

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">
                <div class="add_course_area">
                    <div class="add_course">
                        <a href="{{route('teachingRequestCreate')}}">افزودن دوره جدید</a>
                    </div>
                </div>
                <h6 style="line-height: 100px;">لیست دوره هایی که شما مدرس آنها هستید</h6>

                @if(count($products))
                    <table cellspacing="0">
                        <tr style="background: #00c2e5;">
                            <td style="width: 50%;">نام دوره</td>
                            <td style="width:auto;">تعداد جلسات</td>
                            <td style="width:auto">تعداد دانشجویان</td>
                            <td style="width: auto">تاریخ شروع دوره</td>
                            <td style="width: auto">وضعیت دوره</td>
                            <td style="width: auto">مشاهده</td>
                            <td style="width: auto">درآمد کل</td>
                        </tr>

                        @foreach($products as $product)
                            <tr>
                                <td style="width: 45%;text-align: right;"><a style="text-decoration: none;color: #555; font-size: 12pt;" href="#">
                                        {{$product->title}}
                                    </a>


                                    <div class="add_cours_this">
                                        <ul>
                                            <li><i class="fas fa-pencil-alt"></i><a href="{{ route('teacherEditProducts', $product->id . "-" . str_replace(' ', '-', $product->title) )}}">ویرایش دوره</a></li>
                                            <li><i class="fas fa-plus"></i><a href="{{ route('createProductPostByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title) )}}">افزودن/ویرایش درس</a></li>
                                            <li><i class="fas fa-file-alt"></i><a href="{{ route('ProductFilesByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title) )}}">افزودن/ویرایش فایل</a></li>
                                            <li><i class="fas fa-info-circle"></i><a href="{{ route('ProductNotesByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title)  )}}">افزودن/ویرایش اطلاعیه</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td style="width: 10%;">{{count($product->productPosts)}}</td>
                                <td style="width: 10%">
                                    {{count($product->students)}}
                                    نفر</td>
                                <td style="width: 10%">{{pDate($product->start_at, false, false, true, true)}}</td>
                                <td style="width: 10%">{{$product->statusStr}}</td>
                                <td style="width: 5%"><a href="{{ route('index.index') . '/' . $product->id . "-" . str_replace(' ', '-', $product->title) }}">مشاهده</a></td>
                                <td style="width: 15%">
                                    ---
                                    تومان</td>
                            </tr>
                        @endforeach

                        @foreach($teaching_requests as $teaching_request)
                            <tr>
                                <td>{{ $teaching_request->title }}</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>{{ $teaching_request->statusName }}</td>
                                <td>
                                    <a href="{{route('teachingRequestComments', $teaching_request->id)}}">
                                        پیگیری
                                    </a>
                                </td>
                                <td>-</td>
                            </tr>
                        @endforeach

                    </table>
                @else
                    <span>شما مدرس هیچ دوره ای نیستید. برای تدریس دوره، روی دکمه "افزودن دوره جدید" کلیک کنید.</span>
                @endif


            </div>
        </div>

        @include('partials.footer')

    </div>

@endsection

{{--@section('header')--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">--}}
    {{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('css/bootstrap/css/bootstrap-responsive-rtl.min.css')}}" rel="stylesheet">--}}
    {{--<link rel="stylesheet" href="{{ asset("bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css") }}">--}}
{{--@endsection--}}


    {{--@include('partials.miniHeader')--}}

    {{--<div class="content">--}}

        {{--@include('flash::message')--}}

        {{--@cannot('is-employee')--}}

            {{--@if(count($products))--}}
                {{--<section class="content-header">--}}
                    {{--<h1>--}}
                        {{--لیست دروس شما--}}
                    {{--</h1>--}}
                {{--</section>--}}
                {{--<div class="box box-primary">--}}
                    {{--<div class="box-body " style="padding-right: 20px">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-12 ">--}}
                                {{--<table class="table">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>عنوان درس</th>--}}
                                        {{--<th>عملیات</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}

                                    {{--@foreach($products as $product)--}}

                                        {{--<tr>--}}
                                            {{--<td>--}}
                                                {{--{{ $product->title }}--}}
                                            {{--</td>--}}
                                            {{--<td>--}}
                                                {{--<div class=''>--}}
                                                    {{--<a href="{{ route('index.index')}}/{{ $product->id}}-{{ str_replace(' ', '-', $product->title) }}"--}}
                                                       {{--class='btn btn-primary btn-xs' target="_blank">--}}
                                                        {{--<i class="glyphicon glyphicon-eye-open"> مشاهده </i>--}}
                                                    {{--</a>--}}
                                                    {{--<a href="{{ route('teacherEditProducts', $product->id . "-" . str_replace(' ', '-', $product->title) )}}"--}}
                                                       {{--class='btn btn-default btn-xs' target="_blank">--}}
                                                        {{--<i class="glyphicon glyphicon-edit"> ویرایش </i>--}}
                                                    {{--</a>--}}
                                                    {{--<a href="{{ route('createProductPostByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title) )}}"--}}
                                                       {{--class='btn btn-success btn-xs' target="_blank">--}}
                                                        {{--<i class="glyphicon glyphicon-plus-sign"> افزودن جلسه--}}
                                                            {{--آموزشی </i>--}}
                                                    {{--</a>--}}
                                                    {{--<a href="{{ route('ProductFilesByTeacher', $product->id )}}"--}}
                                                       {{--class='btn btn-success btn-xs' target="_blank">--}}
                                                        {{--<i class="glyphicon glyphicon-plus-sign"> مدیریت فایل های ضمیمه</i>--}}
                                                    {{--</a>--}}
                                                    {{--<a href="{{ route('ProductNotesByTeacher', $product->id  )}}"--}}
                                                       {{--class='btn btn-success btn-xs' target="_blank">--}}
                                                        {{--<i class="glyphicon glyphicon-plus-sign"> مدیریت اطلاعیه های دوره</i>--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                        {{--@php $i = 1; @endphp--}}
                                        {{--@foreach($product->productPosts as $post)--}}
                                            {{--<tr>--}}
                                                {{--<td style="padding-right: 40px">--}}
                                                    {{--<span style="color: grey;font-size: x-small" > (جلسه شماره {{$i++}}--}}
                                                        {{--<span> درس </span>--}}
                                                        {{--<span> {{$product->title}} </span>--}}
                                                        {{--)--}}
                                                    {{--</span>--}}
                                                    {{--{{$post->title}}</td>--}}
                                                {{--<td>--}}
                                                    {{--<a href="{{ route('createProductPostByTeacher', $product->id . "-" . str_replace(' ', '-', $product->title) )}}/{{$post->id}}"--}}
                                                       {{--class='btn btn-default btn-xs' target="_blank">--}}
                                                        {{--<i class="glyphicon glyphicon-edit"> ویرایش </i>--}}
                                                    {{--</a>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                        {{--@endforeach--}}

                                    {{--@endforeach--}}

                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endif--}}

            {{--<section class="content-header">--}}
                {{--<h1>--}}
                    {{--درخواست تدریس--}}
                {{--</h1>--}}
            {{--</section>--}}
            {{--<div class="box box-primary">--}}
                {{--<div class="box-body " style="padding-right: 20px">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-sm-12 ">--}}
                            {{--<p>برای ارسال درخواست تدریس درس جدید کلیک کنید.--}}
                                {{--<a href="{{route('teachingRequestCreate')}}" class="btn btn-info btn-lg">--}}
                                    {{--<span class="glyphicon glyphicon-envelope"></span> ارسال درخواست تدریس--}}
                                {{--</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endcannot--}}

        {{--<br>--}}
        {{--<section class="content-header">--}}
            {{--<h1>--}}
                {{--لیست درخواست های شما--}}
            {{--</h1>--}}
        {{--</section>--}}
        {{--<div class="box box-primary">--}}

            {{--<div class="box-body " style="padding-right: 20px">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-12 ">--}}
                        {{--<table class="table table-hover">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th>شماره درخواست</th>--}}
                                {{--<th>عنوان درخواست</th>--}}
                                {{--<th>وضعیت</th>--}}
                                {{--<th>تاریخ</th>--}}
                                {{--<th>مشاهده</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}
                            {{--@foreach($teaching_requests as $teaching_request)--}}
                                {{--<tr>--}}
                                    {{--<td scope="row">{{ $teaching_request->id }}</td>--}}
                                    {{--<td>{{ $teaching_request->title }}</td>--}}
                                    {{--<td>{{ $teaching_request->statusName }}</td>--}}
                                    {{--<td>{{ $teaching_request->created_at }}</td>--}}
                                    {{--<td>--}}
                                        {{--<a href="{{route('teachingRequestComments', $teaching_request->id)}}"--}}
                                           {{--type="button" class="btn btn-default" aria-label="Left Align">--}}
                                            {{--<span class="fa fa-eye fa-lg" aria-hidden="true">باز کردن</span>--}}
                                        {{--</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}







