@php
    $email = auth()->user()->email;
    \Config::set('elfinder.roots.0.path', storage_path()
        . DIRECTORY_SEPARATOR . 'app'
        . DIRECTORY_SEPARATOR . 'public'
        . DIRECTORY_SEPARATOR . 'files'
        . DIRECTORY_SEPARATOR . "$email");

@endphp
@extends('layouts.index')
@push('header')
    <link rel="stylesheet" href="{{ asset('bower_components/persian-datepicker/dist/css/persian-datepicker.css') }}"/>
    <script src="{{ asset('bower_components/persian-date/dist/persian-date.js') }}"></script>
    <script src="{{ asset('bower_components/persian-datepicker/dist/js/persian-datepicker.js') }}"></script>
@endpush
@section('content')

    @include('partials.header')

    <div id="main">

        <div class="teacher_panel_area">
            <div style="margin-right: -10px">
            @include('partials.profileHeader')

            @include('flash::message')
            @include('adminlte-templates::common.errors')
            </div>
            <div class="teacher_courses">
                <h6 style="line-height: 100px;">افزودن دوره</h6>

                <div class="add_course">

                {!! Form::open(['route'=>['teachingRequestCreate' ], 'method' => 'POST', 'files' => true ]) !!}

                <!-- فیلد عنوان دوره -->
                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                        {!! Form::label('title', 'عنوان دوره') !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('title'))
                            <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                        @endif
                    </div>

                    <label for="category">دسته بندی</label>
                    <select name="category" id="category">
                        <option value="">انتخاب کنید...</option>
                        @if(count($category->children))
                            @php
                                $i = 1;
                            @endphp
                            @foreach($category->children as $child)
                                <option value="{{$child->id}}">{{toPersianNumber($i)}} . {{$child->name_fa}}</option>
                                @php
                                    $j = 1;
                                @endphp
                                @if(count($child->children))
                                    @foreach($child->children as $child2)
                                        <option value="{{$child2->id}}">{{toPersianNumber($i)}}
                                            . {{toPersianNumber($j)}} . {{$child2->name_fa}}</option>
                                        @php

                                            $k = 1;
                                        @endphp
                                        @if(count($child2->children))
                                            @foreach($child2->children as $child3)
                                                <option value="{{$child3->id}}">{{toPersianNumber($i)}}
                                                    . {{toPersianNumber($j)}} . {{toPersianNumber($k)}}
                                                    . {{$child3->name_fa}}</option>
                                                @php
                                                    $k++ ;
                                                @endphp
                                            @endforeach

                                        @endif
                                        @php
                                            $j++ ;
                                        @endphp

                                    @endforeach
                                @endif
                                @php
                                    $i++ ;
                                @endphp
                            @endforeach
                        @endif
                    </select>


                    <!-- فیلد توضیح مختصر -->
                    <div class="form-group {{ $errors->has('description_mini') ? ' has-error' : '' }}">
                        {!! Form::label('description_mini', 'توضیح مختصر') !!}
                        {!! Form::text('description_mini', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('description_mini'))
                            <span class="help-block"><strong>{{ $errors->first('description_mini') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد تاریخ شروع دوره -->
                    <div class="form-group {{ $errors->has('start_at') ? ' has-error' : '' }}">
                        {!! Form::label('start_at', 'تاریخ شروع دوره') !!}
                        {!! Form::text('start_at', null, ['class' => 'form-control pDatePicker']) !!}
                        @if ($errors->has('start_at'))
                            <span class="help-block"><strong>{{ $errors->first('start_at') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد تاریخ پایان دوره -->
                    <div class="form-group {{ $errors->has('end_at') ? ' has-error' : '' }}">
                        {!! Form::label('end_at', 'تاریخ پایان دوره') !!}
                        {!! Form::text('end_at', null, ['class' => 'form-control pDatePicker']) !!}
                        @if ($errors->has('end_at'))
                            <span class="help-block"><strong>{{ $errors->first('end_at') }}</strong></span>
                        @endif
                    </div>

                    <div>
                        <label for="status">وضعیت دوره</label>
                        <select name="status" id="status">
                            @foreach($product->statusStrItems as $key => $statusStrItem)
                                <option value="{{$key}}"
                                        @if($key == $product->status) selected @endif>{{$statusStrItem}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div>
                        <label for="product_type_id">نوع دوره</label>
                        <select name="product_type_id" id="product_type_id">
                            @foreach($productTypes as $productType)
                                <option value="{{$productType->id}}" @if($productType->id == 3) selected @endif>
                                    {{$productType->name_fa}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <!-- فیلد قیمت دوره -->
                    <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                        {!! Form::label('price', 'قیمت دوره') !!}
                        {!! Form::text('price', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('price'))
                            <span class="help-block"><strong>{{ $errors->first('price') }}</strong></span>
                        @endif
                    </div>

                    <!-- فیلد تعداد موجود -->
                    <div class="form-group {{ $errors->has('total_count') ? ' has-error' : '' }}">
                        {!! Form::label('total_count', 'تعداد موجود') !!}
                        {!! Form::text('total_count', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('total_count'))
                            <span class="help-block"><strong>{{ $errors->first('total_count') }}</strong></span>
                        @endif
                        <span class="help-block"> در صورتی که تعداد محصول شما تمام نمیشود این قسمت را خالی بگذارید. </span>
                    </div>

                    <label for="can_get_license">اهداء گواهی</label>
                    <select name="can_get_license" id="can_get_license">
                        <option value="0" selected>به دانشجویان گواهی خاصی داده نمیشود</option>
                        <option value="1">به دانشجویان پس از پایان دوره گواهی داده میشود</option>
                    </select>

                    <label for="can_sign_up">وضعیت ثبت نام دانشجویان</label>
                    <select name="can_sign_up" id="can_sign_up">
                        <option value="0">دانشجویان فعلا نتوانند ثبت نام کنند</option>
                        <option value="1" selected>دانشجویان بتوانند ثبت نام کنند</option>
                    </select>

                    <label for="image">عکس دوره</label>
                {{Form::file('image')}}

                <!-- فیلد توضیحات کامل دوره -->
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        {!! Form::label('description', 'توضیحات کامل دوره') !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'textarea']) !!}
                        @if ($errors->has('description'))
                            <span class="help-block"><strong>{{ $errors->first('description') }}</strong></span>
                        @endif
                    </div>

                    <label for="file">ارسال فایل ضمیمه برای مدیریت</label>
                    {{Form::file('file')}}

                    <input type="submit" value="ثبت دوره">

                    {!! Form::close() !!}

                </div>
            </div>
        </div>

        @include('partials.footer')

    </div>


@endsection
@push('scripts')

    <script src="{{asset('bower_components/tinymce/tinymce.min.js')}}"></script>
    <script>tinymce.init({
            selector: 'textarea',
            language: 'fa_IR',
            directionality: 'rtl',

            width: 800,
            height: 400,

            fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",

            menubar: false,

            default_link_target: "_blank",
            media_live_embeds: true,

            file_picker_types: 'file image media',
            file_browser_callback_types: 'file image media',

            // theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern "
            ],
            toolbar1: "newdocument insertfile undo redo | styleselect | bold italic | hr | charmap | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
            toolbar2: "link image media | forecolor backcolor emoticons | fontsizeselect | visualblocks | table | searchreplace | fullscreen",

            image_advtab: true,

            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Image Responsive', value: 'img-responsive'}
            ],

            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],

            file_browser_callback: elFinderBrowser,

            relative_urls: false,
            convert_urls: false,
            browser_spellchek: false,
            fix_list_elements: true,
            entity_encoding: "row",
            keep_styles: false,
            preview_styles: "font-family font-size font-weight font-style text-decoration text-transform"
        });

        function elFinderBrowser(field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
                title: 'elFinder 2.0',
                width: 900,
                height: 450,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
            return false;
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".pDatePicker").pDatepicker({
                altField: '#inlineExampleAlt',
                altFormat: 'LLLL',
                toolbox: {
                    calendarSwitch: {
                        enabled: true
                    }
                },
                navigator: {
                    scroll: {
                        enabled: false
                    }
                },
                maxDate: new persianDate().add('month', 12).valueOf(),
                minDate: new persianDate().subtract('month', 0).valueOf(),
                timePicker: {
                    enabled: true,
                    meridiem: {
                        enabled: true
                    }
                }
            });
        });
    </script>
@endpush



