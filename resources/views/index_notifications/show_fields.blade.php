<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $indexNotification->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $indexNotification->product_id !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $indexNotification->text !!}</p>
</div>

<!-- Who Edit Field -->
<div class="form-group">
    {!! Form::label('who_edit', 'Who Edit:') !!}
    <p>{!! $indexNotification->who_edit !!}</p>
</div>

<!-- Can Sign Up Field -->
<div class="form-group">
    {!! Form::label('can_sign_up', 'Can Sign Up:') !!}
    <p>{!! $indexNotification->can_sign_up !!}</p>
</div>

<!-- Start At Field -->
<div class="form-group">
    {!! Form::label('start_at', 'Start At:') !!}
    <p>{!! $indexNotification->start_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $indexNotification->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $indexNotification->updated_at !!}</p>
</div>

