@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Index Notification
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('index_notifications.show_fields')
                    <a href="{!! route('indexNotifications.index') !!}" class="btn btn-default">بازگشت</a>
                </div>
            </div>
        </div>
    </div>
@endsection
