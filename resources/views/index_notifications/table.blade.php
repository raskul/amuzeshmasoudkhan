<table class="table table-responsive" id="indexNotifications-table">
    <thead>
        <tr>
            <th>عنوان محصول</th>
        <th>متن</th>
        <th>ثبت نام آزاد است؟</th>
            <th colspan="3">عملیات</th>
        </tr>
    </thead>
    <tbody>
    @foreach($indexNotifications as $indexNotification)
        <tr>
            <td>{!! $indexNotification->product->title !!}</td>
            <td>{!! $indexNotification->text !!}</td>
            <td>@if($indexNotification->can_sign_up == 1) بله @else خیر @endif </td>
            <td>
                {!! Form::open(['route' => ['indexNotifications.destroy', $indexNotification->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('index.index') !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('indexNotifications.edit', [$indexNotification->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('آیا اطمینان دارید؟')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>