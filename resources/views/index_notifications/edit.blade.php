@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ویرایش اطلاعیه آموزشی صفحه اصلی
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($indexNotification, ['route' => ['indexNotifications.update', $indexNotification->id], 'method' => 'patch']) !!}

                        @include('index_notifications.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection