<!-- User Id Field -->
<div class="form-group col-sm-6" style="display: none">
    {!! Form::label('user_id', 'نام کاربری:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6" style="display: none">
    {!! Form::label('product_id', 'نام محصول:') !!}
    {!! Form::text('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Likes Field -->
<div class="form-group col-sm-6" style="display: none">
    {!! Form::label('likes', 'Likes:') !!}
    {!! Form::text('likes', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('comments.index') !!}" class="btn btn-default">لغو</a>
</div>
