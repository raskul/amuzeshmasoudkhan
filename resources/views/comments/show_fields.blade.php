<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره:') !!}
    <p>{!! $comment->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'نام کاربری:') !!}
    <p>{!! $comment->user_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'نام محصول:') !!}
    <p>{!! $comment->product_id !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'متن:') !!}
    <p>{!! $comment->text !!}</p>
</div>

<!-- Likes Field -->
<div class="form-group">
    {!! Form::label('likes', 'لایک ها:') !!}
    <p>{!! $comment->likes !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $comment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ ویرایش:') !!}
    <p>{!! $comment->updated_at !!}</p>
</div>

