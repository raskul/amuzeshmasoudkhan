<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'نام:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Fa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_fa', 'نام فارسی:') !!}
    {!! Form::text('name_fa', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'کد:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', 'ایا فعال است:') !!}
    {!! Form::text('active', null, ['class' => 'form-control']) !!}
</div>

<!-- Who Edit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('who_edit', 'آخرین ویرایش:') !!}
    {!! Form::text('who_edit', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('discounts.index') !!}" class="btn btn-default">لغو</a>
</div>
