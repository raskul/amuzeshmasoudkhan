<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره:') !!}
    <p>{!! $discount->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'نام:') !!}
    <p>{!! $discount->name !!}</p>
</div>

<!-- Name Fa Field -->
<div class="form-group">
    {!! Form::label('name_fa', 'نام فارسی:') !!}
    <p>{!! $discount->name_fa !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'کد:') !!}
    <p>{!! $discount->code !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'آیا فعال است:') !!}
    <p>{!! $discount->active !!}</p>
</div>

<!-- Who Edit Field -->
<div class="form-group">
    {!! Form::label('who_edit', 'آخرین ویرایش:') !!}
    <p>{!! $discount->who_edit !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $discount->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ ویرایش:') !!}
    <p>{!! $discount->updated_at !!}</p>
</div>

