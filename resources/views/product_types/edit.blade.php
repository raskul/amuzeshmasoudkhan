@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            نوع دوره ها
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($productType, ['route' => ['productTypes.update', $productType->id], 'method' => 'patch']) !!}

                        @include('product_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection