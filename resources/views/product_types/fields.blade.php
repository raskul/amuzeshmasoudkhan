<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'نام:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Fa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_fa', 'نام نمایشی(فارسی):') !!}
    {!! Form::text('name_fa', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('productTypes.index') !!}" class="btn btn-default">لغو</a>
</div>

