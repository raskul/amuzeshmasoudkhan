<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شناسه:') !!}
    <p>{!! $productType->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'نام:') !!}
    <p>{!! $productType->name !!}</p>
</div>

<!-- Name Fa Field -->
<div class="form-group">
    {!! Form::label('name_fa', 'نام فارسی:') !!}
    <p>{!! $productType->name_fa !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $productType->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ بروز رسانی:') !!}
    <p>{!! $productType->updated_at !!}</p>
</div>

