@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            نوع دوره
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding:0 20px">
                    @include('product_types.show_fields')
                    <a href="{!! route('productTypes.index') !!}" class="btn btn-default">بازگشت</a>
                </div>
            </div>
        </div>
    </div>
@endsection
