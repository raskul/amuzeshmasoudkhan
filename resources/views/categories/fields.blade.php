@php
    if((\Illuminate\Support\Facades\Input::get())){
        $p_id = \Illuminate\Support\Facades\Input::get();
    }
    if(isset($category)){
        $parent_id = $category->parent_id;
    }else{
        $parent_id = null;
    }
    if (isset($p_id)){
        $parent_id = array_values($p_id)[0];
    }
@endphp

<div class="form-group col-sm-6">
    <span style="color: red">*</span>
    {!! Form::label('name', 'نام دسته در URL:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <span style="color: red">*</span>
    {!! Form::label('name_fa', 'نام فارسی دسته (نام نمایشی در منو):') !!}
    {!! Form::text('name_fa', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 {{ $errors->has('parent_id') ? ' has-error' : '' }}">
    <span style="color: red">*</span>
    {!! Form::label('parent_id', 'نام والد') !!}
    {!! Form::select('parent_id',  $categories, $parent_id , ['class' => 'form-control select2']) !!}
    @if ($errors->has('parent_id'))
        <span class="help-block"><strong>{{ $errors->first('parent_id') }}</strong></span>
    @endif
</div>

<div class="form-group col-sm-6 {{ $errors->has('show_menu') ? ' has-error' : '' }}">
    <span style="color: red">*</span>
    {!! Form::label('show_menu', 'نمایش در منوی اصلی') !!}
    {!! Form::select('show_menu', ['1' => 'show', '2' => 'hide'], (isset($category)?$category->show_menu:null), ['class' => 'form-control']) !!}
    @if ($errors->has('show_menu'))
        <span class="help-block"><strong>{{ $errors->first('show_menu') }}</strong></span>
    @endif
</div>

<div class="form-group col-sm-6 {{ $errors->has('custom_link') ? ' has-error' : '' }}">
    {!! Form::label('custom_link', 'لینک سفارشی (مثلا https//google.com):') !!}
    {!! Form::text('custom_link', null, ['class' => 'form-control']) !!}
    @if ($errors->has('custom_link'))
        <span class="help-block"><strong>{{ $errors->first('custom_link') }}</strong></span>
    @endif
    <div class="help-block">
        لینک سفارشی برای مواقعی استفاده میشود که شما میخواهید در منو یک گزینه بگذارید که بجای باز کردن دسته بندی، یک
        لینک خاص (به عنوان مثال سایت گوگل) باز شود.
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'لینک تصویر') !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
    @if ($errors->has('image'))
        <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
    @endif
    <div class="help-block">
        در یکی از زیر منو های سطح سوم میتوانید یک تصویر بگذارید.تصویر در منو نمایش داده میشود. لینک تصویر را اینجا بگذارید.(مثلا https://www.w3schools.com/w3css/img_fjords.jpg)
    </div>
</div>

<div class="form-group col-sm-6 {{ $errors->has('order') ? ' has-error' : '' }}">
    {!! Form::label('order', 'اولیت') !!}
    {!! Form::text('order', null, ['class' => 'form-control']) !!}
    @if ($errors->has('order'))
        <span class="help-block"><strong>{{ $errors->first('order') }}</strong></span>
    @endif
    <div class="help-block">
        دسته بندی با اولیت بیشتر زودتر از دسته بندی با اولیت کمتر نمایش داده میشود.(اگر خالی رها شود عدد 10 قرار خواهد گرفت)
    </div>
</div>



<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categories.index') !!}" class="btn btn-default">لغو</a>
</div>
