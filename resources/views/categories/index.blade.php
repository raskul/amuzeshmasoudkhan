@extends('layouts.app')
@push('header')
    <style>
        .placeholder {
            outline: 1px dashed #4183C4;
            width: 300px;
            /*-webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            margin: -1px;*/
        }

        .mjs-nestedSortable-error {
            background: #fbe3e4;
            border-color: transparent;
        }

        ol {
            margin: 0;
            padding: 0;
            padding-right: 30px;
        }

        ol.sortable, ol.sortable ol {
            margin: 0 25px 0 0;
            padding: 0;
            list-style-type: none;
        }

        ol.sortable {
            margin: 4em 0;
        }

        .sortable li {
            margin: 5px 0 0 0;
            padding: 0;
        }

        .sortable li div {
            text-align: center;
            width: 300px;
            border: 1px solid #d4d4d4;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            border-color: #D4D4D4 #D4D4D4 #BCBCBC;
            padding: 6px;
            margin: 0;
            cursor: move;
            background: #f6f6f6;
            background: -moz-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffffff), color-stop(47%, #f6f6f6), color-stop(100%, #ededed));
            background: -webkit-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
            background: -o-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
            background: -ms-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
            background: linear-gradient(to bottom, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#ededed', GradientType=0);
        }

        .sortable li.mjs-nestedSortable-branch div {
            background: -moz-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #f0ece9 100%);
            background: -webkit-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #f0ece9 100%);

        }

        .sortable li.mjs-nestedSortable-leaf div {
            background: -moz-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #bcccbc 100%);
            background: -webkit-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #bcccbc 100%);

        }

        li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
            border-color: #999;
            background: #fafafa;
        }

        .disclose {
            cursor: pointer;
            width: 10px;
            display: none;
        }

        .sortable li.mjs-nestedSortable-collapsed > ol {
            display: none;
        }

        .sortable li.mjs-nestedSortable-branch > div > .disclose {
            display: inline-block;
        }

        .sortable li.mjs-nestedSortable-collapsed > div > .disclose > span:before {
            content: '+ ';
        }

        .sortable li.mjs-nestedSortable-expanded > div > .disclose > span:before {
            content: '- ';
        }

        .extra_buttons {
            position: absolute;
            right: 315px;
            width: 85px;
        }

        .extra_buttons a, .bot_keyboard {
            height: 24px;
            padding: 5px;
            margin-top: -2px;
            font-size: 10px;
            display: inline;
            margin-left: 5px;
        }

        .ui-sortable-handle {
            position: relative;
        }
    </style>

    <style>
        .datepicker-plot-area {
            position: relative;
        }

        .tooltip {
            font-family: "Iranian Sans", IRANSans, Tahoma;
        }

        .select2-container--default[dir="rtl"] .select2-selection--multiple .select2-selection__choice {
            color: #746464;
        }
    </style>
@endpush
@section('content')
    <section class="content-header">
        <div class="help-block pull-left">
            (منوی بالای سایت)
        </div>
        <h1 class="pull-left">دسته بندی ها</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="{!! route('categories.create') !!}">افزودن جدید</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
{{--                {{dd($categories[0]->children()->get())}}--}}
                @php

                    function displayHtmlCategoryTree($categories, $first_step = true)
                    {
                        if (empty($categories)) return '';

                        if ($first_step) {
                            $str = '<ol class="sortable">';
                        } else {
                            $str = '<ol>';
                        }

                        foreach ($categories as $category) {
                            $str .= '<li id="list_' . $category->id . '">' .
                                '<div>' . $category->name_fa
                                . '<span class="extra_buttons">'
                                . '<a href="' . route('categories.edit', $category->id) .'" class="btn btn-warning bot_keyboard" title="ویرایش"> '
                                . '<i class="glyphicon glyphicon-pencil"></i>'
                                . '</a>'
                                . '<a href="' . route('categories.create') . '/?parent_id='.$category->parent_id.'" class="btn btn-success bot_keyboard" title="افزودن دکمه">'
                                . '<i class="glyphicon glyphicon-plus"></i>'
                                . '</a>'
                                . Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete', 'style' => 'display: inline'])
                                . '<button type="submit" class="btn btn-danger bot_keyboard btn-sm" title="حذف"'
                                . ' onclick="return confirm(\'آیا اطمینان دارید؟\')">'
                                . '<i class="glyphicon glyphicon-trash"></i>'
                                . '</button>'
                                . Form::close()
                                . '</span>'
                                . '</div>'
                                . displayHtmlCategoryTree($category->children()->get(), false) .
                                '</li>';
                        }
                        $str .= '</ol>';

                        return $str;
                    }

                    echo displayHtmlCategoryTree($categories);

                @endphp
            </div>
        </div>
    </div>
    </div>

    <div class="text-center">

    </div>
    </div>
@endsection

@push('scripts')
    <script src="https://unpkg.com/jqueryui@1.11.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/nestedSortable/2.0.0/jquery.mjs.nestedSortable.js"></script>
    <script>
        $(document).ready(function () {

            $('ol.sortable').nestedSortable({
                forcePlaceholderSize: true,
                handle: 'div',
                helper: 'clone',
                items: 'li',
                opacity: .6,
                placeholder: 'placeholder',
               // revert: 250,
                tabSize: 25,
                tolerance: 'pointer',
                toleranceElement: '> div',
                rtl: true,

                isTree: true,
                expandOnHover: 700,
                startCollapsed: false,
                update: function () {
                    list = $(this).nestedSortable('serialize');
                    $.post(
                        '{{ url('category/' . 1 . '/sort_keyboard') }}',
                        {_token: '{{ csrf_token() }}', list: list}
                    );
                }
            });

            $('.disclose').on('click', function () {
                $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            })

            $('#serialize').click(function () {
                serialized = $('ol.sortable').nestedSortable('serialize');
                $('#serializeOutput').text(serialized + '\n\n');
            })

            $('#toHierarchy').click(function (e) {
                hiered = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
                hiered = dump(hiered);
                (typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
                    $('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
            })

            $('#toArray').click(function (e) {
                arraied = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
                console.log(arraied);
            })

        });

    </script>

@endpush