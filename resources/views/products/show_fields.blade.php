<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شناسه:') !!}
    <p>{!! $product->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'عنوان:') !!}
    <p>{!! $product->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description_mini', 'توضیحات مختصر:') !!}
    <p>{!! $product->description_mini !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'توضیحات بیشتر:') !!}
    <p>{!! $product->description !!}</p>
</div>

<!-- Count Field -->
<div class="form-group">
    {!! Form::label('total_count', 'تعداد کل:') !!}
    <p>{!! $product->total_count !!}</p>
</div>

<!-- Sold Count Field -->
<div class="form-group">
    {!! Form::label('sold_count', 'تعداد فروش رفته:') !!}
    <p>{!! $product->sold_count !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'قیمت:') !!}
    <p>{!! $product->price !!}</p>
</div>

<!-- Discount Field -->
<div class="form-group">
    {!! Form::label('discount', 'تخفیف تکی:') !!}
    <p>{!! $product->discount !!}</p>
</div>

<!-- Teacher Name Field -->
<div class="form-group">
    {!! Form::label('teacher_name', 'نام استاد:') !!}
    <p>{!! $product->user->email !!}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'مدت آموزش:') !!}
    <p>{!! $product->duration !!}</p>
</div>

<!-- Start At Field -->
<div class="form-group">
    {!! Form::label('start_at', 'تاریخ شروع:') !!}
    <p>{!! $product->start_at !!}</p>
</div>

<!-- End At Field -->
<div class="form-group">
    {!! Form::label('end_at', 'تاریخ پایان:') !!}
    <p>{!! $product->end_at !!}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'آیا فعال است؟') !!}
    <p>{!! $product->is_active !!}</p>
</div>


<!-- Image Main Field -->
<div class="form-group">
    {!! Form::label('image_main', 'عکس صفحه اصلی:') !!}
    @if(isset($product->image_main))
        <p><img src="{{showProductImage($product)}}" alt=""></p>
    @else
        ندارد.
    @endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $product->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ آخرین ویرایش:') !!}
    <p>{!! $product->updated_at !!}</p>
</div>

<div class="form-group">
    {!! Form::label('category_id', 'دسته بندی') !!}
    <p>{!! $product->category->name_fa !!}</p>
</div>

<div class="form-group">
    {!! Form::label('status', 'وضعیت') !!}
    <p>{!! $product->statusStr !!}</p>
</div>

<div class="form-group">
    {!! Form::label('vote', 'امتیاز') !!}
    <p>{!! $product->vote !!}</p>
</div>


<!-- Total Voters Field -->
<div class="form-group">
    {!! Form::label('total_voters', 'تعداد رای دهندگان:') !!}
    <p>{!! $product->total_voters !!}</p>
</div>

<div class="form-group">
    {!! Form::label('rating_movie', 'امتیاز فیلم:') !!}
    <p>{!! $product->rating_movie !!}</p>
</div>

<div class="form-group">
    {!! Form::label('rating_voice', 'امتیاز صدا:') !!}
    <p>{!! $product->rating_voice !!}</p>
</div>

<div class="form-group">
    {!! Form::label('rating_complete', 'امتیاز جامع بودن:') !!}
    <p>{!! $product->rating_complete !!}</p>
</div>

<div class="form-group">
    {!! Form::label('rating_teacher', 'امتیاز استاد:') !!}
    <p>{!! $product->rating_teacher !!}</p>
</div>

<div class="form-group">
    {!! Form::label('rating_support', 'امتیاز پشتیبانی:') !!}
    <p>{!! $product->rating_support !!}</p>
</div>

<div class="form-group">
    {!! Form::label('rating_rezayat', 'امتیاز رضایت:') !!}
    <p>{!! $product->rating_rezayat !!}</p>
</div>

<div class="form-group">
    {!! Form::label('total_visit', 'امار کل بازدید:') !!}
    <p>{!! $product->total_visit !!}</p>
</div>

<div class="form-group">
    {!! Form::label('user_earn_score', 'امتیازی که کاربر بعد از خرید محصول دریافت میکند:') !!}
    <p>{!! $product->user_earn_score !!}</p>
</div>

<div class="form-group">
    {!! Form::label('can_get_license', 'آیا کابر گواهی نامه مگیرد؟') !!}
    <p>@if($product->can_get_license == 1) بله @else خیر @endif </p>
</div>

<div class="form-group">
    {!! Form::label('can_sign_up', 'آیا کاربران میتوانند ثبت نام کنند؟') !!}
    <p>@if($product->can_sign_up == 1) بله @else خیر @endif </p>
</div>

<div class="form-group">
    {!! Form::label('product_type_id', 'نوع محصول') !!}
    <p>{!! $product->productType->name_fa !!}</p>
</div>

<div class="form-group">
    {!! Form::label('special_duration', 'طول مدت محصول ویژه') !!}
    <p>{!! $product->special_duration !!}</p>
</div>

<div class="form-group">
    {!! Form::label('special_city', 'شهر محصول ویژه') !!}
    <p>{!! $product->special_city !!}</p>
</div>

<div class="form-group">
    {!! Form::label('special_teacher_name', 'نام استاد محصول ویژه') !!}
    <p>{!! $product->special_teacher_name !!}</p>
</div>

<div class="form-group">
    {!! Form::label('special_address', 'آدرس محصول ویژه') !!}
    <p>{!! $product->special_address !!}</p>
</div>

<div class="form-group">
    {!! Form::label('special_time', 'زمان بندی محصول ویژه') !!}
    <p>{!! $product->special_time !!}</p>
</div>

<div class="form-group">
    {!! Form::label('special_tel', 'تلفن محصول ویژه جهت تماس') !!}
    <p>{!! $product->special_tel !!}</p>
</div>

