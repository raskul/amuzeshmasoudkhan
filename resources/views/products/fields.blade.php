<!-- فیلد دسته بندی: -->
<div class="form-group col-sm-6 {{ $errors->has('category_id') ? ' has-error' : '' }}">
    {!! Form::label('category_id', 'دسته بندی:') !!}
    {!! Form::select('category_id', $categories, (isset($product))?$product->categories:null, ['class' => 'form-control select2']) !!}
    @if ($errors->has('category_id'))
        <span class="help-block"><strong>{{ $errors->first('category_id') }}</strong></span>
    @endif
</div>

<!-- فیلد دسته بندی: -->
<div class="form-group col-sm-6 {{ $errors->has('user_id') ? ' has-error' : '' }}">
    {!! Form::label('user_id', 'انتخاب استاد:') !!}
    {!! Form::select('user_id', $users, (isset($product))?$product->user_id:null, ['class' => 'form-control select2']) !!}
    @if ($errors->has('user_id'))
        <span class="help-block"><strong>{{ $errors->first('user_id') }}</strong></span>
    @endif
</div>

{{--<!-- فیلد دسته بندی: -->--}}
{{--<div class="form-group col-sm-6 {{ $errors->has('tag_id[]') ? ' has-error' : '' }}">--}}
{{--{!! Form::label('tag_id[]', 'برچسب ها:') !!}--}}
{{--{!! Form::select('tag_id[]', $tags, (isset($product))?$product->tags:null, ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}--}}
{{--@if ($errors->has('tag_id[]'))--}}
{{--<span class="help-block"><strong>{{ $errors->first('tag_id[]') }}</strong></span>--}}
{{--@endif--}}
{{--</div>--}}

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'عنوان:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description_mini', 'توضیحات مختصر:') !!}
    {!! Form::text('description_mini', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'توضیحات:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'editorcm']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'قیمت:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', 'تخفیف:') !!}
    {!! Form::text('discount', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sold_count', 'تعداد فروش رفته:') !!}
    {!! Form::text('sold_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_count', 'تعداد کل محصولات موجود در انبار:') !!}
    {!! Form::text('total_count', null, ['class' => 'form-control']) !!}
    <div class="help-block">اگر 0 باشد به تعداد نا محدود میتوان از این محصول فروخت اما اگر مثلا 20 باشد فقط 20 عدد از
        این محصول را مشتریان میتوانند بخرند
    </div>
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vote', 'امتیاز کل:') !!}
    {!! Form::text('vote', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating_movie', 'امتیاز فیلم:') !!}
    {!! Form::text('rating_movie', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating_voice', 'امتیاز صدا:') !!}
    {!! Form::text('rating_voice', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating_complete', 'امتیاز جامع بودن:') !!}
    {!! Form::text('rating_complete', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating_teacher', 'امتیاز استاد:') !!}
    {!! Form::text('rating_teacher', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating_support', 'امتیاز پشتیبانی:') !!}
    {!! Form::text('rating_support', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating_rezayat', 'امتیاز رضایت:') !!}
    {!! Form::text('rating_rezayat', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_voters', 'تعداد رای دهندگان:') !!}
    {!! Form::text('total_voters', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_visit', 'تعداد بازدیدکنندگان:') !!}
    {!! Form::text('total_visit', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_earn_score', 'امتیازی که از خرید محصول کاربران کسب میکنند:') !!}
    {!! Form::text('user_earn_score', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    <label for="">آیا محصول فعال است؟</label>
    <select name="is_active" id="" class="form-control">
        <option value="1" @if((isset($product)) && ($product->is_active == 1)) selected @endif>بله</option>
        <option value="0" @if((isset($product)) && ($product->is_active == 0)) selected @endif >خیر</option>
    </select>
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    <label for="">آیا امکان گرفتن گواهینامه وجود دارد؟</label>
    <select name="can_get_license" id="" class="form-control">
        <option value="1" @if((isset($product)) && ($product->can_get_license == 1)) selected @endif >بله</option>
        <option value="0" @if((isset($product)) && ($product->can_get_license == 0)) selected @endif>خیر</option>
    </select>
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    <label for="">آیا امکان ثبت نام برای کاربران وجود دارد؟</label>
    <select name="can_sign_up" id="" class="form-control">
        <option value="1" @if((isset($product)) && ($product->can_sign_up == 1)) selected @endif>بله</option>
        <option value="0" @if((isset($product)) && ($product->can_sign_up == 0)) selected @endif>خیر</option>
    </select>
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    <label for="">وضعیت؟</label>
    <select name="status" id="" class="form-control">
        <option value="0" @if((isset($product)) && ($product->status == 0)) selected @endif>به زودی برگزار خواهد شد
        </option>
        <option value="1" @if((isset($product)) && ($product->status == 1)) selected @endif>در حال برگزاری</option>
        <option value="2" @if((isset($product)) && ($product->status == 2)) selected @endif>تکمیل شده</option>
    </select>
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    <label for="">نوع محصول؟</label>
    <select name="product_type_id" id="" class="form-control">
        @foreach($productTypes as $productType)
            <option value="{{$productType->id}}"
                    @if((isset($product)) && ($product->product_type_id == $productType->id)) selected @endif>{{$productType->name_fa}}</option>
        @endforeach
    </select>
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_at', 'زمان شروع:') !!}
    {!! Form::text('start_at', null, ['class' => 'form-control pDatePicker']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_at', 'زمان پایان:') !!}
    {!! Form::text('end_at', null, ['class' => 'form-control pDatePicker']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('special_duration', 'مدت دروس ویژه:') !!}
    {!! Form::text('special_duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('special_city', 'شهر دروس ویژه:') !!}
    {!! Form::text('special_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('special_address', 'متن آدرس دروس ویژه:') !!}
    {!! Form::text('special_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('special_time', 'متن زمان دروس ویژه:') !!}
    {!! Form::text('special_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('special_tel', 'متن تلفن دروس ویژه:') !!}
    {!! Form::text('special_tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('special_teacher_name', 'اسم استاد دروس ویژه') !!}
    {!! Form::text('special_teacher_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <label for="image">عکس دوره</label>
    {!! Form::file('image')!!}
    {{--<input type="file" name="image">--}}

    @if(isset($product->image_main))
        <div>
            <label for="nowImage">عکس فعلی:</label>
            <img width="100%" src="{{showProductImage($product)}}" alt="" id="nowImage">
        </div>
    @endif
</div>

<div class="form-group col-sm-6">
    <button type="submit" class="btn btn-success">ذخیره</button>
</div>

@push('scripts')
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editorcm').ckeditor();
            $('html, body').animate({
                scrollTop: $("#myDiv").offset().top
            }, 2000);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".pDatePicker").pDatepicker({
                altField: '#inlineExampleAlt',
                altFormat: 'LLLL',
                toolbox: {
                    calendarSwitch: {
                        enabled: true
                    }
                },
                navigator: {
                    scroll: {
                        enabled: false
                    }
                },
                maxDate: new persianDate().add('month', 12).valueOf(),
                minDate: new persianDate().subtract('month', 0).valueOf(),
                timePicker: {
                    enabled: true,
                    meridiem: {
                        enabled: true
                    }
                }
            });
        });
    </script>

@endpush