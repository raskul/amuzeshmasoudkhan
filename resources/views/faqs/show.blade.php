@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            سوالات و جواب های دوره آموزشی
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding:0 20px">
                    @include('faqs.show_fields')
                    <a href="{!! route('faqs.index') !!}" class="btn btn-default">بازگشت</a>
                </div>
            </div>
        </div>
    </div>
@endsection
