<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره:') !!}
    <p>{!! $post->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'عنوان:') !!}
    <p>{!! $post->title !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'متن:') !!}
    <p>{!! $post->text !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'تاریخ ساخت:') !!}
    <p>{!! $post->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'تاریخ ویرایش:') !!}
    <p>{!! $post->updated_at !!}</p>
</div>

