<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'نام:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'ایمیل:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'کلمه عبور:') !!}
    {!! Form::text('password', null, ['class' => 'form-control']) !!}
</div>

<!-- Role Id Field -->
<div class="form-group col-sm-6 {{ $errors->has('role_id') ? ' has-error' : '' }}">
    {!! Form::label('role_id', 'سمت:') !!}
    {!! Form::select('role_id', $roles, (isset($user))?$user->role_id:null, ['class' => 'form-control']) !!}
    @if ($errors->has('role_id'))
        <span class="help-block"><strong>{{ $errors->first('role_id') }}</strong></span>
    @endif
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('balance', 'موجودی:') !!}
    {!! Form::text('balance', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'استان:') !!}
    {!! Form::text('state', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'شهر:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'آدرس:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tel', 'شماره موبایل:') !!}
    {!! Form::text('tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telephone', 'شماره تلفن:') !!}
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commission', 'درصد کمیسون:') !!}
    {!! Form::text('commission', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('about', 'بیوگرافی:') !!}
    {!! Form::text('about', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('score', 'امتیاز:') !!}
    {!! Form::text('score', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('age', 'سن:') !!}
    {!! Form::text('age', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job', 'شغل:') !!}
    {!! Form::text('job', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('education', 'تحصیلات:') !!}
    {!! Form::text('education', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postal_code', 'کد پستی:') !!}
    {!! Form::text('postal_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telegram', 'تلگرام:') !!}
    {!! Form::text('telegram', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instagram', 'اینستاگرام:') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('linkedin', 'لینکداین:') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('twitter', 'توییتر:') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank', 'نام بانک:') !!}
    {!! Form::text('bank', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_number', 'شماره حساب:') !!}
    {!! Form::text('bank_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_sheba', 'شماره شبا:') !!}
    {!! Form::text('bank_sheba', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <label for="is_spam">آیا اسپمر هست؟</label>
    <select name="is_spam" id="is_spam" class="form-control">
        <option value="0">خیر</option>
        <option value="1">بله</option>
    </select>
</div>

<div class="form-group col-sm-6">
    <label for="is_ban">آیا بن شود؟</label>
    <select name="is_ban" id="is_ban" class="form-control">
        <option value="0">خیر</option>
        <option value="1">بله</option>
    </select>
</div>

<!-- Balance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'تصویر:') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
    @if(isset($user))
        <div >تصویر فعلی:</div>
        <div class="col-sm-6">
            <img width="500" height="100%" src="{{showUserImage($user->id)}}" alt="">
        </div>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">لغو</a>
</div>
