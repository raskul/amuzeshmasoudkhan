@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            کاربران
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-right: 20px">
                    @include('users.show_fields')
                    <a href="{!! route('users.index') !!}" class="btn btn-default">بازگشت</a>
                </div>
            </div>
        </div>
    </div>

    <section class="content-header">
        <h1>
            لیست خرید ها
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-right: 20px">
                    @if(count($factors))

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th width="50px">شماره فاکتور</th>
                                <th>کاربر</th>
                                <th>مجموع</th>
                                <th>تخفیف</th>
                                <th>مبلغ پرداختی</th>
                                <th width="50px">تاریخ</th>
                                <th width="600px">دروس</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($factors as $factor)
                                <tr>
                                    <td>{{$factor->id}}</td>
                                    <td>{{$factor->user->email}}</td>
                                    <td>{{$factor->sum}}</td>
                                    <td>{{$factor->discount_sum}}</td>
                                    <td>{{$factor->amount}}</td>
                                    <td>{{pDate($factor->created_at, true)}}</td>
                                    @php
                                    $details = $factor->detail;
                                    $details = unserialize($details);
                                    @endphp
                                    <td>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>شماره</th>
                                            <th>عنوان</th>
                                            <th>قیمت</th>
                                            <th>استاد</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($details as $detail)
                                            @php
                                            $product = \App\Models\Product::find($detail['product_id']);
                                            @endphp
                                            {{--$product_discount_codes = unserialize($detail['array_discount_cods_products']);--}}
                                            {{--$user_discount_codes = unserialize($detail['array_discount_cods_users']);--}}
                                        <tr>
                                            {{--{{dd($detail)}}--}}
                                            <td>{{$product->id}}</td>
                                            <td>{{$product->title}}</td>
                                            <td>{{$product->price}}</td>
                                            <td>{{$product->user->name}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{--                                    <td>{{$details}}</td>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    @else
                    <span style="font-style: italic;color: #9b9b9b;"> این کاربر تا کینون خریدی انجام نداده است. </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
