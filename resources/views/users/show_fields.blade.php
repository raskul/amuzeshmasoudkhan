<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره عضویت:') !!}
    <span>{!! $user->id !!}</span>
</div>

@if(isset($user->name))
<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'نام:') !!}
    <span>{!! $user->name !!}</span>
</div>
@endif

@if(isset($user->email))
<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'ایمیل:') !!}
    <span>{!! $user->email !!}</span>
</div>
@endif

@if(isset($user->role))
<!-- Role Id Field -->
<div class="form-group">
    {!! Form::label('role_id', 'سمت:') !!}
    <span>{!! $user->role->name_fa !!}</span>
</div>
@endif

@if(isset($user->balance))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'اعتبار:') !!}
    <span>{!! $user->balance !!} <span style="font-size: small">تومان</span> </span>
</div>
@endif

@if(isset($user->image))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('image', 'تصویر:') !!}
    <div><img width="500" src="{{showUserImage($user->id)}}" alt=""></div>
</div>
@endif

@if(isset($user->state))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('state', 'استان:') !!}
    <span>{!! $user->state !!}</span>
</div>
@endif

@if(isset($user->city))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('city', 'شهر:') !!}
    <span>{!! $user->city !!}</span>
</div>
@endif

@if(isset($user->address))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('address', 'آدرس:') !!}
    <span>{!! $user->address !!}</span>
</div>
@endif

@if(isset($user->tel))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('tel', 'موبایل:') !!}
    <span>{!! $user->tel !!}</span>
</div>
@endif

@if(isset($user->telephone))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('telephone', 'تلفن:') !!}
    <span>{!! $user->telephone !!}</span>
</div>
@endif

@if(isset($user->commission) && ($user->commission != 0))
<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('commission', 'درصد کمسیون:') !!}
    <span>{!! $user->commission !!}</span>
    <span> درصد </span>
</div>
@endif

<div class="form-group">
    {!! Form::label('about', 'بیوگرافی:') !!}
    <span>{!! $user->about !!}</span>
</div>

<div class="form-group">
    {!! Form::label('is_spam', 'یوزر اسپمر است؟:') !!}
    <span>@if ($user->is_spam ==0) خیر @else بله @endif </span>
</div>

<div class="form-group">
    {!! Form::label('is_ban', 'یوزر بن شده؟:') !!}
    <span>@if ($user->is_ban ==0) خیر @else بله @endif</span>
</div>

<div class="form-group">
    {!! Form::label('score', 'امتیاز:') !!}
    <span>{!! $user->score !!}</span>
</div>

<div class="form-group">
    {!! Form::label('age', 'سن:') !!}
    <span>{!! $user->age !!}</span>
</div>

<div class="form-group">
    {!! Form::label('job', 'شغل:') !!}
    <span>{!! $user->job !!}</span>
</div>

<div class="form-group">
    {!! Form::label('education', 'تحصیلات:') !!}
    <span>{!! $user->education !!}</span>
</div>

<div class="form-group">
    {!! Form::label('postal_code', 'تلفن:') !!}
    <span>{!! $user->postal_code !!}</span>
</div>

<div class="form-group">
    {!! Form::label('telegram', 'تلگرام:') !!}
    <span>{!! $user->telegram !!}</span>
</div>

<div class="form-group">
    {!! Form::label('instagram', 'اینستا گرام:') !!}
    <span>{!! $user->instagram !!}</span>
</div>

<div class="form-group">
    {!! Form::label('linkedin', 'لینکداین:') !!}
    <span>{!! $user->linkedin !!}</span>
</div>

<div class="form-group">
    {!! Form::label('twitter', 'توییتر:') !!}
    <span>{!! $user->twitter !!}</span>
</div>

<div class="form-group">
    {!! Form::label('bank', 'نام بانک:') !!}
    <span>{!! $user->bank !!}</span>
</div>

<div class="form-group">
    {!! Form::label('bank_number', 'شماره حساب:') !!}
    <span>{!! $user->bank_number !!}</span>
</div>

<div class="form-group">
    {!! Form::label('bank_sheba', 'شماره شبا:') !!}
    <span>{!! $user->bank_sheba !!}</span>
</div>

<div class="form-group">
    {!! Form::label('created_at', 'تاریخ عضویت:') !!}
    <span>{!! $user->created_at !!}</span>
</div>

