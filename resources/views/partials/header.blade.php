<header id="top">
    <!--شروع کد های بالای هدر-->
    @inject('header','App\classes\helpers\header')
    @php
        $category = $header->getCategories();
        $settings = $header->getSettingsForHeader();
    @endphp
    <div id="header-top">
        <div id="header-top-center">
            {!! $settings['header_menu'] !!}
        </div>
        <div id="header-top-left">
            @if(! Auth::check())
                <div id="top-signup"><a href="{{route('reg')}}">ایجاد حساب کاربری</a>
                    <div id="top-signup-icon"><img src="{{asset('assets/images/signup.png')}}" alt=""></div>
                </div>
                <div id="top-login"><a href="#">ورود به سایت</a>
                    <div id="top-login-icon"><img src="{{asset('assets/images/login.png')}}" alt=""></div>
                </div>
            @else
                <div id="top-signup"><a href="{{route('profile.edit')}}">ورود به پنل کاربری</a>
                    <div id="top-login-icon"><img src="{{asset('assets/images/login.png')}}" alt=""></div>
                </div>
                <div id="top-logout">
                    <a href="#"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">خروج</a>
                    <form id="logout-form" action="/logout" method="post"
                          style="display: none">{{ csrf_field() }}</form>
                </div>
            @endif
        </div>
    </div>
    <!--انتهای کد های بالای هدر-->
    <div id="top-menu">
        <div id="top-menu-left">
            <div id="top-menu-top">

                @if(!(strpos(url()->full(), 'category')))
                    <div id="search-area">

                        {!! Form::open(['route'=>['category'], 'method' => 'GET', 'name' => 'search', 'id' => 'search', 'class' => 'search' ]) !!}
                        <input id="search-input" type="text" placeholder="دنبال چی می گردی؟">
                        {{--start hidden inputs for search works correctly--}}
                        <input type="hidden" name="price_all" value="on">
                        <input type="hidden" name="type_all" value="on">
                        <input type="hidden" name="sort_all" value="on">
                        <input type="hidden" name="category_all" value="on">
                        {{--end hidden inputs--}}
                        <div id="search-button" onclick="document.forms['search'].submit()">
                            <button type="submit" id="search-button-icon"><img
                                        src="{{asset('assets/images/search.png')}}"
                                        alt=""></button>
                        </div>
                        <div id="search-button" onClick="document.forms['search'].submit();">
                            <div id="search-button-icon"><img src="{{asset('assets/images/search.png')}}" alt=""></div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                @endif

                @if(Session::has('itemId'))
                    @php
                        $id = session()->get('itemId');
                        $whereData = [ 'myselect' => $id ];
                        $query = '';
                        foreach ($whereData['myselect'] as $select){
                            $query .= " id = $select or";
                        }
                        $query = 'select * from products where'. $query . ' id = 0' ;
                        $products = DB::select(DB::raw($query));
                    @endphp
                @else
                    @php
                        $products = [];
                    @endphp
                @endif
                <div id="menu-basket" style="width: 25%;height: 100%;float: right;padding-top: 14px">
                    <div id="basket-item">
                        <div id="menu-basket-icon">
                            <img src="{{asset('assets/images/basket.png')}}" alt="">
                        </div>
                        <div id="menu-basket-text">
                            <a href="{{ route('shopping.cart') }}">سبد خرید شما <span
                                        style="display: inline-block;width: 30px;height: 30px;background: #00778c;border-radius: 100%;margin-right: 5px;">@{{basket}}</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!--........................................شروع کد نویسی منو بالای سایت .....................................-->


            <nav id="top-menu-bottom">

                @if(count($category->children))
                    <ul>
                        @foreach($category->children as $child)
                            <li>
                                @if(!(strlen($child->custom_link) > 3))
                                    <a href="{{route('category', $child->name)}}">{{ $child->name_fa }}</a>
                                @else
                                    <a href="{{$child->custom_link}}">{{ $child->name_fa }}</a>
                                @endif
                                @if(count($child->children))
                                    <ul class="sub-menu">
                                        @foreach($child->children as $child2)
                                            <li>
                                                @if(!(strlen($child2->custom_link) > 3))
                                                    <a href="{{route('category', $child2->name)}}">{{$child2->name_fa}}</a>
                                                @else
                                                    <a href="{{$child2->custom_link}}">{{$child2->name_fa}}</a>
                                                @endif
                                                @if(count($child2->children))
                                                    <div class="submenu-3">
                                                        <div class="submenu3-col">
                                                            <ul class="submenu3-item">
                                                                @php
                                                                    $z = 0;
                                                                @endphp
                                                                @foreach($child2->children as $child3)
                                                                    @if(($z % 9 == 0) && ($z != 0))
                                                            </ul>
                                                        </div>
                                                        <div class="submenu3-col">
                                                            <ul class="submenu3-item">
                                                                @endif
                                                                @php
                                                                    $z++;
                                                                @endphp
                                                                <li>
                                                                    @if(!(strlen($child3->custom_link) > 3))
                                                                        <a href="{{route('category', $child3->name)}}">{{$child3->name_fa}}</a>
                                                                    @else
                                                                        <a href="{{$child3->custom_link}}">{{$child3->name_fa}}</a>
                                                                    @endif
                                                                </li>
                                                                @if(isset($child3->image))
                                                                    <div class="submenu3-col">
                                                                        <img src="{{$child3->image}}"
                                                                             alt=""
                                                                             style="width: 300px;position: absolute;left: 0;bottom: 0">
                                                                    </div>
                                                                @endif
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif

                <div id="modal-box">
                    <div class="modal-box-area">
                        @if(! Auth::check())
                            <i class="close"></i>
                            <h4>ورود به سایت</h4>
                            <p class="top-text">با ورود به سایت آکادمی ایده پردازان به تمام بخش های سایت دسترسی خواهید
                                داشت</p>

                            {!! Form::open(['route'=>['login' ], 'method' => 'post' ]) !!}


                            <ul>
                                <li>
                                    <label for=""><i style="background-position: -317px -790px"></i>
                                        پست الکترونیک
                                    </label>
                                    <input type="text" placeholder="Enter Your Email" name="email">
                                </li>
                                <a href="{{ url('/password/reset') }}">کلمه عبور خود را فراموش کرده اید؟</a>
                                <li>
                                    <label for=""><i style="background-position: -364px -790px"></i>
                                        رمز عبور
                                    </label>
                                    <input type="password" placeholder="Enter Password" name="password">
                                </li>
                                <div class="log-check-area">
                                    <input class="login-check-box" type="checkbox" name="remember">
                                    <p class="low">مرا به خاطر بسپار</p>
                                    <div class="uncheck-login"></div>
                                </div>
                            </ul>


                            <div class="reg-submit">
                                <div class="reg-submit-icon"></div>
                                <input type="submit" value="ورود به آکادمی ایده پردازان" class="reg-submit-button">
                            </div>

                            <p class="reg-text">حساب کاربری در سایت ندارید؟ <a href="{{ url('/reg') }}"> ثبت نام
                                    کنید</a></p>
                            {!! Form::close() !!}
                        @endif

                    </div>
                </div>
</header>

@push('scripts')
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                basket: {{ count($products) }}
            },
            methods: {
                addToCart: function (product_id) {
                    $.post('{{ url('addToBasket') }}/' + product_id, {
                        _token: '{{ csrf_token() }}'
                    }, function (data) {
                        this.basket = data;
                    }.bind(this));
                }
            }
        });
    </script>
@endpush
