{{--used (in admin panel)--}}
<div class="col-lg-6 col-sm-6 col-12">
    <h6>آپلود فایل</h6>
    <div class="input-group">
        <label class="input-group-btn">
            <span class="btn btn-primary">
                انتخاب&hellip; <input type="file" style="display: none;" name="file">
            </span>
        </label>
        <input type="text" class="form-control" readonly>
    </div>
    <span class="help-block" style="font-size: x-small">
        فایل قابل آپلود zip - jpg - jpeg - pdf
    </span>
</div>
{{--<label for="file">فایل ضمیمه:</label>--}}
{{--<input type="file" name="file" id="file">--}}
@push('scripts')
    <script>
        $(function () {
            $(document).on('change', ':file', function () {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
            $(':file').on('fileselect', function (event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;
                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }
            });
        });
    </script>
@endpush