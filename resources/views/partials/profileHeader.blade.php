<div class="teacher_menu_bar">
    <ul>
        <li class=" @if (Request::is('profile/edit')) active @endif">
            <i class="fas fa-address-card"></i>
            <a href="{{route('profile.edit')}}">پروفایل شما</a>
        </li>
        <li class="@if(Request::is('profile/userCourses')) active @endif">
            <i class="fas fa-caret-square-left"></i>
            <a href="{{route('userCourses')}}">در حال یادگیری</a>
        </li>
        <li class="@if(Request::is('profile/tea*')) active @endif">
            <i class="fas fa-edit"></i>
            <a href="{{route('teachingIndex')}}">دوره های مدرس</a>
        </li>
        <li class="@if(Request::is('profile/courseStudent')) active @endif">
            <i class="fas fa-users"></i>
            <a href="{{route('courseStudent')}}">دانشجویان</a>
        </li>
        <li class="@if(Request::is('profile/financial*')) active @endif">
            <i class="fas fa-dollar-sign"></i>
            <a href="{{route('financialDepartment')}}">امور مالی</a>
        </li>
        <li class="@if(Request::is('profile/ticket*')) active @endif">
            <i class="fas fa-envelope"></i>
            <a href="{{route('ticketIndex')}}">تیکت ها</a>
        </li>

    </ul>
</div>