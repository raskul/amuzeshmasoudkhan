<footer id="footer">
    @inject('footer', 'App\classes\helpers\footer')
    @php
        $settings = $footer->getSettingsForFooter();
    @endphp
    <div class="footer-line"></div>
    <div class="footer-bottom">
        <div class="footer-bottom-right">
            <div class="quick-access">
                <h4>دسترسی سریع</h4>
                {!! $settings['footer_fast'] !!}
            </div>
            {{--<div class="services">--}}
                {{--<h4>خدمات و سرویس ها</h4>--}}
                {{--{!! $settings['footer_services'] !!}--}}
            {{--</div>--}}
        </div>
        <div class="footer-bottom-left">
            <h4>برای اطلاع از تخفیف ها و جدیدترین دوره های ثبت نام کنید! </h4>
            <div class="mobile">
                {!! Form::open(['route'=>['giveMobileNumber'], 'method' => 'post' ]) !!}
                <div class="mobile-input">
                    <input type="text" placeholder="موبایل خود را وارد کنید" name="mobile">
                </div>
                <div class="mobile-button">
                    <button type="submit">ثبت موبایل</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</footer>


@push('scripts')
    <script>
        $("#top-menu-bottom > ul > li").hover(function () {
            $("> ul", this).fadeIn(400);
        }, function () {
            $("> ul", this).fadeOut(400);
        });

        $(".sub-menu > li").hover(function () {
            $("> .submenu-3", this).fadeIn(400);
        }, function () {
            $("> .submenu-3", this).fadeOut(400);
        });

        /*شروع کد های جی کوئری مربوط به اسلایدر اصلی سایت*/


        var slideTag = $('#sliders');
        var slideItems = slideTag.find('.main-slider');
        var slideNum = slideItems.length;
        var nextSlide = 1;
        var slideTimer = 7000;

        slider();

        function slider() {
            if (nextSlide > slideNum) {
                nextSlide = 1;
            }
            if (nextSlide < 1) {
                nextSlide = slideNum;
            }
            slideItems.hide();
            slideItems.eq(nextSlide - 1).fadeIn(500);
            nextSlide++;
        }

        var slideAuto = setInterval(slider, slideTimer);

        function goToNext() {
            clearInterval(slideAuto);
            slider();
        }

        $('#slider-left-arrow').click(function () {
            goToNext();
        });

        function goToPrev() {
            clearInterval(slideAuto);
            nextSlide = nextSlide - 2;
            slider();
        }

        $('#slider-right-arrow').click(function () {
            goToPrev();
        });

        $('.main-slider , #slider-right-arrow , #slider-left-arrow').hover(function () {
            clearInterval(slideAuto);
            $('#slider-right-arrow').show(0);
            $('#slider-left-arrow').show(0);
        }, function () {
            clearInterval(slideAuto);
            slideAuto = setInterval(slider, slideTimer);
            $('#slider-right-arrow').hide(0);
            $('#slider-left-arrow').hide(0);
        });

        /*شروع کدهای جی کوئری اسلایدر اسکرولی*/


        function slider_arrow(tagg) {
            var mainTagg = $(tagg);

            mainTagg.mouseenter(function () {
                $(this).find(".discount-scroll-prev").show(200);
                $(this).find(".discount-scroll-next").show(200);
            });
            mainTagg.mouseleave(function () {
                $(this).find(".discount-scroll-prev").hide(200);
                $(this).find(".discount-scroll-next").hide(200);
            })

        }

        function slide_scroll(direction, prophp) {
            var mainTag = $(prophp);
            var mainSliderTag = mainTag.parent();
            var sliderScrollUL = mainSliderTag.find('.discount-scroll-content ul');
            var sliderScrollLI = sliderScrollUL.find('li');
            var sliderScrollItem = sliderScrollLI.length;
            var slideScrollNum = Math.ceil(sliderScrollItem / 4);
            sliderScrollUL.css('width', sliderScrollItem * 300);
            var maxSlideScrollMargin = -(slideScrollNum - 1) * 1200;
            var marginRightNew;

            var marginRightOld = sliderScrollUL.css('margin-right');
            marginRightOld = parseFloat(marginRightOld);
            if (direction == 'next') {
                marginRightNew = marginRightOld - 300;
            }
            if (direction == 'prev') {
                marginRightNew = marginRightOld + 300;
            }
            if (marginRightNew < maxSlideScrollMargin) {
                marginRightNew = 0;
            }
            if (marginRightNew > 0) {
                marginRightNew = maxSlideScrollMargin;
            }
            sliderScrollUL.animate({'marginRight': marginRightNew}, 200);

        }

        $('.discount-scroll-next').click(function () {
            slide_scroll('next');
        });
        $('.discount-scroll-prev').click(function () {
            slide_scroll('prev');
        });


        /*shuru koce haye hquery marbut be tab box*/

        $('#tab-box li').click(function () {

            $('#tab-box li').removeClass('active');
            $(this).addClass('active');
            $('#new-course').find('section').hide();
            var index = $(this).index();
            $('#new-course').find('section').eq(index).fadeIn(300);
        });


        /*modal login jquery code start*/

        $('.login-check-box').click(function () {
            if ($(this).is(':checked')) {
                $('.uncheck-login').addClass('new-log-check-box');
            } else {
                $('.uncheck-login').removeClass('new-log-check-box');
            }
        });

        $('.register-area .reg-text a').click(function () {
            $('#modal-box').fadeIn(300);
            $('.modal-box-area').fadeIn(300);
        });
        $('#top-login').click(function () {
            $('#modal-box').fadeIn(300);
            $('.modal-box-area').fadeIn(300);
        });
        $('.modal-box-area .close').click(function () {
            $('#modal-box').fadeOut(200);
            $('.modal-box-area').fadeOut(200);
        });


    </script>
@endpush