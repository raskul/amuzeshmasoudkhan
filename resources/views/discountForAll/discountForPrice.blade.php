@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            تخفیف
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">

                <div class="row">
                    <div>
                        {!! Form::open(['route'=>['discountForPrice'], 'method' => 'post' ]) !!}

                        <div class="form-group col-sm-12">
                            <label for="takhfif_line">مبلغ قابل پرداخت از این قیمت بالاتر باشد شامل تخفیف میشود</label>
                            <input type="text" class="form-control" id="takhfif_line" name="takhfif_line" value="{{ $takhfif_line }}">
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="takhfif_value">میزان تخفیف (به درصد یا تومان)</label>
                            <input type="text" class="form-control" id="takhfif_value" name="takhfif_value" value="{{ $takhfif_value }}">
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="type">نوع</label>
                            <select class="form-control" id="takhfif_type" name="takhfif_type" >
                                <option value="0" @if($takhfif_type == 0) selected @endif >تومان</option>
                                <option value="1" @if($takhfif_type == 1) selected @endif >درصد</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success">ذخیره</button>

                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
