@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            تخفیف
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            {!! Form::open(['route' => 'discountFestival']) !!}
            <div class="box-body " style="padding-right: 20px">

                <div class="row">

                    <div class="form-group col-sm-3">
                        <label for="name">نام انگلیسی</label>
                        <br>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="name_fa">نام فارسی</label>
                        <input type="text" class="form-control" id="name_fa" name="name_fa">
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="code">کد</label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-sm-3">
                        <label for="price">قیمت</label>
                        <input type="text" class="form-control" id="price" name="price">
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="start_at">تاریخ شروع تخفیف</label>
                        <input type="text" class="form-control pDatePicker" id="start_at"
                               name="start_at">
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="end_at">تاریخ پایان تخفیف</label>
                        <input type="text" class="form-control pDatePicker" id="end_at"
                               name="end_at">
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-sm-3">
                        <label for="is_percent">برحسب</label>
                        <select name="is_percent" id="is_percent" class="form-control">
                            <option value="0">تومان</option>
                            <option value="1">درصد</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="active">وضعیت</label>
                        <select name="active" id="active" class="form-control">
                            <option value="1">فعال</option>
                            <option value="0">غیر فعال</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="type">نوع</label>
                        <select name="type" id="type" class="form-control">
                            <option value="1">تخفیف برای محصولات</option>
                            <option value="2">تخفیف برای کاربران</option>
                        </select>
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-sm-3">
                        <button type="submit" class="btn btn-success">ذخیره</button>
                    </div>

                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
