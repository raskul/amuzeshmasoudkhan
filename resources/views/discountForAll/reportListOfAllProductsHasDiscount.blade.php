@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            تخفیف
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">

                <div class="row">
                    <div>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>شماره</th>
                                <th>عنوان</th>
                                <th>قیمت</th>
                                <th>تخفیف</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td><a href="{{route('products.show', $product->id)}}"> {{ $product->id }} </a></td>
                                <td>{{ $product->title }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->discount }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
