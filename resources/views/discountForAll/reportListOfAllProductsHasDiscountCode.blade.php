@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            تخفیف
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">

                <div class="row">
                    <div>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>شماره</th>
                                <th>عنوان</th>
                                <th>قیمت</th>
                                <th>تخفیف</th>
                                <th>کد های تخفیف</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                @if(count($product->discounts))
                                    <tr>
                                        <td><a href="{{route('products.show', $product->id)}}"> {{ $product->id }} </a>
                                        </td>
                                        <td>{{ $product->title }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->discount }}</td>
                                        <td>
                                            <table class="table table-hover">
                                                @foreach($product->discounts as $discount)
                                                    <tr>
                                                        <td>{{ $discount->name }}</td>
                                                        <td>{{ $discount->name_fa }}</td>
                                                        <td>{{ $discount->code }}</td>
                                                        <td>{{ $discount->price }}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
