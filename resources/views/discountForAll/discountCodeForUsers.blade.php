@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            تخفیف
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">

                <div class="row">
                    برای ساختن کد برای تخفیف گرفتن یک یا چند یوزر خاص (هر کسی کد تخفیف را وارد کند تخفیف میگیرد)
                    باید یک جشنواره بسازید و "
                    <span style="color: red">
                    نوع
                    </span>
                    " را "
                    <span style="color: red">
                    تخفیف برای کاربران
                    </span>
                    " انتخاب کنید.
                </div>

            </div>
        </div>
    </div>
@endsection
