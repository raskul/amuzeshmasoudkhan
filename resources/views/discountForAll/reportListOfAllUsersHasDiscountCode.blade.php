@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1>
            تخفیف
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            <div class="box-body " style="padding-right: 20px">

                <div class="row">
                    <div>
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>شماره</th>
                                <th>نام</th>
                                <th>ایمیل</th>
                                <th>کد های تخفیف</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                @if(count($user->discounts))
                                    <tr>
                                        <td><a href="{{route('users.show', $user->id)}}"> {{ $user->id }} </a>
                                        </td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <table class="table table-hover">
                                                @foreach($user->discounts as $discount)
                                                    <tr>
                                                        <td>{{ $discount->name }}</td>
                                                        <td>{{ $discount->name_fa }}</td>
                                                        <td>{{ $discount->code }}</td>
                                                        <td>{{ $discount->price }}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
