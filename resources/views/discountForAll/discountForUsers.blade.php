@extends('layouts.app')
@push('header')
    <style>

        .checkbox.checbox-switch {
            padding-: 0;
        }

        .checkbox.checbox-switch label,
        .checkbox-inline.checbox-switch {
            display: inline-block;
            position: relative;
            padding-right: 0;
        }
        .checkbox.checbox-switch label input,
        .checkbox-inline.checbox-switch input {
            display: none;
        }
        .checkbox.checbox-switch label span,
        .checkbox-inline.checbox-switch span {
            width: 35px;
            border-radius: 20px;
            height: 18px;
            border: 1px solid #dbdbdb;
            background-color: rgb(255, 255, 255);
            border-color: rgb(223, 223, 223);
            box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s;
            display: inline-block;
            vertical-align: middle;
            margin-right: 5px;
        }
        .checkbox.checbox-switch label span:before,
        .checkbox-inline.checbox-switch span:before {
            display: inline-block;
            width: 16px;
            height: 16px;
            border-radius: 50%;
            background: rgb(255,255,255);
            content: " ";
            top: 0;
            position: relative;
            right: 0;
            transition: all 0.3s ease;
            box-shadow: 0 1px 4px rgba(0,0,0,0.4);
        }
        .checkbox.checbox-switch label > input:checked + span:before,
        .checkbox-inline.checbox-switch > input:checked + span:before {
            right: 17px;
        }


        /* Switch Default */
        .checkbox.checbox-switch label > input:checked + span,
        .checkbox-inline.checbox-switch > input:checked + span {
            background-color: rgb(180, 182, 183);
            border-color: rgb(180, 182, 183);
            box-shadow: rgb(180, 182, 183) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch label > input:checked:disabled + span,
        .checkbox-inline.checbox-switch > input:checked:disabled + span {
            background-color: rgb(220, 220, 220);
            border-color: rgb(220, 220, 220);
            box-shadow: rgb(220, 220, 220) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch label > input:disabled + span,
        .checkbox-inline.checbox-switch > input:disabled + span {
            background-color: rgb(232,235,238);
            border-color: rgb(255,255,255);
        }
        .checkbox.checbox-switch label > input:disabled + span:before,
        .checkbox-inline.checbox-switch > input:disabled + span:before {
            background-color: rgb(248,249,250);
            border-color: rgb(243, 243, 243);
            box-shadow: 0 1px 4px rgba(0,0,0,0.1);
        }

        /* Switch Light */
        .checkbox.checbox-switch.switch-light label > input:checked + span,
        .checkbox-inline.checbox-switch.switch-light > input:checked + span {
            background-color: rgb(248,249,250);
            border-color: rgb(248,249,250);
            box-shadow: rgb(248,249,250) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }

        /* Switch Dark */
        .checkbox.checbox-switch.switch-dark label > input:checked + span,
        .checkbox-inline.checbox-switch.switch-dark > input:checked + span {
            background-color: rgb(52,58,64);
            border-color: rgb(52,58,64);
            box-shadow: rgb(52,58,64) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch.switch-dark label > input:checked:disabled + span,
        .checkbox-inline.checbox-switch.switch-dark > input:checked:disabled + span {
            background-color: rgb(100, 102, 104);
            border-color: rgb(100, 102, 104);
            box-shadow: rgb(100, 102, 104) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }

        /* Switch Success */
        .checkbox.checbox-switch.switch-success label > input:checked + span,
        .checkbox-inline.checbox-switch.switch-success > input:checked + span {
            background-color: rgb(40, 167, 69);
            border-color: rgb(40, 167, 69);
            box-shadow: rgb(40, 167, 69) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch.switch-success label > input:checked:disabled + span,
        .checkbox-inline.checbox-switch.switch-success > input:checked:disabled + span {
            background-color: rgb(153, 217, 168);
            border-color: rgb(153, 217, 168);
            box-shadow: rgb(153, 217, 168) 0px 0px 0px 8px inset;
        }

        /* Switch Danger */
        .checkbox.checbox-switch.switch-danger label > input:checked + span,
        .checkbox-inline.checbox-switch.switch-danger > input:checked + span {
            background-color: rgb(200, 35, 51);
            border-color: rgb(200, 35, 51);
            box-shadow: rgb(200, 35, 51) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch.switch-danger label > input:checked:disabled + span,
        .checkbox-inline.checbox-switch.switch-danger > input:checked:disabled + span {
            background-color: rgb(216, 119, 129);
            border-color: rgb(216, 119, 129);
            box-shadow: rgb(216, 119, 129) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }

        /* Switch Primary */
        .checkbox.checbox-switch.switch-primary label > input:checked + span,
        .checkbox-inline.checbox-switch.switch-primary > input:checked + span {
            background-color: rgb(0, 105, 217);
            border-color: rgb(0, 105, 217);
            box-shadow: rgb(0, 105, 217) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch.switch-primary label > input:checked:disabled + span,
        .checkbox-inline.checbox-switch.switch-primary > input:checked:disabled + span {
            background-color: rgb(109, 163, 221);
            border-color: rgb(109, 163, 221);
            box-shadow: rgb(109, 163, 221) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }

        /* Switch Info */
        .checkbox.checbox-switch.switch-info label > input:checked + span,
        .checkbox-inline.checbox-switch.switch-info > input:checked + span {
            background-color: rgb(23, 162, 184);
            border-color: rgb(23, 162, 184);
            box-shadow: rgb(23, 162, 184) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch.switch-info label > input:checked:disabled + span,
        .checkbox-inline.checbox-switch.switch-info > input:checked:disabled + span {
            background-color: rgb(102, 192, 206);
            border-color: rgb(102, 192, 206);
            box-shadow: rgb(102, 192, 206) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }

        /* Switch Warning */
        .checkbox.checbox-switch.switch-warning label > input:checked + span,
        .checkbox-inline.checbox-switch.switch-warning > input:checked + span {
            background-color: rgb(255, 193, 7);
            border-color: rgb(255, 193, 7);
            box-shadow: rgb(255, 193, 7) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }
        .checkbox.checbox-switch.switch-warning label > input:checked:disabled + span,
        .checkbox-inline.checbox-switch.switch-warning > input:checked:disabled + span {
            background-color: rgb(226, 195, 102);
            border-color: rgb(226, 195, 102);
            box-shadow: rgb(226, 195, 102) 0px 0px 0px 8px inset;
            transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;
        }

    </style>
@endpush
@section('content')
    <section class="content-header">
        <h1>
            تخفیف
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="box box-primary">

            {!! Form::open(['route' => 'discountForUsers']) !!}
            <div class="box-body " style="padding-right: 20px">

                <div class="row">
                    @if(isset($discounts))
                    <div class="form-group col-sm-12">
                        <label for="discount_id">ابتدا یک جنشواره فروش را انتخاب کنید:</label>

                        <select name="discount_id" id="discount_id" class="form-control select2">
                            @foreach($discounts as $discount)
                                <option value="{{$discount->id}}"
                                >{{ $discount->name }}</option>
                            @endforeach
                        </select>

                    </div>
                    @else
                    <div>ابتدا باید یک جشنواره فروش بسازید!</div>
                    @endif
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="user_id">کاربرانی که میخواهید تخفیف داشته باشند را انتخاب کنید:</label>

                        <select name="user_id[]" id="user_id" class="form-control select2" multiple>
                            @foreach($users as $user)
                                <option value="{{$user->id}}"
                                >{{ $user->name }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1" name="all_checked">
                        <label class="form-check-label" for="exampleCheck1">با زدن این دکمه همه کاربران انتخاب میشوند</label>
                    </div>
                </div>


                <div class="row">
                    <button type="submit" class="btn btn-success">ذخیره</button>
                </div>
                <br>
                <br>
                {{--<div class="row">--}}

                    {{--<div class="col-md-4">--}}
                        {{--<h3>پاک کردن تخفیفات</h3>--}}
                        {{--<div class="panel panel-default">--}}
                            {{--<div class="panel-body">--}}
                                {{--<small class="text-uppercase text-muted"><b> برای پاک کردن تخفیف های تمام محصولات دکمه زیر را بزنید و سپس روی ذخیره کلیک کنید.</b></small>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="checkbox checbox-switch switch-dark">--}}
                                        {{--<label>--}}
                                            {{--<input type="checkbox" name="delete_all_discount" />--}}
                                            {{--<span></span>--}}
                                            {{--پاک کردن تخفیف تمام محصولات--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}

                {!! Form::close() !!}

                <br>

                {{--<div class="row">--}}
                    {{--<span style="color: red">*</span>--}}
                    {{--<span style="font-size: x-small">در صورتی که میخواهید تخفیف چند محصول را پاک کنید، ابتدا محصولات را انتخاب کنید و سپس قیمت تخفیف را صفر ( 0 ) وارد کنید.</span>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection
