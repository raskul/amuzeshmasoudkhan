@extends('layouts.app')
@push('header')
    <style>
        .small-box {
            border-radius: 2px;
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        }

        .small-box > .inner {
            padding: 10px;
        }

        .small-box .icon {
            -webkit-transition: all .3s linear;
            -o-transition: all .3s linear;
            transition: all .3s linear;
            position: absolute;
            top: -10px;
            left: 10px;
            z-index: 0;
            font-size: 90px;
            color: rgba(0, 0, 0, 0.15);
        }

        .small-box > .small-box-footer {
            position: relative;
            text-align: center;
            padding: 3px 0;
            color: #fff;
            color: rgba(255, 255, 255, 0.8);
            display: block;
            z-index: 10;
            background: rgba(0, 0, 0, 0.1);
            text-decoration: none;
        }

        .bg-aqua {
            background-color: #00c0ef !important;
        }

        .bg-green {
            background-color: #00a65a !important;
        }

        .bg-yellow {
            background-color: #f39c12 !important;
        }

        .bg-red {
            background-color: #dd4b39 !important;
        }

        .padding-50-all {
            padding: 50px;
        }
    </style>
@endpush
@section('content')
    {{--@can('admin')--}}
    <div class="row padding-50-all">
        <div class="row padding-50-all">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>150</h3>
                        <small>در حال ساخت</small>
                        <p>ثبت نام های امروز</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>20</h3>
                        <p>خرید های امروز</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">اطلاعات بیشتر <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>158292</h3>

                        <p>تعداد کل کاربران</p>
                    </div>
                    <div class="icon">
                        <i class="ionicons ion-person-stalker"></i>
                    </div>
                    <a href="#" class="small-box-footer">اطلاعات بیشتر <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>6</h3>

                        <p>پیام های پاسخ داده نشده</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">اطلاعات بیشتر <i
                                class="fa fa-arrow-circle-left"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>



        <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        {{--//chart number one : number of students in groups--}}
                        <div id="colorbarchart" style="width:100%; height:500px;"></div>
                    </div>
                    <div>
                        <button id="plain">Plain</button>
                        <button id="inverted">Inverted</button>
                        <button id="polar">Polar</button>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        {{--//chart number one : number of students in groups--}}
                        <div id="group_azmoon" style="width:100%; height:500px;"></div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        {{--//chart number two : number of students in 12 month--}}
                        <div id="expected_balance" style="width:100%; height:500px;"></div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-6">
            <!-- LINE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    {{--//chart number three : banks amount --}}
                    <div class="chart">
                        <div id="banksChart" style="height:500px"></div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-6">
            <!-- PIE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        {{--//chart number four : students gender--}}
                        <div id="gender" style="height:500px"></div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-12">
            <!-- cheque table -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست پیغام هایی که باید پاسخ داده شوند.</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>آیا آموزش php رو هم دارید؟</td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm vosool"> پاسخ</button>
                            </td>
                        </tr>
                        <tr>
                            <td>تعداد درسا رو بیشتر نمیکنید؟</td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm vosool"> پاسخ</button>
                            </td>
                        </tr>
                        <tr>
                            <td>تشکر میکنم بابت خدماتتون</td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm vosool"> پاسخ</button>
                            </td>
                        </tr>
                        <tr>
                            <td>توی هوای برفی فقط اموزش شما شما ادم رو توی خونه نگه میداره</td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm vosool"> پاسخ</button>
                            </td>
                        </tr>
                        <tr>
                            <td>چطوری باید خرید کنیم؟</td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm vosool"> پاسخ</button>
                            </td>
                        </tr>
                        <tr>
                            <td>حالا چی کار کنم؟</td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm vosool"> پاسخ</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-12">
            <!-- cheque table -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">پیغام های پاسخ داده شده اخیر</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>تاریخ</th>
                            <th>متن</th>
                            <th>پاسخ دهنده</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr class="success">
                            <td>10 دقیقه پیش</td>
                            <td>ممنون که از ما خرید کردید.</td>
                            <td>خانم استوایی</td>
                        </tr>
                        <tr class="success">
                            <td>15 دقیقه پیش</td>
                            <td>موفق و پاینده باشید.</td>
                            <td>خانم استوایی</td>
                        </tr>
                        <tr class="success">
                            <td>نیم ساعت پیش</td>
                            <td>باز هم بیاید به سایت ما.</td>
                            <td>خانم استوایی</td>
                        </tr>
                        <tr class="success">
                            <td>یک ساعت پیش</td>
                            <td>ممنون که از ما خرید کردید.</td>
                            <td>آقای فرخزاد</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="clearfix"></div>
@endsection
@push('scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <script src="{{ asset('bower_components/chart.js/dist/Chart.min.js') }}"></script>

    <script>
        $(function () {


            Highcharts.chart('group_azmoon', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'میزان فروش هر دسته بندی'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'میزان فروش - تومان'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'amount: <b>{point.y:.1f}</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        ['php', 230653432],
                        ['java', 130653432],
                        ['java script', 262653432],
                        ['photo shop', 301653432],
                        ['windows', 127653432],
                        ['linux mint', 130653432],
                        ['ubunto', 192653432],
                        ['ICDL', 164653432],
                        ['Guangzhou', 56653432],
                        ['Delhi', 50653432],
                        ['Shenzhen', 56853432],
                        ['Seoul', 58653432],
                        ['Jakarta', 98065432],
                        ['Kinshasa', 37653432],
                        ['Tianjin', 52653432],
                        ['Tokyo', 15553432],
                        ['Cairo', 76853432],
                        ['Dhaka', 78653432],
                        ['Mexico City', 30653432],
                        ['Lima', 10653432]
                    ],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });


            Highcharts.chart('expected_balance', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ' پر درآمدترین اساتید'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Percent',
                    colorByPoint: true,
                    data: [{
                        name: 'جعفری',
                        y: 39000000
                    }, {
                        name: 'توانا',
                        y: 50000000
                    }, {
                        name: 'رستمی',
                        y: 68000000 ,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'دوستان',
                        y: 12988000
                    }, {
                        name: 'گوهری',
                        y: 10000000
                    }, {
                        name: 'سایر اساتید',
                        y: 5000000
                    }]
                }]
            });

// Radialize the colors
            Highcharts.setOptions({
                colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                    return {
                        radialGradient: {
                            cx: 0.5,
                            cy: 0.3,
                            r: 0.7
                        },
                        stops: [
                            [0, color],
                            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                        ]
                    };
                })
            });
            // Build the chart - banksChart
            Highcharts.chart('banksChart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'موجودی بانک ها'
                },
                legend: {
                    enabled: false,
                    rtl: true,
                    useHTML: true
                },
                tooltip: {
                    //                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                    rtl: true,
                    useHTML: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            },
                            connectorColor: 'silver'
                        }
                    }
                },
                series: [{
                    name: 'Rial',
                    data: [
                        {name: 'ملت', y: 12345670},
                        {name: 'پاسارگاد', y: 96543210},
                        {name: 'سامان', y: 59875450},
                        {name: 'ملی', y: 4785210},
                        {name: 'رفاه', y: 65488850},
                        {name: 'سپه', y: 1234560},
                    ]

                }]
            });
            // Build the chart - banksChart
            Highcharts.chart('gender', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'مقایسه فروش این هفته و هفته قبل'
                },
                tooltip: {
                    //                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                    rtl: true,
                    useHTML: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            },
                            connectorColor: 'silver'
                        }
                    }
                },
                series: [{
                    name: 'person',
                    data: [
                        {name: 'این هفته', y: 859},
                        {name: 'هفته قبل', y: 3011}
                    ]
                }]
            });


        });

        var chart = Highcharts.chart('colorbarchart', {

            title: {
                text: 'میزان فروش سال 96'
            },

            subtitle: {
                text: 'تومان'
            },

            xAxis: {
                categories: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند']
            },

            series: [{
                type: 'column',
                name: 'Milion',
                colorByPoint: true,
                data: [45.9, 30.5, 60.4, 50.2, 60.0, 70.0, 90.6, 80.5, 105.4, 73.1, 3.6, 0],
                showInLegend: false
            }]

        });


        $('#plain').click(function () {
            chart.update({
                chart: {
                    inverted: false,
                    polar: false
                },
                subtitle: {
                    text: 'Plain'
                }
            });
        });

        $('#inverted').click(function () {
            chart.update({
                chart: {
                    inverted: true,
                    polar: false
                },
                subtitle: {
                    text: 'Inverted'
                }
            });
        });

        $('#polar').click(function () {
            chart.update({
                chart: {
                    inverted: false,
                    polar: true
                },
                subtitle: {
                    text: 'Polar'
                }
            });
        });



        $('.vosool').click(function () {
            var that = $(this);
            var id = $(this).data('id');
            if (confirm('آیا اطمینان دارید؟')) {
                $.ajax({
                    method: 'patch',
                    url: '{{ url('') }}/cheques/' + id + '/passed',
                    data: {
                        _token: '{{ csrf_token() }}'
                    }
                }).success(function () {
                    that.parents('tr').remove();
                });
            }
        });
    </script>
@endpush
{{--@endcan--}}