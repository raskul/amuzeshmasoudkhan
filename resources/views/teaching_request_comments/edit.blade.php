@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Teaching Request Comment
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($teachingRequestComment, ['route' => ['teachingRequestComments.update', $teachingRequestComment->id], 'method' => 'patch']) !!}

                        @include('teaching_request_comments.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection