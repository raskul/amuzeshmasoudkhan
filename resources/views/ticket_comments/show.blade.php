@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ticket Comment
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('ticket_comments.show_fields')
                    <a href="{!! route('ticketComments.index') !!}" class="btn btn-default">بازگشت</a>
                </div>
            </div>
        </div>
    </div>
@endsection
