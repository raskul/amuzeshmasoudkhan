<!-- Name Field -->
<div class="form-group col-sm-6" style="display: none">
    {!! Form::label('name', 'نام:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
</div>

<!-- Name Fa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_fa', 'توضیح:') !!}
    {!! Form::text('name_fa', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'مقدار:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('settings.index') !!}" class="btn btn-default">لغو</a>
</div>
