@if (isset($paginator) && ($paginator->lastPage() > 1) && ($paginator->lastPage() <= 5))
    <ul>
        <?php
        $interval = isset($interval) ? abs(intval($interval)) : 3;
        $from = $paginator->currentPage() - $interval;
        if ($from < 1) {
            $from = 1;
        }
        $to = $paginator->currentPage() + $interval;
        if ($to > $paginator->lastPage()) {
            $to = $paginator->lastPage();
        }
        ?>
        @for($i = $from; $i <= $to; $i++)
            <?php
            $isCurrentPage = $paginator->currentPage() == $i;
            ?>
            <li class="{{ $isCurrentPage ? 'active' : '' }}">
                <a href="{{ !$isCurrentPage ? $paginator->url($i) : '#' }}">
                    {{ $i }}
                </a>
            </li>
        @endfor
    </ul>
@endif






<?php
// config
$link_limit = 5; // maximum number of links (a little bit inaccurate, but will be ok for now)
?>

@if ($paginator->lastPage() > 5)
    <?php
    $half_total_links = floor($link_limit / 2);
    $from = $paginator->currentPage() - $half_total_links;
    $to = $paginator->currentPage() + $half_total_links;
    if ($paginator->currentPage() < $half_total_links) {
        $to += $half_total_links - $paginator->currentPage();
    }
    if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
        $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
    }
    ?>
    <ul class="pagination">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url(1) }}">اولین</a>
        </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            @if($i ==  1)
                <li class="{{ ($paginator->currentPage() == $i + 1) ? ' disabled' : '' }}">
                    <a href="{{ $paginator->url($i + 1) }}">{{ ++$i }}</a>
                </li>
                @php
                    $i++
                @endphp
            @endif

            @if ($from < $i && $i < $to)
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endif

            @if($i == $from)
                <li style="background-color: rgba(12,12,12,0);color: black;padding: 0;margin: 0 -20px 0 -17px">...</li>
            @endif

            @if($i == $to)
                <li style="background-color: rgba(12,12,12,0);color: black;padding: 0;margin: 0 -20px 0 -17px">...</li>
            @endif

            @if($i ==  $paginator->lastPage() - 2)
                <li class="{{ ($paginator->currentPage() == $paginator->lastPage() - 1) ? ' disabled' : '' }}">
                    <a href="{{ $paginator->url($paginator->lastPage() - 1)  }}">{{ $i+1 }}</a>
                </li>
                @break
            @endif
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url($paginator->lastPage()) }}">آخرین</a>
        </li>
    </ul>
@endif





