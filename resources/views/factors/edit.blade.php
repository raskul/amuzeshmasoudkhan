@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Factor
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($factor, ['route' => ['factors.update', $factor->id], 'method' => 'patch']) !!}

                        @include('factors.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection