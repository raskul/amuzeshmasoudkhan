<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شماره فاکتور:') !!}
    <p>{!! $factor->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'کاربر:') !!}
    <p>{!! $factor->user->email !!}</p>
</div>

<!-- Sum Field -->
<div class="form-group">
    {!! Form::label('sum', 'مجموع:') !!}
    <p>{!! $factor->sum !!}</p>
</div>

<!-- Discount Sum Field -->
<div class="form-group">
    {!! Form::label('discount_sum', 'مجموع تخفیف ها:') !!}
    <p>{!! $factor->discount_sum !!}</p>
</div>

<!-- Is Paid Field -->
<div class="form-group">
    {!! Form::label('is_paid', 'وضعیت پرداخت:') !!}
    <p>@if($factor->is_paid == 1) بله @else خیر @endif</p>
</div>

<div class="form-group">
    {!! Form::label('created_at', 'تاریخ فاکتور:') !!}
    <p>{!! $factor->created_at !!}</p>
</div>

<hr>

<div class="form-group">
    {!! Form::label('created_at', 'موبایل:') !!}
    <p>{!! $factor->mobile !!}</p>
</div>
<div class="form-group">
    {!! Form::label('status', 'وضعیت:') !!}
    <p>{!! $factor->status !!}</p>
</div>
<div class="form-group">
    {!! Form::label('authority', 'شماره پیگیری:') !!}
    <p>{!! $factor->authority !!}</p>
</div>
<div class="form-group">
    {!! Form::label('refid', 'شماره پیگیری دوم:') !!}
    <p>{!! $factor->refid !!}</p>
</div>

<hr>

<div class="form-group">
    {!! Form::label('is_by_post', 'درخواست ارسال با پست؟:') !!}
    <p>@if($factor->is_by_post == 1) این کاربر درخواست خرید محصولات از طریق ارسال با پست را دارد. @else خیر @endif</p>
</div>
<div class="form-group">
    {!! Form::label('post_amount', 'مبلغ پست:') !!}
    <p>{!! $factor->post_amount !!}</p>
</div>
