@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            فاکتور
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-right: 20px">
                    @include('factors.show_fields')

                </div>
            </div>
        </div>
    </div>

    <section class="content-header">
        <h1>
            لیست محصولات خریداری شده
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-right: 20px">

                    @php
                        $factorProducts = unserialize($factor->detail)

                    @endphp

                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>شماره محصول</th>
                            <th>قیمت محصول</th>
                            <th>تخفیف محصول</th>
                            <th>تخفیفات دیگر محصول</th>
                            <th>مجموع پرداختی برای محصول</th>
                            <th>تمام تخفیفات محصول محصول</th>
                            <th>ایمیل استاد محصول</th>
                            <th>سهم استاد از این محصول</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($factorProducts as $factorProduct)
                            <tr>
                                <td>{{$factorProduct['product_id']}}</td>
                                <td>{{$factorProduct['product_price']}}</td>
                                <td>{{$factorProduct['product_self_discount']}}</td>
                                <td>{{$factorProduct['product_code_and_line_discount']}}</td>
                                <td>{{$factorProduct['product_pay_price']}}</td>
                                <td>{{$factorProduct['product_all_discount']}}</td>
                                <td>{{\App\User::find($factorProduct['teacher_id'])->pluck('email')->first()}}</td>
                                <td>{{$factorProduct['teacher_commission_money']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>

        <a href="{!! route('factors.index') !!}" class="btn btn-default">بازگشت</a>

    </div>
@endsection
