@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Index Vip Product
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('index_vip_products.show_fields')
                    <a href="{!! route('indexVipProducts.index') !!}" class="btn btn-default">بازگشت</a>
                </div>
            </div>
        </div>
    </div>
@endsection
