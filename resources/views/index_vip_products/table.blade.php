<table class="table table-responsive" id="indexVipProducts-table">
    <thead>
        <tr>
            <th>عنوان محصول</th>
        <th>عنوان</th>
        <th>متن</th>
        <th>ثبت نام آزاد است؟</th>
            <th colspan="3">عملیات</th>
        </tr>
    </thead>
    <tbody>
    @foreach($indexVipProducts as $indexVipProduct)
        <tr>
            <td>{!! $indexVipProduct->product->title !!}</td>
            <td>{!! $indexVipProduct->title !!}</td>
            <td>{!! $indexVipProduct->text !!}</td>
            <td>{!! $indexVipProduct->can_sign_up !!}</td>
            <td>
                {!! Form::open(['route' => ['indexVipProducts.destroy', $indexVipProduct->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('index.index') !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('indexVipProducts.edit', [$indexVipProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('آیا اطمینان دارید؟')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>