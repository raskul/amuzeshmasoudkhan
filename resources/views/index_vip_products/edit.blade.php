@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ویرایش دوره حضوری ویژه
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($indexVipProduct, ['route' => ['indexVipProducts.update', $indexVipProduct->id], 'method' => 'patch']) !!}

                        @include('index_vip_products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection