<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $indexVipProduct->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $indexVipProduct->product_id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $indexVipProduct->title !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $indexVipProduct->text !!}</p>
</div>

<!-- Who Edit Field -->
<div class="form-group">
    {!! Form::label('who_edit', 'Who Edit:') !!}
    <p>{!! $indexVipProduct->who_edit !!}</p>
</div>

<!-- Can Sign Up Field -->
<div class="form-group">
    {!! Form::label('can_sign_up', 'Can Sign Up:') !!}
    <p>{!! $indexVipProduct->can_sign_up !!}</p>
</div>

<!-- Start At Field -->
<div class="form-group">
    {!! Form::label('start_at', 'Start At:') !!}
    <p>{!! $indexVipProduct->start_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $indexVipProduct->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $indexVipProduct->updated_at !!}</p>
</div>

