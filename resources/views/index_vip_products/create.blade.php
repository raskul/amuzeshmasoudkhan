@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ساخت دوره حضوری ویژه
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'indexVipProducts.store']) !!}

                        @include('index_vip_products.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
