<!-- فیلد محصول را انتخاب کنید: -->
<div class="form-group col-sm-6 {{ $errors->has('product_id') ? ' has-error' : '' }}">
    {!! Form::label('product_id', 'محصول را انتخاب کنید:') !!}
    {!! Form::select('product_id', $products, (isset($products)?$products:null), ['class' => 'form-control select2']) !!}
    @if ($errors->has('product_id'))
        <span class="help-block"><strong>{{ $errors->first('product_id') }}</strong></span>
    @endif
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'عنوان:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'متن:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Who Edit Field -->
<div class="form-group col-sm-6" style="display: none">
    {!! Form::label('who_edit', 'Who Edit:') !!}
    {!! Form::text('who_edit', null, ['class' => 'form-control']) !!}
</div>

<!-- Can Sign Up Field -->
<div class="form-group col-sm-6">
    <label for="">ثبت نام آزاد است؟</label>
    <select name="can_sign_up" id="">
        <option value="1" @if(isset($indexVipProduct->can_sign_up)) @if($indexVipProduct->can_sign_up == 1) selected @endif @endif >بله</option>
        <option value="0" @if(isset($indexVipProduct->can_sign_up)) @if($indexVipProduct->can_sign_up == 0) selected @endif @endif >خیر</option>
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('indexVipProducts.index') !!}" class="btn btn-default">لغو</a>
</div>
