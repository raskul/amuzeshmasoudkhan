@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            فایل ضمیمه دوره آموزشی
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($productFile, ['route' => ['productFiles.update', $productFile->id], 'method' => 'patch']) !!}

                        @include('product_files.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection