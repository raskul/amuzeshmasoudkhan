<!-- فیلد محصول را انتخاب کنید: -->
<div class="form-group col-sm-6 {{ $errors->has('product_id') ? ' has-error' : '' }}">
    {!! Form::label('product_id', 'محصول را انتخاب کنید:') !!}
    {!! Form::select('product_id', $products, (isset($products)?$products:null), ['class' => 'form-control select2']) !!}
    @if ($errors->has('product_id'))
        <span class="help-block"><strong>{{ $errors->first('product_id') }}</strong></span>
    @endif
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'شناسه استاد:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'عنوان:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file', 'فایل:') !!}
    {!! Form::text('file', null, ['class' => 'form-control']) !!}
</div>

<!-- Size Field -->
<div class="form-group col-sm-6">
    {!! Form::label('size', 'سایز:') !!}
    {!! Form::text('size', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('ذخیره', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('productFiles.index') !!}" class="btn btn-default">لغو</a>
</div>
