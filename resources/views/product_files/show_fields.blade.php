<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'شناسه:') !!}
    <p>{!! $productFile->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', ' محصول:') !!}
    <p>{!! $productFile->product->title !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'ایمیل استاد:') !!}
    <p>{!! $productFile->user->email !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'عنوان:') !!}
    <p>{!! $productFile->title !!}</p>
</div>

<!-- File Field -->
<div class="form-group">
    {!! Form::label('file', 'فایل:') !!}
    <p>{!! $productFile->file !!}</p>
</div>

<!-- Size Field -->
<div class="form-group">
    {!! Form::label('size', 'سایز:') !!}
    <p>{!! $productFile->size !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $productFile->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $productFile->updated_at !!}</p>
</div>

