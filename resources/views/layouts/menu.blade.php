@can('is-employee')
<li class="{{ Request::is('/') ? 'active' : '' }}" >
    <a href="{!! route('index.index') !!}"><i class="fa fa-television"></i><span>وبسایت</span></a>
</li>

<li class="{{ Request::is('home*') ? 'active' : '' }}" >
    <a href="{!! route('home') !!}"><i class="fa fa-home"></i><span>خانه</span></a>
</li>

<li class="{{ Request::is('managerReport*') ? 'active' : '' }}" style="border-bottom: #3c3c3c solid 1px">
    <a href="{!! route('manager.report') !!}"><i class="fa fa-bar-chart"></i><span>آمار</span></a>
</li>

<li class="{{ Request::is('upload') ? 'active' : '' }}" style="border-bottom: #3c3c3c solid 1px">
    <a href="{!! route('upload.get') !!}"><i class="fa fa-cloud-upload"></i><span>آپلود</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user-o"></i><span>کاربران</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-bars"></i><span>دسته بندی ها</span></a>
</li>

<li class="{{ Request::is('adminFinancialWithdraw*') ? 'active' : '' }}">
    <a href="{!! route('adminFinancialWithdraw') !!}"><i class="fa fa-credit-card"></i><span>درخواست های برداشت</span></a>
</li>

<li class="{{ Request::is('teachingIndex*') ? 'active' : '' }}">
    <a href="{!! route('teachingIndex') !!}"><i class="fa fa-hand-o-up"></i><span>درخواست های تدریس</span></a>
</li>

<li class="{{ Request::is('profile/ticketIndex*') ? 'active' : '' }}">
    <a href="{!! route('ticketIndex') !!}"><i class="fa fa-comment"></i><span>تیکت ها</span></a>
</li>

<li class="treeview
{{ Request::is('products*') ? 'active' : '' }}
{{ Request::is('productTypes*') ? 'active' : '' }}
{{ Request::is('productPosts*') ? 'active' : '' }}
{{ Request::is('productFiles*') ? 'active' : '' }}
{{ Request::is('productNotes*') ? 'active' : '' }}
{{ Request::is('comments*') ? 'active' : '' }}
{{ Request::is('faqs*') ? 'active' : '' }}
        ">

    <a href="#" style="color: #21ff00" >
        <i class="fa fa-graduation-cap"></i><span>دوره های آموزشی</span>
        <i class="fa pull-left fa-angle-left"  ></i>
    </a>

    <ul class="treeview-menu">

        <li class="{{ Request::is('productTypes*') ? 'active' : '' }}">
            <a href="{!! route('productTypes.index') !!}"><i class="fa fa-graduation-cap"></i><span>نوع دوره ها</span></a>
        </li>

        <li class="{{ Request::is('products*') ? 'active' : '' }}">
            <a href="{!! route('products.index') !!}"><i
                        class="fa fa-graduation-cap"></i><span>دوره ها</span></a>
        </li>

        <li class="{{ Request::is('productPosts*') ? 'active' : '' }}">
            <a href="{!! route('productPosts.index') !!}"><i
                        class="fa fa-graduation-cap"></i><span>پست های دوره ها</span></a>
        </li>

        <li class="{{ Request::is('teacherUploadFileReport') ? 'active' : '' }}">
            <a href="{!! route('teacherUploadFileReport') !!}"><i class="fa fa-graduation-cap"></i><span>فایل های آپلودی پست ها</span></a>
        </li>

        <li class="{{ Request::is('productFiles*') ? 'active' : '' }}">
            <a href="{!! route('productFiles.index') !!}"><i class="fa fa-graduation-cap"></i><span>فایل های دوره ها</span></a>
        </li>

        <li class="{{ Request::is('productNotes*') ? 'active' : '' }}">
            <a href="{!! route('productNotes.index') !!}"><i class="fa fa-graduation-cap"></i><span>اعلامیه های دوره ها</span></a>
        </li>

        <li class="{{ Request::is('comments*') ? 'active' : '' }}">
            <a href="{!! route('comments.index') !!}"><i class="fa fa-comment"></i><span>دیدگاه های دوره ها</span></a>
        </li>

        <li class="{{ Request::is('faqs*') ? 'active' : '' }}">
            <a href="{!! route('faqs.index') !!}"><i class="fa fa-comments"></i><span>سوالات و جواب های دوره ها</span></a>
        </li>

    </ul>
</li>
{{--<li class="{{ Request::is('messages*') ? 'active' : '' }}">--}}
{{--<a href="{!! route('messages.index') !!}"><i class="fa fa-edit"></i><span>پیام ها</span></a>--}}
{{--</li>--}}


<li class="treeview
{{ Request::is('discountFor*') ? 'active' : '' }}
{{ Request::is('discounts*') ? 'active' : '' }}
{{ Request::is('discountCodeFor*') ? 'active' : '' }}
{{ Request::is('reportListOfAll*') ? 'active' : '' }}
{{ Request::is('eshantion*') ? 'active' : '' }}
        ">

    <a href="#" style="color: #ffda00">
        <i class="fa fa-shopping-basket"></i> <span>جشنواره ها و تخفیف ها</span></span>
        <i class="fa fa-angle-left pull-left"></i>
    </a>

    <ul class="treeview-menu">
        {{--<li class="treeview">--}}

        <li>
            <a style="color: #ff0000" href="{!! route('discounts.index') !!}"><i
                        class="fa fa-shopping-basket"></i><span>جشنواره های فروش</span></a>
        </li>

        <li class="{{ Request::is('discountCodeFor*') ? 'active' : '' }}">
            <a href="#" style="color: #ff0000">
                <span class="pull-right"><i class='fa fa-shopping-basket'></i> <span>کدهای تخفیف</span></span> <i
                        class="fa fa-angle-left pull-left"></i>
                <span class="clearfix"></span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::is('discountCodeForProducts*') ? 'active' : '' }}">
                    <a href="{{route('discountCodeForProducts')}}"><i
                                class="fa fa-edit"></i><span>کد تخفیف برای محصولات</span></a>
                </li>
                <li class="{{ Request::is('discountCodeForCategoryOfProducts*') ? 'active' : '' }}">
                    <a href="{{ route('discountCodeForCategoryOfProducts') }}"><i class="fa fa-edit"></i><span>کد تخفیف برای <br>یک دسته از محصولات</span></a>
                </li>
                <li class="{{ Request::is('discountCodeForUsers*') ? 'active' : '' }}">
                    <a href="{{route('discountCodeForUsers')}}"><i
                                class="fa fa-edit"></i><span>کد تخفیف برای یوزرها</span></a>
                </li>
            </ul>
        </li>


        <li class="{{ Request::is('discountFor*') ? 'active' : '' }}">
            <a href="#" style="color: #ff5733">
                <span class="pull-right"><i class='fa fa-shopping-basket'></i> <span>تخفیف ها</span></span> <i
                        class="fa fa-angle-left pull-left"></i>
                <span class="clearfix"></span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::is('discountForProducts') ? 'active' : '' }}">
                    <a href="{{ route('discountForProducts') }}"><i
                                class="fa fa-edit"></i><span>تخفیف برای محصولات</span></a>
                </li>
                <li class="{{ Request::is('discountForCategoryOfProducts') ? 'active' : '' }}">
                    <a href="{{route('discountForCategoryOfProducts')}}"><i
                                class="fa fa-edit"></i><span> تخفیف برای <br>یک دسته از محصولات</span></a>
                </li>
                <li class="{{ Request::is('discountForUsers') ? 'active' : '' }}">
                    <a href="{{route('discountForUsers')}}"><i class="fa fa-edit"></i><span>تخفیف برای<br> یک یا چند یوزر خاص</span></a>
                </li>
            </ul>
        </li>

        <li class="treeview-menu{{ Request::is('discountForPrice') ? 'active' : '' }}">
            <a style="color: #ff5733" href="{{ route('discountForPrice') }}"><i class="fa fa-shopping-basket"></i><span>تخفیف به ازای قیمت</span></a>
        </li>

        <li class="{{ Request::is('eshantion*') ? 'active' : '' }}">
            <a href="{!! route('eshantion') !!}"><i class="fa fa-gift"></i><span>اشانتیون ها</span></a>
        </li>

        <li class="{{ Request::is('reportListOfAll*') ? 'active' : '' }}">
            <a href="#" style="color: #ffdddf">
                <span class="pull-right"><i class='fa fa-list'></i> <span>لیست تخفیف ها</span></span> <i
                        class="fa fa-angle-left pull-left"></i>
                <span class="clearfix"></span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::is('reportListOfAllProductsHasDiscount') ? 'active' : '' }}">
                    <a href="{{route('reportListOfAllProductsHasDiscount')}}"><i class="fa fa-edit"></i><span>لیست تمام محصولات <br> دارای تخفیف</span></a>
                </li>
                <li class="{{ Request::is('reportListOfAllProductsHasDiscountCode') ? 'active' : '' }}">
                    <a href="{{route('reportListOfAllProductsHasDiscountCode')}}"><i class="fa fa-edit"></i><span>لیست تمام محصولات<br> دارای کد تخفیف </span></a>
                </li>
                <li class="{{ Request::is('reportListOfAllUsersHasDiscountCode') ? 'active' : '' }}">
                    <a href="{{route('reportListOfAllUsersHasDiscountCode')}}"><i class="fa fa-edit"></i><span>لیست تمام کاربرانی <br>که کد تخفیف را وارد کرده اند</span></a>
                </li>
                <li class="{{ Request::is('reportListOfAllUsersBuyWithDiscountCode') ? 'active' : '' }}">
                    <a href="{{route('reportListOfAllUsersBuyWithDiscountCode')}}"><i class="fa fa-edit"></i><span>لیست تمام کاربرانی <br>که با کد تخفیف خرید کرده اند</span></a>
                </li>
            </ul>
        </li>
    </ul>
</li>

<li class="treeview
{{ Request::is('indexNotifications*') ? 'active' : '' }}
{{ Request::is('indexVipProducts*') ? 'active' : '' }}
{{ Request::is('posts*') ? 'active' : '' }}
">
    <a href="">
        <span class="pull-right"><i class='fa fa-file'></i> <span>نوشته های وبسایت</span></span>
        <i class="fa fa-angle-left pull-left"></i>
        <span class="clearfix"></span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('indexNotifications*') ? 'active' : '' }}">
            <a href="{!! route('indexNotifications.index') !!}"><i class="fa fa-edit"></i><span>اطلاعیه های آموزشی <br> صفحه اصلی</span></a>
        </li>
        <li class="{{ Request::is('indexVipProducts*') ? 'active' : '' }}">
            <a href="{!! route('indexVipProducts.index') !!}"><i class="fa fa-edit"></i><span> دوره های حضوری ویژه<br> صفحه اصلی </span></a>
        </li>
        <li class="{{ Request::is('posts*') ? 'active' : '' }}">
            <a href="{!! route('posts.index') !!}"><i class="fa fa-edit"></i><span>برگه ها</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('factors*') ? 'active' : '' }}">
    <a href="{!! route('factors.index') !!}"><i class="fa fa-list-alt"></i><span>فاکتور ها</span></a>
</li>

<li class="{{ Request::is('mobiles*') ? 'active' : '' }}">
    <a href="{!! route('mobiles.index') !!}"><i class="fa fa-mobile-phone"></i><span>شماره موبایل ها</span></a>
</li>

<li class="">
    <a href="#"><i class="fa fa-edit"></i><span>حسابداری</span></a>
</li>

<li class="{{ Request::is('visitorlog*') ? 'active' : '' }}">
    <a href="{{route('visitorlog')}}"><i class="fa fa-low-vision"></i><span>لاگ بازدیدکنندگان</span></a>
</li>


<li class="{{ Request::is('settings*') ? 'active' : '' }}">
    <a href="{!! route('settings.index') !!}"><i class="fa fa-cogs"></i><span>تنظیمات</span></a>
</li>

@endcan

@cannot('is-admin')
    @cannot('is-employee')
{{--<li class="{{ Request::is('teachingRequests*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('teachingRequests.index') !!}"><i class="fa fa-edit"></i><span>Teaching Requests</span></a>--}}
{{--</li>--}}

{{--<li class="{{ Request::is('teachingRequestComments*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('teachingRequestComments.index') !!}"><i class="fa fa-edit"></i><span>Teaching Request Comments</span></a>--}}
{{--</li>--}}

{{--<li class="{{ Request::is('userDocuments*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('userDocuments.index') !!}"><i class="fa fa-edit"></i><span>User Documents</span></a>--}}
{{--</li>--}}

{{--<li class="{{ Request::is('ticketComments*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('ticketComments.index') !!}"><i class="fa fa-edit"></i><span>Ticket Comments</span></a>--}}
{{--</li>--}}


{{--<li class="{{ Request::is('tags*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('tags.index') !!}"><i class="fa fa-edit"></i><span>برچسب ها</span></a>--}}
{{--</li>--}}

    @endcannot
@endcannot