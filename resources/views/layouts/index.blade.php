<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-45593720-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-45593720-3');
    </script>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>آموزش کلاس a</title>
    <link rel="stylesheet" href="{{ asset('assets/fonts/fonts.css') }}">
    <link rel="stylesheet" href="{{asset('assets/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/css/fontawesome-all.min.css')}}">
    <script type="text/javascript" src="{{asset('assets/js/jquery.js')}}"></script>

    @yield('header')

    @stack('header')

    <style>
        .alert-danger {
            background-color: #ff6150;
            width: 1200px;
            margin: auto;
            color: white;
            margin-top: 10px;
            padding-top: 10px;
            padding-right: 10px;
            height: 40px;
        }
        .alert-success {
            background-color: #6ABC6E;
            width: 1200px;
            margin: auto;
            color: white;
            margin-top: 10px;
            padding-top: 10px;
            padding-right: 10px;
            height: 40px;
        }
    </style>

</head>

<body >
<div id="app">
@yield('content')
</div>

<script type="text/javascript" src="{{asset('assets/js/jquery.royal-timer.js')}}"></script>
<script src="{{ asset('bower_components/vue/dist/vue.js') }}"></script>
@yield('scripts')
@stack('scripts')
</body>
</html>