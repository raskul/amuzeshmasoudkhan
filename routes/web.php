<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'visitorlog'], function () {

    Route::get('/', 'IndexController@index')->name('index.index');

    Route::get('category/{category?}', 'IndexController@category')->name('category');
    Route::post('category/' . 1 . '/sort_keyboard', 'AdminController@sortCategory');
    Route::match(['DELETE', 'PUT', 'GET', 'POST'], 'eshantion/{id?}', 'AdminController@eshantion')->name('eshantion');
    Route::get('visitorlog', 'AdminController@visitorlog')->name('visitorlog');

    //Route::get('profile/show/', 'IndexController@profile')->name('user.profile');//unused

    //Route::post('/profile/payFactor', 'IndexController@payFactor')->name('pay.factor');//unused

    //Route::get('/pmBox', 'IndexController@pmBox')->name('pmBox.get');//unused

    Route::post('/discountPost', 'IndexController@discountPost')->name('discount.post');

    Route::any('discountFestival', 'DiscountForAllController@discountFestival')->name('discountFestival');

    Route::any('discountForProducts', 'DiscountForAllController@discountForProducts')->name('discountForProducts');
    Route::any('discountForCategoryOfProducts', 'DiscountForAllController@discountForCategoryOfProducts')->name('discountForCategoryOfProducts');
    Route::any('discountForUsers', 'DiscountForAllController@discountForUsers')->name('discountForUsers');

    Route::any('discountCodeForProducts', 'DiscountForAllController@discountCodeForProducts')->name('discountCodeForProducts');
    Route::any('discountCodeForCategoryOfProducts', 'DiscountForAllController@discountCodeForCategoryOfProducts')->name('discountCodeForCategoryOfProducts');
    Route::any('discountCodeForUsers', 'DiscountForAllController@discountCodeForUsers')->name('discountCodeForUsers');

    Route::any('discountForPrice', 'DiscountForAllController@discountForPrice')->name('discountForPrice');

    //Route::get('/userFactors', 'ProfileController@userFactors')->name('userFactors');//unused
    //Route::get('/profile', 'ProfileController@profileGet')->name('profile.get');//unused
    Route::get('/profile/edit', 'ProfileController@profileEdit')->name('profile.edit');
    Route::post('/profile/post', 'ProfileController@profilePost')->name('profile.post');
    Route::post('profile/financialIncreaseBalance', 'ProfileController@increaseBalancePost')->name('increase.balance.post');
    Route::get('profile/financialIncreaseBalance', 'ProfileController@increaseBalanceGet')->name('increase.balance.get');
    Route::get('profile/financialDepartment', 'ProfileController@financialDepartment')->name('financialDepartment');
    Route::match(['GET', 'POST'], 'profile/financialWithdraw', 'ProfileController@financialWithdraw')->name('financialWithdraw');

    Route::match(['GET', 'POST'], 'profile/ticketCreate', 'ProfileController@ticketCreate')->name('ticketCreate');
    Route::match(['GET', 'POST'], 'profile/ticketComments/{id}', 'ProfileController@ticketComments')->name('ticketComments');
    Route::get('profile/ticketIndex', 'ProfileController@ticketIndex')->name('ticketIndex');//unused
    Route::get('profile/userCourses', 'ProfileController@userCourses')->name('userCourses');
    Route::get('profile/courseStudent', 'ProfileController@courseStudent')->name('courseStudent');

    Route::get('profile/teachingIndex', 'ProfileController@teachingIndex')->name('teachingIndex');
    Route::match(['GET', 'POST'], 'profile/teachingRequestCreate', 'ProfileController@teachingRequestCreate')->name('teachingRequestCreate');
    Route::match(['GET', 'POST'], 'profile/teachingRequestComments/{id}', 'ProfileController@teachingRequestComments')->name('teachingRequestComments');
    Route::get('profile/teacherEditProducts/{product}', 'ProductController@teacherEditProducts')->name('teacherEditProducts');
    Route::post('profile/teacherUpdateProducts/{product}', 'ProductController@teacherUpdateProducts')->name('teacherUpdateProducts');

    Route::get('post/{post}', 'IndexController@showAPost')->name('show.post');
    //Route::get('productPost/{productPost}', 'IndexController@showProductPost')->name('show.product.post');//unused

    Route::match(['GET', 'POST'], 'adminFinancialWithdraw/{balanceHistory?}', 'AdminController@adminFinancialWithdraw')->name('adminFinancialWithdraw');
    Route::get('upload', 'UploadController@uploadGet')->name('upload.get');
    Route::post('upload', 'UploadController@uploadPost')->name('upload.post');

    Route::post('addToBasket/{id}', 'IndexController@addToBasket')->name('add.to.basket');
    Route::post('removeFromBasket/{id}', 'IndexController@removeFromBasket')->name('remove.from.basket');
    Route::put('signUpNoPrice/{id}', 'IndexController@signUpNoPrice')->name('signUpNoPrice');

    Route::get('checkOut', 'IndexController@checkOut')->name('check.out');

    Route::get('shoppingCart', 'IndexController@shoppingCart')->name('shopping.cart');

    Route::get('Reg', 'IndexController@Reg')->name('reg');
    Route::post('userRegister', 'UserController@userRegister')->name('user.register')->middleware('throttle:5,1');
    Route::post('fastRegister', 'Auth\RegisterController@fastRegister')->name('fastRegister')->middleware('throttle:5,1');

    Route::post('commentStoreSec', 'CommentController@storesec')->name('comment.store.sec');
    Route::get('commentEdit/{comment}', 'CommentController@commentEdit')->name('commentEdit');
    Route::match(['GET', 'POST'], 'commentAnswer/{comment}', 'CommentController@commentAnswer')->name('commentAnswer');

    Route::post('faq', 'FaqController@storesec')->name('faq.store.sec');
    Route::get('faqEdit/{faq}', 'FaqController@faqEdit')->name('faqEdit');
    Route::match(['GET', 'POST'], 'faqAnswer/{faq}', 'FaqController@faqAnswer')->name('faqAnswer');

    Route::get('reportListOfAllProductsHasDiscount', 'DiscountForAllController@reportListOfAllProductsHasDiscount')->name('reportListOfAllProductsHasDiscount');
    Route::get('reportListOfAllProductsHasDiscountCode', 'DiscountForAllController@reportListOfAllProductsHasDiscountCode')->name('reportListOfAllProductsHasDiscountCode');
    Route::get('reportListOfAllUsersHasDiscountCode', 'DiscountForAllController@reportListOfAllUsersHasDiscountCode')->name('reportListOfAllUsersHasDiscountCode');
    Route::get('reportListOfAllUsersBuyWithDiscountCode', 'DiscountForAllController@reportListOfAllUsersBuyWithDiscountCode')->name('reportListOfAllUsersBuyWithDiscountCode');
    Route::get('reportListOfAllProductsHasDiscount', 'DiscountForAllController@reportListOfAllProductsHasDiscount')->name('reportListOfAllProductsHasDiscount');

    Route::get('managerReport', 'AdminController@managerReport')->name('manager.report');

    Route::get('teacherUploadFileReport', 'AdminController@teacherUploadFileReport')->name('teacherUploadFileReport');

    Route::match(['GET', 'POST', 'DELETE'], 'profile/teacherProductPost/{product}/{id?}', 'ProfileController@createProductPostByTeacher')->name('createProductPostByTeacher');
    Route::match(['GET', 'POST'], 'profile/teacherProductFiles/{product}/{id?}', 'ProfileController@ProductFilesByTeacher')->name('ProductFilesByTeacher');
    Route::match(['GET', 'POST'], 'profile/teacherProductNotes/{product}/{id?}', 'ProfileController@ProductNotesByTeacher')->name('ProductNotesByTeacher');

    Route::post('productRating', 'IndexController@productRating')->name('productRating');

    Route::post('giveMobileNumber', 'IndexController@giveMobileNumber')->name('giveMobileNumber');

    Route::patch('editUserAddress', 'IndexController@editUserAddress')->name('editUserAddress');

    Route::get('post/{post}', 'IndexController@indexPost')->name('indexPost');

    //Gate:
    Route::get('functionGate/{factor}', 'GateController@functionGate')->name('functionGate');
    Route::get('callbackFunctionGate', 'GateController@callbackFunctionGate')->name('callbackFunctionGate');

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('products', 'ProductController');
    Route::resource('users', 'UserController');
    Route::resource('factors', 'FactorController');
    Route::resource('categories', 'CategoryController');
    Route::resource('tags', 'TagController');
    Route::resource('comments', 'CommentController');
    Route::resource('posts', 'PostController');
    Route::resource('productPosts', 'ProductPostController');
    Route::resource('tickets', 'TicketController');
    Route::resource('messages', 'MessageController');
    Route::resource('discounts', 'DiscountController');
    Route::resource('settings', 'SettingController');
    Route::resource('teachingRequests', 'TeachingRequestController');
    Route::resource('teachingRequestComments', 'TeachingRequestCommentController');
    Route::resource('userDocuments', 'UserDocumentController');
    Route::resource('ticketComments', 'TicketCommentController');
    Route::resource('productFiles', 'ProductFileController');
    Route::resource('productNotes', 'ProductNoteController');
    Route::resource('productTypes', 'ProductTypeController');
    Route::resource('faqs', 'FaqController');
    Route::resource('indexNotifications', 'IndexNotificationController');
    Route::resource('indexVipProducts', 'IndexVipProductController');
    Route::resource('mobiles', 'MobileController');


    Route::group(['middleware' => 'elf'], function () {
        Route::get('elf/tinymce4', function () {
            $dir = '/packages/barryvdh/elfinder';
            $locale = 'fa';

            return view('vendor.elfinder.tinymce4', compact('dir', 'locale'));
        })->name('elfinder.tinymce4');
    });

    Route::get('downloadTeachingRequestFiles/{file}', function ($file) {
        $file = str_replace(':', '/', $file);

        return Storage::download('/public/teachingRequest/' . $file);
    })->name('downloadTeachingRequestFiles');

    Route::get('downloadTicketFiles/{file}', function ($file) {
        $file = str_replace(':', '/', $file);

        return Storage::download('/public/tickets/' . $file);
    })->name('downloadTicketFiles');


    Route::get('download/{file}', function ($file) {
        $file = str_replace(':', '/', $file);

        return Storage::download('/public/productFiles/' . $file);
    })->name('downloadFiles');

    ////last address
    Route::get('{product}/{id?}', 'IndexController@showProduct')->name('show.product.page');

});